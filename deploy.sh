echo user name:
read -s user
echo "*******"
echo ""

echo password:
read -s pass

echo "**********"
echo ""

ftp -i -n technostalgic.tech <<EOF
user $user $pass

cd ./public_html/games/kinesists
put index.html

mkdir dist
cd dist

put ./dist/bundle.js

mkdir assets
cd assets

binary

mput ./dist/assets/*

mkdir graphics
cd graphics

mput ./dist/assets/graphics/*

bye
EOF

echo "connection closed"
read -s -n 1 -p "Press any key to exit . . ."