# Kinesis TS

This is a small game I'm working on to help me teach myself developing in a Typescript / Pixi.js / Webpack environment.

<hr>

## Building:

Requirements:

* [git](https://git-scm.com)
* [node.js](https://nodejs.org/en/)
* [pnpm](https://pnpm.io/)
* simple local server software (I use [Live Server VSCode extension](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer))
	
### 1. Clone the repo

From git, run  
`git clone https://gitlab.com/Technostalgic/kinesists`  
to clone the git repository to your machine

### 2. Install project dependencies

Open a terminal in the directory where you cloned the repository, and run  
`pnpm i`  
this automatically checks the pnpm-lock.yaml file for project dependencies and installs them locally in the project directory

### 3. Build/Bundle with webpack

(NOTE: this step will *not* work without node.js installed on your machine)

**-- With VSCode --**

I created an automated build and run with debugging task for VSCode users. Just launch VSCode in the project directory and go to "Run and Debug" (or press F5) and select `Build and Run(debug)`. This will automatically perform all the tasks defined below, and also launch an instance of google chrome to show the result. However, the chrome webpage will not load unless you have already started a server in the project directory. See step #4 for information on how to do that with Live Server VSCode extension.

**-- Without VSCode --**

When pnpm installed the project dependencies, it should have created a `node_modules` directory in your project folder. This holds all the project dependencies including webpack. To build with webpack you can run the command from the project directory  
on linux or bash: `./node_modules/.bin/webpack`  
or on windows with powershell: `./node_modules/.bin/webpack.CMD`

### 4. Run the game

Now to run the game you'll need to start a server in your project directory and navigate to it in your browser. With Live Server VSCode extension, you just open the project directory in VSCode and click on "Go Live" on the bottom right of the VSCode status bar.

--
<hr>
## Output
The build step compiles all source typescript files into one bundle.js file, everything in `./src/` gets compiled by ts-loader and bundled by webpack into `./dist/bundle.js`.  
In order for the game to work as a standalone, you need to have a skeleton html file that references the two scripts `./dist/pixi.min.js` and `./dist/bundle.js`, preferably in the `<head>` tag. Due to the way that pixi.js handles importing assets, it is unfortunately impossible for the html file to be run locally without a server, unless a specialized browser is used.