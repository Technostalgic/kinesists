/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ 8091:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Actor": () => (/* binding */ Actor)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _this = undefined;

var Actor = /** @class */ (function (_super) {
    __extends(Actor, _super);
    function Actor() {
        var _this = _super.call(this) || this;
        // ---------------------------------------------------------------------------------------------
        _this._stats = null;
        _this._graphics = null;
        _this._allegiance = null;
        _this._controller = new _internal__WEBPACK_IMPORTED_MODULE_0__.IdleController();
        return _this;
    }
    Object.defineProperty(Actor.prototype, "stats", {
        get: function () { return this._stats; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Actor.prototype, "graphics", {
        get: function () { return this._graphics; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Actor.prototype, "allegiance", {
        get: function () { return this._allegiance; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Actor.prototype, "team", {
        get: function () { return this._allegiance.team; },
        set: function (val) { this._allegiance.team = val; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Actor.prototype, "_maxHealth", {
        get: function () { return this._stats.health.maxValue; },
        set: function (val) { this._stats.health.baseValue = val; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    Actor.CreateDefault = function () {
        return null;
    };
    Actor.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(this, function (x) { return [
            x._graphics,
            x._stats,
            x._allegiance,
            x._controller
        ]; });
    };
    // ---------------------------------------------------------------------------------------------
    Actor.prototype.Update = function (deltaTime) {
        if (deltaTime > 0) {
            this._controller.Update(deltaTime, this);
        }
        this.PostControllerUpdate(deltaTime);
        _super.prototype.Update.call(this, deltaTime);
    };
    /**
     * meant to be overridden, called immediately after _controller.Update and before
     * (Actor)super.Update
     * @param deltaTime
     */
    Actor.prototype.PostControllerUpdate = function (deltaTime) { };
    // ---------------------------------------------------------------------------------------------
    Actor.prototype.Hit = function (damage, impulse, hitPoint, damager) {
        if (damager === void 0) { damager = null; }
        this.stats.health.value -= damage;
        this.ApplyImpulse(impulse);
        // kill check
        if (this.stats.health.value <= 0)
            this.Kill(damager);
    };
    // ---------------------------------------------------------------------------------------------
    Actor.prototype.Initialize = function () {
        _super.prototype.Initialize.call(this);
        if (this.collider == null)
            this.CreateCollider();
        this.collider.onCollision.AddListener(this.OnCollision.bind(this));
    };
    Actor.GetEditorProperties = function () {
        var r = _super.GetEditorProperties.call(this);
        r.push(_internal__WEBPACK_IMPORTED_MODULE_0__.EditorProperty.CreateNumberProp("_maxHealth", "Max Health"), _internal__WEBPACK_IMPORTED_MODULE_0__.EditorProperty.CreateEntityProp("_deleteThis", "randomEntity"));
        return r;
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    Actor.prototype.destroy = function () {
        this.collider.destroy();
        _super.prototype.destroy.call(this);
    };
    /**
     * kills the enemy
     * @param killer the object that killed the enemy
     */
    Actor.prototype.Kill = function (killer) {
        if (killer === void 0) { killer = null; }
        console.log(this.name + " killed by " + (killer === null || killer === void 0 ? void 0 : killer.name));
        this.RemoveFromScene();
    };
    var _a;
    _a = Actor;
    Actor.Types = [];
    (function () {
        _a.SetSerializableFields();
    })();
    return Actor;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.PhysicsEntity));



/***/ }),

/***/ 1470:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Enemy": () => (/* binding */ Enemy),
/* harmony export */   "TestDummy": () => (/* binding */ TestDummy)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var Enemy = /** @class */ (function (_super) {
    __extends(Enemy, _super);
    function Enemy() {
        var _this = _super.call(this) || this;
        _this._allegiance = _internal__WEBPACK_IMPORTED_MODULE_0__.Allegiance.EnemyDefault();
        return _this;
    }
    // ---------------------------------------------------------------------------------------------
    /** called when a collison occurs with this object */
    Enemy.prototype.OnCollision = function (collision) {
        var other = _internal__WEBPACK_IMPORTED_MODULE_0__.Collision.OtherCollider(collision, this.collider);
        // terrain collision
        if (_internal__WEBPACK_IMPORTED_MODULE_0__.ISolid.ImplementedValue(other)) {
            this.ResolveSolidCollision(collision);
            return;
        }
    };
    return Enemy;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.Actor));

var TestDummy = /** @class */ (function (_super) {
    __extends(TestDummy, _super);
    function TestDummy() {
        var _this = _super.call(this) || this;
        _this._name = "Test Dummy";
        _this.mass = 10;
        _this._stats = new _internal__WEBPACK_IMPORTED_MODULE_0__.Stats();
        _this._stats.health.baseValue = 1000;
        return _this;
    }
    // ---------------------------------------------------------------------------------------------
    TestDummy.prototype.CreateCollider = function () {
        this._collider = _internal__WEBPACK_IMPORTED_MODULE_0__.CircleCollider.Create(10);
        this._collider.CreateWireframe(0xFF0000, 1);
        this._collider.layer = _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.actors;
        this.addChild(this._collider);
    };
    TestDummy.prototype.GetActionType = function (action) {
        return _internal__WEBPACK_IMPORTED_MODULE_0__.IControllable.ActionMode.None;
    };
    TestDummy.prototype.DoAction = function (action, value) { };
    TestDummy.prototype.CanDoAction = function (action) { return false; };
    return TestDummy;
}(Enemy));



/***/ }),

/***/ 3783:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EnemyBehaviors": () => (/* binding */ EnemyBehaviors)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

var EnemyBehaviors;
(function (EnemyBehaviors) {
    var _this = this;
    var _a, _b, _c, _d;
    var Idle = /** @class */ (function (_super) {
        __extends(Idle, _super);
        function Idle() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Idle.prototype.Update = function (controller, deltaTime, actor) { };
        return Idle;
    }(_internal__WEBPACK_IMPORTED_MODULE_0__.AiBehavior));
    _a = Idle;
    (function () {
        _a.SetSerializableFields();
    })();
    EnemyBehaviors.Idle = Idle;
    var Wander = /** @class */ (function (_super) {
        __extends(Wander, _super);
        function Wander() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._lastMoveDir = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create();
            _this._moveDir = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create();
            _this._moveTime = 0;
            return _this;
        }
        Wander.prototype.Update = function (controller, deltaTime, actor) {
            // choose new direction if necessary
            if (this._moveTime <= 0)
                this.ChooseRandomDirection();
            else
                this._moveTime -= deltaTime;
            // apply movement
            actor.DoAction(_internal__WEBPACK_IMPORTED_MODULE_0__.IControllable.Action.XMove, this._moveDir.x);
            actor.DoAction(_internal__WEBPACK_IMPORTED_MODULE_0__.IControllable.Action.YMove, this._moveDir.y);
        };
        Wander.prototype.ChooseRandomDirection = function () {
            // create list of move directions that are not equal to the current move direction or last
            // move direction
            var moveDirs = [
                _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(1, 0),
                _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(-1, 0),
                _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(0, 1),
                _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(0, -1)
            ];
            for (var i = moveDirs.length - 1; i >= 0; i--) {
                if (_internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Equal(moveDirs[i], this._moveDir) || _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Equal(moveDirs[i], this._lastMoveDir))
                    moveDirs.splice(i, 1);
            }
            // choose random move direction
            this._lastMoveDir = this._moveDir;
            this._moveDir = moveDirs[Math.floor(Math.random() * moveDirs.length)];
            this._moveTime = 1.5;
        };
        return Wander;
    }(_internal__WEBPACK_IMPORTED_MODULE_0__.AiBehavior));
    _b = Wander;
    (function () {
        _b.SetSerializableFields();
    })();
    EnemyBehaviors.Wander = Wander;
    var ChaseTarget = /** @class */ (function (_super) {
        __extends(ChaseTarget, _super);
        function ChaseTarget() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._moveDir = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create();
            _this._moveTime = 0;
            return _this;
        }
        ChaseTarget.prototype.Update = function (controller, deltaTime, actor) {
            // do nothing if no target
            if (controller.target == null)
                return;
            // move toward target
            if (this._moveTime <= 0)
                this.SetMoveDir(controller, actor);
            else
                this._moveTime -= deltaTime;
            // apply movement
            actor.DoAction(_internal__WEBPACK_IMPORTED_MODULE_0__.IControllable.Action.XMove, this._moveDir.x);
            actor.DoAction(_internal__WEBPACK_IMPORTED_MODULE_0__.IControllable.Action.YMove, this._moveDir.y);
        };
        ChaseTarget.prototype.SetMoveDir = function (controller, actor) {
            // move toward target
            this._moveDir = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Normalize(_internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(controller.target.globalPosition, actor.globalPosition));
            this._moveTime = 0.5;
        };
        return ChaseTarget;
    }(_internal__WEBPACK_IMPORTED_MODULE_0__.AiBehavior));
    _c = ChaseTarget;
    (function () {
        _c.SetSerializableFields();
    })();
    EnemyBehaviors.ChaseTarget = ChaseTarget;
    var ShootTarget = /** @class */ (function (_super) {
        __extends(ShootTarget, _super);
        function ShootTarget() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        ShootTarget.prototype.Update = function (controller, deltaTime, actor) {
            if (controller.target == null)
                return;
            // stop moving
            actor.DoAction(_internal__WEBPACK_IMPORTED_MODULE_0__.IControllable.Action.XMove, 0);
            actor.DoAction(_internal__WEBPACK_IMPORTED_MODULE_0__.IControllable.Action.YMove, 0);
            // rotate toward target
            var targetRot = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Direction(_internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(controller.target.globalPosition, actor.globalPosition));
            actor.DoAction(_internal__WEBPACK_IMPORTED_MODULE_0__.IControllable.Action.AimRotate, targetRot);
            // shoot target
            this.Attack(controller, actor);
        };
        ShootTarget.prototype.Attack = function (controller, actor) {
            var target = controller.target;
            if (target == null)
                return;
            var aimThresh = 0.5;
            var posDif = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(target.globalPosition, actor.globalPosition);
            var aimDif = _internal__WEBPACK_IMPORTED_MODULE_0__.Maths.SignedAngleDif(_internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Direction(posDif), actor.rotation);
            if (Math.abs(aimDif) <= aimThresh) {
                actor.DoAction(_internal__WEBPACK_IMPORTED_MODULE_0__.IControllable.Action.AttackPrimary, 1);
            }
        };
        return ShootTarget;
    }(_internal__WEBPACK_IMPORTED_MODULE_0__.AiBehavior));
    _d = ShootTarget;
    (function () {
        _d.SetSerializableFields();
    })();
    EnemyBehaviors.ShootTarget = ShootTarget;
})(EnemyBehaviors || (EnemyBehaviors = {}));


/***/ }),

/***/ 3144:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Gunner": () => (/* binding */ Gunner)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a, _b;
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var Gunner = /** @class */ (function (_super) {
    __extends(Gunner, _super);
    function Gunner() {
        var _this = _super.call(this) || this;
        _this._spriteSheet = new _internal__WEBPACK_IMPORTED_MODULE_1__.SpriteSheet("./dist/assets/graphics/Gunner.png");
        _this._sprite = null;
        _this._movement = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        _this._aimDirection = _this.rotation;
        _this._weapon = new GunnerWeapon();
        _this._controller = new _internal__WEBPACK_IMPORTED_MODULE_1__.GunnerAi();
        _this.CreateGraphics();
        _this._name = "Gunner";
        _this.mass = 55;
        _this._stats = new _internal__WEBPACK_IMPORTED_MODULE_1__.Stats();
        _this._stats.health.baseValue = 25;
        _this._stats.speed.baseValue = 75;
        _this.addChild(_this._weapon);
        return _this;
    }
    // ---------------------------------------------------------------------------------------------
    Gunner.CreateDefault = function () {
        return new Gunner();
    };
    Gunner.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.MakeSerializable(this, function (x) { return []; });
    };
    // ---------------------------------------------------------------------------------------------
    Gunner.prototype.CreateGraphics = function () {
        this._graphics = new _internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity();
        this._sprite = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Sprite(null);
        this._sprite.anchor.set(0.5, 0.5);
        this._graphics.addChild(this._sprite);
        this.addChild(this._graphics);
    };
    /** @inheritdoc */
    Gunner.prototype.CreateCollider = function () {
        this._collider = _internal__WEBPACK_IMPORTED_MODULE_1__.CircleCollider.Create(12);
        // this._collider.CreateWireframe(0xFF0000, 1);
        this._collider.layer = _internal__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.actors;
        this.addChild(this._collider);
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    Gunner.prototype.Update = function (delta) {
        _super.prototype.Update.call(this, delta);
        this.HandleMovement(delta);
        this.HandleAiming(delta);
        this._controller.Update(delta, this);
        this._weapon.Update(delta);
        this.HandleAnimation();
    };
    // ---------------------------------------------------------------------------------------------
    Gunner.prototype.HandleAnimation = function () {
        if (!this._spriteSheet.isLoaded) {
            return;
        }
        else if (this._spriteSheet.sprites.length <= 1) {
            this._spriteSheet.GenerateSprites(2, 1, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0.5));
            if (this._sprite.texture == null) {
                this._sprite.texture = this._spriteSheet.sprites[0];
            }
        }
        var combatMode = this._controller.combatMode;
        if (combatMode == _internal__WEBPACK_IMPORTED_MODULE_1__.CombatMode.engaged || combatMode == _internal__WEBPACK_IMPORTED_MODULE_1__.CombatMode.suspicious) {
            this._sprite.texture = this._spriteSheet.sprites[1];
        }
        else if (combatMode == _internal__WEBPACK_IMPORTED_MODULE_1__.CombatMode.passive) {
            this._sprite.texture = this._spriteSheet.sprites[0];
        }
    };
    Gunner.prototype.Hit = function (damage, impulse, hitPoint, damager) {
        var maxRotImpulse = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Magnitude(impulse) * 0.5;
        var hitNorm = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Normalize(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Subtract(this.collider.globalPosition, hitPoint));
        hitNorm = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Rotate(hitNorm, Math.PI * -0.5);
        var dot = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Dot(hitNorm, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Normalize(impulse));
        var rotImp = dot * maxRotImpulse;
        var directFactor = (1 - Math.abs(dot));
        _super.prototype.Hit.call(this, damage * directFactor, impulse, hitPoint, damager);
        this.ApplyTorqueImpulse(rotImp);
    };
    // ---------------------------------------------------------------------------------------------
    Gunner.prototype.GetActionType = function (action) {
        var act = _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.Action;
        switch (action) {
            case act.XMove: return _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.Analogue;
            case act.YMove: return _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.Analogue;
            case act.AimRotate: return _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.Analogue;
            case act.AttackPrimary: return _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.Digital;
        }
        return _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.None;
    };
    Gunner.prototype.CanDoAction = function (action) {
        var _c;
        var act = _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.Action;
        switch (action) {
            case act.AttackPrimary: return (_c = this._weapon) === null || _c === void 0 ? void 0 : _c.readyPrimary;
        }
        return this.GetActionType(action) != _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.None;
    };
    Gunner.prototype.DoAction = function (action, value) {
        var act = _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.Action;
        switch (action) {
            case act.XMove:
                this._movement.x = value;
                break;
            case act.YMove:
                this._movement.y = value;
                break;
            case act.AimRotate:
                this._aimDirection = value;
                break;
            case act.AimRotate:
                this._aimDirection = value;
                break;
            case act.AttackPrimary:
                this._weapon.Use(this, this.rotation);
                break;
        }
    };
    Gunner.prototype.HandleMovement = function (deltaTime, movementVect) {
        if (movementVect === void 0) { movementVect = this._movement; }
        var speed = this.stats.speed.value;
        // apply movement
        this.AccelerateToVelocity(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(movementVect, speed), deltaTime * speed * 2);
    };
    Gunner.prototype.HandleAiming = function (deltaTime, aimDir) {
        if (aimDir === void 0) { aimDir = this._aimDirection; }
        // dampen rotational velocity
        this._rotationalVelocity *= Math.pow(0.9, deltaTime * 60);
        var rotSpeed = 4;
        // enforce rotation speed limit
        var rotDif = _internal__WEBPACK_IMPORTED_MODULE_1__.Maths.SignedAngleDif(aimDir, this.rotation);
        if (Math.abs(rotDif) > rotSpeed * deltaTime) {
            rotDif = Math.sign(rotDif) * rotSpeed * deltaTime;
        }
        this.rotation += rotDif;
    };
    return Gunner;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.Enemy));

_a = Gunner;
(function () {
    _a.SetSerializableFields();
})();
/** weapon used by gunner enemy */
var GunnerWeapon = /** @class */ (function (_super) {
    __extends(GunnerWeapon, _super);
    function GunnerWeapon() {
        var _this = _super.call(this) || this;
        _this._barrelPosition = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(10, 0);
        _this._fireWait = 0;
        _this._fireDelay = 0.45;
        _this.spread = 0.15;
        _this.clipSize = 3;
        _this.reloadTime = 2.5;
        _this._payload = null;
        _this._roundsInClip = _this.clipSize;
        _this._payload = new _internal__WEBPACK_IMPORTED_MODULE_1__.Projectile();
        _this._payload.damage = 5;
        _this._payload.size = 2;
        _this._payload.mass = 0.55;
        return _this;
    }
    Object.defineProperty(GunnerWeapon.prototype, "readyPrimary", {
        get: function () { return this._fireWait <= 0; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    GunnerWeapon.CreateDefault = function () {
        return new this();
    };
    GunnerWeapon.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.MakeSerializable(this, function (x) { return []; });
    };
    // ---------------------------------------------------------------------------------------------
    GunnerWeapon.prototype.Update = function (deltaTime) {
        _super.prototype.Update.call(this, deltaTime);
        // deduct the fire timer
        if (this._fireWait > 0)
            this._fireWait -= deltaTime;
        else if (this._fireDelay < 0)
            this._fireWait = 0;
    };
    GunnerWeapon.prototype.Use = function (user, aim) {
        // fire if able
        if (this._fireWait <= 0) {
            this.Fire(user, aim);
        }
    };
    GunnerWeapon.prototype.Fire = function (user, aim) {
        this._roundsInClip--;
        this._fireWait = this._fireDelay;
        if (this._roundsInClip <= 0) {
            this._fireWait = this.reloadTime;
            this._roundsInClip = this.clipSize;
        }
        var tpos = this.barrelPosition;
        var dir = aim + (Math.random() - 0.5) * this.spread;
        var proj = this._payload.FastClone();
        proj.FireFrom(user, tpos, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.FromDirection(dir, 500));
        this.parentScene.addChild(proj);
    };
    return GunnerWeapon;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.Weapon));
_b = GunnerWeapon;
(function () {
    _b.SetSerializableFields();
})();


/***/ }),

/***/ 5860:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GunnerAi": () => (/* binding */ GunnerAi)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

var GunnerAi = /** @class */ (function (_super) {
    __extends(GunnerAi, _super);
    function GunnerAi() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._combatMode = _internal__WEBPACK_IMPORTED_MODULE_0__.CombatMode.none;
        _this._moveTime = 0;
        _this._calmBehavior = new _internal__WEBPACK_IMPORTED_MODULE_0__.EnemyBehaviors.Wander();
        _this._aggroBehavior = new _internal__WEBPACK_IMPORTED_MODULE_0__.EnemyBehaviors.ShootTarget();
        return _this;
    }
    Object.defineProperty(GunnerAi.prototype, "combatMode", {
        get: function () { return this._combatMode; },
        enumerable: false,
        configurable: true
    });
    /// --------------------------------------------------------------------------------------------
    GunnerAi.CreateDefault = function () { return new this(); };
    // ---------------------------------------------------------------------------------------------
    GunnerAi.prototype.Update = function (deltaTime, actor) {
        this._moveTime -= deltaTime;
        // if the move time has expired, search for target
        if (this._moveTime <= 0) {
            this.FindTarget(actor);
            // shoot target if found
            if (this.target != null) {
                this._currentBehavior = this._aggroBehavior;
                this._combatMode = _internal__WEBPACK_IMPORTED_MODULE_0__.CombatMode.engaged;
            }
            else {
                this._currentBehavior = this._calmBehavior;
                this._combatMode = _internal__WEBPACK_IMPORTED_MODULE_0__.CombatMode.passive;
            }
        }
        _super.prototype.Update.call(this, deltaTime, actor);
    };
    GunnerAi.prototype.FindTarget = function (actor) {
        // create a collider search query a specific radius around the actor
        var searchCollider = _internal__WEBPACK_IMPORTED_MODULE_0__.CircleCollider.Create(500);
        searchCollider.globalPosition = actor.globalPosition;
        // find all potential target colliders
        var partitions = actor.parentScene.colliders;
        var targets = partitions.FindOverlapCollidersOnLayers(searchCollider, _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.actors);
        // iterate through each potential target
        var targetFound = false;
        for (var i = targets.length - 1; i >= 0; i--) {
            var target = targets[i].parentEntity;
            if (!_internal__WEBPACK_IMPORTED_MODULE_0__.ITarget.ImplementedIn(target))
                continue;
            // chech if actor is aggressive against potential target
            var aggression = actor.allegiance.AggresionToward(target);
            if (aggression == _internal__WEBPACK_IMPORTED_MODULE_0__.Aggression.hostile) {
                // see if target is in line of sight
                var ray = _internal__WEBPACK_IMPORTED_MODULE_0__.Ray.FromPoints(actor.globalPosition, target.globalPosition);
                var cols = partitions.RaycastAgainstLayers(ray, _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain);
                if (cols.length <= 0) {
                    // if it's in line of sight, set the target field
                    this._target = target;
                    targetFound = true;
                    break;
                }
            }
        }
        // remove target if it can no longer be seen
        if (!targetFound) {
            this._target = null;
        }
        // dispose of the collider used for search query
        searchCollider.destroy();
    };
    return GunnerAi;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.AiController));

_a = GunnerAi;
(function () {
    _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(_a, function (x) { return [
        x._combatMode,
        x._moveTime,
        x._calmBehavior,
        x._aggroBehavior
    ]; });
})();


/***/ }),

/***/ 4517:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Seeker": () => (/* binding */ Seeker)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var Seeker = /** @class */ (function (_super) {
    __extends(Seeker, _super);
    function Seeker() {
        var _this = _super.call(this) || this;
        // ---------------------------------------------------------------------------------------------
        _this._spriteSheet = new _internal__WEBPACK_IMPORTED_MODULE_1__.SpriteSheet("./dist/assets/graphics/Seeker.png");
        _this._sprite = null;
        _this._attackRecovery = 0;
        _this._movement = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        _this._controller = new _internal__WEBPACK_IMPORTED_MODULE_1__.SeekerAi();
        _this.CreateGraphics();
        _this._name = "Seeker";
        _this.mass = 55;
        _this._stats = new _internal__WEBPACK_IMPORTED_MODULE_1__.Stats();
        _this._stats.health.baseValue = 15;
        _this._stats.speed.baseValue = 75;
        return _this;
    }
    // ---------------------------------------------------------------------------------------------
    Seeker.CreateDefault = function () {
        return new Seeker();
    };
    Seeker.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.MakeSerializable(this, function (x) { return []; });
    };
    // ---------------------------------------------------------------------------------------------
    Seeker.prototype.CreateGraphics = function () {
        var sprite = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Sprite(null);
        this._graphics = new _internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity();
        this._sprite = sprite;
        this._graphics.addChild(sprite);
        this.addChild(this._graphics);
    };
    Seeker.prototype.CreateCollider = function () {
        // this._collider = AABBCollider.Create(Vect.Create(10));
        this._collider = _internal__WEBPACK_IMPORTED_MODULE_1__.CircleCollider.Create(11);
        //this._collider.CreateWireframe(0xFF0000, 1);
        this._collider.layer = _internal__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.actors;
        this.addChild(this._collider);
    };
    Seeker.prototype.OnCollision = function (col) {
        _super.prototype.OnCollision.call(this, col);
        var other = _internal__WEBPACK_IMPORTED_MODULE_1__.Collision.OtherCollider(col, this._collider);
        if (other.layer != _internal__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.terrain) {
            if (this._attackRecovery <= 0) {
                if (_internal__WEBPACK_IMPORTED_MODULE_1__.ITarget.ImplementedIn(other.parentEntity)) {
                    var aggro = this.allegiance.AggresionToward(other.parentEntity);
                    if (aggro == _internal__WEBPACK_IMPORTED_MODULE_1__.Aggression.hostile) {
                        this.Attack(other.parentEntity);
                    }
                }
            }
        }
    };
    // ---------------------------------------------------------------------------------------------
    Seeker.prototype.Attack = function (target) {
        var impulseMag = 7500;
        var impulse = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Normalize(this.velocity);
        impulse.x *= impulseMag;
        impulse.y *= impulseMag;
        var attackPoint = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MidPoint(this.globalPosition, target.globalPosition);
        target.Hit(1, impulse, attackPoint, this);
        impulse.x *= -1;
        impulse.y *= -1;
        this.ApplyImpulse(impulse);
        this._attackRecovery = 0.25;
    };
    /** @inheritdoc */
    Seeker.prototype.Kill = function (killer) {
        _super.prototype.Kill.call(this, killer);
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    Seeker.prototype.Update = function (delta) {
        _super.prototype.Update.call(this, delta);
        this.HandleMovement(delta);
        this._controller.Update(delta, this);
        this.HandleAnimation();
        this._attackRecovery -= delta;
    };
    // ---------------------------------------------------------------------------------------------
    Seeker.prototype.HandleAnimation = function () {
        if (!this._spriteSheet.isLoaded) {
            return;
        }
        else {
            if (this._spriteSheet.sprites.length <= 1)
                this._spriteSheet.GenerateSprites(2, 1, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0.5));
        }
        // calculate difference between the current rotation and the direction of the target
        var target = this._controller.target;
        var targetDir = 0;
        if (target != null)
            targetDir = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Direction(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Subtract(target.globalPosition, this.globalPosition));
        else
            targetDir = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Direction(this.velocity);
        var dirDif = _internal__WEBPACK_IMPORTED_MODULE_1__.Maths.SignedAngleDif(targetDir, this._graphics.rotation);
        this._graphics.rotation += dirDif;
        this._graphics.rotation = _internal__WEBPACK_IMPORTED_MODULE_1__.Maths.Mod(this._graphics.rotation, Math.PI * 2);
        if (this._graphics.rotation > Math.PI)
            this._graphics.rotation -= Math.PI * 2;
        // flip it upright
        if (Math.abs(this._graphics.rotation) > Math.PI / 2) {
            this._graphics.scale.y = Math.abs(this._graphics.scale.y) * -1;
        }
        else {
            this._graphics.scale.y = Math.abs(this._graphics.scale.y);
        }
        switch (this._controller.combatMode) {
            case _internal__WEBPACK_IMPORTED_MODULE_1__.CombatMode.engaged:
                this._sprite.texture = this._spriteSheet.sprites[1];
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_1__.CombatMode.passive:
                this._sprite.texture = this._spriteSheet.sprites[0];
                break;
        }
        var anchor = this._sprite.texture.defaultAnchor;
        this._sprite.anchor.set(anchor.x, anchor.y);
        if (this._attackRecovery > 0.1) {
            this._sprite.texture = this._spriteSheet.sprites[0];
        }
    };
    // ---------------------------------------------------------------------------------------------
    Seeker.prototype.GetActionType = function (action) {
        var act = _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.Action;
        switch (action) {
            case act.XMove: return _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.Analogue;
            case act.YMove: return _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.Analogue;
        }
        return _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.None;
    };
    Seeker.prototype.CanDoAction = function (action) {
        return this.GetActionType(action) != _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.None;
    };
    Seeker.prototype.DoAction = function (action, value) {
        var act = _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.Action;
        switch (action) {
            case act.XMove:
                this._movement.x = value;
                break;
            case act.YMove:
                this._movement.y = value;
                break;
        }
    };
    Seeker.prototype.HandleMovement = function (deltaTime, movementVect) {
        if (movementVect === void 0) { movementVect = this._movement; }
        var speed = this._stats.speed.value;
        var combatMode = this._controller.combatMode;
        if (combatMode == _internal__WEBPACK_IMPORTED_MODULE_1__.CombatMode.engaged)
            speed = 150;
        // apply movement
        this.AccelerateToVelocity(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(movementVect, speed), deltaTime * speed * 2);
    };
    return Seeker;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.Enemy));

_a = Seeker;
(function () {
    _a.SetSerializableFields();
})();


/***/ }),

/***/ 1128:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SeekerAi": () => (/* binding */ SeekerAi)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

var SeekerAi = /** @class */ (function (_super) {
    __extends(SeekerAi, _super);
    function SeekerAi() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._combatMode = _internal__WEBPACK_IMPORTED_MODULE_0__.CombatMode.none;
        _this._moveDir = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create();
        _this._behaviorTime = 0;
        _this._behaviorCalm = new _internal__WEBPACK_IMPORTED_MODULE_0__.EnemyBehaviors.Wander();
        _this._behaviorAggro = new _internal__WEBPACK_IMPORTED_MODULE_0__.EnemyBehaviors.ChaseTarget();
        return _this;
    }
    Object.defineProperty(SeekerAi.prototype, "combatMode", {
        get: function () { return this._combatMode; },
        enumerable: false,
        configurable: true
    });
    /// --------------------------------------------------------------------------------------------
    SeekerAi.CreateDefault = function () { return new this(); };
    // ---------------------------------------------------------------------------------------------
    SeekerAi.prototype.Update = function (deltaTime, actor) {
        // search for target on timer expire
        if (this._behaviorTime <= 0) {
            this.FindTarget(actor);
            // if target found, chase them otherwise wander aimlessly
            if (this.target != null) {
                this._currentBehavior = this._behaviorAggro;
                this._combatMode = _internal__WEBPACK_IMPORTED_MODULE_0__.CombatMode.engaged;
            }
            else {
                this._currentBehavior = this._behaviorCalm;
                this._combatMode = _internal__WEBPACK_IMPORTED_MODULE_0__.CombatMode.passive;
            }
        }
        // otherwise deduct the behavior time
        else
            this._behaviorTime -= deltaTime;
        // do current behavior
        _super.prototype.Update.call(this, deltaTime, actor);
    };
    SeekerAi.prototype.FindTarget = function (actor) {
        // create a collider search query a specific radius around the actor
        var searchCollider = _internal__WEBPACK_IMPORTED_MODULE_0__.CircleCollider.Create(500);
        searchCollider.globalPosition = actor.globalPosition;
        // find all potential target colliders
        var partitions = actor.parentScene.colliders;
        var targets = partitions.FindOverlapCollidersOnLayers(searchCollider, _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.actors);
        // iterate through each potential target
        var targetFound = false;
        for (var i = targets.length - 1; i >= 0; i--) {
            var target = targets[i].parentEntity;
            if (!_internal__WEBPACK_IMPORTED_MODULE_0__.ITarget.ImplementedIn(target))
                continue;
            // chech if actor is aggressive against potential target
            var aggression = actor.allegiance.AggresionToward(target);
            if (aggression == _internal__WEBPACK_IMPORTED_MODULE_0__.Aggression.hostile) {
                // see if target is in line of sight
                var ray = _internal__WEBPACK_IMPORTED_MODULE_0__.Ray.FromPoints(actor.globalPosition, target.globalPosition);
                var cols = partitions.RaycastAgainstLayers(ray, _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain);
                if (cols.length <= 0) {
                    // if it's in line of sight, set the target field
                    this._target = target;
                    targetFound = true;
                    break;
                }
            }
        }
        // remove target if it can no longer be seen
        if (!targetFound) {
            this._target = null;
        }
        // dispose of the collider used for search query
        searchCollider.destroy();
    };
    return SeekerAi;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.AiController));

_a = SeekerAi;
(function () {
    _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(_a, function (x) { return [
        x._combatMode,
        x._moveDir,
        x._behaviorTime,
        x._behaviorCalm,
        x._behaviorAggro
    ]; });
})();


/***/ }),

/***/ 9331:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Player": () => (/* binding */ Player)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


/**
 * the character entity which the player has control over
 * */
var Player = /** @class */ (function (_super) {
    __extends(Player, _super);
    function Player() {
        var _this = _super.call(this) || this;
        /// --------------------------------------------------------------------------------------------
        /// private field declarations
        _this._controller = new _internal__WEBPACK_IMPORTED_MODULE_1__.PlayerController();
        _this._upDirection = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.up;
        _this._isFlipped = false;
        _this._onGround = false;
        _this._movement = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        _this._aimDirection = 0;
        _this._aimMagnitude = 0;
        _this._aimVect = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        _this._aimVector = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        _this._weaponPosition = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(5, -1.5);
        _this._isStrafing = false;
        _this._aimReticle = null;
        _this._spriteAnim = 2;
        _this._animFrame = 1;
        _this._animWait = 0;
        _this._animDelay = 0.05;
        _this._name = "Player";
        _this.mass = 65;
        _this._stats = new _internal__WEBPACK_IMPORTED_MODULE_1__.Stats();
        _this._stats.health.baseValue = 100;
        _this._stats.speed.baseValue = 30;
        _this._allegiance = _internal__WEBPACK_IMPORTED_MODULE_1__.Allegiance.PlayerDefault();
        return _this;
    }
    Object.defineProperty(Player.prototype, "maxSpeed", {
        /// --------------------------------------------------------------------------------------------
        /// public field and property accessors
        get: function () { return this.stats.speed.value * 7; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "jumpPower", {
        get: function () { return this.stats.speed.value * 7; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "groundAcceleration", {
        get: function () { return this._stats.speed.value * 40; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "airAcceleration", {
        get: function () { return this._stats.speed.value * 10; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "colliderAABB", {
        get: function () { return this._collider; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "graphic", {
        get: function () { return this._graphic; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "upDirection", {
        /** the relative upward direction of the player, aka the direction that their head is pointing */
        get: function () {
            return this._upDirection;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "forwardDirection", {
        /** the relative forward direction of the player, aka the direction they are facing */
        get: function () {
            if (this._isFlipped)
                return _internal__WEBPACK_IMPORTED_MODULE_1__.Side.RotateCCW(this._upDirection);
            else
                return _internal__WEBPACK_IMPORTED_MODULE_1__.Side.RotateCW(this._upDirection);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "isHorizontal", {
        /** whether or not the player is standing on a horizontal surface, facing either left or right */
        get: function () {
            return this._upDirection == _internal__WEBPACK_IMPORTED_MODULE_1__.Side.up || this._upDirection == _internal__WEBPACK_IMPORTED_MODULE_1__.Side.down;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "weapon", {
        /** the currently equipped active item of the player */
        get: function () { return this._weapon; },
        set: function (wep) {
            // destroy previous weapon and remove from child array
            if (this._weapon != null) {
                this._weapon.RemoveFromScene();
            }
            // this._entities.push(wep);
            this._orientationContainer.addChild(wep);
            this._weapon = wep;
        },
        enumerable: false,
        configurable: true
    });
    ;
    Object.defineProperty(Player.prototype, "aimVector", {
        /** the direction vector that the player is aiming (not necessarily normalized) */
        get: function () { return this._aimVector; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "isFlipped", {
        /** whether or not the player's sprite is flipped along the y-axis */
        get: function () { return this._isFlipped; },
        set: function (flipped) {
            this._isFlipped = flipped;
            this._orientationContainer.scale.x =
                Math.abs(this._orientationContainer.scale.x) * (flipped ? -1 : 1);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Player.prototype, "onGround", {
        /** whether or not the player is on a surface that can be walked on */
        get: function () { return this._onGround; },
        enumerable: false,
        configurable: true
    });
    /// --------------------------------------------------------------------------------------------
    Player.CreateDefault = function () {
        return new Player();
    };
    Player.SetSerializableFields = function () {
        // TODO
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.MakeSerializable(this, function (x) { return [
            x._upDirection,
            x._isFlipped,
            x._onGround,
            x._size,
            x._weapon,
            x._weaponPosition,
            x._orientationContainer
        ]; });
    };
    // ---------------------------------------------------------------------------------------------
    /// Initilialization
    Player.prototype.Initialize = function () {
        _super.prototype.Initialize.call(this);
        if (this._orientationContainer == null) {
            this._orientationContainer = new _internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity();
            this._orientationContainer.name = "Orient Container";
            this.addChild(this._orientationContainer);
        }
        this.CreateGraphic();
        if (this.weapon == null)
            this.weapon = new _internal__WEBPACK_IMPORTED_MODULE_1__.BeamLaser();
        else if (this.weapon.parentEntity != this._orientationContainer) {
            this._orientationContainer.addChild(this.weapon);
        }
    };
    Player.prototype.CreateGraphic = function () {
        var size = { x: this._size.x, y: this._size.y };
        this._graphic = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Graphics();
        this._graphic.beginFill(0xC0C0C0, 1);
        this._graphic.drawRoundedRect(size.x / -2, size.y / -2, size.x, size.y, 2);
        this._graphic.endFill();
        this._orientationContainer.addChild(this._graphic);
        this._graphic.visible = false;
        var sheet = new _internal__WEBPACK_IMPORTED_MODULE_1__.SpriteSheet("./dist/assets/graphics/Player.png");
        sheet.GenerateSprites(5, 5, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0.5));
        this._spriteSheet = sheet;
        this._sprite = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Sprite(null);
        this._sprite.anchor.set(0.5, 0.5);
        this._sprite.pivot.y = 1;
        this._orientationContainer.addChild(this._sprite);
        this._aimReticle = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Graphics();
        this._aimReticle.beginFill(0x00FF00, 1);
        this._aimReticle.drawRect(-0.5, -0.5, 1, 1);
        this._aimReticle.endFill();
        this.addChild(this._aimReticle);
    };
    Player.prototype.CreateCollider = function () {
        this._size = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(8, 14);
        var col = new _internal__WEBPACK_IMPORTED_MODULE_1__.AABBCollider();
        col.layer = _internal__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.actors;
        col.halfExtents.x = this._size.x * 0.5;
        col.halfExtents.y = this._size.y * 0.5;
        this._collider = col;
        this.addChild(this._collider);
    };
    /// --------------------------------------------------------------------------------------------
    /// Main logic step overrides
    /** @inheritdoc */
    Player.prototype.Update = function (deltaTime) {
        this.HandleGroundAndWallChecking();
        //this.HandleInput(deltaTime);
        _super.prototype.Update.call(this, deltaTime);
        this.HandleMovementPhysics(deltaTime);
        this.HandleAiming(this._aimDirection, this._aimMagnitude);
        this.HandleCamera(deltaTime);
    };
    Player.prototype.PostControllerUpdate = function (deltaTime) {
        this.HandleAnimation(deltaTime);
    };
    /// --------------------------------------------------------------------------------------------
    /// Internal world interaction callbacks
    Player.prototype.OnCollision = function (collision) {
        var otherCol = _internal__WEBPACK_IMPORTED_MODULE_1__.Collision.OtherCollider(collision, this.collider);
        if (this.IsValidGround(otherCol)) {
            if (!this.onGround) {
                this.FindGround(collision);
            }
            this.ResolveSolidCollision(collision);
        }
    };
    Player.prototype.FindGround = function (collision) {
        var norm = collision.directionNormal;
        if (this._collider == collision.colliderB) {
            norm = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Invert(norm);
        }
        var nextUpDir = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.FromDirection(norm);
        // if the player is rotating 180 degrees, flip to ensure forward stays the same
        if (_internal__WEBPACK_IMPORTED_MODULE_1__.Side.Opposite(nextUpDir) == this._upDirection)
            this.isFlipped = !this.isFlipped;
        this.SetUpDirection(nextUpDir);
        _internal__WEBPACK_IMPORTED_MODULE_1__.Collision.Recalculate(collision);
        this._onGround = true;
    };
    Player.prototype.DoWallClimb = function (collision) {
        // get the collisions normal, and reverse it if the collision is from the perspective of
        // the other collider
        var norm = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Clone(collision.directionNormal);
        if (collision.colliderA != this.collider) {
            norm.x *= -1;
            norm.y *= -1;
        }
        // ensure that the wall climb is not happening on the ground the player is standing on
        var colSide = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.FromDirection(norm);
        if (_internal__WEBPACK_IMPORTED_MODULE_1__.Side.IsHorizontal(colSide) == _internal__WEBPACK_IMPORTED_MODULE_1__.Side.IsHorizontal(this.upDirection))
            return;
        // only allow wall climb if player is moving in the opposite-ish direction as the 
        // collision normal
        // let movDot = Vect.Dot(this._movement, norm);
        // if(movDot >= 0)
        // 	return;
        // maintain speed if the player is moving in the proper diagonal velocity-normal direction
        // let velDot = Vect.Dot(Vect.Normalize(this._movement), Vect.Normalize(this._velocity));
        // if(velDot < 1){
        // 	let proj = Vect.Project(this._movement, Vect.Rotate(norm, Math.PI * 0.5));
        // 	let dir = Vect.Normalize(proj);
        // 	let vel = Vect.MultiplyScalar(dir, Vect.Magnitude(this._velocity));
        // 	this.velocity = vel;
        // }
        var side = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.FromDirection(norm);
        this.SetUpDirection(side);
        _internal__WEBPACK_IMPORTED_MODULE_1__.Collision.Recalculate(collision);
        this.FindGround(collision);
    };
    Player.prototype.SetUpDirection = function (dir) {
        this._upDirection = dir;
        var drawAng = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.GetAngle(_internal__WEBPACK_IMPORTED_MODULE_1__.Side.RotateCW(this._upDirection));
        this._orientationContainer.rotation = drawAng;
        this.UpdateCollider();
    };
    /// --------------------------------------------------------------------------------------------
    /// private state handling
    Player.prototype.HandleGroundAndWallChecking = function () {
        // shift the collider in the relative player down direction by 1 pixel
        var colShift = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.GetVector(this._upDirection);
        colShift.x *= -1;
        colShift.y *= -1;
        this.collider.position.x += colShift.x;
        this.collider.position.y += colShift.y;
        this._onGround = false;
        // iterate through each collider in scene
        var cols = this._parentScene.colliders.FindOverlapColliders(this.collider);
        for (var i = cols.length - 1; i >= 0; i--) {
            // don't collide with self
            if (cols[i] == this._collider)
                continue;
            // don't check invalid grounds
            if (!this.IsValidGround(cols[i]))
                continue;
            // there is a collision with the shifted collider, so we are on the ground
            this._onGround = true;
            break;
        }
        // undo the collider shift
        this.collider.position.x -= colShift.x;
        this.collider.position.y -= colShift.y;
        // handle wall checking while in air
        if (!this._onGround) {
            var wallCol = this.DoWallCheck();
            if (wallCol != null)
                this.DoWallClimb(wallCol);
        }
    };
    Player.prototype.DoWallCheck = function () {
        var tCol = null;
        // shift the collider in the relative player forward direction by 1 pixel
        var colShift = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.GetVector(this.forwardDirection);
        this.collider.position.x += colShift.x;
        this.collider.position.y += colShift.y;
        // iterate through each collider overlapping the shifted collider
        var cols = this._parentScene.colliders.FindOverlapColliders(this.collider);
        for (var i = cols.length - 1; i >= 0; i--) {
            // don't collide with self
            if (cols[i] == this._collider)
                continue;
            // there is a collision with the shifted collider, so we are against a wall
            tCol = cols[i];
            break;
        }
        // undo the collider shift
        this.collider.position.x -= colShift.x;
        this.collider.position.y -= colShift.y;
        // shift the collider in the other direction
        colShift.x *= -1;
        colShift.y *= -1;
        this.collider.position.x += colShift.x;
        this.collider.position.y += colShift.y;
        // iterate through each collider overlapping the shifted collider
        cols = this._parentScene.colliders.FindOverlapColliders(this.collider);
        for (var i = cols.length - 1; i >= 0; i--) {
            // don't collide with self
            if (cols[i] == this._collider)
                continue;
            // there is a collision with the shifted collider, so we are against a wall
            tCol = cols[i];
            break;
        }
        // undo the collider shift
        this.collider.position.x -= colShift.x;
        this.collider.position.y -= colShift.y;
        // return the wall collision or null if there was none
        if (tCol == null)
            return null;
        // if the collider was part of an object that cannot be walked on, return nothing
        if (!this.IsValidGround(tCol))
            return null;
        var col = this.collider.GetCollision(tCol);
        return col;
    };
    Player.prototype.IsValidGround = function (collider) {
        if (!_internal__WEBPACK_IMPORTED_MODULE_1__.ISolid.ImplementedValue(collider))
            return false;
        if (_internal__WEBPACK_IMPORTED_MODULE_1__.ISolid instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.Actor)
            return false;
        return true;
    };
    Player.prototype.HandleAnimation = function (deltaTime) {
        // calculate animation speed from velocity
        var velMag = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Magnitude(this._velocity);
        var animSpeed = velMag / this.maxSpeed;
        // animate the running animation cycle
        this._animWait -= deltaTime * animSpeed;
        if (this._animWait <= 0) {
            this._animFrame++;
            if (this._animFrame > 3) {
                this._animFrame = 1;
            }
            this._animWait = this._animDelay + this._animWait % this._animDelay;
        }
        // if standing on ground, use 'standing' animation frame
        var animFrame = this._animFrame;
        if (this._onGround) {
            if (velMag < this.maxSpeed * 0.15)
                animFrame = 0;
        }
        // if in air, use 'jumping' frame
        else {
            animFrame = 4;
        }
        // apply the sprite frame
        if (!this._spriteSheet.isLoaded)
            return;
        var frame = animFrame + this._spriteAnim * 5;
        this._sprite.texture = this._spriteSheet.sprites[frame];
    };
    Player.prototype.HandleCamera = function (deltaTime) {
        if (!(this.parentScene instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.GameScene))
            return;
        var cam = this.parentScene.camera;
        if (cam == null)
            return;
        cam.CenterAt(this.globalPosition);
    };
    /** updates the player's collider to have the proper size and orientation */
    Player.prototype.UpdateCollider = function () {
        var col = this.colliderAABB;
        var hExts = {
            x: this._size.x * 0.5,
            y: this._size.y * 0.5
        };
        if (this.isHorizontal) {
            col.halfExtents.x = hExts.x;
            col.halfExtents.y = hExts.y;
        }
        else {
            col.halfExtents.x = hExts.y;
            col.halfExtents.y = hExts.x;
        }
    };
    Player.prototype.HandleAimingAnimation = function (aim) {
        if (aim === void 0) { aim = this._aimVector; }
        // if the weapon is not ready we want the player sprite to flip based on their velocity
        if (!this.weapon.readyPrimary)
            aim = this._velocity;
        switch (this._upDirection) {
            case _internal__WEBPACK_IMPORTED_MODULE_1__.Side.up:
                if (aim.x < 0)
                    this.isFlipped = true;
                else if (aim.x > 0)
                    this.isFlipped = false;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_1__.Side.down:
                if (aim.x < 0)
                    this.isFlipped = false;
                else if (aim.x > 0)
                    this.isFlipped = true;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_1__.Side.left:
                if (aim.y < 0)
                    this.isFlipped = false;
                else if (aim.y > 0)
                    this.isFlipped = true;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_1__.Side.right:
                if (aim.y < 0)
                    this.isFlipped = true;
                else if (aim.y > 0)
                    this.isFlipped = false;
                break;
        }
        // if weapon is not ready we don't need to do the aim animation
        if (!this.weapon.readyPrimary) {
            this._spriteAnim = 2;
            return;
        }
        // if no aim, set to default aim animation
        if (aim.x == 0 && aim.y == 0) {
            this._spriteAnim = 2;
            return;
        }
        var orientVect = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Direction(_internal__WEBPACK_IMPORTED_MODULE_1__.Side.GetVector(_internal__WEBPACK_IMPORTED_MODULE_1__.Side.RotateCW(this._upDirection)));
        var orientAim = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Direction(aim);
        var dif = _internal__WEBPACK_IMPORTED_MODULE_1__.Maths.SignedAngleDif(orientVect, orientAim);
        var aimOff = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.FromDirection(dif, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Magnitude(aim));
        if (this.isFlipped)
            aimOff.x *= -1;
        var po4 = Math.PI * 0.25;
        var offDir = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Direction(aimOff);
        offDir /= po4;
        offDir = Math.round(offDir);
        // if(offDir <= -2 && this._onGround)
        // 	offDir = -1;
        this._spriteAnim = 2 - offDir;
    };
    Player.prototype.HandleWeaponOrientation = function () {
        var weppos = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Clone(this._weaponPosition);
        switch (this._spriteAnim) {
            case 0:
                weppos.x -= 2.5;
                weppos.y -= 4.5;
                break;
            case 1:
                weppos.x -= 1;
                weppos.y -= 3.5;
                break;
            case 2:
                break;
            case 3:
                weppos.x -= 1;
                weppos.y += 2;
                break;
            case 4:
                weppos.x -= 4.5;
                weppos.y += 2.5;
                break;
        }
        this._weapon.position.set(weppos.x, weppos.y);
        var trot = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Direction(this._aimVector);
        var brot = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.GetAngle(this.forwardDirection);
        if (this._isFlipped) {
            trot *= -1;
            if (!this.isHorizontal)
                trot += Math.PI;
        }
        this._weapon.rotation = _internal__WEBPACK_IMPORTED_MODULE_1__.Maths.SignedAngleDif(trot, brot);
    };
    /// --------------------------------------------------------------------------------------------
    /// input controller
    /** @inheritdoc */
    Player.prototype.GetActionType = function (action) {
        var act = _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.Action;
        switch (action) {
            case act.AimRotate:
            case act.AimMagnitude:
            case act.XMove:
            case act.YMove:
                return _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.Analogue;
            case act.Jump:
            case act.InteractSend:
            case act.UseItemPrimary:
            case act.UseItemAlternate:
            case act.UseItemTertiary:
                return _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.Digital;
        }
        return _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.None;
    };
    /** @inheritdoc */
    Player.prototype.CanDoAction = function (action) {
        var _b, _c;
        var act = _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.Action;
        switch (action) {
            case act.AimRotate: return true;
            case act.AimMagnitude: return true;
            case act.XMove: return true;
            case act.YMove: return true;
            case act.Jump: return this.onGround;
            case act.UseItemPrimary: return this.weapon != null;
            case act.UseItemAlternate: return (_b = this.weapon) === null || _b === void 0 ? void 0 : _b.readyAlternate;
            case act.UseItemTertiary: return (_c = this.weapon) === null || _c === void 0 ? void 0 : _c.readyTertiary;
        }
        return this.GetActionType(action) !== _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.ActionMode.None;
    };
    /** @inheritdoc */
    Player.prototype.DoAction = function (action, value) {
        if (!this.CanDoAction(action))
            return;
        var act = _internal__WEBPACK_IMPORTED_MODULE_1__.IControllable.Action;
        switch (action) {
            case act.XMove:
                this._movement.x = value;
                break;
            case act.YMove:
                this._movement.y = value;
                break;
            case act.AimRotate:
                this._aimDirection = value;
                break;
            case act.AimMagnitude:
                this._aimMagnitude = value;
                break;
            case act.Jump:
                this.Jump();
                break;
            case act.UseItemPrimary:
                this.UseWeapon();
                break;
        }
    };
    /**
     * forces the player to aim in the specified direction
     * @param inputVector the direction the player should aim
     * @returns
     */
    Player.prototype.HandleAiming = function (rotation, magnitude) {
        if (magnitude === void 0) { magnitude = 1; }
        this._aimVector = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.FromDirection(rotation, magnitude);
        // if(inputVector.x == 0 && inputVector.y == 0){
        // 	this._aimVector = Side.GetVector(this.forwardDirection);
        // 	this.HandleAimingAnimation(this._aimVector);
        // 	this.HandleWeaponOrientation();
        // 	return;
        // }
        var downSide = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.Opposite(this._upDirection);
        var downVec = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.GetVector(downSide);
        var downDir = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Direction(downVec);
        var inputDir = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Direction(this._aimVector);
        var dirDif = _internal__WEBPACK_IMPORTED_MODULE_1__.Maths.SignedAngleDif(downDir, inputDir);
        var po4 = Math.PI * 0.25;
        var dirDifSeg = Math.round(dirDif / po4);
        var r = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Clone(this._aimVector);
        // if(dirDifSeg == 0 && this.onGround){
        // 	if(this._isFlipped)
        // 		r = Vect.Rotate(r, po4);
        // 	else
        // 		r = Vect.Rotate(r, -po4);
        // }
        this._aimVector = r;
        this.HandleAimingAnimation(this._aimVector);
        this.HandleWeaponOrientation();
        if (this.weapon.readyPrimary) {
            this._aimReticle.visible = true;
            _internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity.SetGlobalPosition(this._aimReticle, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Add(this._weapon.barrelPosition, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(this._aimVector, 10)));
        }
        else
            this._aimReticle.visible = false;
    };
    // god mode
    // public Hit(damage: number, impulse: IVect, hitPoint: IVect, damager?: SceneEntity): void { }
    /**
     * make the player move with the specified paramaters
     * @param deltaTime how long the player movement should be simulated
     * @param movement the direction of movement
     */
    Player.prototype.HandleMovementPhysics = function (deltaTime, movement) {
        if (movement === void 0) { movement = this._movement; }
        var mov = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Clone(movement);
        var acc = this.airAcceleration;
        var dvel = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Normalize(mov);
        // touching ground
        if (this._onGround) {
            acc = this.groundAcceleration;
            // only allow movement along ground plane
            if (this.isHorizontal)
                dvel.y = 0;
            else
                dvel.x = 0;
        }
        // in mid-air
        else {
            // accelerate slower in air than on ground
            acc *= 0.65;
            // if facing left/right
            if (_internal__WEBPACK_IMPORTED_MODULE_1__.Side.IsHorizontal(this.forwardDirection)) {
                dvel.y *= 0.275;
            }
            // facing up/down
            else {
                dvel.x *= 0.275;
            }
        }
        // apply acceleration over delta time
        dvel.x *= deltaTime * acc;
        dvel.y *= deltaTime * acc;
        // calculate target velocity
        var tVel = {
            x: this._velocity.x + dvel.x,
            y: this._velocity.y + dvel.y
        };
        // enforce max speed horizontally
        if (tVel.x > this.maxSpeed || tVel.x < -this.maxSpeed) {
            tVel.x = this.maxSpeed * Math.sign(tVel.x);
        }
        // enforce max speed vertically
        if (tVel.y > this.maxSpeed || tVel.y < -this.maxSpeed) {
            tVel.y = this.maxSpeed * Math.sign(tVel.y);
        }
        // enforce friction while on ground
        if (this._onGround) {
            // apply friction
            var dirDot = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Dot(mov, this._velocity);
            var velMag = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Magnitude(this._velocity);
            if (velMag > 0 && dirDot <= 0) {
                var fricMag = 600 * deltaTime;
                if (fricMag < velMag) {
                    var fric = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Normalize(this._velocity), -fricMag);
                    tVel.x += fric.x;
                    tVel.y += fric.y;
                }
                else {
                    tVel.x = 0;
                    tVel.y = 0;
                }
            }
        }
        // apply the movement
        this._velocity.x = tVel.x;
        this._velocity.y = tVel.y;
        // cache the movement from this handler call
        this._movement.x = movement.x;
        this._movement.y = movement.y;
    };
    /** make the player jump in their relative upward direction */
    Player.prototype.Jump = function () {
        var jumpVel = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.GetVector(this._upDirection);
        jumpVel = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(jumpVel, this.jumpPower);
        this._velocity.x += jumpVel.x;
        this._velocity.y += jumpVel.y;
        this._onGround = false;
    };
    /** make the player use the currently equiped weapon */
    Player.prototype.UseWeapon = function () {
        if (this._weapon == null)
            return;
        this.weapon.Use(this, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Direction(this.aimVector));
    };
    return Player;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.Actor));

_a = Player;
(function () {
    _a.SetSerializableFields();
})();


/***/ }),

/***/ 3358:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PlayerController": () => (/* binding */ PlayerController)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;

var PlayerController = /** @class */ (function (_super) {
    __extends(PlayerController, _super);
    function PlayerController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._input_aimX = _internal__WEBPACK_IMPORTED_MODULE_0__.InputControl.CreateAnalogue(_internal__WEBPACK_IMPORTED_MODULE_0__.InputType.mouseAnalogueX, 0);
        _this._input_aimY = _internal__WEBPACK_IMPORTED_MODULE_0__.InputControl.CreateAnalogue(_internal__WEBPACK_IMPORTED_MODULE_0__.InputType.mouseAnalogueY, 0);
        _this._input_moveLeft = _internal__WEBPACK_IMPORTED_MODULE_0__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_0__.InputType.keyboardButton, 65, _internal__WEBPACK_IMPORTED_MODULE_0__.InputCondition.whileHeld);
        _this._input_moveRight = _internal__WEBPACK_IMPORTED_MODULE_0__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_0__.InputType.keyboardButton, 68, _internal__WEBPACK_IMPORTED_MODULE_0__.InputCondition.whileHeld);
        _this._input_moveUp = _internal__WEBPACK_IMPORTED_MODULE_0__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_0__.InputType.keyboardButton, 87, _internal__WEBPACK_IMPORTED_MODULE_0__.InputCondition.whileHeld);
        _this._input_moveDown = _internal__WEBPACK_IMPORTED_MODULE_0__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_0__.InputType.keyboardButton, 83, _internal__WEBPACK_IMPORTED_MODULE_0__.InputCondition.whileHeld);
        _this._input_jump = _internal__WEBPACK_IMPORTED_MODULE_0__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_0__.InputType.keyboardButton, 32, _internal__WEBPACK_IMPORTED_MODULE_0__.InputCondition.onPress);
        _this._input_useWeaponPrimary = _internal__WEBPACK_IMPORTED_MODULE_0__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_0__.InputType.mouseButton, 0, _internal__WEBPACK_IMPORTED_MODULE_0__.InputCondition.whileHeld);
        _this._input_useWeaponAlt = _internal__WEBPACK_IMPORTED_MODULE_0__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_0__.InputType.mouseButton, 2, _internal__WEBPACK_IMPORTED_MODULE_0__.InputCondition.whileHeld);
        return _this;
    }
    PlayerController.CreateDefault = function () { return new this(); };
    Object.defineProperty(PlayerController.prototype, "input_aimX", {
        get: function () { return this._input_aimX; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PlayerController.prototype, "input_aimY", {
        get: function () { return this._input_aimY; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PlayerController.prototype, "input_moveLeft", {
        get: function () { return this._input_moveLeft; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PlayerController.prototype, "input_moveRight", {
        get: function () { return this._input_moveRight; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PlayerController.prototype, "input_moveUp", {
        get: function () { return this._input_moveUp; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PlayerController.prototype, "input_moveDown", {
        get: function () { return this._input_moveDown; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PlayerController.prototype, "input_jump", {
        get: function () { return this._input_jump; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PlayerController.prototype, "input_useWeaponPrimary", {
        get: function () { return this._input_useWeaponPrimary; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PlayerController.prototype, "input_useWeaponAlt", {
        get: function () { return this._input_useWeaponAlt; },
        enumerable: false,
        configurable: true
    });
    PlayerController.prototype.Update = function (deltaTime, controlled) {
        var player = controlled;
        if (player.parentGame == null)
            return;
        // update control input states
        this._input_aimX.Update(player.parentGame);
        this._input_aimY.Update(player.parentGame);
        this._input_moveLeft.Update(player.parentGame);
        this._input_moveRight.Update(player.parentGame);
        this._input_moveUp.Update(player.parentGame);
        this._input_moveDown.Update(player.parentGame);
        this._input_jump.Update(player.parentGame);
        this._input_useWeaponPrimary.Update(player.parentGame);
        this._input_useWeaponAlt.Update(player.parentGame);
        // shortcut ref
        var action = _internal__WEBPACK_IMPORTED_MODULE_0__.IControllable.Action;
        // handle movement input
        var moveX = 0;
        var moveY = 0;
        if (this._input_moveRight.IsTriggered())
            moveX += 1;
        if (this._input_moveLeft.IsTriggered())
            moveX -= 1;
        if (this._input_moveDown.IsTriggered())
            moveY += 1;
        if (this._input_moveUp.IsTriggered())
            moveY -= 1;
        controlled.DoAction(action.XMove, moveX);
        controlled.DoAction(action.YMove, moveY);
        if (this._input_jump.IsTriggered()) {
            controlled.DoAction(action.Jump, 1);
        }
        // handle aiming
        var aimVect = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(this._input_aimX.GetValue(), this._input_aimY.GetValue());
        if (_internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Magnitude(aimVect) > 1) {
            aimVect = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Normalize(aimVect);
            this._input_aimX.SetValue(aimVect.x);
            this._input_aimY.SetValue(aimVect.y);
        }
        controlled.DoAction(action.AimRotate, _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Direction(aimVect));
        controlled.DoAction(action.AimMagnitude, _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Magnitude(aimVect));
        // weapon/item usage
        if (this._input_useWeaponPrimary.IsTriggered()) {
            controlled.DoAction(action.UseItemPrimary, 1);
        }
        if (this._input_useWeaponAlt.IsTriggered()) {
            controlled.DoAction(action.UseItemAlternate, 1);
        }
    };
    return PlayerController;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.ObjectController));

_a = PlayerController;
(function () {
    _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(_a, function (x) { return []; });
})();


/***/ }),

/***/ 2750:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AiBehavior": () => (/* binding */ AiBehavior)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

var AiBehavior = /** @class */ (function (_super) {
    __extends(AiBehavior, _super);
    function AiBehavior() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AiBehavior;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.Serializable));



/***/ }),

/***/ 5962:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CombatMode": () => (/* binding */ CombatMode),
/* harmony export */   "AiController": () => (/* binding */ AiController)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

var CombatMode;
(function (CombatMode) {
    CombatMode[CombatMode["none"] = 0] = "none";
    CombatMode[CombatMode["passive"] = 1] = "passive";
    CombatMode[CombatMode["suspicious"] = 2] = "suspicious";
    CombatMode[CombatMode["engaged"] = 3] = "engaged";
    CombatMode[CombatMode["retreating"] = 4] = "retreating";
})(CombatMode || (CombatMode = {}));
var AiController = /** @class */ (function (_super) {
    __extends(AiController, _super);
    function AiController() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(AiController.prototype, "currentBehavior", {
        get: function () { return this._currentBehavior; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AiController.prototype, "target", {
        get: function () { return this._target; },
        enumerable: false,
        configurable: true
    });
    AiController.prototype.Update = function (deltaTime, actor) {
        this.currentBehavior.Update(this, deltaTime, actor);
    };
    return AiController;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.ActorController));



/***/ }),

/***/ 9125:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Team": () => (/* binding */ Team),
/* harmony export */   "Aggression": () => (/* binding */ Aggression),
/* harmony export */   "Allegiance": () => (/* binding */ Allegiance)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _this = undefined;

var Team;
(function (Team) {
    Team[Team["none"] = 0] = "none";
    Team[Team["player"] = 1] = "player";
    Team[Team["neutral"] = 2] = "neutral";
    Team[Team["hazard"] = 4] = "hazard";
    Team[Team["enemy"] = 8] = "enemy";
    Team[Team["all"] = 255] = "all";
})(Team || (Team = {}));
var Aggression;
(function (Aggression) {
    Aggression[Aggression["neutral"] = 0] = "neutral";
    Aggression[Aggression["friendly"] = 1] = "friendly";
    Aggression[Aggression["hostile"] = 2] = "hostile";
})(Aggression || (Aggression = {}));
var Allegiance = /** @class */ (function (_super) {
    __extends(Allegiance, _super);
    function Allegiance() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.team = Team.neutral;
        _this.hostileToward = Team.none;
        _this.friendlyToward = Team.none;
        _this.hostileExceptions = [];
        return _this;
    }
    Allegiance.Hazard = function () {
        var r = new Allegiance();
        r.team = Team.hazard;
        r.hostileToward = Team.player | Team.enemy;
        return r;
    };
    Allegiance.PlayerDefault = function () {
        var r = new Allegiance();
        r.team = Team.player;
        r.hostileToward = Team.enemy;
        r.friendlyToward = Team.player;
        return r;
    };
    Allegiance.EnemyDefault = function () {
        var r = new Allegiance();
        r.team = Team.enemy;
        r.hostileToward = Team.player;
        r.friendlyToward = Team.enemy;
        return r;
    };
    /// --------------------------------------------------------------------------------------------
    Allegiance.CreateDefault = function () { return new this(); };
    /// --------------------------------------------------------------------------------------------
    /**
     * determines whether this allegiance finds the specified target hostile, friendly, or nuetral
     * @param target
     */
    Allegiance.prototype.AggresionToward = function (target) {
        var r = Aggression.neutral;
        if ((this.hostileToward & target.team) > 0 || this.hostileExceptions.includes(target)) {
            return Aggression.hostile;
        }
        if ((this.friendlyToward & target.team) > 0) {
            return Aggression.friendly;
        }
        return r;
    };
    var _a;
    _a = Allegiance;
    /// --------------------------------------------------------------------------------------------
    Allegiance.HAZARD = _a.Hazard();
    (function () {
        _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(_a, function (x) { return [
            x.team,
            x.hostileToward,
            x.friendlyToward,
            x.hostileExceptions
        ]; });
    })();
    return Allegiance;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.Serializable));



/***/ }),

/***/ 6033:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AnimatedEffect": () => (/* binding */ AnimatedEffect)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var AnimatedEffect = /** @class */ (function (_super) {
    __extends(AnimatedEffect, _super);
    function AnimatedEffect() {
        var _this = _super.call(this) || this;
        _this._frameTimer = 0;
        _this._frameDelay = 0;
        _this._currentFrame = 0;
        return _this;
    }
    AnimatedEffect.HitEffect = function (scene, pos, vel, direction, color) {
        // TODO velocity
        var r = AnimatedEffect.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.Effect.Sprite_HitEffect, 0.03333, color);
        scene.addChild(r);
        r.globalPosition = pos;
        r.rotation = direction;
        return r;
    };
    /**
     * create an animated effect from the provided information
     * @param spriteAnim the sprite for the effect to animate
     * @param frameDelay the amount of seconds that each frame will be shown for
     */
    AnimatedEffect.Create = function (spriteAnim, frameDelay, color) {
        if (color === void 0) { color = 0xFFFFFF; }
        var r = new AnimatedEffect();
        r._spriteAnim = spriteAnim;
        r._sprite = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Sprite(null);
        if (r._spriteAnim.isLoaded) {
            r._sprite.texture = r._spriteAnim.sprites[0];
            r._sprite.anchor.set(r._sprite.texture.defaultAnchor.x, r._sprite.texture.defaultAnchor.y);
        }
        r._sprite.tint = color;
        r._frameDelay = frameDelay;
        r._frameTimer = frameDelay;
        r.addChild(r._sprite);
        return r;
    };
    AnimatedEffect.prototype.Update = function (deltaTime) {
        _super.prototype.Update.call(this, deltaTime);
        // deduct frame timer and test to see if it has fully elapsed
        this._frameTimer -= deltaTime;
        if (this._frameTimer <= 0) {
            // reset frame timer and get next frame
            this._frameTimer = this._frameDelay;
            this._currentFrame += 1;
            if (this._spriteAnim.isLoaded) {
                // if next frame exceeds total frame count, animaiton is complete, so we remove the 
                // effect
                if (this._currentFrame >= this._spriteAnim.sprites.length) {
                    this.RemoveFromScene();
                }
                // increment the frame
                else {
                    var tex = this._spriteAnim.sprites[this._currentFrame];
                    this._sprite.texture = tex;
                    this._sprite.anchor.set(tex.defaultAnchor.x, tex.defaultAnchor.y);
                }
            }
        }
    };
    return AnimatedEffect;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.Effect));



/***/ }),

/***/ 9263:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Camera": () => (/* binding */ Camera)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var Camera = /** @class */ (function (_super) {
    __extends(Camera, _super);
    function Camera() {
        var _this = _super.call(this) || this;
        _this._viewMatrix = pixi_js__WEBPACK_IMPORTED_MODULE_0__.Matrix.IDENTITY.clone();
        _this.zoomScalar = 1;
        _this.zoomVector = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(1, 1);
        return _this;
    }
    // ---------------------------------------------------------------------------------------------
    Camera.CreateDefault = function () {
        return new Camera();
    };
    Camera.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.MakeSerializable(this, function (x) { return [
            x.zoomScalar,
            x.zoomVector
        ]; });
    };
    // ---------------------------------------------------------------------------------------------
    Camera.prototype.CenterAt = function (target) {
        this.globalPosition = target;
    };
    /**
     * gets the view matrix of the camer to transform the graphics by
     * @param rtSize the size of the render target that the camer will be drawing to, the game's
     * 	main renderer canvas size will be used by default if not specified
     */
    Camera.prototype.GetViewMatrix = function (rtSize) {
        if (rtSize === void 0) { rtSize = null; }
        var trot = 0;
        var tscl = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this.zoomVector.x * this.zoomScalar, this.zoomVector.y * this.zoomScalar);
        rtSize = rtSize || _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this.parentGame.renderer.view.width, this.parentGame.renderer.view.height);
        var tpos = this.globalPosition;
        tpos.x = tpos.x * tscl.x;
        tpos.y = tpos.y * tscl.y;
        var tpiv = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(-((rtSize.x / tscl.x) * 0.5), -((rtSize.y / tscl.y) * 0.5));
        this._viewMatrix.setTransform(-tpos.x, -tpos.y, tpiv.x, tpiv.y, tscl.x, tscl.y, trot, 0, 0);
        return this._viewMatrix;
    };
    return Camera;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity));

_a = Camera;
(function () {
    _a.SetSerializableFields();
})();


/***/ }),

/***/ 7354:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CollisionLayer": () => (/* binding */ CollisionLayer),
/* harmony export */   "Collider": () => (/* binding */ Collider),
/* harmony export */   "AABBCollider": () => (/* binding */ AABBCollider),
/* harmony export */   "CircleCollider": () => (/* binding */ CircleCollider)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a, _b;
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var CollisionLayer;
(function (CollisionLayer) {
    CollisionLayer[CollisionLayer["none"] = 0] = "none";
    CollisionLayer[CollisionLayer["terrain"] = 1] = "terrain";
    CollisionLayer[CollisionLayer["actors"] = 2] = "actors";
    CollisionLayer[CollisionLayer["props"] = 4] = "props";
    CollisionLayer[CollisionLayer["projectiles"] = 8] = "projectiles";
    CollisionLayer[CollisionLayer["effects"] = 16] = "effects";
})(CollisionLayer || (CollisionLayer = {}));
var Collider = /** @class */ (function (_super) {
    __extends(Collider, _super);
    function Collider() {
        var _this = _super.call(this) || this;
        // ---------------------------------------------------------------------------------------------
        _this._layer = CollisionLayer.none;
        _this._onCollision = new _internal__WEBPACK_IMPORTED_MODULE_1__.GameEvent();
        _this._wireframeGraphic = null;
        _this._wireframeInfo = null;
        _this.isSolid = false;
        _this.materialFriction = 0.5;
        _this.materialRestitution = 0.2;
        _this._isActivated = true;
        return _this;
    }
    // ---------------------------------------------------------------------------------------------
    Collider.CreateDefault = function () { return null; };
    Collider.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.MakeSerializable(this, function (x) { return [
            x.isSolid,
            x.materialFriction,
            x.materialRestitution,
            x._layer,
            x._wireframeInfo
        ]; });
    };
    Object.defineProperty(Collider.prototype, "isActivated", {
        get: function () { return this._isActivated; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Collider.prototype, "layer", {
        get: function () { return this._layer; },
        set: function (lyr) {
            if (this.parentScene != null)
                throw "not implemented";
            this._layer = lyr;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Collider.prototype, "collider", {
        get: function () { return this; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Collider.prototype, "onCollision", {
        get: function () { return this._onCollision; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    Collider.prototype.OnAddedToScene = function (scene) {
        _super.prototype.OnAddedToScene.call(this, scene);
        if (this.isActivated)
            this.OnActivated();
    };
    /** @inheritdoc */
    Collider.prototype.OnRemovedFromScene = function (scene) {
        if (this.isActivated) {
            // remove from collider list
            scene === null || scene === void 0 ? void 0 : scene.colliders.RemoveCollider(this);
        }
        _super.prototype.OnRemovedFromScene.call(this, scene);
    };
    Collider.prototype.OnActivated = function () {
        // add to parent scene collider array
        if (this.parentScene != null)
            this.parentScene.colliders.AddCollider(this);
        this._isActivated = true;
    };
    Collider.prototype.OnDeactivated = function () {
        var _c;
        // remove from collider list
        (_c = this.parentScene) === null || _c === void 0 ? void 0 : _c.colliders.RemoveCollider(this);
        this._isActivated = false;
    };
    /** @inheritdoc */
    Collider.prototype.SetActivated = function (active) {
        if (active)
            this.Activate();
        else
            this.Deactivate();
    };
    Collider.prototype.Activate = function () {
        if (this._isActivated)
            return;
        this.OnActivated();
        this._isActivated = true;
    };
    Collider.prototype.Deactivate = function () {
        if (!this._isActivated)
            return;
        this.OnDeactivated();
        this._isActivated = false;
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    Collider.prototype.destroy = function (options) {
        this.OnRemovedFromScene(this.parentScene);
        _super.prototype.destroy.call(this, options);
    };
    // ---------------------------------------------------------------------------------------------
    /**
     * creates an outline graphic of the collider's axis aligned bounding box, is not added to self
     * @param color the color of the wireframe
     * @param width the thickness of the wireframe
     */
    Collider.prototype.CreateAABBWireframe = function (color, width) {
        var r = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Graphics();
        var bounds = this.GetColliderAABB();
        r.lineStyle({
            alpha: 1,
            width: width,
            color: color
        });
        r.drawRect(0, 0, bounds.size.x, bounds.size.y);
        return r;
    };
    /**
     * redraw the wireframe graphic
     */
    Collider.prototype.RedrawWireframe = function () {
        var _c;
        if (this._wireframeInfo != null) {
            (_c = this._wireframeGraphic) === null || _c === void 0 ? void 0 : _c.destroy();
            this._wireframeGraphic = null;
            this.CreateWireframe(this._wireframeInfo.color, this._wireframeInfo.width);
        }
    };
    /**
     * record wireframe info when a wireframe graphic is created so that it can be serialized
     * @param color the color that the wireframe will draw as
     * @param width the line width of the wire frame graphic
     */
    Collider.prototype.RecordWireFrameInfo = function (color, width) {
        this._wireframeInfo = { color: color, width: width };
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    Collider.prototype.OnPostDeserialize = function () {
        this.RedrawWireframe();
    };
    return Collider;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity));

var AABBCollider = /** @class */ (function (_super) {
    __extends(AABBCollider, _super);
    function AABBCollider() {
        var _this = _super.call(this) || this;
        // ---------------------------------------------------------------------------------------------
        _this._halfExtents = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        return _this;
    }
    /**
     * Create an aabb collider from the specified parameters
     * @param hextents the half extends of the collider
     */
    AABBCollider.Create = function (hextents) {
        var r = new AABBCollider();
        r._halfExtents.x = hextents.x;
        r._halfExtents.y = hextents.y;
        return r;
    };
    Object.defineProperty(AABBCollider.prototype, "halfExtents", {
        get: function () { return this._halfExtents; },
        set: function (vec) {
            this._halfExtents.x = vec.x;
            this._halfExtents.y = vec.y;
            this.RedrawWireframe();
        },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    AABBCollider.CreateDefault = function () { return new AABBCollider(); };
    AABBCollider.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.MakeSerializable(this, function (x) { return [
            x._halfExtents,
        ]; });
    };
    // ---------------------------------------------------------------------------------------------
    AABBCollider.prototype.GetResizeRect = function () {
        return this.GetColliderAABB();
    };
    AABBCollider.prototype.SetResizeRect = function (rect) {
        this.SetRect(rect);
        this.RedrawWireframe();
    };
    // ---------------------------------------------------------------------------------------------
    /**
     * set the collider to match the specified rect
     * @param rect
     */
    AABBCollider.prototype.SetRect = function (rect) {
        this.globalPosition = _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.Center(rect);
        this.halfExtents = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(rect.size, 0.5);
    };
    /** @inheritdoc */
    AABBCollider.prototype.OverlapsCollider = function (collider) {
        if (collider instanceof AABBCollider) {
            var selfBox = this.GetColliderAABB();
            var oBox = collider.GetColliderAABB();
            return _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.Overlaps(selfBox, oBox);
        }
        else if (collider instanceof CircleCollider) {
            return collider.OverlapsCollider(this);
        }
        throw "shape overlap detection not implemented";
    };
    /** @inheritdoc */
    AABBCollider.prototype.GetCollision = function (collider) {
        if (collider instanceof AABBCollider) {
            return this.GetCollision_AABB(collider);
        }
        else if (collider instanceof CircleCollider) {
            return collider.GetCollision(this);
        }
        return null;
    };
    AABBCollider.prototype.GetCollision_AABB = function (collider) {
        var selfBounds = this.GetColliderAABB();
        var overlapRect = _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.OverlapRect(selfBounds, collider.GetColliderAABB());
        var overlapCenter = _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.Center(overlapRect);
        // console.log(overlapRect.size.x);
        // left side
        var sideDist = Math.abs(overlapCenter.x - selfBounds.position.x);
        var colSide = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.left;
        // right side
        var rightSideDist = Math.abs(overlapCenter.x - (selfBounds.position.x + selfBounds.size.x));
        if (sideDist > rightSideDist) {
            sideDist = rightSideDist;
            colSide = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.right;
        }
        // top side
        var topSideDist = Math.abs(overlapCenter.y - selfBounds.position.y);
        if (sideDist > topSideDist) {
            sideDist = topSideDist;
            colSide = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.up;
        }
        // bottom side
        var bottomSideDist = Math.abs(overlapCenter.y - (selfBounds.position.y + selfBounds.size.y));
        if (sideDist > bottomSideDist) {
            sideDist = bottomSideDist;
            colSide = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.down;
        }
        // calculate collision normal based on which side of the collider is hit
        var colNormal = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.GetVector(_internal__WEBPACK_IMPORTED_MODULE_1__.Side.Opposite(colSide));
        return _internal__WEBPACK_IMPORTED_MODULE_1__.Collision.Create(this, collider, colNormal, overlapCenter, sideDist * 2);
    };
    /** @inheritdoc */
    AABBCollider.prototype.OverlapsPoint = function (point) {
        var gpos = this.globalPosition;
        var hext = this._halfExtents;
        return !(point.x < gpos.x - hext.x ||
            point.x > gpos.x + hext.x ||
            point.y < gpos.y - hext.y ||
            point.y > gpos.y + hext.y);
    };
    /** @inheritdoc */
    AABBCollider.prototype.ClosestPoint = function (point) {
        var gpos = this.globalPosition;
        var hext = this._halfExtents;
        return {
            x: Math.min(Math.max(point.x, gpos.x - hext.x), gpos.x + hext.x),
            y: Math.min(Math.max(point.y, gpos.y - hext.y), gpos.y + hext.y)
        };
    };
    /** @inheritdoc */
    AABBCollider.prototype.GetColliderAABB = function () {
        var gpos = this.globalPosition;
        return _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(gpos.x - this._halfExtents.x, gpos.y - this._halfExtents.y), _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this._halfExtents.x * 2, this._halfExtents.y * 2));
    };
    /** @inheritdoc */
    AABBCollider.prototype.RayCast = function (ray) {
        // calculate raycast result and return it
        var r = _internal__WEBPACK_IMPORTED_MODULE_1__.Ray.CastAgainstRect(ray, this.GetColliderAABB());
        // apply collider reference to point to self
        if (r != null) {
            r.collider = this;
        }
        return r;
    };
    /** @inheritdoc */
    AABBCollider.prototype.CircleCast = function (ray, radius) {
        // TODO Optomize all this shit
        // calculate the widest possible hit on the rect
        var rect = this.GetColliderAABB();
        var radius2 = radius * 2;
        rect.position.x -= radius;
        rect.position.y -= radius;
        rect.size.x += radius2;
        rect.size.y += radius2;
        var rectCastResult = _internal__WEBPACK_IMPORTED_MODULE_1__.Ray.CastAgainstRect(ray, rect);
        if (rectCastResult == null) {
            return null;
        }
        var fcont = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Add(rectCastResult.entryPoint, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(rectCastResult.entryNormal, -radius));
        var lcont = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Add(rectCastResult.exitPoint, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(rectCastResult.exitNormal, radius));
        var r = _internal__WEBPACK_IMPORTED_MODULE_1__.RaycastResult.PrecalculatedCirclecastResult(ray, radius, this, rectCastResult.didEnter, rectCastResult.didExit, rectCastResult.entryPoint, rectCastResult.exitPoint, fcont, lcont, rectCastResult.entryNormal, rectCastResult.exitNormal, rectCastResult.penetration);
        // if the entry point is a corner case
        var insetEntry = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Add(rectCastResult.entryPoint, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(rectCastResult.entryNormal, radius * 0.5));
        if (!this.OverlapsPoint(insetEntry)) {
            // perform raycast against circle at nearest corner position with radius of circlecast
            var corner = this.ClosestPoint(insetEntry);
            var circCastResult = _internal__WEBPACK_IMPORTED_MODULE_1__.Ray.CastAgainstCircle(ray, corner, radius);
            // if the circle is hit, its the same as if a circlecast hit the corner of the box
            if (circCastResult != null && circCastResult.didEnter) {
                r.didEnter = true;
                r.entryPoint = circCastResult.entryPoint;
                r.entryNormal = circCastResult.entryNormal;
            }
            else {
                return null;
            }
        }
        // if the exit point is a corner case
        var insetExit = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Add(rectCastResult.exitPoint, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(rectCastResult.exitNormal, radius * 0.5));
        if (!this.OverlapsPoint(insetExit)) {
            // perform raycast against circle at nearest corner position with radius of circlecast
            var corner = this.ClosestPoint(insetExit);
            var circCastResult = _internal__WEBPACK_IMPORTED_MODULE_1__.Ray.CastAgainstCircle(ray, corner, radius);
            // if the circle is hit, its the same as if a circlecast hit the corner of the box
            if (circCastResult != null && circCastResult.didExit) {
                r.didExit = true;
                r.exitPoint = circCastResult.exitPoint;
                r.exitNormal = circCastResult.exitNormal;
            }
            else {
                // return null;
            }
        }
        // return null if no entry or exit
        if (!r.didEnter && !r.didExit) {
            return null;
        }
        return r;
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    AABBCollider.prototype.CreateWireframe = function (color, width) {
        this.RecordWireFrameInfo(color, width);
        if (this._wireframeGraphic != null) {
            this._wireframeGraphic.destroy();
        }
        this._wireframeGraphic = this.CreateAABBWireframe(color, width);
        this.addChild(this._wireframeGraphic);
        _internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity.SetGlobalPosition(this._wireframeGraphic, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Subtract(this.globalPosition, this.halfExtents));
        return this._wireframeGraphic;
    };
    // ---------------------------------------------------------------------------------------------
    AABBCollider.GetEditorProperties = function () {
        var r = _super.GetEditorProperties.call(this);
        r.push(_internal__WEBPACK_IMPORTED_MODULE_1__.EditorProperty.CreateVectProp("halfExtents", "Half Extents"));
        return r;
    };
    return AABBCollider;
}(Collider));

_a = AABBCollider;
(function () {
    _a.SetSerializableFields();
})();
var CircleCollider = /** @class */ (function (_super) {
    __extends(CircleCollider, _super);
    function CircleCollider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._radius = 0;
        return _this;
    }
    CircleCollider.Create = function (radius) {
        var r = new CircleCollider();
        r._radius = radius;
        return r;
    };
    Object.defineProperty(CircleCollider.prototype, "radius", {
        get: function () { return this._radius; },
        set: function (rad) {
            this._radius = rad;
            this.RedrawWireframe();
        },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    CircleCollider.CreateDefault = function () { return new CircleCollider(); };
    CircleCollider.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.MakeSerializable(this, function (x) { return [
            x._radius,
        ]; });
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    CircleCollider.prototype.GetColliderAABB = function () {
        var pos = this.globalPosition;
        pos.x -= this._radius;
        pos.y -= this._radius;
        var r = _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.Create(pos, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this._radius * 2));
        return r;
    };
    /** @inheritdoc */
    CircleCollider.prototype.OverlapsCollider = function (collider) {
        if (collider instanceof CircleCollider) {
            var oCirc = collider;
            var distSq = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.DistanceSquared(oCirc.globalPosition, this.globalPosition);
            var radSq = oCirc.radius + this.radius;
            radSq *= radSq;
            return distSq <= radSq;
        }
        var pos = this.globalPosition;
        var radius = this.radius;
        return _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.DistanceSquared(collider.ClosestPoint(pos), pos) <= (radius * radius);
    };
    /** @inheritdoc */
    CircleCollider.prototype.GetCollision = function (collider) {
        if (collider instanceof CircleCollider) {
            return this.GetCollision_Circle(collider);
        }
        else if (collider instanceof AABBCollider) {
            return this.GetCollision_AABB(collider);
        }
        throw new Error('Method not implemented.');
    };
    CircleCollider.prototype.GetCollision_Circle = function (other) {
        var posA = this.globalPosition;
        var posB = other.globalPosition;
        var invDif = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Subtract(posA, posB);
        var point = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create((posA.x + posB.x) * 0.5, (posA.y + posB.y) * 0.5);
        return _internal__WEBPACK_IMPORTED_MODULE_1__.Collision.Create(this, other, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Normalize(invDif), point, (this.radius + other.radius) - _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Magnitude(invDif));
    };
    CircleCollider.prototype.GetCollision_AABB = function (other) {
        var posA = this.globalPosition;
        var posB = other.ClosestPoint(posA);
        var dif = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Subtract(posB, posA);
        var norm = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Invert(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Normalize(dif));
        var pen = this.radius - _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Magnitude(dif);
        var point = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Add(posA, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(norm, -this.radius - (pen * 0.5)));
        return _internal__WEBPACK_IMPORTED_MODULE_1__.Collision.Create(this, other, norm, point, pen);
    };
    /** @inheritdoc */
    CircleCollider.prototype.OverlapsPoint = function (point) {
        var pos = this.globalPosition;
        var dx = pos.x - point.x;
        var dy = pos.y - point.y;
        var distSq = dx * dx + dy * dy;
        var radSq = this._radius * this._radius;
        return distSq <= radSq;
    };
    /** @inheritdoc */
    CircleCollider.prototype.ClosestPoint = function (point) {
        var pos = this.globalPosition;
        var dif = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Subtract(point, pos);
        var mag = Math.min(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Magnitude(dif), this.radius);
        dif = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Normalize(dif);
        dif = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(dif, mag);
        return _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Add(pos, dif);
    };
    /** @inheritdoc */
    CircleCollider.prototype.RayCast = function (ray) {
        var r = _internal__WEBPACK_IMPORTED_MODULE_1__.Ray.CastAgainstCircle(ray, this.globalPosition, this._radius);
        if (r != null) {
            r.collider = this;
        }
        return r;
    };
    /** @inheritdoc */
    CircleCollider.prototype.CircleCast = function (ray, radius) {
        var castResult = _internal__WEBPACK_IMPORTED_MODULE_1__.Ray.CastAgainstCircle(ray, this.globalPosition, this._radius + radius);
        // null if no intersection
        if (castResult == null)
            return null;
        var fc = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Add(castResult.entryPoint, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(castResult.entryNormal, -radius));
        var lc = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Add(castResult.exitPoint, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(castResult.exitNormal, -radius));
        // create circlecast result from raycast
        return _internal__WEBPACK_IMPORTED_MODULE_1__.RaycastResult.PrecalculatedCirclecastResult(ray, radius, this, castResult.didEnter, castResult.didExit, castResult.entryPoint, castResult.exitPoint, fc, lc, castResult.exitNormal, castResult.exitNormal, castResult.penetration);
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    CircleCollider.prototype.CreateWireframe = function (color, width) {
        this.RecordWireFrameInfo(color, width);
        var r = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Graphics();
        if (this._wireframeGraphic != null)
            this._wireframeGraphic.destroy();
        this._wireframeGraphic = r;
        r.lineStyle({
            color: color,
            width: width
        });
        r.drawCircle(0, 0, this.radius);
        this.addChild(r);
        return r;
    };
    // ---------------------------------------------------------------------------------------------
    CircleCollider.GetEditorProperties = function () {
        var r = _super.GetEditorProperties.call(this);
        r.push(_internal__WEBPACK_IMPORTED_MODULE_1__.EditorProperty.CreateNumberProp("radius", "Radius"));
        return r;
    };
    return CircleCollider;
}(Collider));

_b = CircleCollider;
(function () {
    _b.SetSerializableFields();
})();


/***/ }),

/***/ 3466:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ColliderPartitions": () => (/* binding */ ColliderPartitions)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);

var ColliderPartitions = /** @class */ (function () {
    function ColliderPartitions() {
        this.any = new Array();
        this.terrain = new Array();
        this.actors = new Array();
        this.props = new Array();
        this.projectiles = new Array();
        this.effects = new Array();
    }
    ColliderPartitions.prototype.GetCompatibleCollisionLayers = function (layer) {
        var r = new Array();
        // TODO finish implementation
        switch (layer) {
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.none:
                r.push(this.any, this.terrain, this.actors, this.props, this.projectiles, this.effects);
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.actors:
                r.push(this.terrain, this.actors, this.props, this.projectiles);
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.projectiles:
                r.push(this.terrain, this.actors, this.props);
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain:
                r.push(this.any, this.actors, this.projectiles, this.props);
                break;
        }
        return r;
    };
    ColliderPartitions.prototype.GetCollisionLayers = function (layers) {
        var r = new Array();
        if ((layers & _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.none) > 0)
            r.push(this.any);
        if ((layers & _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.actors) > 0)
            r.push(this.actors);
        if ((layers & _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain) > 0)
            r.push(this.terrain);
        if ((layers & _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.props) > 0)
            r.push(this.props);
        if ((layers & _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.projectiles) > 0)
            r.push(this.projectiles);
        if ((layers & _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.effects) > 0)
            r.push(this.effects);
        return r;
    };
    ColliderPartitions.prototype.AddCollider = function (collider, layer) {
        if (layer === void 0) { layer = null; }
        if (layer != null) {
            collider.layer = layer;
        }
        var array = this.any;
        switch (collider.layer) {
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain:
                array = this.terrain;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.actors:
                array = this.actors;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.props:
                array = this.props;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.projectiles:
                array = this.projectiles;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.effects:
                array = this.effects;
                break;
        }
        array.push(collider);
    };
    ColliderPartitions.prototype.RemoveCollider = function (collider) {
        var array = this.any;
        switch (collider.layer) {
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain:
                array = this.terrain;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.actors:
                array = this.actors;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.props:
                array = this.props;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.projectiles:
                array = this.projectiles;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.effects:
                array = this.effects;
                break;
        }
        var index = array.indexOf(collider);
        if (index >= 0) {
            array.splice(index, 1);
        }
    };
    ColliderPartitions.prototype.ForceCollision = function (colliderA, colliderB) {
        if (this._forcedCols == null) {
            this._forcedCols = new Array();
        }
        this._forcedCols.push(colliderA, colliderB);
    };
    ColliderPartitions.prototype.ContainsForcedCollision = function (colA, colB) {
        // iterate through each forced collision pair
        if (this._forcedCols != null)
            for (var i = this._forcedCols.length - 1; i > 0; i -= 2) {
                if ((this._forcedCols[i - 1] == colA && this._forcedCols[i] == colB) ||
                    (this._forcedCols[i - 1] == colB && this._forcedCols[i] == colA))
                    return true;
            }
        return false;
    };
    ColliderPartitions.prototype.FindOverlapColliders = function (col) {
        return this.FindOverlapCollidersOnLayerArrs(col, this.GetCompatibleCollisionLayers(col.layer));
    };
    ColliderPartitions.prototype.FindOverlapCollidersOnLayers = function (col, layers) {
        return this.FindOverlapCollidersOnLayerArrs(col, this.GetCollisionLayers(layers));
    };
    ColliderPartitions.prototype.FindOverlapCollidersOnLayerArrs = function (col, layers) {
        var r = new Array();
        for (var i0 = layers.length - 1; i0 >= 0; i0--) {
            for (var i1 = layers[i0].length - 1; i1 >= 0; i1--) {
                if (col.OverlapsCollider(layers[i0][i1])) {
                    r.push(layers[i0][i1]);
                }
            }
        }
        return r;
    };
    ColliderPartitions.prototype.FindCollisions = function () {
        var r = new Array();
        // iterate through each terrain collider
        for (var i0 = this.terrain.length - 1; i0 >= 0; i0--) {
            var colA = this.terrain[i0];
            // check collisions for terrain vs actors
            for (var i1 = this.actors.length - 1; i1 >= 0; i1--) {
                var colB = this.actors[i1];
                if (this.ContainsForcedCollision(colA, colB))
                    continue;
                if (colA.OverlapsCollider(colB))
                    r.push(colA.GetCollision(colB));
            }
            // check collisions for terrain vs projectiles
            for (var i1 = this.projectiles.length - 1; i1 >= 0; i1--) {
                var colB = this.projectiles[i1];
                if (this.ContainsForcedCollision(colA, colB))
                    continue;
                if (colA.OverlapsCollider(colB))
                    r.push(colA.GetCollision(colB));
            }
        }
        // iterate through all actor colliders
        for (var i0 = this.actors.length - 1; i0 >= 0; i0--) {
            var colA = this.actors[i0];
            // check collisions for projectiles vs actors
            for (var i1 = this.projectiles.length - 1; i1 >= 0; i1--) {
                var colB = this.projectiles[i1];
                if (this.ContainsForcedCollision(colA, colB))
                    continue;
                if (colA.OverlapsCollider(colB))
                    r.push(colA.GetCollision(colB));
            }
            // check collisions for actors vs actors
            for (var i1 = this.actors.length - 1; i1 >= 0; i1--) {
                var colB = this.actors[i1];
                // no self collision >:c
                if (colA == colB)
                    continue;
                if (this.ContainsForcedCollision(colA, colB))
                    continue;
                if (colA.OverlapsCollider(colB))
                    r.push(colA.GetCollision(colB));
            }
        }
        // iterate through each forced collision
        if (this._forcedCols != null) {
            for (var i = this._forcedCols.length - 1; i > 0; i -= 2) {
                var colA = this._forcedCols[i - 1];
                var colB = this._forcedCols[i];
                r.push(colA.GetCollision(colB));
            }
            // clear the forced collisions
            this._forcedCols.splice(0, this._forcedCols.length);
        }
        return r;
    };
    /**
     * Casts a ray as if the ray was a collider on the specified layer
     * @param ray the ray to cast
     * @param fromLayer the layer that the ray should be on, and uses collision matrix from that
     * 	layer to determine which layers to cast against
     * @returns
     */
    ColliderPartitions.prototype.RaycastFromLayer = function (ray, fromLayer) {
        // each layer array that the raycast can collide with
        var layers = this.GetCompatibleCollisionLayers(fromLayer);
        return this.RaycastInArrays(ray, layers);
    };
    /**
     * Raycast against all colliders in specified layer or layers
     * @param ray the ray to cast
     * @param layers the layers to cast against
     */
    ColliderPartitions.prototype.RaycastAgainstLayers = function (ray, layers) {
        var arrs = this.GetCollisionLayers(layers);
        return this.RaycastInArrays(ray, arrs);
    };
    ColliderPartitions.prototype.RaycastInArrays = function (ray, arrays) {
        var r = new Array();
        // iterate through each layer that the raycast can collide with
        for (var i0 = arrays.length - 1; i0 >= 0; i0--) {
            // iterate through each collider array
            var layer = arrays[i0];
            for (var i1 = layer.length - 1; i1 >= 0; i1--) {
                // calculate raycast and append to results if necessary
                var collider = layer[i1];
                var result = collider.RayCast(ray);
                if (result != null && (result.didEnter || result.didExit)) {
                    r.push(result);
                }
            }
        }
        return r;
    };
    ColliderPartitions.prototype.CircleCast = function (ray, radius, layer) {
        var r = new Array();
        // iterate through each layer that the raycast can collide with
        var layers = this.GetCompatibleCollisionLayers(layer);
        for (var i0 = layers.length - 1; i0 >= 0; i0--) {
            // iterate through each collider layer
            var layer_1 = layers[i0];
            for (var i1 = layer_1.length - 1; i1 >= 0; i1--) {
                // calculate circlecast and append to results if necessary
                var collider = layer_1[i1];
                var result = collider.CircleCast(ray, radius);
                if (result != null && (result.didEnter || result.didExit)) {
                    r.push(result);
                }
            }
        }
        return r;
    };
    return ColliderPartitions;
}());



/***/ }),

/***/ 3224:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Effect": () => (/* binding */ Effect)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var Effect = /** @class */ (function (_super) {
    __extends(Effect, _super);
    function Effect() {
        return _super.call(this) || this;
    }
    Effect.Load = function () {
        // load the hit effect graphic
        // let texture = PIXI.BaseTexture.from('./dist/assets/graphics/SmallHit.png', {
        // 	scaleMode: PIXI.SCALE_MODES.NEAREST
        // });
        // let texSize = Vect.Create(35, 8);
        // let sprTextures: Array<PIXI.Texture> = [];
        // let frameSize = Vect.Create(5, 8);
        // for(let x = 0; x < texSize.x; x += frameSize.x){
        // 	let tex = new PIXI.Texture(texture, new PIXI.Rectangle(
        // 		x, 0, frameSize.x, frameSize.y
        // 	));
        // 	sprTextures.push(tex);
        // }
        this.Sprite_HitEffect = new _internal__WEBPACK_IMPORTED_MODULE_0__.SpriteSheet('./dist/assets/graphics/SmallHit.png');
        this.Sprite_HitEffect.GenerateSprites(7, 1, _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(1, 0.5));
    };
    return Effect;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.SceneEntity));



/***/ }),

/***/ 4544:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Game": () => (/* binding */ Game)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


/**
 * data structure that handles the core game logic and loop
 */
var Game = /** @class */ (function () {
    /**
     * creates a game with the specified render resolution
     * @param width the x resolution of the game renderer
     * @param height the y resolution of the game renderer, equal to width if not specified
     */
    function Game(width, height) {
        if (width === void 0) { width = 0; }
        if (height === void 0) { height = width; }
        this._animFrameRequestID = -1;
        this._frameRateCap = 11.11111; // 90 FPS
        this._keysPressed = new Array(256);
        this._mousePos = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        this._mouseButton = 0;
        this._mouseScroll = 0;
        this._mouseButtonsPressed = new Array(3);
        this._mousePressed = false;
        this._renderer = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Renderer({
            clearBeforeRender: false,
            backgroundAlpha: 1,
            width: width,
            height: height
        });
        if (Game._instance != null) {
            throw "Multiple singleton instances not allowed";
        }
        Game._instance = this;
        this._currentScene = _internal__WEBPACK_IMPORTED_MODULE_1__.GameScene.BasicScene(this);
        this._currentScene.OnEnter();
        console.log(this);
    }
    Object.defineProperty(Game, "instance", {
        get: function () { return this._instance; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "renderer", {
        /**
         * The pixijs renderer object used for rendering everything in the game
         */
        get: function () {
            return this._renderer;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "mouseScroll", {
        get: function () { return this._mouseScroll; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "mousePos", {
        get: function () { return this._mousePos; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "mouseButton", {
        get: function () { return this._mouseButton; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "mousePressed", {
        get: function () { return this._mousePressed; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "pointerIsLocked", {
        get: function () { return document.pointerLockElement == this.renderer.view; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "maxFramesPerSecond", {
        get: function () {
            return 1000 / this._frameRateCap;
        },
        set: function (fps) {
            this._frameRateCap = 1000 / fps;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "currentScene", {
        get: function () { return this._currentScene; },
        set: function (scene) {
            this._currentScene.OnExit();
            this._currentScene = scene;
            this._currentScene.OnEnter();
            if (!scene.requestPointerLock) {
                document.exitPointerLock();
            }
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "keysPressed", {
        get: function () { return this._keysPressed; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Game.prototype, "mouseButtonsPressed", {
        get: function () { return this._mouseButtonsPressed; },
        enumerable: false,
        configurable: true
    });
    /** starts listening to mouse events for the game */
    Game.prototype.AttachMouseEvents = function () {
        var ths = this;
        // NO RIGHT CLIK MENU >:C
        this.renderer.view.addEventListener('contextmenu', function (e) {
            e.preventDefault();
            return false;
        });
        this.renderer.view.addEventListener('wheel', function (e) {
            ths.OnScroll(e);
        });
        this.renderer.view.addEventListener('mousedown', function (e) {
            ths.OnMouseDown(e);
        });
        this.renderer.view.addEventListener('mouseup', function (e) {
            ths.OnMouseUp(e);
        });
        this.renderer.view.addEventListener('mouseout', function (e) {
            ths.OnMouseOut(e);
        });
        this.renderer.view.addEventListener('mousemove', function (e) {
            ths.OnMouseMove(e);
        });
    };
    /**
     * starts the game listening to keyboard events
     */
    Game.prototype.AttachKeyboardEvents = function () {
        var ths = this;
        window.addEventListener('keydown', function (e) {
            //console.log(e.key + ": " + e.keyCode);
            if (e.keyCode !== 73 && e.keyCode !== 123) {
                e.preventDefault();
            }
            ths.OnKeyDown(e);
        });
        window.addEventListener('keyup', function (e) {
            ths.OnKeyUp(e);
        });
    };
    /**
     * stops listening for keyboard events
     */
    Game.prototype.DetachKeyboardEvents = function () {
        // TODO
    };
    /**
     * resets all the keyboard input flags to false
     */
    Game.prototype.ResetKeyboardInput = function () {
        for (var i = this._keysPressed.length - 1; i >= 0; i--) {
            this._keysPressed[i] = false;
        }
    };
    Game.prototype.OnKeyDown = function (e) {
        console.log(e.key + ": " + e.keyCode);
        this._keysPressed[e.keyCode] = true;
    };
    Game.prototype.OnKeyUp = function (e) {
        this._keysPressed[e.keyCode] = false;
    };
    Game.prototype.OnScroll = function (e) {
        this._mouseScroll = Math.sign(e.deltaY);
    };
    Game.prototype.OnMouseDown = function (e) {
        var _a;
        if ((_a = this.currentScene) === null || _a === void 0 ? void 0 : _a.requestPointerLock) {
            this.renderer.view.requestPointerLock();
        }
        // console.log(e.button);
        this._mouseButtonsPressed[e.button] = true;
        this._mouseButton = e.button;
        this._mousePressed = true;
        if (!this.pointerIsLocked) {
            this._mousePos = {
                x: e.offsetX,
                y: e.offsetY
            };
        }
    };
    Game.prototype.OnMouseMove = function (e) {
        if (this.pointerIsLocked) {
            this._mousePos.x += e.movementX;
            this._mousePos.y += e.movementY;
            return;
        }
        this._mousePos = {
            x: e.offsetX,
            y: e.offsetY
        };
    };
    Game.prototype.OnMouseUp = function (e) {
        this._mouseButtonsPressed[e.button] = false;
        this._mousePressed = false;
        if (!this.pointerIsLocked) {
            this._mousePos = {
                x: e.offsetX,
                y: e.offsetY
            };
        }
    };
    Game.prototype.OnMouseOut = function (e) {
        for (var i = this._mouseButtonsPressed.length - 1; i >= 0; i--) {
            this._mouseButtonsPressed[i] = false;
        }
        this._mousePressed = false;
        if (!this.pointerIsLocked) {
            this._mousePos = {
                x: e.offsetX,
                y: e.offsetY
            };
        }
    };
    /**
     * sets the game to start continuously performing update and draw cycles
     */
    Game.prototype.StartGameLoop = function () {
        if (this._animFrameRequestID >= 0)
            return;
        this._lastStepTimestamp = performance.now();
        var ths = this;
        this._animFrameRequestID = requestAnimationFrame(function (time) {
            ths.Step(time);
        });
    };
    /**
     * stops the game from performing anymore update/draw cycles automatically
     */
    Game.prototype.StopGameLoop = function () {
        cancelAnimationFrame(this._animFrameRequestID);
        this._animFrameRequestID = -1;
    };
    Game.prototype.Step = function (time) {
        var ths = this;
        this._animFrameRequestID = requestAnimationFrame(function (time) {
            ths.Step(time);
        });
        var deltaTime = time - this._lastStepTimestamp;
        if (deltaTime <= this._frameRateCap)
            return;
        this._lastStepTimestamp = time;
        this.Update(deltaTime * 0.001);
        this.Draw();
        this._mouseScroll = 0;
    };
    /**
     * Simulate gameplay logic for the specified amount of time
     * @param deltaTime the ampunt of time in seconds that will be simulated in this update cycle
     */
    Game.prototype.Update = function (deltaTime) {
        var _a;
        (_a = this._currentScene) === null || _a === void 0 ? void 0 : _a.Update(deltaTime);
    };
    /**
     * Render the current game state to the render canvas
     */
    Game.prototype.Draw = function () {
        var _a;
        // clear the render canvas to the default background color
        this._renderer.clear();
        (_a = this._currentScene) === null || _a === void 0 ? void 0 : _a.Draw(this.renderer);
    };
    return Game;
}());



/***/ }),

/***/ 7757:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GameEvent": () => (/* binding */ GameEvent)
/* harmony export */ });
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
var GameEvent = /** @class */ (function () {
    function GameEvent() {
        /**
         * all the listeners that will be invoked when the event is fired
         */
        this.listeners = new Array();
    }
    /**
     * add a listener to be invoked when the event is invoked, listeners can not be added multiple
     * times
     * @param func the funciton to invoke when the event is fired
     */
    GameEvent.prototype.AddListener = function (func) {
        // ensure listener is not already added
        var index = this.listeners.indexOf(func);
        if (index >= 0)
            return;
        this.listeners.push(func);
    };
    /**
     * remove the specified listener from the event so it is no longer invoked
     * @param func
     */
    GameEvent.prototype.RemoveListener = function (func) {
        var index = this.listeners.indexOf(func);
        if (index >= 0)
            this.listeners.splice(index, 1);
    };
    /** removes all the event listeners from the event */
    GameEvent.prototype.ClearListeners = function () {
        this.listeners.splice(0, this.listeners.length);
    };
    /**
     * fire the event, invoke all listeners
     * @param data the event data to pass to the listener functions
     */
    GameEvent.prototype.Invoke = function (data) {
        for (var i = 0; i < this.listeners.length; i++) {
            this.listeners[i](data);
        }
    };
    return GameEvent;
}());



/***/ }),

/***/ 1570:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GameScene": () => (/* binding */ GameScene)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;

var GameScene = /** @class */ (function (_super) {
    __extends(GameScene, _super);
    function GameScene(game) {
        var _this = _super.call(this, game) || this;
        _this._camera = null;
        _this._player = null;
        _this._requestPointerLock = true;
        _this._name = "GameScene";
        return _this;
        // for testing
        // let psystem = new ParticleSystem(LineParticle, 100);
        // let emitter = new ParticleEmitter(psystem);
        // this.addChild(psystem);
        // this._camera.addChild(emitter);
        // emitter.emissionOverTime = 10;
        // emitter.emissionOverDist = 10;
        // emitter.minSpeed = 0;
        // emitter.maxSpeed = 0;
        // emitter.particleLife = 0.5;
        // emitter.particleLifeVar = 1;
        // emitter.radius = 10;
        // emitter.halfExtents = Vect.Create(20, 5)
        // emitter.emitterVelocityRatio = 1;
    }
    GameScene.BasicScene = function (game) {
        var r = new GameScene(game);
        r.generateObjects();
        return r;
    };
    Object.defineProperty(GameScene.prototype, "camera", {
        get: function () { return this._camera; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    GameScene.CreateDefault = function () {
        return new GameScene(null);
    };
    GameScene.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(this, function (x) { return [
            x._camera,
            x._player
        ]; });
    };
    // ---------------------------------------------------------------------------------------------
    GameScene.prototype.Initialize = function () {
        _super.prototype.Initialize.call(this);
        if (this._camera == null) {
            this._camera = new _internal__WEBPACK_IMPORTED_MODULE_0__.Camera();
            this._camera.zoomScalar = 2;
            this.addChild(this._camera);
        }
    };
    GameScene.prototype.generateObjects = function () {
        var terrain = new _internal__WEBPACK_IMPORTED_MODULE_0__.Terrain();
        var groundBottom = new _internal__WEBPACK_IMPORTED_MODULE_0__.AABBCollider();
        groundBottom.layer = _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain;
        groundBottom.halfExtents = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(150, 10);
        groundBottom.position.set(200, 250);
        groundBottom.CreateWireframe(0xFFFFFF, 1);
        var groundLeft = new _internal__WEBPACK_IMPORTED_MODULE_0__.AABBCollider();
        groundLeft.layer = _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain;
        groundLeft.halfExtents = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(10, 75);
        groundLeft.position.set(50, 175);
        groundLeft.CreateWireframe(0xFFFFFF, 1);
        var groundRight = new _internal__WEBPACK_IMPORTED_MODULE_0__.AABBCollider();
        groundRight.layer = _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain;
        groundRight.halfExtents = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(10, 75);
        groundRight.position.set(350, 175);
        groundRight.CreateWireframe(0xFFFFFF, 1);
        var groundTop = new _internal__WEBPACK_IMPORTED_MODULE_0__.AABBCollider();
        groundTop.layer = _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain;
        groundTop.halfExtents = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(150, 10);
        groundTop.position.set(200, 100);
        groundTop.CreateWireframe(0xFFFFFF, 1);
        var groundMid = new _internal__WEBPACK_IMPORTED_MODULE_0__.AABBCollider();
        groundMid.layer = _internal__WEBPACK_IMPORTED_MODULE_0__.CollisionLayer.terrain;
        groundMid.halfExtents = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(20, 20);
        groundMid.position.set(200, 175);
        groundMid.CreateWireframe(0xFFFFFF, 1);
        terrain.addChild(groundBottom);
        terrain.addChild(groundLeft);
        terrain.addChild(groundRight);
        terrain.addChild(groundTop);
        terrain.addChild(groundMid);
        var plr = new _internal__WEBPACK_IMPORTED_MODULE_0__.Player();
        plr.position.set(200);
        this._player = plr;
        var enemy = new _internal__WEBPACK_IMPORTED_MODULE_0__.Gunner();
        enemy.position.set(200, 125);
        this.addChild(terrain);
        this.addChild(plr);
        this.addChild(enemy);
    };
    GameScene.prototype.Draw = function (renderer) {
        this.InitializationCheck();
        this.onPreRender.Invoke();
        this.DrawTransformed(renderer, this._camera.GetViewMatrix());
    };
    return GameScene;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.Scene));

_a = GameScene;
(function () {
    _a.SetSerializableFields();
})();


/***/ }),

/***/ 3945:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IActivate": () => (/* binding */ IActivate)
/* harmony export */ });
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
var IActivate;
(function (IActivate) {
    /** Type checking discriminator for IActivate */
    function ImplementedIn(obj) {
        var r = obj;
        return (obj.isActivated !== undefined &&
            obj.SetActivated !== undefined);
    }
    IActivate.ImplementedIn = ImplementedIn;
})(IActivate || (IActivate = {}));


/***/ }),

/***/ 9960:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IAllied": () => (/* binding */ IAllied)
/* harmony export */ });
var IAllied;
(function (IAllied) {
    /** type checking discriminator fro IAllied interface */
    function ImplementedIn(obj) {
        if (obj == null)
            return false;
        var r = obj;
        return (r.allegiance !== undefined);
    }
    IAllied.ImplementedIn = ImplementedIn;
})(IAllied || (IAllied = {}));


/***/ }),

/***/ 4103:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ICollidable": () => (/* binding */ ICollidable)
/* harmony export */ });
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
var ICollidable;
(function (ICollidable) {
    /** Type checking discriminator for IActivate */
    function ImplementedIn(obj) {
        if (obj == null)
            return false;
        var r = obj;
        return (r.collider !== undefined);
    }
    ICollidable.ImplementedIn = ImplementedIn;
})(ICollidable || (ICollidable = {}));


/***/ }),

/***/ 8379:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Collision": () => (/* binding */ Collision)
/* harmony export */ });
var Collision;
(function (Collision) {
    /**
     * Create a collision with the specified data
     * @param a the primary collider involved in the collision
     * @param b the other collider involved in the collision
     * @param normal the normalized direciton vector that the 2-way collision force is directed in
     * @param point the centroid of the collision overlap area
     * @param penetration how much much the colliders are overlapping in the specified normal
     * 	direction
     */
    function Create(a, b, normal, point, penetration) {
        return {
            colliderA: a, colliderB: b,
            directionNormal: normal, collisionPoint: point,
            penetration: penetration
        };
    }
    Collision.Create = Create;
    /**
     * Recalculates a collision if either of the properties of the colliders have been modified
     * @param collision the collision to recalculate
     */
    function Recalculate(collision) {
        var r = collision.colliderA.GetCollision(collision.colliderB);
        collision.colliderA = r.colliderA;
        collision.colliderB = r.colliderB;
        collision.collisionPoint = r.collisionPoint;
        collision.directionNormal = r.directionNormal;
        collision.penetration = r.penetration;
    }
    Collision.Recalculate = Recalculate;
    /**
     * returns the collider involved in the collsision who is not the specified self
     * @param collision the collision to get the collider of
     * @param self the colldier that we do not want
     * @returns
     */
    function OtherCollider(collision, self) {
        if (collision.colliderA == self)
            return collision.colliderB;
        if (collision.colliderB == self)
            return collision.colliderA;
        return null;
    }
    Collision.OtherCollider = OtherCollider;
})(Collision || (Collision = {}));


/***/ }),

/***/ 4556:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IControllable": () => (/* binding */ IControllable)
/* harmony export */ });
var IControllable;
(function (IControllable) {
    var Action;
    (function (Action) {
        Action[Action["None"] = 0] = "None";
        Action[Action["XMove"] = 1] = "XMove";
        Action[Action["YMove"] = 2] = "YMove";
        Action[Action["AimRotate"] = 3] = "AimRotate";
        Action[Action["AimMagnitude"] = 4] = "AimMagnitude";
        Action[Action["Jump"] = 5] = "Jump";
        Action[Action["UseItemPrimary"] = 6] = "UseItemPrimary";
        Action[Action["UseItemAlternate"] = 7] = "UseItemAlternate";
        Action[Action["UseItemTertiary"] = 8] = "UseItemTertiary";
        Action[Action["AttackPrimary"] = 9] = "AttackPrimary";
        Action[Action["AttackAlternate"] = 10] = "AttackAlternate";
        Action[Action["AttackTertiary"] = 11] = "AttackTertiary";
        Action[Action["Defend"] = 12] = "Defend";
        Action[Action["InteractSend"] = 13] = "InteractSend";
        Action[Action["InteractReceive"] = 14] = "InteractReceive";
        Action[Action["Misc1"] = 15] = "Misc1";
        Action[Action["Misc2"] = 16] = "Misc2";
        Action[Action["Other"] = 17] = "Other";
    })(Action = IControllable.Action || (IControllable.Action = {}));
    var ActionMode;
    (function (ActionMode) {
        ActionMode[ActionMode["None"] = 0] = "None";
        ActionMode[ActionMode["Digital"] = 1] = "Digital";
        ActionMode[ActionMode["Analogue"] = 2] = "Analogue";
    })(ActionMode = IControllable.ActionMode || (IControllable.ActionMode = {}));
    /** type discriminator for IControllable */
    function ImplementedIn(obj) {
        if (obj == null)
            return false;
        var cont = obj;
        return (cont.CanDoAction !== undefined &&
            cont.DoAction !== undefined &&
            cont.GetActionType !== undefined);
    }
    IControllable.ImplementedIn = ImplementedIn;
})(IControllable || (IControllable = {}));


/***/ }),

/***/ 11:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IDisplayTintable": () => (/* binding */ IDisplayTintable)
/* harmony export */ });
var IDisplayTintable;
(function (IDisplayTintable) {
    /** type discriminator for IDisplayable */
    function ImplementedIn(obj) {
        if (obj == null)
            return;
        var r = obj;
        return (r.tint !== undefined);
    }
    IDisplayTintable.ImplementedIn = ImplementedIn;
})(IDisplayTintable || (IDisplayTintable = {}));


/***/ }),

/***/ 6818:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ISerializable": () => (/* binding */ ISerializable),
/* harmony export */   "Serializable": () => (/* binding */ Serializable)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

var ISerializable;
(function (ISerializable) {
    ISerializable.SFIELD_TYPE = "_serializedType";
    /**
     * Deserialize an object based on it's 'type' property in the scope of the specified game
     * @param obj the object to deserialized
     * @param gameRef the reference to the game who's type registry will be used
     */
    function GetDeserializedObject(obj) {
        if (obj == null)
            return null;
        if (obj[ISerializable.SFIELD_TYPE] === undefined)
            return null;
        if (obj[ISerializable.SFIELD_TYPE] === this.TYPE_CHILDREF) {
            throw ("Objects cannot be parsed as children without a reference to their sibling list," +
                "use SceneEntity.GetDeserializedField(...) instead");
        }
        // search for the serialized type
        var type = ISerializable.typeRegistry.get(obj[ISerializable.SFIELD_TYPE]);
        var ur = type.CreateDefault();
        ur.Deserialize(obj);
        return ur;
    }
    ISerializable.GetDeserializedObject = GetDeserializedObject;
    /**
     * Type discriminator for ISerializable type
     * @param obj the object to check and see if it is ISerializable
     */
    function ImplementedIn(obj) {
        if (obj == null)
            return false;
        var r = obj;
        return r instanceof _internal__WEBPACK_IMPORTED_MODULE_0__.SceneEntity || (r.GetSerializedObject !== undefined &&
            r.Deserialize !== undefined);
    }
    ISerializable.ImplementedIn = ImplementedIn;
    // ---------------------------------------------------------------------------------------------
    /** gets a safe serialized object from any reference type object if possible */
    function GetSerializedObject(obj) { return obj.GetSerializedObject(); }
    ISerializable.GetSerializedObject = GetSerializedObject;
    /**
     * serializes an object according to default serialization algorithm, it is meant for use in
     * ISerializable.GetSerializedObject specific class implementations and to be expanded upon if
     * needed
     * @param obj the ISerializable instance to serialize
     * @param doFields whether or not the fields of the object should be parsed (this should pretty
     * 	much always be true, only false when you're trying to implement a custom solution for the
     * 	field serialization)
     */
    function GetSerializedObject_DefaultImplementation(obj, doFields) {
        if (doFields === void 0) { doFields = true; }
        var type = Object.getPrototypeOf(obj).constructor;
        if (ISerializable.typeRegistry.get(type.name) == null) {
            throw "Type for specified object is not registered for serialization";
        }
        // define the return type and store the type data in it
        var r = {};
        r[ISerializable.SFIELD_TYPE] = type.name;
        // early return without serialized fields if specified
        if (!doFields)
            return r;
        // serialize fields based on fields marked as serializable
        var untypedObj = obj;
        var fields = ISerializable.serializedFields.get(type);
        fields.forEach(function (field) {
            var fieldData = untypedObj[field];
            if (fieldData != null)
                r[field] = GetSerializedField(fieldData);
        });
        return r;
    }
    ISerializable.GetSerializedObject_DefaultImplementation = GetSerializedObject_DefaultImplementation;
    /**
     * gets a safe serialized object from any type, including value types such as strings
     * and numbers
     */
    function GetSerializedField(data) {
        if (data instanceof Object) {
            // recursively serialize elements
            if (data instanceof Array) {
                var r = [];
                for (var i = 0; i < data.length; i++) {
                    r.push(GetSerializedField(data[i]));
                }
                return r;
            }
            // serialize it according to serialization technique if it implements ISerializable
            if (ImplementedIn(data))
                return data.GetSerializedObject();
            // recursively iterate through each field of an anonymous object type
            else {
                var r = {};
                for (var innerField in data) {
                    r[innerField] = GetSerializedField(data[innerField]);
                }
                return r;
            }
        }
        // basic math types to serialize
        // if(Vect.ImplementedIn(data)) return Vect.Create(data.x, data.y);
        // if(Rect.ImplementedIn(data)) return Rect.Create(Vect.Clone(data.position), Vect.Clone(data.size));
        return data;
    }
    ISerializable.GetSerializedField = GetSerializedField;
    /**
     * Deserialize serialized object data into a new object of the specified type
     * @param type the type to parse the serialized object as
     * @param data the serialized object
     * @param gameRef a reference to the game (unused - will be removed)
     */
    function DeserializeObject(type, data) {
        // create a new instance of specified type
        var t = ISerializable.typeRegistry.get(data[ISerializable.SFIELD_TYPE]);
        var target = t.CreateDefault();
        // deserialize the target as an instance of the specified type
        target.Deserialize(data);
        return target;
        // let fields = serializedFields.get(type);
        // if(fields == null) return target;
        // 
        // let untypedData = data as any;
        // fields.forEach(function(field){
        // 	
        // 	if(untypedData[SFIELD_TYPE] != null){
        // 		// TODO Deserialize by type
        // 	}
        //
        // 	else target[field as keyof T] = untypedData[field];
        // });
        //
        // return target;
    }
    ISerializable.DeserializeObject = DeserializeObject;
    /**
     * the default implementation for deserialization behavior, meant to be expanded upon for
     * individual serialization cases
     * @param target the target object to deserialize the data into
     * @param data the serialized data
     */
    function DeserializeObject_DefaultImplementation(target, data) {
        var untypedData = data;
        var serializedType = ISerializable.typeRegistry.get(untypedData[ISerializable.SFIELD_TYPE]);
        var targetType = Object.getPrototypeOf(target).constructor;
        // check to see if type signature on the serialized type is the same as the type specified
        var sType = untypedData[ISerializable.SFIELD_TYPE];
        if (sType != null) {
            if (serializedType != targetType)
                throw ("Type mismatch, specified type is " + targetType.name +
                    ", but serialized type is" + sType);
        }
        // if there is no type field, we can't really deserialize it
        else {
            throw "Type not specified in serialized data";
        }
        // get the serialized fields registered to the type
        var fields = ISerializable.serializedFields.get(targetType);
        // iterate through each field marked as serializable and grab the data from them
        fields.forEach(function (field) {
            target[field] = GetDeserializedField(untypedData[field]);
        });
        return;
    }
    ISerializable.DeserializeObject_DefaultImplementation = DeserializeObject_DefaultImplementation;
    /**
     * Deserialize a field from a serialized data object, can be any type, number, string, obj, etc
     * @param fieldData JSON parsed the data for the field
     */
    function GetDeserializedField(fieldData) {
        // if the field data is an Object type, it may be an array or ISerializable type
        if (fieldData instanceof Object) {
            // if the field is an array parse each individual entry
            if (fieldData instanceof Array) {
                var r = [];
                for (var i = 0; i < fieldData.length; i++) {
                    r.push(GetDeserializedField(fieldData[i]));
                }
                return r;
            }
            // if the field is an ISerializable object, deserialize it according to it's type
            var fieldTypeKey = fieldData[ISerializable.SFIELD_TYPE];
            if (fieldTypeKey !== undefined) {
                var fieldType = ISerializable.typeRegistry.get(fieldTypeKey);
                var r = DeserializeObject(fieldType, fieldData);
                // references should be handled differently somehow
                // if(r instanceof SceneReference){
                // 	// TODO
                // 	// push to some reference query on fieldTarget
                // 	throw "Not Implemented";
                // }
                return r;
            }
            // if the field is an anonymous object type, we need to check each inner field
            else {
                var r = {};
                for (var innerField in fieldData) {
                    var innerData = fieldData[innerField];
                    r[innerField] = GetDeserializedField(innerData);
                }
                return r;
            }
        }
        return fieldData;
    }
    ISerializable.GetDeserializedField = GetDeserializedField;
    // ---------------------------------------------------------------------------------------------
    /** holds meta data about which types the type names represent */
    ISerializable.typeRegistry = new Map();
    /** the meta data object that holds info about serialized fields for each serializable class */
    ISerializable.serializedFields = new Map();
    var keyProxy = new Proxy({}, { get: function (_, p) { return p; } });
    /**
     * returns true if the specified type is registered and marked as serializable
     * @param ctor the type to check for
     */
    function IsTypeRegistered(ctor) {
        return ISerializable.serializedFields.has(ctor);
    }
    ISerializable.IsTypeRegistered = IsTypeRegistered;
    /**
     * marks a certain field on the specified type to be serialized
     * @param ctor the constructor object of the specified type (the class object)
     * @param makeFields the proxy for the fields to make serializable (ie x=>[x.*fieldName*, ...])
     */
    function MakeSerializable(ctor, makeFields) {
        if (makeFields === void 0) { makeFields = function () { return []; }; }
        // register the type if it has not yet been registered
        var type = ISerializable.typeRegistry.get(ctor.name);
        if (type == null)
            ISerializable.typeRegistry.set(ctor.name, ctor);
        // get the set of fields that are already marked as serialized from the specified type
        var props = (function () {
            var res = ISerializable.serializedFields.get(ctor);
            if (!res) {
                res = new Set();
                ISerializable.serializedFields.set(ctor, res);
            }
            return res;
        })();
        // add the specified fields to the set to mark them as serialized
        var fields = makeFields(keyProxy);
        for (var _i = 0, fields_1 = fields; _i < fields_1.length; _i++) {
            var field = fields_1[_i];
            props.add(field);
        }
    }
    ISerializable.MakeSerializable = MakeSerializable;
    //	|// example on how to make serializable fields within a class
    //	|class Foo implements ISerializable {
    //	|	
    //	|	GetSerializedObject(): any { throw "not impl"; }
    //	|	Deserialize(data: any, gameRef: Game): void { throw "not impl"; }
    //	|	
    //	|	someProp: string = "";
    //	|	static SetSerializableFields(): void {
    //	|		MakeSerializable(this, x => [x.someProp]);
    //	|	}
    //	|	static {
    //	|		this.SetSerializableFields();
    //	|	}
    //	|}
    //	|class Bar extends Foo{
    //	|	anotherProp: string = "";
    //	|	thirdProp: string = "";
    //	|	static SetSerializableFields(): void {
    //	|		super.SetSerializableFields();
    //	|		MakeSerializable(this, x => [x.anotherProp, x.thirdProp]);
    //	|	}
    //	|	static{
    //	|		this.SetSerializableFields();
    //	|		// console.log(metaData.get(Bar)); // set of "someProp", "anotherProp", and "thirdProp"
    //	|	}
    //	|}
})(ISerializable || (ISerializable = {}));
var Serializable = /** @class */ (function () {
    function Serializable() {
    }
    Serializable.CreateDefault = function () {
        return new this();
    };
    Serializable.SetSerializableFields = function () {
        ISerializable.MakeSerializable(this);
    };
    // To mark as serializable, use:
    // static { this.SetSerializableFields(); }
    // ---------------------------------------------------------------------------------------------
    Serializable.prototype.GetSerializedObject = function () {
        return ISerializable.GetSerializedObject_DefaultImplementation(this);
    };
    Serializable.prototype.Deserialize = function (data) {
        ISerializable.DeserializeObject_DefaultImplementation(this, data);
    };
    return Serializable;
}());



/***/ }),

/***/ 2494:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);



/***/ }),

/***/ 2386:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ISolid": () => (/* binding */ ISolid)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

var ISolid;
(function (ISolid) {
    /** type checking discriminator for ISolid interface */
    function ImplementedIn(obj) {
        var r = obj;
        return (r.isSolid !== undefined &&
            r.materialFriction !== undefined &&
            r.materialRestitution !== undefined &&
            _internal__WEBPACK_IMPORTED_MODULE_0__.ICollidable.ImplementedIn(r));
    }
    ISolid.ImplementedIn = ImplementedIn;
    /**
     * discriminator and type checker just like ISolid.ImplementedIn, but only returns true if the
     * object also has 'isSolid' set to true
     */
    function ImplementedValue(obj) {
        if (obj == null)
            return false;
        var r = obj;
        return (ImplementedIn(r) &&
            r.isSolid);
    }
    ISolid.ImplementedValue = ImplementedValue;
})(ISolid || (ISolid = {}));


/***/ }),

/***/ 5928:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ITarget": () => (/* binding */ ITarget)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);

var ITarget;
(function (ITarget) {
    /** Type checking discriminator for ITarget */
    function ImplementedIn(obj) {
        if (obj == null)
            return false;
        var r = obj;
        return (r.Hit !== undefined &&
            _internal__WEBPACK_IMPORTED_MODULE_0__.ICollidable.ImplementedIn(obj));
    }
    ITarget.ImplementedIn = ImplementedIn;
})(ITarget || (ITarget = {}));


/***/ }),

/***/ 1529:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InputCondition": () => (/* binding */ InputCondition),
/* harmony export */   "InputType": () => (/* binding */ InputType),
/* harmony export */   "InputControl": () => (/* binding */ InputControl)
/* harmony export */ });
var InputCondition;
(function (InputCondition) {
    InputCondition[InputCondition["whileHeld"] = 0] = "whileHeld";
    InputCondition[InputCondition["onPress"] = 1] = "onPress";
    InputCondition[InputCondition["onRelease"] = 2] = "onRelease";
})(InputCondition || (InputCondition = {}));
var InputType;
(function (InputType) {
    InputType[InputType["keyboardButton"] = 0] = "keyboardButton";
    InputType[InputType["mouseButton"] = 1] = "mouseButton";
    InputType[InputType["gamepadButton"] = 2] = "gamepadButton";
    InputType[InputType["mouseAnalogueX"] = 3] = "mouseAnalogueX";
    InputType[InputType["mouseAnalogueY"] = 4] = "mouseAnalogueY";
    InputType[InputType["gamepadAnalogue"] = 5] = "gamepadAnalogue";
})(InputType || (InputType = {}));
(function (InputType) {
    function IsAnalogue(itype) {
        return itype > InputType.gamepadButton;
    }
    InputType.IsAnalogue = IsAnalogue;
})(InputType || (InputType = {}));
var InputControl = /** @class */ (function () {
    function InputControl() {
        this._triggerCondition = InputCondition.whileHeld;
        this._inputCode = 0;
        this._type = InputType.keyboardButton;
        this._isHeld = false;
        this._heldLastTick = false;
        this._inputValue = 0;
        this._lastMouseAnalogue = 0;
    }
    /**
     * Create an input control with the specified input
     * @param type what piece of hardware the input is coming from
     * @param code the id of the button on the hardware (ie keyCode if from keyboard)
     * @param condition controls when the control is triggered
     */
    InputControl.Create = function (type, code, condition) {
        var r = new InputControl();
        r._type = type;
        r._inputCode = code;
        r._triggerCondition = condition;
        return r;
    };
    InputControl.CreateAnalogue = function (type, code) {
        var r = new InputControl();
        r._type = type;
        r._inputCode = code;
        r._triggerCondition = InputCondition.whileHeld;
        return r;
    };
    Object.defineProperty(InputControl.prototype, "triggerCondition", {
        get: function () { return this._triggerCondition; },
        set: function (val) { this._triggerCondition = val; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(InputControl.prototype, "inputCode", {
        get: function () { return this._inputCode; },
        set: function (val) { this._inputCode = val; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(InputControl.prototype, "type", {
        get: function () { return this._type; },
        set: function (val) { this._type = val; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    /**
     * update the control to reflect thr current gamestate
     * @param game the game instance that this control is reporting input for
     */
    InputControl.prototype.Update = function (game) {
        var held = false;
        if (InputType.IsAnalogue(this._type)) {
            this.UpdateAnalogue(game);
        }
        // for digital inputs
        else {
            this._inputValue = 0;
            switch (this._type) {
                case InputType.keyboardButton:
                    held = game.keysPressed[this._inputCode];
                    this._inputValue = 1;
                    break;
                case InputType.mouseButton:
                    held = game.mousePressed;
                    this._inputValue = 1;
                    break;
                case InputType.gamepadButton:
                    // TODO
                    break;
            }
        }
        this._heldLastTick = this._isHeld;
        this._isHeld = held;
    };
    InputControl.prototype.UpdateAnalogue = function (game) {
        if (this._type == InputType.gamepadAnalogue) {
            // TODO
        }
        // if mouse
        else {
            var mouseval = 0;
            if (this._type == InputType.mouseAnalogueX) {
                mouseval = game.mousePos.x;
            }
            else if (this._type == InputType.mouseAnalogueY) {
                mouseval = game.mousePos.y;
            }
            var mouseDelta = mouseval - this._lastMouseAnalogue;
            this._inputValue += mouseDelta * 0.01;
            if (this._inputValue > 1)
                this._inputValue = 1;
            else if (this._inputValue < -1)
                this._inputValue = -1;
            this._lastMouseAnalogue = mouseval;
        }
    };
    /** determines whether or not the control has been triggered based on it's trigger condition */
    InputControl.prototype.IsTriggered = function () {
        switch (this._triggerCondition) {
            case InputCondition.whileHeld:
                return this._isHeld;
            case InputCondition.onPress:
                return (!this._heldLastTick) && this._isHeld;
            case InputCondition.onRelease:
                return this._heldLastTick && (!this._isHeld);
        }
        return false;
    };
    /** returns true if the input is currently held down */
    InputControl.prototype.IsHeld = function () {
        return this._isHeld;
    };
    /**
     * the input value from -1 to 1 for analogue inputs. For digital inputs it will be either 0
     * or 1 depending on if it's held or not
     */
    InputControl.prototype.GetValue = function () {
        return this._inputValue;
    };
    InputControl.prototype.SetValue = function (val) {
        this._inputValue = val;
    };
    return InputControl;
}());



/***/ }),

/***/ 1287:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Side": () => (/* binding */ Side),
/* harmony export */   "Maths": () => (/* binding */ Maths),
/* harmony export */   "Vect": () => (/* binding */ Vect),
/* harmony export */   "Matrix": () => (/* binding */ Matrix),
/* harmony export */   "Rect": () => (/* binding */ Rect)
/* harmony export */ });
var Side;
(function (Side) {
    Side[Side["none"] = 0] = "none";
    Side[Side["left"] = 1] = "left";
    Side[Side["right"] = 2] = "right";
    Side[Side["up"] = 4] = "up";
    Side[Side["top"] = 4] = "top";
    Side[Side["down"] = 8] = "down";
    Side[Side["bottom"] = 8] = "bottom";
    Side[Side["all"] = 15] = "all";
})(Side || (Side = {}));
(function (Side) {
    function RotateCW(side) {
        switch (side) {
            case Side.left: return Side.up;
            case Side.right: return Side.down;
            case Side.up: return Side.right;
            case Side.down: return Side.left;
        }
        return Side.none;
    }
    Side.RotateCW = RotateCW;
    function RotateCCW(side) {
        switch (side) {
            case Side.left: return Side.down;
            case Side.right: return Side.up;
            case Side.up: return Side.left;
            case Side.down: return Side.right;
        }
        return Side.none;
    }
    Side.RotateCCW = RotateCCW;
    function Opposite(side) {
        switch (side) {
            case Side.left: return Side.right;
            case Side.right: return Side.left;
            case Side.up: return Side.down;
            case Side.down: return Side.up;
        }
        return Side.none;
    }
    Side.Opposite = Opposite;
    function FromDirection(direction) {
        if (direction.x == 0 && direction.y == 0) {
            console.error("No direction from zero vector");
            return Side.none;
        }
        if (Math.abs(direction.x) > Math.abs(direction.y)) {
            return direction.x < 0 ? Side.left : Side.right;
        }
        else {
            return direction.y < 0 ? Side.up : Side.down;
        }
    }
    Side.FromDirection = FromDirection;
    function GetAngle(side) {
        switch (side) {
            case Side.left: return Math.PI;
            case Side.right: return 0;
            case Side.up: return -Math.PI / 2;
            case Side.down: return Math.PI / 2;
        }
        console.error("Invalid side for angle value");
    }
    Side.GetAngle = GetAngle;
    function GetVector(side) {
        switch (side) {
            case Side.left: return Vect.Create(-1, 0);
            case Side.right: return Vect.Create(1, 0);
            case Side.up: return Vect.Create(0, -1);
            case Side.down: return Vect.Create(0, 1);
        }
        return Vect.Create(0);
    }
    Side.GetVector = GetVector;
    function IsHorizontal(side) {
        return (side == Side.right ||
            side == Side.left);
    }
    Side.IsHorizontal = IsHorizontal;
})(Side || (Side = {}));
var Maths;
(function (Maths) {
    /**
     * calculate the modulus properly
     * @param num number to get the modulus of
     * @param mod the divisor
     */
    function Mod(num, mod) {
        var r = num % mod;
        if (r < 0)
            r += mod;
        return r;
    }
    Maths.Mod = Mod;
    /**
     * calculates the smallest signed distance between two angles in radians
     * @param target the target angle to subtract from the source
     * @param source the source angle to be subtracted from
     */
    function SignedAngleDif(target, source) {
        var twoPi = Math.PI * 2;
        var pi = Math.PI;
        var r = target - source;
        r = ((r + pi) % twoPi + twoPi) % twoPi;
        r -= pi;
        return r;
    }
    Maths.SignedAngleDif = SignedAngleDif;
    /**
     * interpolate between two numerical values
     * @param a number to lerp from
     * @param b number to lerp to
     * @param delta percentage from a to be value returned is
     */
    function LerpValue(a, b, delta) {
        var dif = b - a;
        return a + dif * delta;
    }
    Maths.LerpValue = LerpValue;
    /**
     * interpolate between two colors (where the colors are represented as numbers from hexadecimal
     * values - i.e. 0xFFFFFF is white, 0x00FF00, is green, etc)
     * @param a the color to lerp from
     * @param b the color to lerp to
     * @param delta how far from a toward b the result will be
     */
    function LerpColor(a, b, delta) {
        var MASK1 = 0xff00ff;
        var MASK2 = 0x00ff00;
        var f2 = Math.floor(256 * delta);
        var f1 = 256 - f2;
        return ((((((a & MASK1) * f1) + ((b & MASK1) * f2)) >> 8) & MASK1) |
            (((((a & MASK2) * f1) + ((b & MASK2) * f2)) >> 8) & MASK2));
    }
    Maths.LerpColor = LerpColor;
    /**
     * generates a psuedo-unique hash number from the specified string
     * @param str the string to generate a hash number from
     */
    function StringToHash(str) {
        var hash = 0;
        if (str.length == 0)
            return hash;
        for (var i = 0; i < str.length; i++) {
            var charCode = str.charCodeAt(i);
            hash = ((hash << 7) - hash) + charCode;
            hash = hash & hash;
        }
        return hash;
    }
    Maths.StringToHash = StringToHash;
})(Maths || (Maths = {}));
var Vect;
(function (Vect) {
    /**
     * create a vector with the specified components
     * @param x the x component of the vector
     * @param y the y component of the vector
     */
    function Create(x, y) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = x; }
        return {
            x: x,
            y: y
        };
    }
    Vect.Create = Create;
    function Clone(vect) {
        return {
            x: vect.x,
            y: vect.y
        };
    }
    Vect.Clone = Clone;
    /**
     * create a vector that points in the specified direction
     * @param direction the direcion the vector will point in
     * @param magnitude the length of the vector
     * @param vector the vector to store the value in, new vector created if not specified
     * @returns
     */
    function FromDirection(direction, magnitude, vector) {
        if (magnitude === void 0) { magnitude = 1; }
        if (vector === void 0) { vector = Create(); }
        vector.x = Math.cos(direction) * magnitude;
        vector.y = Math.sin(direction) * magnitude;
        return vector;
    }
    Vect.FromDirection = FromDirection;
    /**
     * interpolate from point a to b, returns the interpolated value
     * @param from the vector to interpolate from, vector A
     * @param to the vector to interpolate to, vector B
     * @param delta how much interpolation from A to B has elapsed; 0 for none, 1 for 100%
     */
    function Lerp(from, to, delta, vector) {
        if (vector === void 0) { vector = Create(); }
        var invDelta = 1 - delta;
        vector.x = from.x * invDelta + to.x * delta;
        vector.y = from.y * invDelta + to.y * delta;
        return vector;
    }
    Vect.Lerp = Lerp;
    /**
     * calculate the euclidean distance between two vectors
     * @param a the first vector
     * @param b the second vector
     */
    function Distance(a, b) {
        var dx = a.x - b.x;
        var dy = a.y - b.y;
        return Math.sqrt(dx * dx + dy * dy);
    }
    Vect.Distance = Distance;
    /**
     * returns the distance between two vectors, squared (more performant than calculating distance)
     * @param a the first vector
     * @param b the second vector
     * @returns
     */
    function DistanceSquared(a, b) {
        var dx = a.x - b.x;
        var dy = a.y - b.y;
        return dx * dx + dy * dy;
    }
    Vect.DistanceSquared = DistanceSquared;
    /** finds the midpoint of the specified vectors */
    function MidPoint() {
        var vects = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            vects[_i] = arguments[_i];
        }
        var total = Vect.Create();
        for (var i = vects.length - 1; i >= 0; i--) {
            total.x += vects[i].x;
            total.y += vects[i].y;
        }
        var recip = 1 / vects.length;
        total.x *= recip;
        total.y *= recip;
        return total;
    }
    Vect.MidPoint = MidPoint;
    /**
     * Add together two vectors
     */
    function Add(a, b, vector) {
        if (vector === void 0) { vector = Create(); }
        vector.x = a.x + b.x;
        vector.y = a.y + b.y;
        return vector;
    }
    Vect.Add = Add;
    /**
     * subtract b from a
     */
    function Subtract(a, b, vector) {
        if (vector === void 0) { vector = Create(); }
        vector.x = a.x - b.x;
        vector.y = a.y - b.y;
        return vector;
    }
    Vect.Subtract = Subtract;
    /**
     * multiply each component of a with each component of b
     */
    function Multiply(a, b, vector) {
        if (vector === void 0) { vector = Create(); }
        vector.x = a.x * b.x;
        vector.y = a.y * b.y;
        return vector;
    }
    Vect.Multiply = Multiply;
    /**
     * scale multuply each component a vector by a scalar
     */
    function MultiplyScalar(a, scalar, vector) {
        if (vector === void 0) { vector = Create(); }
        vector.x = a.x * scalar;
        vector.y = a.y * scalar;
        return vector;
    }
    Vect.MultiplyScalar = MultiplyScalar;
    function Magnitude(a) {
        return Math.sqrt(a.x * a.x + a.y * a.y);
    }
    Vect.Magnitude = Magnitude;
    function Equal(a, b, leniency) {
        if (leniency === void 0) { leniency = 0.001; }
        return (Math.abs(a.x - b.x) <= leniency &&
            Math.abs(a.y - b.y) <= leniency);
    }
    Vect.Equal = Equal;
    /**
     * Calculate the dot product between two vectors
     * @param a the first vector
     * @param b the second vector
     */
    function Dot(a, b) {
        return a.x * b.x + a.y * b.y;
    }
    Vect.Dot = Dot;
    /**
     * calculate the projection of the specified vector onto another vector
     * @param a the vector to project
     * @param target the vector that a will be projected onto
     */
    function Project(a, target, vector) {
        if (vector === void 0) { vector = Create(); }
        var dot = Dot(a, target);
        var quotient = Dot(target, target);
        var factor = dot / quotient;
        return MultiplyScalar(target, factor, vector);
    }
    Vect.Project = Project;
    function Direction(a) {
        return Math.atan2(a.y, a.x);
    }
    Vect.Direction = Direction;
    function Normalize(a, vector) {
        if (vector === void 0) { vector = Create(); }
        var mag = Magnitude(a);
        if (mag <= 0)
            return { x: 0, y: 0 };
        return MultiplyScalar(a, 1 / mag, vector);
    }
    Vect.Normalize = Normalize;
    function Rotate(a, rotation, vector) {
        if (vector === void 0) { vector = Create(); }
        var mag = Magnitude(a);
        var dir = Direction(a);
        dir += rotation;
        return FromDirection(dir, mag, vector);
    }
    Vect.Rotate = Rotate;
    function Invert(a, vector) {
        if (vector === void 0) { vector = Create(); }
        vector.x = -a.x;
        vector.y = -a.y;
        return vector;
    }
    Vect.Invert = Invert;
    /** Type checking discriminator for IVect */
    function ImplementedIn(obj) {
        if (!(obj instanceof Object))
            return false;
        return ("x" in obj && "y" in obj);
    }
    Vect.ImplementedIn = ImplementedIn;
})(Vect || (Vect = {}));
var Matrix;
(function (Matrix) {
    function GetTranslation(mat) {
        return { x: mat.tx, y: mat.ty };
    }
    Matrix.GetTranslation = GetTranslation;
    function GetScale(mat) {
        return {
            x: Math.sqrt(mat.a * mat.a + mat.c * mat.c),
            y: Math.sqrt(mat.b * mat.b + mat.d * mat.d)
        };
    }
    Matrix.GetScale = GetScale;
})(Matrix || (Matrix = {}));
var Rect;
(function (Rect) {
    function ImplementedIn(obj) {
        var r = obj;
        return (r.position != undefined &&
            r.size != undefined);
    }
    Rect.ImplementedIn = ImplementedIn;
    function Create(position, size) {
        if (position === void 0) { position = Vect.Create(); }
        if (size === void 0) { size = Vect.Create(); }
        return {
            position: position,
            size: size
        };
    }
    Rect.Create = Create;
    function Clone(rect) {
        return {
            position: Vect.Clone(rect.position),
            size: Vect.Clone(rect.size)
        };
    }
    Rect.Clone = Clone;
    function Center(rect) {
        return {
            x: rect.position.x + rect.size.x * 0.5,
            y: rect.position.y + rect.size.y * 0.5
        };
    }
    Rect.Center = Center;
    function Overlaps(a, b) {
        return !(a.position.x > b.position.x + b.size.x ||
            a.position.x + a.size.x < b.position.x ||
            a.position.y > b.position.y + b.size.y ||
            a.position.y + a.size.y < b.position.y);
    }
    Rect.Overlaps = Overlaps;
    /**
     * expands the rect a to fully overlap rect b, returns the modified rect a
     * @param a the rect to expand (this rect is modified)
     * @param b the rect that this rect should overlap
     */
    function ExpandToOverlap(a, b) {
        var minX = Math.min(a.position.x, b.position.x);
        var minY = Math.min(a.position.y, b.position.y);
        var maxX = Math.max(a.position.x + a.size.x, b.position.x + b.size.x);
        var maxY = Math.max(a.position.y + b.size.y, b.position.y + b.size.y);
        if (minX < a.position.x) {
            var dif = a.position.x - minX;
            a.position.x -= dif;
            a.size.x += dif;
        }
        if (minY < a.position.y) {
            var dif = a.position.y - minY;
            a.position.y -= dif;
            a.size.y += dif;
        }
        if (maxX > a.position.x + a.size.x) {
            a.size.x += maxX - (a.position.x + a.size.x);
        }
        if (maxY > a.position.y + a.size.y) {
            a.size.y += maxY - (a.position.y + a.size.y);
        }
        return a;
    }
    Rect.ExpandToOverlap = ExpandToOverlap;
    /** Return a rect that overlaps each specified rect */
    function OverlapRect(a, b) {
        var minX = a.position.x;
        var minY = a.position.y;
        var maxX = a.position.x + a.size.x;
        var maxY = a.position.y + a.size.y;
        minX = Math.max(minX, b.position.x);
        minY = Math.max(minY, b.position.y);
        maxX = Math.min(maxX, b.position.x + b.size.x);
        maxY = Math.min(maxY, b.position.y + b.size.y);
        return Rect.Create(Vect.Create(minX, minY), Vect.Create(maxX - minX, maxY - minY));
    }
    Rect.OverlapRect = OverlapRect;
    /**
     * Returns the side of the rect that is closest to the specified point
     * @param a the rect to check the sides of
     * @param b the point to compare the sides to
     */
    function ClosestSide(rect, point) {
        var aspect = rect.size.x / rect.size.y;
        var dif = Vect.Subtract(point, Rect.Center(rect));
        dif.x /= aspect;
        if (Math.abs(dif.x) > Math.abs(dif.y)) {
            return dif.x > 0 ? Side.right : Side.left;
        }
        return dif.y > 0 ? Side.down : Side.up;
    }
    Rect.ClosestSide = ClosestSide;
})(Rect || (Rect = {}));


/***/ }),

/***/ 8551:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ObjectController": () => (/* binding */ ObjectController),
/* harmony export */   "IdleController": () => (/* binding */ IdleController),
/* harmony export */   "EntityController": () => (/* binding */ EntityController),
/* harmony export */   "ActorController": () => (/* binding */ ActorController)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

var ObjectController = /** @class */ (function (_super) {
    __extends(ObjectController, _super);
    function ObjectController() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ObjectController;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.Serializable));

var IdleController = /** @class */ (function (_super) {
    __extends(IdleController, _super);
    function IdleController() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    IdleController.prototype.Update = function (deltTime, controlled) { };
    return IdleController;
}(ObjectController));

_a = IdleController;
(function () {
    _a.SetSerializableFields();
})();
var EntityController = /** @class */ (function (_super) {
    __extends(EntityController, _super);
    function EntityController() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    EntityController.prototype.Update = function (deltaTime, entity) {
        if (!(entity instanceof _internal__WEBPACK_IMPORTED_MODULE_0__.SceneEntity))
            throw "Invalid control target";
    };
    return EntityController;
}(ObjectController));

var ActorController = /** @class */ (function (_super) {
    __extends(ActorController, _super);
    function ActorController() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ActorController.prototype.Update = function (deltaTime, actor) {
        if (!(actor instanceof _internal__WEBPACK_IMPORTED_MODULE_0__.Actor))
            throw "Invalid control target";
    };
    return ActorController;
}(ObjectController));



/***/ }),

/***/ 2288:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Particle": () => (/* binding */ Particle),
/* harmony export */   "LineParticle": () => (/* binding */ LineParticle)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var Particle = /** @class */ (function () {
    function Particle() {
        this.lifetime = 0;
        this.maxLifetime = 1;
        this.velocity = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        this.fadeStart = 0.5;
    }
    Object.defineProperty(Particle.prototype, "displayObj", {
        get: function () { return this._displayObj; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Particle.prototype, "isAlive", {
        get: function () { return this.lifetime < this.maxLifetime; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Particle.prototype, "isRemoved", {
        get: function () { var _a; return ((_a = this._displayObj) === null || _a === void 0 ? void 0 : _a.parent) == null; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Particle.prototype, "deltaLifetime", {
        get: function () { return this.lifetime / this.maxLifetime; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Particle.prototype, "globalPosition", {
        get: function () { return _internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity.GetGlobalPosition(this._displayObj); },
        set: function (val) { _internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity.SetGlobalPosition(this._displayObj, val); },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    Particle.prototype.Update = function (deltaTime) {
        // do nothing if the particle is not alive
        if (!this.isAlive)
            return;
        var lifedelta = this.deltaLifetime;
        this.HandelVelocity(deltaTime);
        this.HandleFade(lifedelta);
        this.lifetime += deltaTime;
    };
    Particle.prototype.HandleFade = function (lifedelta) {
        var fadeStart = this.maxLifetime * this.fadeStart;
        var fadeDelta = lifedelta - fadeStart;
        if (fadeDelta < 0) {
            fadeDelta = 0;
        }
        else {
            var fadeTime = this.maxLifetime - fadeStart;
            fadeDelta /= fadeTime;
            if (fadeDelta > 1)
                fadeDelta = 1;
        }
        this._displayObj.alpha = 1 - fadeDelta;
    };
    Particle.prototype.HandelVelocity = function (deltaTime) {
        // apply velocity
        var opos = this._displayObj.position;
        this._displayObj.position.set(opos.x + this.velocity.x * deltaTime, opos.y + this.velocity.y * deltaTime);
    };
    /**
     * removes the particle's display object from the scene - note this does not destroy the display
     * object, so it may cause memory leaks if used improperly. You can manually call
     * this._displayObj.destroy() if calling this function outside it's intended scope
     */
    Particle.prototype.Remove = function () {
        var _a;
        if (this._displayObj == null)
            return;
        (_a = this._displayObj.parent) === null || _a === void 0 ? void 0 : _a.removeChild(this._displayObj);
    };
    /** resets the particle so it is ready to be re-used as if it were new */
    Particle.prototype.Reset = function () {
        this.lifetime = 0;
        this._displayObj.position.set(0, 0);
        this._displayObj.rotation = 0;
        this._displayObj.scale.set(1, 1);
        this._displayObj.alpha = 1;
        this._displayObj.tint = 0xFFFFFF;
    };
    Particle.DEFAULT_SHEET = new _internal__WEBPACK_IMPORTED_MODULE_1__.SpriteSheet("./dist/assets/graphics/Pixel.png");
    return Particle;
}());

var LineParticle = /** @class */ (function (_super) {
    __extends(LineParticle, _super);
    function LineParticle() {
        var _this = _super.call(this) || this;
        _this._lastPos = null;
        _this.lineWidth = 2;
        _this.minLength = 2;
        _this._displayObj = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Sprite(Particle.DEFAULT_SHEET.sprites[0]);
        return _this;
    }
    Object.defineProperty(LineParticle.prototype, "displayObj", {
        get: function () { return this._displayObj; },
        enumerable: false,
        configurable: true
    });
    LineParticle.prototype.Update = function (deltaTime) {
        if (this._lastPos == null)
            this._lastPos = this.globalPosition;
        _super.prototype.Update.call(this, deltaTime);
        var curPos = this.globalPosition;
        var dif = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Subtract(curPos, this._lastPos);
        var difMag = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Magnitude(dif);
        var difDir = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Direction(dif);
        var fx = this.displayObj;
        fx.scale.set(Math.max(this.minLength, difMag), this.lineWidth);
        fx.rotation = difDir;
        this._lastPos = curPos;
    };
    LineParticle.prototype.Reset = function () {
        _super.prototype.Reset.call(this);
        this._lastPos = null;
        this.lineWidth = 1;
    };
    return LineParticle;
}(Particle));



/***/ }),

/***/ 9745:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ParticleEmitter": () => (/* binding */ ParticleEmitter)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;

var ParticleEmitter = /** @class */ (function (_super) {
    __extends(ParticleEmitter, _super);
    function ParticleEmitter(particleSystem) {
        var _this = _super.call(this) || this;
        _this._particleSystem = null;
        _this._emissionDelta = 0;
        _this._lastPos = null;
        _this.emitterVelocityRatio = 0;
        _this.emissionOverTime = 10;
        _this.emissionOverDist = 0;
        _this.minSpeed = 0;
        _this.maxSpeed = 100;
        _this.startVelocityMin = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create();
        _this.startVelocityMax = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create();
        _this.minParticleLife = 0.5;
        _this.maxParticleLife = 1;
        _this.colorA = 0xFF0000;
        _this.colorB = 0xFFFF00;
        _this.halfExtents = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create();
        _this.radius = 0;
        _this._particleSystem = particleSystem;
        return _this;
    }
    Object.defineProperty(ParticleEmitter.prototype, "particleSystem", {
        get: function () { return this._particleSystem; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ParticleEmitter.prototype, "speed", {
        // ---------------------------------------------------------------------------------------------
        /** midpoint between min and max speed */
        get: function () { return (this.minSpeed + this.maxSpeed) * 0.5; },
        set: function (val) {
            var off = this.speedVar * 0.5;
            this.minSpeed = val - off;
            this.maxSpeed = val + off;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ParticleEmitter.prototype, "speedVar", {
        /** difference of min and max speed */
        get: function () { return this.maxSpeed - this.minSpeed; },
        set: function (val) {
            var spd = this.speed;
            var halfVal = val * 0.5;
            this.minSpeed = spd - halfVal;
            this.maxSpeed = spd + halfVal;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ParticleEmitter.prototype, "startVelocity", {
        /**
         * The starting velocity that each particle will have. Also a convenient getter and setter for
         * the midpoint between velocity offset min and max
         */
        get: function () {
            return _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Lerp(this.startVelocityMax, this.startVelocityMin, 0.5);
        },
        set: function (val) {
            var voDif = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(val, this.startVelocity);
            this.startVelocityMin.x += voDif.x;
            this.startVelocityMin.y += voDif.y;
            this.startVelocityMax.x += voDif.x;
            this.startVelocityMax.y += voDif.y;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ParticleEmitter.prototype, "startVelocityVar", {
        /** the amount of variation between the min and max start velocity */
        get: function () {
            return _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(this.startVelocityMax, this.startVelocityMin);
        },
        set: function (val) {
            var vel = this.startVelocity;
            var hval = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.MultiplyScalar(val, 0.5);
            this.startVelocityMin.x = vel.x - hval.x;
            this.startVelocityMin.y = vel.y - hval.y;
            this.startVelocityMax.x = vel.x + hval.x;
            this.startVelocityMax.y = vel.y + hval.y;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ParticleEmitter.prototype, "particleLife", {
        /** midpoint between min and max life */
        get: function () { return (this.minParticleLife + this.maxParticleLife) * 0.5; },
        set: function (val) {
            var off = this.particleLifeVar * 0.5;
            this.minParticleLife = val - off;
            this.maxParticleLife = val + off;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ParticleEmitter.prototype, "particleLifeVar", {
        /** difference between min and max life */
        get: function () { return this.maxParticleLife - this.minParticleLife; },
        set: function (val) {
            var lf = this.particleLife;
            var halfVal = val * 0.5;
            this.minParticleLife = lf - halfVal;
            this.maxParticleLife = lf + halfVal;
        },
        enumerable: false,
        configurable: true
    });
    /// --------------------------------------------------------------------------------------------
    ParticleEmitter.CreateDefault = function () {
        return new ParticleEmitter(null);
    };
    // ---------------------------------------------------------------------------------------------
    ParticleEmitter.prototype.Update = function (deltaTime) {
        _super.prototype.Update.call(this, deltaTime);
        if (this._lastPos == null)
            this._lastPos = this.globalPosition;
        this.HandleEmission(deltaTime);
    };
    ParticleEmitter.prototype.HandleEmission = function (deltaTime) {
        // calculate distance moved fro last position
        var gpos = this.globalPosition;
        var dist = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Distance(this._lastPos, gpos);
        // increase emission amount by emission parameters
        this._emissionDelta += this.emissionOverTime * deltaTime;
        this._emissionDelta += this.emissionOverDist * dist;
        // get particle amount to emit
        var count = Math.floor(this._emissionDelta);
        this._emissionDelta -= count;
        // emit the particles
        if (count > 0)
            this.EmitAt(this._lastPos, gpos, count, deltaTime);
        this._lastPos = gpos;
    };
    /**
     * emits the specified amount of particles from the emitter as if it were currently at the
     * specified position with the specified velocity
     * @param posA the position of the emitter
     * @param posB where the emitter was at the previous frame
     * @param count the amount of particles to emit along the path from a to b
     * @param emitterVel the velocity of the emitter as it emits these particles
     */
    ParticleEmitter.prototype.EmitAt = function (posA, posB, count, deltaTime) {
        // warn if particle system is gone
        if (this._particleSystem.removed) {
            console.error("Attempting to add particles to a system that no longer exists");
            return;
        }
        // get the particles from the particle system
        var parts = this._particleSystem.GetParticles(count);
        // shortcut for min/max velocity offset
        var voMin = this.startVelocityMin;
        var voMax = this.startVelocityMax;
        // use emitter velocity if necessary
        var evx = null;
        var evy = null;
        if (this.emitterVelocityRatio > 0) {
            var dpos = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(posB, posA);
            evx = dpos.x / deltaTime * this.emitterVelocityRatio;
            evy = dpos.y / deltaTime * this.emitterVelocityRatio;
        }
        // iterate through each particle and set their properties
        for (var i = parts.length - 1; i >= 0; i--) {
            // calculate some values used on the particle
            var tspdFactor = Math.random();
            var tspd = _internal__WEBPACK_IMPORTED_MODULE_0__.Maths.LerpValue(this.minSpeed, this.maxSpeed, tspdFactor);
            var tvel = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.FromDirection(Math.PI * 2 * Math.random(), 1);
            var tpos = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Add(_internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Lerp(posA, posB, Math.random()), _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.MultiplyScalar(tvel, tspdFactor * this.radius));
            var toff = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(_internal__WEBPACK_IMPORTED_MODULE_0__.Maths.LerpValue(-this.halfExtents.x, this.halfExtents.x, Math.random()), _internal__WEBPACK_IMPORTED_MODULE_0__.Maths.LerpValue(-this.halfExtents.y, this.halfExtents.y, Math.random()));
            tpos.x += toff.x;
            tpos.y += toff.y;
            tvel.x *= tspd;
            tvel.y *= tspd;
            tvel.x += _internal__WEBPACK_IMPORTED_MODULE_0__.Maths.LerpValue(voMin.x, voMax.x, Math.random());
            tvel.y += _internal__WEBPACK_IMPORTED_MODULE_0__.Maths.LerpValue(voMin.y, voMax.y, Math.random());
            var tcol = _internal__WEBPACK_IMPORTED_MODULE_0__.Maths.LerpColor(this.colorA, this.colorB, Math.random());
            // add emitter velocity if necessary
            if (evx !== null) {
                tvel.x += evx;
                tvel.y += evy;
            }
            // set it's properties
            var part = parts[i];
            part.maxLifetime = _internal__WEBPACK_IMPORTED_MODULE_0__.Maths.LerpValue(this.minParticleLife, this.maxParticleLife, Math.random());
            part.globalPosition = tpos;
            part.velocity = tvel;
            part.displayObj.tint = tcol;
        }
    };
    return ParticleEmitter;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.SceneEntity));

_a = ParticleEmitter;
(function () {
    _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(_a, function (x) { return [
        x._particleSystem,
        x.emitterVelocityRatio,
        x.emissionOverTime,
        x.emissionOverDist,
        x.minSpeed,
        x.maxSpeed,
        x.startVelocityMin,
        x.startVelocityMax,
        x.minParticleLife,
        x.maxParticleLife,
        x.colorA,
        x.colorB,
        x.halfExtents,
        x.radius
    ]; });
})();


/***/ }),

/***/ 3781:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ParticlePool": () => (/* binding */ ParticlePool)
/* harmony export */ });
/**
 * an object used to recycle and reuse particle instances instead of constantly creating new ones
 */
var ParticlePool = /** @class */ (function () {
    /**
     * create a new particle pool for the specified type of particle
     */
    function ParticlePool(particleType, count) {
        this._particleType = null;
        this._unusedParticles = new Array();
        this._particleType = particleType;
        this.ReserveParticles(count);
    }
    // ---------------------------------------------------------------------------------------------
    /**
     * create the specified amount of particles and store them without adding them to the scene
     * @param count the amount of particles that will be created
     */
    ParticlePool.prototype.ReserveParticles = function (count) {
        for (var i = count - 1; i >= 0; i--) {
            var part = new this._particleType();
            this._unusedParticles.push(part);
        }
    };
    /** gets an unused particle from the pool and returns it */
    ParticlePool.prototype.GetParticle = function (container) {
        var r = this._unusedParticles.length > 0 ?
            this._unusedParticles.pop() :
            new this._particleType();
        r.Reset();
        container.addChild(r.displayObj);
        return r;
    };
    /** removes a particle from the scene, puts it in the pool and reserves it for later use */
    ParticlePool.prototype.PoolParticle = function (particle) {
        this._unusedParticles.push(particle);
        particle.Remove();
    };
    /** dispose of all the particles in the pool from memory (?) */
    ParticlePool.prototype.Dispose = function () {
        for (var i = this._unusedParticles.length - 1; i >= 0; i--) {
            this._unusedParticles[i].displayObj.destroy();
        }
        this._unusedParticles = null;
    };
    return ParticlePool;
}());



/***/ }),

/***/ 6026:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ParticleSystem": () => (/* binding */ ParticleSystem)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var ParticleSystem = /** @class */ (function (_super) {
    __extends(ParticleSystem, _super);
    /**
     * create a particle system of the specified particle type
     * @param particleType the particle type
     * @param count the amount of particles to initialize the particle pool with
     */
    function ParticleSystem(particleType, count) {
        if (count === void 0) { count = 0; }
        var _this = _super.call(this) || this;
        _this._container = null;
        _this._pool = null;
        _this._particles = new Array();
        _this.removeOnEmpty = false;
        _this._container = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.ParticleContainer();
        _this._pool = new _internal__WEBPACK_IMPORTED_MODULE_1__.ParticlePool(particleType, count);
        return _this;
    }
    Object.defineProperty(ParticleSystem.prototype, "container", {
        /** the particle container that the particles will be added to */
        get: function () { return this._container; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ParticleSystem.prototype, "pool", {
        get: function () { return this._pool; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ParticleSystem.prototype, "particles", {
        /** an array of particles created from the particle system that are currently in the scene */
        get: function () { return this._particles; },
        enumerable: false,
        configurable: true
    });
    ParticleSystem.prototype.OnAddedToScene = function (scene) {
        _super.prototype.OnAddedToScene.call(this, scene);
        scene.particleSystems.push(this);
    };
    ParticleSystem.prototype.RemoveFromScene = function () {
        // iterate through all particles and remove them from the scene
        for (var i = this._particles.length - 1; i >= 0; i--) {
            var part = this._particles[i];
            this._pool.PoolParticle(part);
        }
        // remove the particle refs from the particles array
        this._particles.splice(0, this._particles.length);
        // free memory allocated for this particle system (?)
        this._pool.Dispose();
        this._container.destroy();
        // remove particle system from parent scene
        var ind = this.parentScene.particleSystems.indexOf(this);
        if (ind >= 0)
            this.parentScene.particleSystems.splice(ind, 1);
        _super.prototype.RemoveFromScene.call(this);
    };
    // ---------------------------------------------------------------------------------------------
    ParticleSystem.prototype.Update = function (deltaTime) {
        _super.prototype.Update.call(this, deltaTime);
        // remove particle system from scene if necessary
        if (this._particles.length <= 0) {
            if (this.removeOnEmpty)
                this.RemoveFromScene();
        }
        // iterate through all particles and update them
        else {
            for (var i = this._particles.length - 1; i >= 0; i--) {
                var part = this._particles[i];
                part.Update(deltaTime);
                if (!part.isAlive) {
                    this._pool.PoolParticle(part);
                    this._particles.splice(i, 1);
                }
            }
        }
    };
    /** creates an array of particles */
    ParticleSystem.prototype.GetParticles = function (count) {
        var r = new Array();
        for (var i = count - 1; i >= 0; i--) {
            var part = this._pool.GetParticle(this._container);
            this._particles.push(part);
            r.push(part);
        }
        return r;
    };
    return ParticleSystem;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity));



/***/ }),

/***/ 8267:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PhysicsEntity": () => (/* binding */ PhysicsEntity)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

var PhysicsEntity = /** @class */ (function (_super) {
    __extends(PhysicsEntity, _super);
    function PhysicsEntity() {
        var _this = _super.call(this) || this;
        _this._collider = null;
        _this._velocity = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create();
        _this._rotationalVelocity = 0;
        _this._mass = 1;
        _this._inertia = null;
        return _this;
    }
    Object.defineProperty(PhysicsEntity.prototype, "collider", {
        get: function () { return this._collider; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PhysicsEntity.prototype, "velocity", {
        get: function () { return this._velocity; },
        set: function (val) {
            this._velocity.x = val.x;
            this._velocity.y = val.y;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PhysicsEntity.prototype, "rotationalVelocity", {
        /** be careful with this, not all collision shapes support rotation */
        get: function () { return this._rotationalVelocity; },
        set: function (val) { this._rotationalVelocity = val; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PhysicsEntity.prototype, "mass", {
        get: function () { return this._mass; },
        set: function (val) {
            this._mass = val;
            if (val > 0)
                this._inertia = 1 / this.mass;
            else
                this._inertia = Number.POSITIVE_INFINITY;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PhysicsEntity.prototype, "inertia", {
        get: function () {
            if (this._inertia == null)
                this.mass = this.mass;
            return this._inertia;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(PhysicsEntity.prototype, "momentum", {
        get: function () {
            return _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.MultiplyScalar(this.velocity, this.mass);
        },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    PhysicsEntity.CreateDefault = function () { return new this(); };
    PhysicsEntity.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(this, function (x) { return [
            x._velocity,
            x._rotationalVelocity,
            x._mass,
            x._collider
        ]; });
    };
    // ---------------------------------------------------------------------------------------------
    PhysicsEntity.prototype.Update = function (deltaTime) {
        this.Simulate(deltaTime);
        _super.prototype.Update.call(this, deltaTime);
    };
    /**
     * accelerate the object toward a specified velocity
     * @param velocity the velocity to approach
     * @param maxAccel the maximum magnitude of acceleration to apply toward the velocity
     */
    PhysicsEntity.prototype.AccelerateToVelocity = function (vel, maxAccel) {
        // if target velocity is reachable in this step, set velocity to target velocity
        var dif = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(vel, this.velocity);
        if (_internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Magnitude(dif) < maxAccel) {
            this._velocity.x = vel.x;
            this._velocity.y = vel.y;
            return;
        }
        // apply maximum possible acceleration if velocity is not reachable
        var acc = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.MultiplyScalar(_internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Normalize(dif), maxAccel);
        this._velocity.x += acc.x;
        this._velocity.y += acc.y;
    };
    /**
     * applies an impulse to to the physics object
     * @param impulse the impulse to apply
     */
    PhysicsEntity.prototype.ApplyImpulse = function (impulse) {
        var acc = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.MultiplyScalar(impulse, this._inertia);
        this._velocity = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Add(this._velocity, acc);
    };
    /**
     * applies a force to the object over a specified time
     * @param force the force to apply
     * @param deltaTime the amount of time to apply the force over (note, the cumulative force is
     * 	applied instantly, even if a long deltaTime is specified)
     */
    PhysicsEntity.prototype.ApplyForce = function (force, deltaTime) {
        var acc = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.MultiplyScalar(force, this.inertia * deltaTime);
        this._velocity = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Add(this._velocity, acc);
    };
    PhysicsEntity.prototype.ApplyTorqueImpulse = function (torque) {
        var rotAcc = torque * this.inertia;
        this._rotationalVelocity += rotAcc;
    };
    PhysicsEntity.prototype.ApplyTorque = function (torque, deltaTime) {
        var rotAcc = torque * this.inertia * deltaTime;
        this._rotationalVelocity += rotAcc;
    };
    PhysicsEntity.prototype.ResolveSolidCollision = function (collision) {
        var norm = collision.directionNormal;
        if (this._collider == collision.colliderB) {
            norm = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Invert(norm);
        }
        // add velocity against collision normal
        var dot = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Dot(this._velocity, norm);
        if (dot < 0) {
            var dVel = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Project(this._velocity, norm);
            dVel = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Invert(dVel);
            this._velocity = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Add(this._velocity, dVel);
        }
        // modify position to move entity outside of collider
        var pos = this.globalPosition;
        this.position.set(pos.x + norm.x * collision.penetration, pos.y + norm.y * collision.penetration);
    };
    PhysicsEntity.prototype.Simulate = function (deltaTime) {
        // translate position by velocity applied over delta time
        var pos = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Clone(this.globalPosition);
        pos = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Add(pos, _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.MultiplyScalar(this._velocity, deltaTime));
        // rotate the object by it's rotational velocity
        this.rotation += this._rotationalVelocity * deltaTime;
        // applying the new position
        this.globalPosition = pos;
    };
    return PhysicsEntity;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.SceneEntity));

_a = PhysicsEntity;
(function () {
    _a.SetSerializableFields();
})();


/***/ }),

/***/ 116:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Ray": () => (/* binding */ Ray)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

var Ray = /** @class */ (function (_super) {
    __extends(Ray, _super);
    function Ray() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._rayStart = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create();
        _this._rayEnd = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create();
        return _this;
    }
    Ray.CastAgainstRect = function (ray, rect) {
        var rectMin = rect.position;
        var rectMax = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Add(rectMin, rect.size);
        var rayOrigin = ray.rayStart;
        var dirFrac = ray.directionFrac;
        var t1 = (rectMin.x - rayOrigin.x) * dirFrac.x; // left side
        var t2 = (rectMax.x - rayOrigin.x) * dirFrac.x; // right side
        var t3 = (rectMin.y - rayOrigin.y) * dirFrac.y; // top side
        var t4 = (rectMax.y - rayOrigin.y) * dirFrac.y; // bottom side
        // the distance from entry point to ray origin
        var tmin = Math.max(Math.min(t1, t2), Math.min(t3, t4));
        // no hit
        if (tmin > ray.magnitude || tmin < 0) {
            return null;
        }
        // the distance from exit point to ray origin
        var tmax = Math.min(Math.max(t1, t2), Math.max(t3, t4));
        // no hit
        if (tmin > tmax) {
            return null;
        }
        var entryPoint = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Add(rayOrigin, _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.MultiplyScalar(ray.directionNormal, tmin));
        var exitPoint = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Add(rayOrigin, _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.MultiplyScalar(ray.directionNormal, tmax));
        var didEnter = tmin >= 0 && tmin <= ray.magnitude;
        var didExit = tmax >= 0 && tmax <= ray.magnitude;
        // calculate the penetration based on which parts of the ray entered and exited the collider
        var penetration = 0;
        if (!didExit && !didEnter) {
            penetration = ray.magnitude;
        }
        else if (!didExit) {
            penetration = ray.magnitude - tmin;
        }
        else if (!didEnter) {
            penetration = tmax;
        }
        else {
            penetration = tmax - tmin;
        }
        // calculate normals by seeing which side of the AABB collider the ray hit
        var entryNorm = null;
        switch (tmin) {
            case t1:
                entryNorm = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(-1, 0);
                break;
            case t2:
                entryNorm = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(1, 0);
                break;
            case t3:
                entryNorm = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(0, -1);
                break;
            case t4:
                entryNorm = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(0, 1);
                break;
        }
        var exitNorm = null;
        switch (tmax) {
            case t1:
                exitNorm = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(-1, 0);
                break;
            case t2:
                exitNorm = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(1, 0);
                break;
            case t3:
                exitNorm = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(0, -1);
                break;
            case t4:
                exitNorm = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(0, 1);
                break;
        }
        // construct raycast result object and return it
        var r = _internal__WEBPACK_IMPORTED_MODULE_0__.RaycastResult.PrecalculatedRaycastResult(ray, null, didEnter, didExit, entryPoint, exitPoint, entryNorm, exitNorm, penetration);
        return r;
    };
    // TODO test this function
    Ray.CastAgainstCircle = function (ray, circlePos, circleRadius) {
        // offset the line to be relative to the circle
        var rayStart = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(ray.rayStart, circlePos);
        var rayEnd = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(ray.rayEnd, circlePos);
        // calculate the dot products between the origin and target vectors
        var aoa = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Dot(rayStart, rayStart);
        var aob = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Dot(rayStart, rayEnd);
        var bob = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Dot(rayEnd, rayEnd);
        // calculate the ratios used to find the determinant and the t-values
        var qa = aoa - 2.0 * aob + bob;
        var qb = -2.0 * aoa + 2.0 * aob;
        var qc = aoa - circleRadius * circleRadius;
        // caclulate the determinant used to determine if there was an intersection or not
        var determinant = qb * qb - 4.0 * qa * qc;
        // if the determinant is negative, the ray completely misses
        if (determinant < 0) {
            return null;
        }
        // compute the nearest of the two t values
        var tmin = (-qb - Math.sqrt(determinant)) / (2.0 * qa);
        // compute the furthest of the t values
        var tmax = (-qb + Math.sqrt(determinant)) / (2.0 * qa);
        // calculate raycast results from tmin and tmax
        var entered = tmin >= 0 && tmin <= 1;
        var exited = tmax >= 0 && tmin <= 1;
        var entryPoint = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Lerp(ray.rayStart, ray.rayEnd, tmin);
        var exitPoint = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Lerp(ray.rayStart, ray.rayEnd, tmax);
        var entryNorm = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Normalize(_internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(entryPoint, circlePos));
        var exitNorm = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Normalize(_internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(exitPoint, circlePos));
        // calculate penetration based on where and whether the ray enters and exits the circle
        var penetration = 0;
        if (!entered && !exited) {
            penetration = ray.magnitude;
        }
        else if (!exited) {
            penetration = ray.magnitude - (tmin * ray.magnitude);
        }
        else if (!entered) {
            penetration = tmax * ray.magnitude;
        }
        else {
            penetration = (tmax - tmax) * ray.magnitude;
        }
        // create and return the raycast result
        var r = _internal__WEBPACK_IMPORTED_MODULE_0__.RaycastResult.PrecalculatedRaycastResult(ray, null, entered, exited, entryPoint, exitPoint, entryNorm, exitNorm, penetration);
        return r;
    };
    /**
     * Create a ray object from the specified data
     * @param start the starting point of the ray
     * @param directionNormal the normal direction that the ray is facing (MUST be normalized)
     * @param length the length of the ray from start to end
     */
    Ray.Create = function (start, directionNormal, length) {
        var r = new Ray();
        r._rayStart = start;
        r._rayEnd = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Add(start, _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.MultiplyScalar(directionNormal, length));
        r._directionNormal = directionNormal;
        r._magnitude = length;
        return r;
    };
    /**
     * Create a ray object from the specified start and end points
     * @param start the start of the ray
     * @param end the end of the ray
     */
    Ray.FromPoints = function (start, end) {
        var r = new Ray();
        r._rayStart = start;
        r._rayEnd = end;
        return r;
    };
    Object.defineProperty(Ray.prototype, "rayStart", {
        get: function () { return this._rayStart; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Ray.prototype, "rayEnd", {
        get: function () { return this._rayEnd; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Ray.prototype, "difference", {
        /** the vector from the start of the ray to the end of the ray */
        get: function () { var _b; return (_b = this._difference) !== null && _b !== void 0 ? _b : (this._difference = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Subtract(this._rayEnd, this._rayStart)); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Ray.prototype, "magnitude", {
        /** the length of the ray from start to end */
        get: function () { var _b; return (_b = this._magnitude) !== null && _b !== void 0 ? _b : (this._magnitude = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Magnitude(this.difference)); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Ray.prototype, "directionNormal", {
        /** the normalized vector that points in the same direction as the ray */
        get: function () { var _b; return (_b = this._directionNormal) !== null && _b !== void 0 ? _b : (this._directionNormal = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Normalize(this.difference)); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Ray.prototype, "directionFrac", {
        /** the multplicative inverse of the direction normal */
        get: function () {
            var _b;
            return (_b = this._directionFrac) !== null && _b !== void 0 ? _b : (this._directionFrac = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(1 / this.directionNormal.x, 1 / this.directionNormal.y));
        },
        enumerable: false,
        configurable: true
    });
    /// --------------------------------------------------------------------------------------------
    Ray.CreateDefault = function () { return new this(); };
    return Ray;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.Serializable));

_a = Ray;
(function () {
    _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(_a, function (x) { return [
        x._rayStart,
        x._rayEnd
    ]; });
})();


/***/ }),

/***/ 2930:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RaycastResult": () => (/* binding */ RaycastResult)
/* harmony export */ });
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
var RaycastResult;
(function (RaycastResult) {
    /**
     * Create a raycast result from the specified data
     * @param entered
     * @param exited
     * @param entryPoint
     * @param exitPoint
     * @param entryNorm
     * @param exitNorm
     * @param penetration
     */
    function PrecalculatedRaycastResult(ray, collider, entered, exited, entryPoint, exitPoint, entryNorm, exitNorm, penetration) {
        var r = {
            ray: ray,
            collider: collider,
            didEnter: entered,
            didExit: exited,
            entryPoint: entryPoint,
            exitPoint: exitPoint,
            entryNormal: entryNorm,
            exitNormal: exitNorm,
            penetration: penetration
        };
        return r;
    }
    RaycastResult.PrecalculatedRaycastResult = PrecalculatedRaycastResult;
    /**
     * Create a circlecast result from the specified data
     * @param entered
     * @param exited
     * @param entryPoint
     * @param exitPoint
     * @param entryNorm
     * @param exitNorm
     * @param penetration
     */
    function PrecalculatedCirclecastResult(ray, radius, collider, entered, exited, entryPoint, exitPoint, firstContact, lastContact, entryNorm, exitNorm, penetration) {
        var r = {
            ray: ray,
            rayRadius: radius,
            collider: collider,
            didEnter: entered,
            didExit: exited,
            entryPoint: entryPoint,
            exitPoint: exitPoint,
            firstContactPoint: firstContact,
            lastContactPoint: lastContact,
            entryNormal: entryNorm,
            exitNormal: exitNorm,
            penetration: penetration
        };
        return r;
    }
    RaycastResult.PrecalculatedCirclecastResult = PrecalculatedCirclecastResult;
})(RaycastResult || (RaycastResult = {}));


/***/ }),

/***/ 803:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Scene": () => (/* binding */ Scene)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


/**
 * data structure that contains game entities
 */
var Scene = /** @class */ (function (_super) {
    __extends(Scene, _super);
    function Scene(game) {
        var _this = _super.call(this) || this;
        // ---------------------------------------------------------------------------------------------
        _this._entitiesToRemove = new Array();
        _this._entitiesToRemovePostRender = new Array();
        _this._particleSystems = new Array();
        _this._colliders = null;
        _this._currentTime = 0;
        _this._initializationQuery = [];
        _this._requestPointerLock = false;
        _this._parentGame = null;
        _this._particleRenderer = null;
        _this.onPreRender = new _internal__WEBPACK_IMPORTED_MODULE_1__.GameEvent();
        if (game !== undefined) {
            _this._parentGame = game;
        }
        _this._parentScene = _this;
        _this._name = "Scene";
        _this._colliders = new _internal__WEBPACK_IMPORTED_MODULE_1__.ColliderPartitions();
        return _this;
    }
    Object.defineProperty(Scene.prototype, "parentScene", {
        get: function () { return this; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Scene.prototype, "parentGame", {
        get: function () { return this._parentGame; },
        set: function (val) { if (this.parentGame != null)
            return; this._parentGame = val; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Scene.prototype, "sceneRefQuery", {
        get: function () { return this._sceneRefQuery; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Scene.prototype, "entitiesToRemovePreRender", {
        get: function () { return this._entitiesToRemove; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Scene.prototype, "entitiesToRemovePostRender", {
        get: function () { return this._entitiesToRemovePostRender; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Scene.prototype, "colliders", {
        get: function () { return this._colliders; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Scene.prototype, "particleSystems", {
        get: function () { return this._particleSystems; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Scene.prototype, "currentTime", {
        get: function () { return this._currentTime; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Scene.prototype, "requestPointerLock", {
        get: function () { return this._requestPointerLock; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    Scene.CreateDefault = function () {
        return new Scene(null);
    };
    Scene.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.MakeSerializable(this, function (x) { return [
            x._requestPointerLock,
        ]; });
    };
    // ---------------------------------------------------------------------------------------------
    /**
     * should be called when scene is switched to
     */
    Scene.prototype.OnEnter = function () { };
    /**
     * should be called when scene is switched away from
     */
    Scene.prototype.OnExit = function () { };
    // ---------------------------------------------------------------------------------------------
    Scene.prototype.addChild = function () {
        var childs = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            childs[_i] = arguments[_i];
        }
        var r = _super.prototype.addChild.apply(this, childs);
        for (var i = 0; i < childs.length; i++) {
            // if it is not a scene entity, only perform base class addChild functionality
            if (!(childs[i] instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity))
                continue;
            this._initializationQuery.push(childs[i]);
        }
        return r;
    };
    // ---------------------------------------------------------------------------------------------
    /**
     * Check collisions between all the colliders in the scene
     */
    Scene.prototype.CheckCollisions = function () {
        var collisions = this._colliders.FindCollisions();
        for (var i = collisions.length - 1; i >= 0; i--) {
            collisions[i].colliderA.onCollision.Invoke(collisions[i]);
            collisions[i].colliderB.onCollision.Invoke(collisions[i]);
        }
    };
    Scene.prototype.RemoveFlaggedEntities = function () {
        if (this._entitiesToRemove.length <= 0)
            return;
        // iterate through each entity flagged for removal and remove them all
        for (var i = this._entitiesToRemove.length - 1; i >= 0; i--) {
            this._entitiesToRemove[i].destroy();
        }
        // remove them from the list
        this._entitiesToRemove.splice(0, this._entitiesToRemove.length);
    };
    Scene.prototype.RemoveFlaggedEntitiesPostRender = function () {
        if (this._entitiesToRemovePostRender.length <= 0)
            return;
        // iterate through each entity flagged for removal and remove them all
        for (var i = this._entitiesToRemovePostRender.length - 1; i >= 0; i--) {
            this._entitiesToRemovePostRender[i].destroy();
        }
        // remove them from the list
        this._entitiesToRemovePostRender.splice(0, this._entitiesToRemovePostRender.length);
    };
    /**
     * override to implement update functionality for the scene
     * @param deltaTime
     */
    Scene.prototype.Update = function (deltaTime) {
        // initialize any entities that have not yet been initialized
        if (this._initializationQuery.length > 0)
            this.InitializeEntities();
        // update all direct children
        _super.prototype.Update.call(this, deltaTime);
        // check collisions
        this.CheckCollisions();
        // remove entities that are to be removed
        this.RemoveFlaggedEntities();
        // increment the amount of time the scene has been active for
        this._currentTime += deltaTime;
    };
    /**
     * override to implement rendering functionality for the scene
     */
    Scene.prototype.Draw = function (renderer) {
        this.onPreRender.Invoke();
        this.DrawTransformed(renderer, pixi_js__WEBPACK_IMPORTED_MODULE_0__.Matrix.IDENTITY);
    };
    Scene.prototype.DrawTransformed = function (renderer, transformation) {
        // render the scene graph
        renderer.render(this, {
            transform: transformation
        });
        if (this._particleRenderer == null) {
            this._particleRenderer = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.ParticleRenderer(renderer);
        }
        for (var i = 0; i < this._particleSystems.length; i++) {
            this._particleRenderer.render(this._particleSystems[i].container);
        }
        this.RemoveFlaggedEntitiesPostRender();
    };
    // ---------------------------------------------------------------------------------------------
    Scene.prototype.InitializeEntities = function () {
        for (var i = this._initializationQuery.length - 1; i >= 0; i--) {
            this._initializationQuery[i].InitializationCheck();
            this._initializationQuery.splice(i, 1);
        }
    };
    /** @inheritdoc */
    Scene.prototype.OnPostDeserialize = function () {
        this.ParseSceneRefs();
    };
    return Scene;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity));

_a = Scene;
(function () {
    _a.SetSerializableFields();
})();


/***/ }),

/***/ 5477:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SceneEntity": () => (/* binding */ SceneEntity)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var SceneEntity = /** @class */ (function (_super) {
    __extends(SceneEntity, _super);
    function SceneEntity() {
        var _this = _super.call(this) || this;
        _this._sceneRefQuery = [];
        _this._initialized = false;
        _this._sUid = SceneEntity.nextSUid;
        _this._sUidsChecked = false;
        _this._removed = false;
        _this._name = null;
        _this._parentScene = null;
        _this._parentEntity = null;
        _this._entities = new Array();
        return _this;
    }
    // Set Serializable Metadata -------------------------------------------------------------------
    SceneEntity.CreateDefault = function () {
        return new this();
    };
    SceneEntity.SetSerializableFields = function () {
        _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.MakeSerializable(this, function (x) { return [
            x._name,
            x._sUid,
            x.localPosition,
            x.localRotation,
            x.localScale,
            x.alpha,
            x.visible
        ]; });
    };
    Object.defineProperty(SceneEntity.prototype, "initialized", {
        get: function () { return this._initialized; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity, "nextSUid", {
        get: function () { return this._curSUid++; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "SUid", {
        get: function () { return this._sUid; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "name", {
        get: function () {
            if (this._name != null)
                return this._name;
            if (this.parent != null && this.parent instanceof SceneEntity)
                return this.parent.name + "_chld";
            return "Entity";
        },
        set: function (nm) {
            this._name = nm;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "removed", {
        get: function () { return this._removed; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "parentGame", {
        get: function () { return this._parentScene.parentGame; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "parentScene", {
        get: function () { return this._parentScene; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "parentEntity", {
        get: function () {
            var _b;
            return (_b = this._parentEntity) !== null && _b !== void 0 ? _b : (this._parentEntity = this.parent);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "entities", {
        get: function () { return this._entities; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "globalPosition", {
        get: function () { return this.toGlobal(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create()); },
        set: function (pos) {
            var _b, _c;
            var localPos = (_c = (_b = this.parent) === null || _b === void 0 ? void 0 : _b.toLocal(pos)) !== null && _c !== void 0 ? _c : _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Clone(pos);
            this.position.set(localPos.x, localPos.y);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "globalRotation", {
        get: function () { return SceneEntity.GetGlobalRotation(this); },
        set: function (val) { SceneEntity.SetGlobalRotation(this, val); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "GlobalScale", {
        get: function () { return SceneEntity.GetGlobalScale(this); },
        enumerable: false,
        configurable: true
    });
    SceneEntity.GetGlobalRotation = function (obj) {
        var ent = obj;
        var r = obj.rotation;
        while (ent.parent != null) {
            r += ent.parent.rotation;
            ent = ent.parent;
        }
        return r;
    };
    SceneEntity.SetGlobalRotation = function (obj, rot) {
        var parRot = 0;
        if (obj.parent != null) {
            var parent_1 = obj.parent;
            parRot = parent_1.rotation;
            while (parent_1.parent != null) {
                parRot += parent_1.parent.rotation;
                parent_1 = parent_1.parent;
            }
        }
        obj.rotation = rot - parRot;
    };
    SceneEntity.GetGlobalScale = function (obj) {
        var ent = obj;
        var r = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Clone(obj.scale);
        while (ent.parent != null) {
            r.x *= ent.parent.scale.x;
            r.y *= ent.parent.scale.y;
            ent = ent.parent;
        }
        return r;
    };
    Object.defineProperty(SceneEntity.prototype, "localPosition", {
        get: function () { return _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this.position.x, this.position.y); },
        set: function (val) { this.position.set(val.x, val.y); },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "localRotation", {
        get: function () { return this.rotation; },
        set: function (val) { this.rotation = val; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneEntity.prototype, "localScale", {
        get: function () { return _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this.scale.x, this.scale.y); },
        set: function (val) { this.scale.set(val.x, val.y); },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    SceneEntity.GetGlobalPosition = function (obj) {
        return obj.toGlobal(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create());
    };
    SceneEntity.SetGlobalPosition = function (obj, pos) {
        var localPos = obj.parent.toLocal(pos);
        obj.position.set(localPos.x, localPos.y);
    };
    // ---------------------------------------------------------------------------------------------
    /// Hierarchy
    /** called when this entity is added to another scene entity as a child */
    SceneEntity.prototype.OnAddedToParent = function (parent) {
        this._parentEntity = parent;
    };
    /** called when added to a new scene */
    SceneEntity.prototype.OnAddedToScene = function (scene) {
        this._parentScene = scene;
    };
    SceneEntity.prototype.Initialize = function () { };
    SceneEntity.prototype.InitializationCheck = function () {
        if (!this._initialized) {
            this.Initialize();
            this._initialized = true;
        }
    };
    /** called when removed from a scene */
    SceneEntity.prototype.OnRemovedFromScene = function (scene) { };
    /**
     * for all child entities, ensure that they are also removed from the scene if this entity is
     * removed from a scene
     * @param scene
     */
    SceneEntity.prototype.UpdateChildrenRemovedFromScene = function (scene) {
        // iterate through each child entity
        for (var i = 0; i < this.entities.length; i++) {
            var ent = this.entities[i];
            ent.OnRemovedFromScene(scene);
            ent.UpdateChildrenRemovedFromScene(scene);
        }
    };
    /**
     * for all the entities that are children of this object, ensure that they share the
     * same parent scene as this object
     */
    SceneEntity.prototype.UpdateChildrenParentRef = function () {
        for (var i = this._entities.length - 1; i >= 0; i--) {
            // update entity new parent scene
            var ent = this._entities[i];
            ent.OnAddedToScene(this.parentScene);
            ent.OnAddedToParent(this);
            ent.UpdateChildrenParentRef();
        }
    };
    /** @inheritdoc */
    SceneEntity.prototype.addChild = function () {
        var childs = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            childs[_i] = arguments[_i];
        }
        // base functionality
        var t = _super.prototype.addChild.apply(this, childs);
        // iterate through each child provided
        for (var i = 0; i < childs.length; i++) {
            // if it is not a scene entity, only perform base class addChild functionality
            if (!(childs[i] instanceof SceneEntity))
                continue;
            // otherwise, provide scene entity data
            var child = childs[i];
            // parent the child entity to our scene, and all of it's children too
            child.OnAddedToScene(this.parentScene);
            child.OnAddedToParent(this);
            child.UpdateChildrenParentRef();
            // add to child entities
            this._entities.push(child);
        }
        // mark the flag to ensure SUids of children are checked when serialized
        this._sUidsChecked = false;
        return t;
    };
    /** @inheritdoc */
    SceneEntity.prototype.removeChild = function () {
        var childs = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            childs[_i] = arguments[_i];
        }
        // iterate through each child provided
        for (var i = 0; i < childs.length; i++) {
            // if it is not a scene entity, only perform base class removeChild functionality
            if (!(childs[0] instanceof SceneEntity))
                continue;
            var child = childs[0];
            var oscn = child.parentScene;
            child._parentEntity = null;
            child._parentScene = null;
            if (oscn != null) {
                child.OnRemovedFromScene(oscn);
                child.UpdateChildrenRemovedFromScene(oscn);
            }
            // remove from child entities
            var index = this._entities.indexOf(child);
            this._entities.splice(index, 1);
        }
        // base functionality
        return _super.prototype.removeChild.apply(this, childs);
    };
    /** @inheritdoc */
    SceneEntity.prototype.destroy = function (options) {
        if (!this._removed) {
            this._removed = true;
            if (this.parentScene != null)
                this.OnRemovedFromScene(this.parentScene);
        }
        var pent = this.parentEntity;
        this._parentEntity = null;
        _super.prototype.destroy.call(this, options);
        if (pent == null)
            return;
        var pind = pent._entities.indexOf(this);
        if (pind >= 0)
            pent._entities.splice(pind, 1);
        this._parentScene = null;
    };
    /**
     * Removes the entity from the scene at the end of the update cycle (before being rendered)
     * @param removePostRender whether or not the object should render at the end of this frame
     */
    SceneEntity.prototype.RemoveFromScene = function (removePostRender) {
        if (removePostRender === void 0) { removePostRender = false; }
        if (this.parentScene != null) {
            this.OnRemovedFromScene(this.parentScene);
            var removeArray = removePostRender ?
                this.parentScene.entitiesToRemovePostRender :
                this.parentScene.entitiesToRemovePreRender;
            removeArray.push(this);
        }
        this._removed = true;
    };
    /**
     * returns the first child entity of the specified type
     * @param type the type (constructor function) value to look for, must be the same type passed
     * 	in as the generic parameter
     */
    SceneEntity.prototype.GetChildOfType = function (type) {
        for (var i = 0; i < this.entities.length; i++) {
            if (this.entities[i] instanceof type) {
                return this.entities[i];
            }
        }
        return null;
    };
    /**
     * search all the child entities recursively for the specified child, return an array
     * of each child's index until the target is found
     * @param child the child to search for
     */
    SceneEntity.prototype.FindRecursiveChildIndex = function (child) {
        var r = new Array();
        var chRef = child;
        // (it's secretly actually a backwards-up search, not a recursive downward traversal)
        // iterate through the child parents until it reaches this object, or null
        while (chRef != null && chRef instanceof SceneEntity) {
            if (chRef.parent == null)
                return null;
            var chRefParent = chRef.parentEntity;
            var index = chRefParent._entities.indexOf(chRef);
            // if the specified child is actually not a child of this entity, return null
            if (index < 0)
                return null;
            // insert index at beginning of array
            r.splice(0, 0, index);
            // if the parent was reached, break out of the loop
            if (chRefParent == this)
                return r;
            chRef = chRefParent;
        }
        return null;
    };
    /**
     * return the nested child with the specified indices
     * @param deepIndex the sequential indices of the nested child
     */
    SceneEntity.prototype.GetDeepChild = function (deepIndex) {
        var r = this;
        for (var i = 0; i < deepIndex.length; i++) {
            r = r.entities[deepIndex[i]];
        }
        return r;
    };
    /**
     * finds the child (or self) of this entity that matches the specified suid, return null if
     * none found
     * @param suid the suid of the entity to search for
     */
    SceneEntity.prototype.GetEntityBySUid = function (suid) {
        if (this._sUid == suid)
            return this;
        var rec = null;
        for (var i = this.entities.length - 1; i >= 0; i--) {
            rec = this.entities[i].GetEntityBySUid(suid);
            if (rec != null)
                break;
        }
        return rec;
    };
    ///---------------------------------------------------------------------------------------------
    /// Scene Methods
    SceneEntity.prototype.Update = function (deltaTime) {
        // iterate through each scene entity child in the entity container
        for (var i = 0; i < this._entities.length; i++) {
            // update the scene entity's game logic
            this._entities[i].Update(deltaTime);
        }
    };
    SceneEntity.prototype.Draw = function (renderer) { };
    ///---------------------------------------------------------------------------------------------
    SceneEntity.GetDefaultEditorProperties = function () {
        var r = new Array(_internal__WEBPACK_IMPORTED_MODULE_1__.EditorProperty.CreateStringProp("_name", "Name"), _internal__WEBPACK_IMPORTED_MODULE_1__.EditorProperty.CreateBoolProp("visible", "Visible"), _internal__WEBPACK_IMPORTED_MODULE_1__.EditorProperty.CreateVectProp("globalPosition", "Position"));
        return r;
    };
    SceneEntity.GetEditorProperties = function () {
        return this.GetDefaultEditorProperties();
    };
    ///---------------------------------------------------------------------------------------------
    /// Cloning
    SceneEntity.prototype.CopyFields = function (source) {
        var serObj = SceneEntity.GetSerializedObject_NoChilds(source);
        this.Deserialize(serObj);
        // let pos = source.position;
        // this.position.set(pos.x, pos.y);
        // this._name = source._name;
        // // this._parentEntity = source._parentEntity;
    };
    SceneEntity.prototype.CopyChildren = function (source) {
        var childs = source.entities;
        for (var i = 0; i < childs.length; i++) {
            var childClone = childs[i].Clone();
            if (childClone != null)
                this.addChild(childClone);
        }
    };
    /** creates a clone of the object and returns it */
    SceneEntity.prototype.Clone = function () {
        var r = (Object.getPrototypeOf(this).constructor).CreateDefault();
        r.CopyChildren(this);
        r.CopyFields(this);
        r.ParseSceneRefs();
        return r;
    };
    ///---------------------------------------------------------------------------------------------
    /// Serialization
    /**
     * default implementation for getting a serialized scene entity object without it's
     * child entities
     * @param ent the scene entity to serialize
     */
    SceneEntity.GetSerializedObject_NoChilds = function (ent) {
        var r = _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.GetSerializedObject_DefaultImplementation(ent, false);
        // get the fields marked as serializable
        var type = Object.getPrototypeOf(ent).constructor;
        var untypedObj = ent;
        var fields = _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.serializedFields.get(type);
        // individually serialize each field
        var ths = this;
        fields.forEach(function (field) {
            var fieldData = untypedObj[field];
            if (fieldData != null) {
                // handle field serialization with custom GetSerializedField function specifically
                // for fields on SceneEntities
                r[field] = ths.GetSerializedField(fieldData);
            }
        });
        return r;
    };
    /**
     * default implementation for turning a field on a scene entity into a serialized object
     * @param data the field data object
     */
    SceneEntity.GetSerializedField = function (data) {
        if (data instanceof Object) {
            // serialize scene entities as scene references
            if (data instanceof SceneEntity) {
                return (new _internal__WEBPACK_IMPORTED_MODULE_1__.SceneReference(data.SUid)).GetSerializedObject();
            }
            // serialize each array entry seperately so that each scene entity can be converted to
            // a scene reference
            if (data instanceof Array) {
                var r = [];
                for (var i = 0; i < data.length; i++) {
                    r.push(this.GetSerializedField(data[i]));
                }
                return r;
            }
            // serialize it normally if it's ISerializable but not a Scene Entity
            if (_internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.ImplementedIn(data))
                _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.GetSerializedField(data);
            // recursively iterate through each field of an anonymous object type to check for 
            // scene entities
            else {
                var r = {};
                for (var innerField in data) {
                    r[innerField] = this.GetSerializedField(data[innerField]);
                }
                return r;
            }
        }
        // if it's not an object, we can just serialize as it normally would be serialized
        return _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.GetSerializedField(data);
    };
    /**
     * recursive call for EnsureNoRedundantChildSUids to gather data for finding matchin SUids,
     * do not use this function outside of 'EnsureNoRedundantChildSUids' function call
     */
    SceneEntity.prototype.EnsureNoRedundantChildSUids_Recursive = function (passIn, ids, matchingIds) {
        this._sUidsChecked = true;
        // append ranges to passin if provided
        passIn.low = Math.min(this._sUid, passIn.low);
        passIn.high = Math.max(this._sUid, passIn.low);
        // if the id of this entity is redundant, add it to the matchingIDs array
        if (ids.includes(this.SUid))
            matchingIds.push(this);
        // add their own id to the id array if not redundant
        else
            ids.push(this.SUid);
        // iterate thourgh children and recursively call this func
        for (var i = this.entities.length - 1; i >= 0; i--) {
            this.entities[i].EnsureNoRedundantChildSUids_Recursive(passIn, ids, matchingIds);
        }
    };
    /**
     * ensure that this entity or any of it's recursive children do not share the same SUid, this
     * should be called before an object is serialized
     */
    SceneEntity.prototype.EnsureNoRedundantChildSUids = function () {
        // create passin if not provided
        var passIn = {
            low: this._sUid,
            high: this._sUid
        };
        // create id and matchingID arrays if not passed in
        var ids = [];
        var matchingIds = [];
        // recursively call on self and children to get matchin uids
        this.EnsureNoRedundantChildSUids_Recursive(passIn, ids, matchingIds);
        // enforce new SUids on the children who have matchin IDs
        SceneEntity._curSUid = Math.max(passIn.high + 1, SceneEntity._curSUid);
        for (var i = matchingIds.length - 1; i >= 0; i--) {
            matchingIds[i]._sUid = SceneEntity.nextSUid;
        }
    };
    /** returns a JSON serializable object that has all the necessary data to recreate itself */
    SceneEntity.prototype.GetSerializedObject = function () {
        // check children SUids to ensure none of them have the same SUid
        if (!this._sUidsChecked)
            this.EnsureNoRedundantChildSUids();
        // iterate through each serialized field, serialize it's value and append it to the return object
        var r = SceneEntity.GetSerializedObject_NoChilds(this);
        // serialize the children
        var childs = this.GetSerializedChildren();
        r[SceneEntity.SFIELD_CHILDREN] = childs;
        return r;
    };
    /** get all the serialized child data in the form of an array of anonymous serialized objects */
    SceneEntity.prototype.GetSerializedChildren = function () {
        // get all the children that are serializable and put them in an array
        var entities = this.entities;
        var toSerialize = [];
        for (var i = 0; i < entities.length; i++) {
            var cType = Object.getPrototypeOf(entities[i]).constructor;
            if (_internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.IsTypeRegistered(cType))
                toSerialize.push(entities[i]);
        }
        // iterate through the array of serializable children serialize each one		
        var r = [];
        for (var i = 0; i < toSerialize.length; i++) {
            var serEnt = null;
            serEnt = toSerialize[i].GetSerializedObject();
            r.push(serEnt);
        }
        return r;
    };
    /**
     * copy all the fields of the entity into an anonymous object for serialization
     * @param obj the object to copy our fields into
     */
    SceneEntity.prototype.SerializeFieldsInto = function (obj, omittedTypes) {
        if (omittedTypes === void 0) { omittedTypes = []; }
        var dif = new SceneEntity();
        delete dif._sUid;
        var untypedSelf = this;
        omittedTypes.push.apply(omittedTypes, [
            Function,
            _internal__WEBPACK_IMPORTED_MODULE_1__.GameEvent,
            _internal__WEBPACK_IMPORTED_MODULE_1__.AiController
        ]);
        // iterate though each field we have and copy the necesary ones over to our 
        // serialized object
        fieldCopy: for (var field in untypedSelf) {
            // we don't want to copy any of these fields or
            // any field if it's shared with the base entity type (so we don't copy over a
            // bunch of useless redundant garbage into the serialized object)
            if (untypedSelf[field] === undefined ||
                field === "_parentGame" ||
                field === "_parentScene" ||
                dif[field] !== undefined) {
                continue fieldCopy;
            }
            // skip if it's a native pixi object
            var isPixi = (untypedSelf[field] instanceof pixi_js__WEBPACK_IMPORTED_MODULE_0__.DisplayObject &&
                !(untypedSelf[field] instanceof SceneEntity));
            if (isPixi)
                continue fieldCopy;
            // dont copy omitted types
            for (var i = omittedTypes.length - 1; i >= 0; i--) {
                if (untypedSelf[field] instanceof omittedTypes[i]) {
                    continue fieldCopy;
                }
            }
            // if it's a scene entity field, it must be a child or scene reference
            var ent = untypedSelf[field];
            if (ent instanceof SceneEntity) {
                // TODO find recursive child index
                // if the field references an entity outside this object's children, don't store it
                var crIndex = this.FindRecursiveChildIndex(ent);
                if (crIndex == null) {
                    // look for scene reference
                    if (this.parentScene !== null) {
                        // store the scene reference
                        var sceneRef = new _internal__WEBPACK_IMPORTED_MODULE_1__.SceneReference(this.SUid);
                        obj[field] = sceneRef.GetSerializedObject();
                    }
                    continue fieldCopy;
                }
                // serialize a field to the reference into the object
                var childRef = {};
                childRef.type = SceneEntity.TYPE_CHILDREF;
                childRef[SceneEntity.SFIELD_RECURSIVE_INDEX] = crIndex;
                obj[field] = childRef;
                continue fieldCopy;
            }
            // if the field value is serializable, get and store the serialized value
            var fieldVal = untypedSelf[field];
            if (_internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.ImplementedIn(fieldVal)) {
                var val = fieldVal.GetSerializedObject();
                obj[field] = val;
                continue fieldCopy;
            }
            obj[field] = untypedSelf[field];
        }
        dif.destroy();
    };
    /**
     * Deserialize a field from a serialized data object, can be any type, number, string, obj, etc
     * @param fieldData JSON parsed the data for the field
     */
    SceneEntity.prototype.GetDeserializedField = function (fieldData) {
        var r = _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.GetDeserializedField(fieldData);
        return r;
    };
    /**
     * copy the field values from a serialized object
     * @param obj the serialized object to parse from
     */
    SceneEntity.prototype.CopyFieldsFromSerialized = function (obj) {
        // cast self to anonymous object
        var untypedSelf = this;
        var selfType = Object.getPrototypeOf(this).constructor;
        var serializedFields = _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.serializedFields.get(selfType);
        // iterate through each property marked as serializable
        var ths = this;
        serializedFields.forEach(function (field) {
            // get the deserialized and typed value from the serialzed data
            var fieldVal = ths.GetDeserializedField(obj[field]);
            untypedSelf[field] = fieldVal;
            // add the field to the scene ref query if it is a scene reference
            if (fieldVal instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.SceneReference) {
                ths._sceneRefQuery.push(field);
            }
        });
        // // iterate through each of the object's fields
        // for(let fieldName in obj){
        // 	// don't copy the children
        // 	if(fieldName == SceneEntity.SFIELD_CHILDREN){
        // 		continue;
        // 	}
        // 	// copy the field from the serialized object if we have a field of the same name
        // 	if(serializedFields.has(fieldName)){
        // 		// destroy the entity if it already exists
        // 		if(untypedSelf[fieldName] != null){
        // 			let selfFieldObj = untypedSelf[fieldName];
        // 			if(selfFieldObj instanceof SceneEntity){
        // 				selfFieldObj.destroy();
        // 			}
        // 		}
        // 		let fieldVal = ISerializable.GetDeserializedField(obj[fieldName]);
        // 		if(fieldVal instanceof SceneReference){
        // 			this._sceneRefQuery.push({fieldName: fieldName });
        // 			untypedSelf[fieldName] = fieldVal.targetSUid;
        // 			continue;
        // 		}
        // 		untypedSelf[fieldName] = fieldVal;
        // 	}
        // 
    };
    /**
     * copy the children into this object from a serialized object
     * @param obj the serialized object whose child list to parse and copy
     */
    SceneEntity.prototype.CopyChildrenFromSerialized = function (obj) {
        if (obj[SceneEntity.SFIELD_CHILDREN] == null)
            return;
        var childs = obj[SceneEntity.SFIELD_CHILDREN];
        for (var i = 0; i < childs.length; i++) {
            var child = _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.GetDeserializedObject(childs[i]);
            if (child instanceof SceneEntity) {
                // if(this instanceof Scene) child._parentScene = this;
                this.addChild(child);
            }
        }
    };
    /** @inheritdoc */
    SceneEntity.prototype.Deserialize = function (data) {
        this.CopyChildrenFromSerialized(data);
        this.CopyFieldsFromSerialized(data);
        this.OnPostDeserialize();
    };
    /** called after an object is deserialized */
    SceneEntity.prototype.OnPostDeserialize = function () { };
    /** iterate through all the non parsed scene references and dereference them */
    SceneEntity.prototype.ParseSceneRefs = function () {
        // iterate through each entry in the scene reference query
        for (var i = this._sceneRefQuery.length - 1; i >= 0; i--) {
            var sceneRefField = this._sceneRefQuery[i];
            var untypedTarget = this;
            // try parsing the scene reference
            var sceneRef = untypedTarget[sceneRefField];
            var ent = sceneRef.GetDereferencedEntity(this.parentScene || this);
            // if the parse is successful, remove the field from the query
            if (ent != null) {
                untypedTarget[sceneRefField] = ent;
                this._sceneRefQuery.splice(i, 1);
            }
        }
        // recursive scene refs
        for (var i = this.entities.length - 1; i >= 0; i--) {
            this.entities[i].ParseSceneRefs();
        }
    };
    var _a;
    _a = SceneEntity;
    SceneEntity.TYPE_CHILDREF = "_childRef";
    SceneEntity.SFIELD_CHILDREN = "_entities";
    SceneEntity.SFIELD_RECURSIVE_INDEX = "childIndex";
    (function () {
        _a.SetSerializableFields();
    })();
    // ---------------------------------------------------------------------------------------------
    SceneEntity._curSUid = 0;
    return SceneEntity;
}(pixi_js__WEBPACK_IMPORTED_MODULE_0__.Container));



/***/ }),

/***/ 9238:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SceneReference": () => (/* binding */ SceneReference)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

var SceneReference = /** @class */ (function (_super) {
    __extends(SceneReference, _super);
    function SceneReference(suid) {
        var _this = _super.call(this) || this;
        _this._targetSUid = null;
        _this._targetEntity = null;
        _this._targetSUid = suid;
        return _this;
    }
    Object.defineProperty(SceneReference.prototype, "targetSUid", {
        get: function () { return this._targetSUid; },
        set: function (val) { this._targetSUid = val; this._targetEntity = null; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SceneReference.prototype, "targetEntity", {
        get: function () { return this._targetEntity; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    SceneReference.CreateDefault = function () { return new this(0); };
    // ---------------------------------------------------------------------------------------------
    /**
     * finds the reference to the entity in the specified scene, if it exists
     * @param scene the scene to search for the reference in
     */
    SceneReference.prototype.GetDereferencedEntity = function (scene) {
        // if the scene is a container for the reference, find it and return it
        if (this._targetEntity == null)
            this._targetEntity = scene.GetEntityBySUid(this.targetSUid);
        return this._targetEntity;
    };
    return SceneReference;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.Serializable));

_a = SceneReference;
(function () {
    _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(_a, function (x) { return [
        x._targetSUid
    ]; });
})();


/***/ }),

/***/ 7714:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SpriteSheet": () => (/* binding */ SpriteSheet)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var SpriteSheet = /** @class */ (function (_super) {
    __extends(SpriteSheet, _super);
    function SpriteSheet(path) {
        var _this = _super.call(this) || this;
        _this._uid = SpriteSheet._sheetID++;
        _this._texPath = "./dist/assets/graphics/Pixel.png";
        _this._resource = null;
        _this._isLoaded = false;
        _this._onLoad = new _internal__WEBPACK_IMPORTED_MODULE_1__.GameEvent();
        _this._isInQueue = false;
        _this._pathHash = 0;
        if (path != null)
            _this.SetTexture(path);
        return _this;
    }
    SpriteSheet.GetFromPath = function (path) {
        var r = new SpriteSheet(path);
        return r;
    };
    Object.defineProperty(SpriteSheet.prototype, "uid", {
        get: function () { return this._uid; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SpriteSheet.prototype, "uniqueHashString", {
        get: function () { return "_sheet" + this._pathHash; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SpriteSheet.prototype, "isLoaded", {
        get: function () { return this._isLoaded; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SpriteSheet.prototype, "baseTexture", {
        get: function () { return this._baseTexture; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SpriteSheet.prototype, "sprites", {
        get: function () { return this._sprites; },
        enumerable: false,
        configurable: true
    });
    /// --------------------------------------------------------------------------------------------
    SpriteSheet.CreateDefault = function () { return new SpriteSheet(null); };
    /// --------------------------------------------------------------------------------------------
    SpriteSheet.prototype.SetTexture = function (path) {
        this._uid = SpriteSheet._sheetID++;
        this._texPath = path;
        this._pathHash = _internal__WEBPACK_IMPORTED_MODULE_1__.Maths.StringToHash(this._texPath);
        this._baseTexture = null;
        this._isLoaded = false;
        this._resource = null;
        this.GetTexture();
        if (this._resource == null) {
            this.TryLoad();
        }
    };
    SpriteSheet.prototype.GetTexture = function () {
        var loader = pixi_js__WEBPACK_IMPORTED_MODULE_0__.Loader.shared;
        var resource = loader.resources[this.uniqueHashString];
        if (resource == null) {
            this._isLoaded = false;
            this._resource = null;
            return;
        }
        // delay get texture until next step if it's not yet loaded (can't use resource.onComplete 
        // because PIXI is broken)
        this._resource = resource;
        if (!resource.isComplete) {
            this._isLoaded = false;
            var ths_1 = this;
            setTimeout(function () { ths_1.GetTexture(); });
            return;
        }
        // delay get texture until next step if it's not yet loaded and it's an image type (can't 
        // use resource.onComplete because PIXI is broken)
        if (resource.texture == null && resource.type == pixi_js__WEBPACK_IMPORTED_MODULE_0__.LoaderResource.TYPE.IMAGE) {
            this._isLoaded = false;
            var ths_2 = this;
            setTimeout(function () { ths_2.GetTexture(); });
            return;
        }
        this.OnGetResource(resource);
    };
    SpriteSheet.prototype.TryLoad = function () {
        console.log("requested: " + this._texPath);
        var loader = pixi_js__WEBPACK_IMPORTED_MODULE_0__.Loader.shared;
        if (!loader.loading) {
            this.DoLoad(loader);
        }
        else {
            var ths_3 = this;
            if (!ths_3._isInQueue) {
                loader.onLoad.add(function (loader, resource) {
                    // TODO if getting weird resource issues, try not fucking with this flag:
                    loader.loading = false;
                    console.log("dequeued: " + ths_3._texPath);
                    ths_3.TryLoad();
                });
                ths_3._isInQueue = true;
                console.log("queuing: " + this._texPath);
            }
        }
    };
    // ---------------------------------------------------------------------------------------------
    SpriteSheet.prototype.DoLoad = function (loader) {
        // check to see if resource is already loaded
        var resource = loader.resources[this.uniqueHashString];
        if (resource != null) {
            var ths_4 = this;
            setTimeout(function () {
                ths_4.GetTexture();
            });
            return;
        }
        var ths = this;
        loader.add(this.uniqueHashString, this._texPath, {}, function (resource) {
            ths.OnLoad(resource);
        });
        this._isInQueue = false;
        setTimeout(function () {
            console.log("LOADING: " + ths._texPath);
            if (!loader.loading)
                loader.load();
        }, 0);
    };
    SpriteSheet.prototype.OnLoad = function (resource) {
        this.OnGetResource(resource);
        console.log("LOADED: " + resource.url);
    };
    SpriteSheet.prototype.OnGetResource = function (resource) {
        if ((resource === null || resource === void 0 ? void 0 : resource.texture) == null) {
            throw "texture not found at " + this._texPath;
        }
        this._baseTexture = resource.texture.baseTexture;
        this._baseTexture.scaleMode = pixi_js__WEBPACK_IMPORTED_MODULE_0__.SCALE_MODES.NEAREST;
        this._resource = resource;
        this._isLoaded = true;
        if (this.sprites == null)
            this.GenerateSprites(1, 1, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0.5));
        this._onLoad.Invoke(resource);
        this._onLoad.ClearListeners();
    };
    // ---------------------------------------------------------------------------------------------
    SpriteSheet.prototype.DoOnLoaded = function (listener) {
        if (this.isLoaded)
            listener(this._resource);
        else {
            this._onLoad.AddListener(listener);
        }
    };
    SpriteSheet.prototype.GenerateSprites = function (columns, rows, anchor, lastRowCount) {
        if (anchor === void 0) { anchor = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0.5); }
        if (lastRowCount === void 0) { lastRowCount = columns; }
        if (!this.isLoaded) {
            var ths_5 = this;
            this._onLoad.AddListener(function (resource) {
                ths_5.GenerateSprites(columns, rows, anchor, lastRowCount);
            });
            return;
        }
        // create new sprite array
        this._sprites = new Array();
        // firgure sprite dimensions
        var width = this._baseTexture.width / columns;
        var height = this._baseTexture.height / rows;
        // iterate through each row/column cell
        for (var y = 0; y < rows; y++) {
            var yPos = Math.floor(y * height);
            for (var x = 0; x < columns; x++) {
                var xPos = Math.floor(x * width);
                // create a sprite frame texture from the cell rect and add it to the sprite array
                var rect = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Rectangle(xPos, yPos, Math.floor(width), Math.floor(height));
                var tex = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Texture(this._baseTexture, rect);
                tex.defaultAnchor.set(anchor.x, anchor.y);
                this._sprites.push(tex);
            }
        }
    };
    // ---------------------------------------------------------------------------------------------
    SpriteSheet.prototype.GetSerializedObject = function () {
        var sprites = [];
        for (var i = 0; i < this._sprites.length; i++) {
            sprites.push(_internal__WEBPACK_IMPORTED_MODULE_1__.Rect.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this._sprites[i]._frame.x, this._sprites[i]._frame.y), _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this._sprites[i]._frame.width, this._sprites[i]._frame.height)));
        }
        var r = {
            _texPath: this._texPath,
            _sprites: sprites
        };
        r[_internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.SFIELD_TYPE] = "SpriteSheet";
        return r;
    };
    SpriteSheet.prototype.Deserialize = function (data) {
        this.SetTexture(data._texPath);
        var sprites = data._sprites;
        var ths = this;
        this.DoOnLoaded(function () {
            ths._sprites = [];
            for (var i = 0; i < sprites.length; i++) {
                ths._sprites.push(new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Texture(ths._baseTexture, new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Rectangle(sprites[i].position.x, sprites[i].position.y, sprites[i].size.x, sprites[i].size.y)));
            }
        });
    };
    var _a;
    _a = SpriteSheet;
    // ---------------------------------------------------------------------------------------------
    SpriteSheet._sheetID = 0;
    (function () {
        _a.SetSerializableFields();
    })();
    return SpriteSheet;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.Serializable));



/***/ }),

/***/ 6896:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Stat": () => (/* binding */ Stat),
/* harmony export */   "ConsumableStat": () => (/* binding */ ConsumableStat),
/* harmony export */   "StaticStat": () => (/* binding */ StaticStat)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a, _b;
var _this = undefined;

/**
 * base type for any type of character stat
 */
var Stat = /** @class */ (function (_super) {
    __extends(Stat, _super);
    function Stat() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._modifiers = [];
        return _this;
    }
    Object.defineProperty(Stat.prototype, "modifiers", {
        get: function () { return this._modifiers; },
        enumerable: false,
        configurable: true
    });
    /** applies the specified stat modifier to this stat */
    Stat.prototype.ApplyModifier = function (mod) {
        if (mod.stat != null)
            throw "modifier is already applied to a stat";
        this._modifiers.push(mod);
        mod.stat = this;
        return mod;
    };
    /** calculates the total amount of stat modification from all the modifiers applied to this stat*/
    Stat.prototype.GetCumulativeModifiers = function () {
        var r = { add: 0, multiply: 1 };
        for (var i = this._modifiers.length - 1; i >= 0; i++) {
            r.add += this._modifiers[i].add;
            r.multiply *= this._modifiers[i].multiply;
        }
        return r;
    };
    return Stat;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.Serializable));

/**
 * a stat that has a separate max value and current state value, i.e. heath has a "max health" and
 * a "current health" value
 */
var ConsumableStat = /** @class */ (function (_super) {
    __extends(ConsumableStat, _super);
    function ConsumableStat(baseMax) {
        var _this = _super.call(this) || this;
        _this._baseMaxValue = 0;
        _this._currentValue = 0;
        _this._cachedCurMaxVal = null;
        _this._baseMaxValue = baseMax;
        _this._currentValue = _this.maxValue;
        return _this;
    }
    Object.defineProperty(ConsumableStat.prototype, "value", {
        get: function () {
            return this._currentValue;
        },
        /** @inheritdoc */
        set: function (val) {
            this._currentValue = val;
            if (this._currentValue > this.maxValue)
                this._currentValue = this.maxValue;
            else if (this._currentValue < 0)
                this._currentValue = 0;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ConsumableStat.prototype, "maxValue", {
        /** the max possible value of the stat's 'current' state, with modifiers applied */
        get: function () {
            if (this._cachedCurMaxVal == null)
                this.CalculateMaxValue();
            return this._cachedCurMaxVal;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ConsumableStat.prototype, "baseValue", {
        /** the base max value of the stat's 'current' state, WITHOUT modifiers applied */
        get: function () {
            return this._baseMaxValue;
        },
        set: function (val) {
            var percent = this.value / this.maxValue;
            this._cachedCurMaxVal = null;
            this._baseMaxValue = val;
            this._currentValue = percent * this.maxValue;
        },
        enumerable: false,
        configurable: true
    });
    /// --------------------------------------------------------------------------------------------
    ConsumableStat.CreateDefault = function () { return new this(1); };
    ConsumableStat.prototype.Deserialize = function (data) {
        _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.DeserializeObject_DefaultImplementation(this, data);
        this._cachedCurMaxVal = null;
    };
    /// --------------------------------------------------------------------------------------------
    ConsumableStat.prototype.CalculateMaxValue = function () {
        var cum = this.GetCumulativeModifiers();
        this._cachedCurMaxVal = (this._baseMaxValue * cum.multiply) + cum.add;
    };
    return ConsumableStat;
}(Stat));

_a = ConsumableStat;
(function () {
    _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(_a, function (x) { return [
        x._baseMaxValue,
        x._currentValue
    ]; });
})();
/**
 * a generic stat, such as speed or attack, that can be used to calculate other things, such as
 * the damage of an attack, or how fast a character can move
 */
var StaticStat = /** @class */ (function (_super) {
    __extends(StaticStat, _super);
    function StaticStat(baseVal) {
        var _this = _super.call(this) || this;
        _this._baseValue = 0;
        _this._cachedVal = null;
        _this._baseValue = baseVal;
        return _this;
    }
    Object.defineProperty(StaticStat.prototype, "value", {
        /** @inheritdoc */
        get: function () {
            if (this._cachedVal == null)
                this.CalculateValue();
            return this._cachedVal;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StaticStat.prototype, "baseValue", {
        /** @inheritdoc */
        get: function () {
            return this._baseValue;
        },
        set: function (val) {
            this._baseValue = val;
        },
        enumerable: false,
        configurable: true
    });
    /// --------------------------------------------------------------------------------------------
    StaticStat.CreateDefault = function () { return new this(1); };
    /// --------------------------------------------------------------------------------------------
    StaticStat.prototype.CalculateValue = function () {
        var cum = this.GetCumulativeModifiers();
        this._cachedVal = (this._baseValue * cum.multiply) + cum.add;
    };
    return StaticStat;
}(Stat));

_b = StaticStat;
(function () {
    _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(_b, function (x) { return [
        x._baseValue
    ]; });
})();


/***/ }),

/***/ 4606:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StatModifier": () => (/* binding */ StatModifier)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;

/**
 * data structure used to modify individual stats
 */
var StatModifier = /** @class */ (function (_super) {
    __extends(StatModifier, _super);
    function StatModifier(add, multiply) {
        if (multiply === void 0) { multiply = 1; }
        var _this = _super.call(this) || this;
        _this._add = 0;
        _this._multiply = 1;
        _this._stat = null;
        _this._add = add;
        _this._multiply = multiply;
        return _this;
    }
    Object.defineProperty(StatModifier.prototype, "add", {
        get: function () { return this._add; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StatModifier.prototype, "multiply", {
        get: function () { return this._multiply; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(StatModifier.prototype, "stat", {
        get: function () { return this._stat; },
        set: function (val) {
            if (this._stat != null)
                throw "this modifier has already been applied to a stat";
            this._stat = val;
        },
        enumerable: false,
        configurable: true
    });
    /// --------------------------------------------------------------------------------------------
    StatModifier.CreateDefault = function () { return new this(0, 1); };
    return StatModifier;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.Serializable));

_a = StatModifier;
(function () {
    _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(_a, function (x) { return [
        x._add,
        x._multiply
    ]; });
})();


/***/ }),

/***/ 4561:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Stats": () => (/* binding */ Stats)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _this = undefined;

/**
 * data structure used to keep track of individual character stats
 */
var Stats = /** @class */ (function (_super) {
    __extends(Stats, _super);
    function Stats() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._health = new _internal__WEBPACK_IMPORTED_MODULE_0__.ConsumableStat(10);
        _this._energy = new _internal__WEBPACK_IMPORTED_MODULE_0__.ConsumableStat(10);
        _this._attack = new _internal__WEBPACK_IMPORTED_MODULE_0__.StaticStat(0);
        _this._speed = new _internal__WEBPACK_IMPORTED_MODULE_0__.StaticStat(15);
        return _this;
    }
    Object.defineProperty(Stats, "DEFAULT", {
        /** the default stats set for the player */
        get: function () {
            if (this._DEFAULT == null)
                this.InitializeDefault();
            return this._DEFAULT;
        },
        enumerable: false,
        configurable: true
    });
    /// --------------------------------------------------------------------------------------------
    Stats.CreateDefault = function () { return new this(); };
    /// --------------------------------------------------------------------------------------------
    /**
     * creates the stats.DEFAULT stats object
     */
    Stats.InitializeDefault = function () {
        var def = new Stats();
        def._health = new _internal__WEBPACK_IMPORTED_MODULE_0__.ConsumableStat(10);
        def._energy = new _internal__WEBPACK_IMPORTED_MODULE_0__.ConsumableStat(10);
        def._attack = new _internal__WEBPACK_IMPORTED_MODULE_0__.StaticStat(0);
        def._speed = new _internal__WEBPACK_IMPORTED_MODULE_0__.StaticStat(15);
        Stats._DEFAULT = def;
    };
    Object.defineProperty(Stats.prototype, "health", {
        get: function () { return this._health; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Stats.prototype, "energy", {
        get: function () { return this._energy; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Stats.prototype, "attack", {
        get: function () { var _b; return (_b = this._attack) !== null && _b !== void 0 ? _b : Stats.DEFAULT.attack; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Stats.prototype, "speed", {
        get: function () { var _b; return (_b = this._speed) !== null && _b !== void 0 ? _b : Stats.DEFAULT.speed; },
        enumerable: false,
        configurable: true
    });
    var _a;
    _a = Stats;
    Stats._DEFAULT = null;
    (function () {
        _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(_a, function (x) { return [
            x._health,
            x._energy,
            x._attack,
            x._speed
        ]; });
    })();
    return Stats;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.Serializable));



/***/ }),

/***/ 3322:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Terrain": () => (/* binding */ Terrain)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a;
var _this = undefined;

var Terrain = /** @class */ (function (_super) {
    __extends(Terrain, _super);
    function Terrain() {
        var _this = _super.call(this) || this;
        // ---------------------------------------------------------------------------------------------
        _this._isActivated = true;
        _this._colliders = new Array();
        _this._name = "Terrain";
        return _this;
    }
    Object.defineProperty(Terrain.prototype, "isActivated", {
        /** @inheritdoc */
        get: function () { return this._isActivated; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    Terrain.CreateDefault = function () { return new this(); };
    Terrain.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable.MakeSerializable(this, function (x) { return [
            x._isActivated,
            x._colliders
        ]; });
    };
    // ---------------------------------------------------------------------------------------------
    Terrain.prototype.addChild = function () {
        var childs = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            childs[_i] = arguments[_i];
        }
        var r = _super.prototype.addChild.apply(this, childs);
        // iterate through each collider in the children being added to the terrain
        for (var i = childs.length - 1; i >= 0; i--) {
            var child = childs[i];
            if (!(child instanceof _internal__WEBPACK_IMPORTED_MODULE_0__.Collider)) {
                continue;
            }
            // add the collider to the terrain collider array
            this._colliders.push(child);
            child.isSolid = true;
        }
        return r;
    };
    Terrain.prototype.removeChild = function () {
        var childs = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            childs[_i] = arguments[_i];
        }
        var r = _super.prototype.removeChild.apply(this, childs);
        // iterate through each collider in the children being removed from the terrain
        for (var i = childs.length - 1; i >= 0; i--) {
            if (!(childs[i] instanceof _internal__WEBPACK_IMPORTED_MODULE_0__.Collider)) {
                continue;
            }
            // get the index of the collider in the collider array and remove it
            var colIndex = this._colliders.indexOf(childs[i]);
            if (colIndex >= 0) {
                this._colliders.splice(colIndex, 1);
            }
        }
        return r;
    };
    // ---------------------------------------------------------------------------------------------
    Terrain.prototype.SetActivated = function (active) {
        if (active)
            this.Activate();
        else
            this.Deactivate();
    };
    Terrain.prototype.Activate = function () {
        if (this.isActivated)
            return;
        for (var i = this._colliders.length - 1; i >= 0; i--) {
            this._colliders[i].SetActivated(true);
        }
        this._isActivated = true;
    };
    Terrain.prototype.Deactivate = function () {
        if (!this.isActivated)
            return;
        for (var i = this._colliders.length - 1; i >= 0; i--) {
            this._colliders[i].SetActivated(false);
        }
        this._isActivated = false;
    };
    return Terrain;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.SceneEntity));

_a = Terrain;
(function () {
    _a.SetSerializableFields();
})();


/***/ }),

/***/ 3668:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PropertyType": () => (/* binding */ PropertyType),
/* harmony export */   "EditorProperty": () => (/* binding */ EditorProperty)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

/** the type that the object property is */
var PropertyType;
(function (PropertyType) {
    PropertyType[PropertyType["bool"] = 0] = "bool";
    PropertyType[PropertyType["string"] = 1] = "string";
    PropertyType[PropertyType["decimal"] = 2] = "decimal";
    PropertyType[PropertyType["integer"] = 3] = "integer";
    PropertyType[PropertyType["vector2"] = 4] = "vector2";
    PropertyType[PropertyType["keyValuePair"] = 5] = "keyValuePair";
    PropertyType[PropertyType["entityReference"] = 6] = "entityReference";
})(PropertyType || (PropertyType = {}));
/** used in level editor for changing properties of an object in a level */
var EditorProperty = /** @class */ (function () {
    function EditorProperty() {
        /// --------------------------------------------------------------------------------------------
        this._name = "";
        this._funcGet = null;
        this._funcSet = null;
    }
    EditorProperty.Create = function (name, type, getter, setter) {
        var r = new EditorProperty();
        r._name = name;
        r._propertyType = type;
        r._funcGet = getter;
        r._funcSet = setter;
        return r;
    };
    /**
     * create a vector property for the specified type
     * @param field the name of the field in the type that contains the vector object
     * @param name the name to call the property
     */
    EditorProperty.CreateVectProp = function (field, name) {
        return this.Create(name, PropertyType.vector2, function (target) { return target[field]; }, function (target, value) { target[field] = value; });
    };
    /**
     * create a string property for the specified type
     * @param field the name of the field in the type that contains the string value
     * @param name the name to call the property
     */
    EditorProperty.CreateStringProp = function (field, name, nullIfEmpty) {
        if (nullIfEmpty === void 0) { nullIfEmpty = false; }
        var setFunc = nullIfEmpty ?
            function (target, value) {
                var v = value;
                if (v.length <= 0)
                    v = null;
                target[field] = value;
            } :
            function (target, value) {
                var v = value;
                target[field] = value;
            };
        return this.Create(name, PropertyType.string, function (target) { return target[field]; }, setFunc);
    };
    EditorProperty.CreateNumberProp = function (field, name) {
        return this.Create(name, PropertyType.decimal, function (target) { return target[field]; }, function (target, value) { target[field] = value; });
    };
    EditorProperty.CreateBoolProp = function (field, name) {
        return this.Create(name, PropertyType.bool, function (target) { return target[field]; }, function (target, value) { target[field] = value; });
    };
    EditorProperty.CreateEntityProp = function (field, name) {
        return this.Create(name, PropertyType.entityReference, function (target) { return target[field]; }, function (target, value) { target[field] = value; });
    };
    EditorProperty.CreateKVProp = function (field, name, pairs) {
        var r = EditorKVProperty.Create(name, PropertyType.keyValuePair, function (target) { return target[field]; }, function (target, value) {
            target[field] = this.pairs[this.index].value;
        });
        r.pairs = pairs;
        return r;
    };
    Object.defineProperty(EditorProperty.prototype, "name", {
        get: function () { return this._name; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(EditorProperty.prototype, "propertyType", {
        get: function () { return this._propertyType; },
        enumerable: false,
        configurable: true
    });
    /// --------------------------------------------------------------------------------------------
    EditorProperty.prototype.GetValue = function (target) {
        return this._funcGet(target);
    };
    EditorProperty.prototype.SetValue = function (target, value) {
        this._funcSet(target, value);
    };
    /// --------------------------------------------------------------------------------------------
    EditorProperty.prototype.GetUIElement = function (instance) {
        var ths = this;
        var r = new _internal__WEBPACK_IMPORTED_MODULE_0__.UIElement();
        r.uiSize.x = 1;
        var title = _internal__WEBPACK_IMPORTED_MODULE_0__.UITextElement.CreateFromString(this._name);
        r.addChild(title);
        var titleHeight = title.GetUiRect().size.y;
        r.uiSizeOffset.y += titleHeight;
        var propValue = this.GetValue(instance);
        var valElement = null;
        switch (this.propertyType) {
            case PropertyType.string:
                valElement = EditorProperty.GetUIStringField(propValue);
                valElement.submitAction.AddListener(function (action) {
                    var str = valElement.GetChildOfType(_internal__WEBPACK_IMPORTED_MODULE_0__.UITextInputField).text.text;
                    ths.SetValue(instance, str);
                });
                break;
            case PropertyType.decimal:
                valElement = EditorProperty.GetUINumberField(propValue);
                valElement.submitAction.AddListener(function (action) {
                    var str = valElement.GetChildOfType(_internal__WEBPACK_IMPORTED_MODULE_0__.UITextInputField).text.text;
                    ths.SetValue(instance, Number.parseFloat(str));
                });
                break;
            case PropertyType.integer:
                valElement = EditorProperty.GetUINumberField(propValue);
                valElement.submitAction.AddListener(function (action) {
                    var str = valElement.GetChildOfType(_internal__WEBPACK_IMPORTED_MODULE_0__.UITextInputField).text.text;
                    ths.SetValue(instance, Number.parseFloat(str));
                });
                break;
            case PropertyType.bool:
                valElement = EditorProperty.GetUIBoolField(propValue);
                valElement.submitAction.AddListener(function (action) {
                    var val = valElement.GetChildOfType(_internal__WEBPACK_IMPORTED_MODULE_0__.UICheckboxElement).value;
                    ths.SetValue(instance, val);
                });
                break;
            case PropertyType.vector2:
                valElement = EditorProperty.GetUIVectorField(propValue);
                valElement.submitAction.AddListener(function (action) {
                    var xstr = valElement.entities[0].text.text;
                    var ystr = valElement.entities[1].text.text;
                    ths.SetValue(instance, _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(Number.parseFloat(xstr), Number.parseFloat(ystr)));
                });
                break;
            case PropertyType.entityReference:
                valElement = EditorProperty.GetUIEntityField(this, instance, propValue);
                break;
        }
        if (valElement != null) {
            valElement.uiOffset.y += titleHeight;
            r.addChild(valElement);
            r.uiSizeOffset.y += valElement.GetUiRect().size.y + 5;
        }
        return r;
    };
    /// --------------------------------------------------------------------------------------------
    EditorProperty.GetUIVectorField = function (value) {
        if (value == null)
            value = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create();
        var xElem = _internal__WEBPACK_IMPORTED_MODULE_0__.UITextElement.CreateFromString(value.x.toString()).ToInputField(_internal__WEBPACK_IMPORTED_MODULE_0__.TextInputMode.decimal);
        xElem.name = "X Input";
        xElem.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(50, 15);
        xElem.UpdatePanelGraphic();
        xElem.UpdatePanelCollider();
        var yElem = _internal__WEBPACK_IMPORTED_MODULE_0__.UITextElement.CreateFromString(value.y.toString()).ToInputField(_internal__WEBPACK_IMPORTED_MODULE_0__.TextInputMode.decimal);
        yElem.name = "Y Input";
        yElem.uiOffset.x = 65;
        yElem.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(50, 15);
        yElem.UpdatePanelGraphic();
        yElem.UpdatePanelCollider();
        var valElement = new _internal__WEBPACK_IMPORTED_MODULE_0__.UIInteractiveElement();
        valElement.uiOffset.x = 15;
        valElement.uiSizeOffset.x = -15;
        valElement.uiSize.x = 1;
        valElement.addChild(xElem, yElem);
        var func_submit = function (action) {
            valElement.Submit(action);
        };
        xElem.submitAction.AddListener(func_submit);
        yElem.submitAction.AddListener(func_submit);
        valElement.ExpandToFitChildren();
        return valElement;
    };
    EditorProperty.GetUIStringField = function (value) {
        if (value == null)
            value = "";
        var nameInput = _internal__WEBPACK_IMPORTED_MODULE_0__.UITextElement.CreateFromString(value).ToInputField(_internal__WEBPACK_IMPORTED_MODULE_0__.TextInputMode.allowEverything);
        nameInput.name = "Name Input";
        nameInput.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(100, 15);
        nameInput.UpdatePanelGraphic();
        nameInput.UpdatePanelCollider();
        var valElement = new _internal__WEBPACK_IMPORTED_MODULE_0__.UIInteractiveElement();
        valElement.uiOffset.x = 15;
        valElement.uiSizeOffset.x = -15;
        valElement.uiSize.x = 1;
        valElement.addChild(nameInput);
        var func_submit = function (action) {
            valElement.Submit(action);
        };
        nameInput.submitAction.AddListener(func_submit);
        valElement.ExpandToFitChildren();
        return valElement;
    };
    EditorProperty.GetUINumberField = function (value, onlyInteger) {
        if (onlyInteger === void 0) { onlyInteger = false; }
        if (value == null)
            value = 0;
        var numInput = _internal__WEBPACK_IMPORTED_MODULE_0__.UITextElement.CreateFromString(value.toString()).ToInputField(_internal__WEBPACK_IMPORTED_MODULE_0__.TextInputMode.integer);
        numInput.name = "Number Input";
        numInput.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(100, 15);
        numInput.UpdatePanelGraphic();
        numInput.UpdatePanelCollider();
        var valElement = new _internal__WEBPACK_IMPORTED_MODULE_0__.UIInteractiveElement();
        valElement.uiOffset.x = 15;
        valElement.uiSizeOffset.x = -15;
        valElement.uiSize.x = 1;
        valElement.addChild(numInput);
        var func_submit = function (action) {
            valElement.Submit(action);
        };
        numInput.submitAction.AddListener(func_submit);
        valElement.ExpandToFitChildren();
        return valElement;
    };
    EditorProperty.GetUIBoolField = function (value) {
        if (value == null)
            value = false;
        var boolInput = new _internal__WEBPACK_IMPORTED_MODULE_0__.UICheckboxElement(value);
        boolInput.name = "Bool Input";
        var valElement = new _internal__WEBPACK_IMPORTED_MODULE_0__.UIInteractiveElement();
        valElement.uiOffset.x = 15;
        valElement.addChild(boolInput);
        valElement.ExpandToFitChildren();
        boolInput.UpdatePanelGraphic();
        boolInput.UpdatePanelCollider();
        var func_submit = function (action) {
            valElement.Submit(action);
        };
        boolInput.submitAction.AddListener(func_submit);
        return valElement;
    };
    EditorProperty.GetUIEntityField = function (prop, target, value) {
        var _a;
        var buttonInput = new _internal__WEBPACK_IMPORTED_MODULE_0__.UIInteractiveElement();
        buttonInput.uiOffset.x = 15;
        buttonInput.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(100, 15);
        buttonInput.UpdatePanelGraphic();
        buttonInput.UpdatePanelCollider();
        var txtElem = _internal__WEBPACK_IMPORTED_MODULE_0__.UITextElement.CreateFromString((_a = value === null || value === void 0 ? void 0 : value.name) !== null && _a !== void 0 ? _a : "null");
        buttonInput.addChild(txtElem);
        buttonInput.ExpandToFitChildren();
        var func_submit = function () {
            txtElem.text.text = "<select entity>";
            var editor = buttonInput.ParentContainer.parentRootScene;
            if (!(editor instanceof _internal__WEBPACK_IMPORTED_MODULE_0__.LevelEditor)) {
                throw "property can only be modified within an editor scene";
            }
            editor.StartEntitySelection(prop, target, txtElem);
        };
        buttonInput.submitAction.AddListener(func_submit);
        return buttonInput;
    };
    return EditorProperty;
}());

var EditorKVProperty = /** @class */ (function (_super) {
    __extends(EditorKVProperty, _super);
    function EditorKVProperty() {
        var _this = _super.call(this) || this;
        _this._index = 0;
        _this.pairs = new Array();
        return _this;
    }
    EditorKVProperty.Create = function (name, type, getter, setter) {
        var r = new EditorKVProperty();
        r._name = name;
        r._propertyType = PropertyType.keyValuePair;
        r._funcGet = getter;
        r._funcSet = setter;
        return r;
    };
    Object.defineProperty(EditorKVProperty.prototype, "index", {
        get: function () { return this._index; },
        enumerable: false,
        configurable: true
    });
    EditorKVProperty.prototype.CycleIndexForward = function () {
        if (++this._index >= this.pairs.length)
            this._index = 0;
    };
    EditorKVProperty.prototype.CycleIndexBack = function () {
        if (--this._index < 0)
            this._index = this.pairs.length - 1;
    };
    EditorKVProperty.prototype.GetValueString = function (instance) {
        var val = this.GetValue(instance);
        var propString = val.toString();
        for (var i = this.pairs.length - 1; i >= 0; i--) {
            if (this.pairs[i].value == val) {
                propString = this.pairs[i].key;
                break;
            }
        }
        return propString;
    };
    EditorKVProperty.prototype.GetUIElement = function (instance) {
        var ths = this;
        var r = _super.prototype.GetUIElement.call(this, instance);
        var oHeight = r.GetUiRect().size.y;
        var longestKeyStr = "";
        for (var i = this.pairs.length - 1; i >= 0; i--) {
            if (this.pairs[i].key.length > longestKeyStr.length)
                longestKeyStr = this.pairs[i].key;
        }
        var propString = this.GetValueString(instance);
        var displayText = _internal__WEBPACK_IMPORTED_MODULE_0__.UITextElement.CreateFromString(longestKeyStr);
        displayText.AlignedCenter();
        displayText.text.text = propString;
        var cycleInput = new _internal__WEBPACK_IMPORTED_MODULE_0__.UIInteractiveElement();
        cycleInput.addChild(displayText);
        cycleInput.ExpandToFitChildren();
        cycleInput.name = "KV Cycle Input";
        var valElement = new _internal__WEBPACK_IMPORTED_MODULE_0__.UIInteractiveElement();
        valElement.uiOffset.x = 15;
        valElement.addChild(cycleInput);
        valElement.ExpandToFitChildren();
        cycleInput.UpdatePanelGraphic();
        cycleInput.UpdatePanelCollider();
        var func_submit = function (action) {
            if (action == 0) {
                ths.CycleIndexForward();
            }
            else {
                ths.CycleIndexBack();
            }
            var val = ths.pairs[ths.index].value;
            ths.SetValue(instance, val);
            displayText.text.text = ths.GetValueString(instance);
            valElement.Submit(action);
        };
        cycleInput.submitAction.AddListener(func_submit);
        if (valElement != null) {
            valElement.uiOffset.y += oHeight;
            r.addChild(valElement);
            r.uiSizeOffset.y += valElement.GetUiRect().size.y + 5;
        }
        return r;
    };
    return EditorKVProperty;
}(EditorProperty));


/***/ }),

/***/ 4630:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IEditorResizable": () => (/* binding */ IEditorResizable)
/* harmony export */ });
var IEditorResizable;
(function (IEditorResizable) {
    /** type checking and discriminator for IEditorResizable */
    function ImplementedIn(obj) {
        var r = obj;
        return (r.ClosestPoint !== undefined &&
            r.GetResizeRect !== undefined &&
            r.SetResizeRect !== undefined);
    }
    IEditorResizable.ImplementedIn = ImplementedIn;
})(IEditorResizable || (IEditorResizable = {}));


/***/ }),

/***/ 3141:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SelectionModifier": () => (/* binding */ SelectionModifier),
/* harmony export */   "LevelEditor": () => (/* binding */ LevelEditor)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var SelectionModifier;
(function (SelectionModifier) {
    SelectionModifier[SelectionModifier["neutral"] = 0] = "neutral";
    SelectionModifier[SelectionModifier["positive"] = 1] = "positive";
    SelectionModifier[SelectionModifier["negative"] = -1] = "negative";
})(SelectionModifier || (SelectionModifier = {}));
var EditMode;
(function (EditMode) {
    EditMode[EditMode["none"] = 0] = "none";
    EditMode[EditMode["select"] = 1] = "select";
    EditMode[EditMode["terrain"] = 2] = "terrain";
    EditMode[EditMode["entities"] = 3] = "entities";
})(EditMode || (EditMode = {}));
(function (EditMode) {
    function ToString(mode) {
        switch (mode) {
            case EditMode.select: return "selection";
            case EditMode.terrain: return "terrain";
            case EditMode.entities: return "entities";
        }
        return "none";
    }
    EditMode.ToString = ToString;
})(EditMode || (EditMode = {}));
var LevelEditor = /** @class */ (function (_super) {
    __extends(LevelEditor, _super);
    function LevelEditor(game) {
        var _this = _super.call(this, game) || this;
        //----------------------------------------------------------------------------------------------
        _this._isTesting = false;
        _this._uiContextMenu = null;
        _this._uiObjectPropertyWindow = null;
        _this._uiEditModeButton = null;
        _this._selectEntityProp = null;
        _this._selectEntityPropText = null;
        _this._selectEntityPropTarget = null;
        _this._dragGraphic = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Graphics();
        _this._selectedObjectsGraphic = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Graphics();
        _this._emphasizedSelectionEntity = null;
        _this._selectedObjects = new Array();
        _this._clipboardEntities = new Array();
        _this._selectedCreationObject = new _internal__WEBPACK_IMPORTED_MODULE_1__.Seeker();
        _this._entityCreationPool = [
            new _internal__WEBPACK_IMPORTED_MODULE_1__.Player(),
            new _internal__WEBPACK_IMPORTED_MODULE_1__.Seeker(),
            new _internal__WEBPACK_IMPORTED_MODULE_1__.Gunner(),
            new _internal__WEBPACK_IMPORTED_MODULE_1__.TestDummy(),
            new _internal__WEBPACK_IMPORTED_MODULE_1__.Spikes(),
            new _internal__WEBPACK_IMPORTED_MODULE_1__.JumpPad()
        ];
        _this._editMode = EditMode.select;
        _this._snapGridSize = 5;
        _this._isMouseHeld = false;
        _this._isSelecting = false;
        _this._isMoving = false;
        _this._isRectEditing = false;
        _this._rectEditSide = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.none;
        _this._isPanning = false;
        _this._selectionMod = SelectionModifier.neutral;
        _this._dragStart = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        _this._lastDragPos = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        _this._camTransform = pixi_js__WEBPACK_IMPORTED_MODULE_0__.Matrix.IDENTITY.clone();
        _this._testScene = new _internal__WEBPACK_IMPORTED_MODULE_1__.GameScene(game);
        _this._editorRoot = new _internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity();
        _this._objectRoot = new _internal__WEBPACK_IMPORTED_MODULE_1__.Scene(game);
        _this.InitializeUI();
        _this.InitializeInputControls();
        _this._editorRoot.addChild(_this._dragGraphic);
        _this._editorRoot.addChild(_this._selectedObjectsGraphic);
        return _this;
    }
    Object.defineProperty(LevelEditor.prototype, "isSelectingEntity", {
        get: function () { return this._selectEntityProp != null; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(LevelEditor.prototype, "worldMousePos", {
        get: function () {
            return this._camTransform.applyInverse(this.parentGame.mousePos);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(LevelEditor.prototype, "isUIEnabled", {
        get: function () { return this._uiRoot.parentEntity == this; },
        enumerable: false,
        configurable: true
    });
    //----------------------------------------------------------------------------------------------
    /** @inheritdoc */
    LevelEditor.prototype.Update = function (deltaTime) {
        var keys = this.parentGame.keysPressed;
        this._control_testLevel.Update(this.parentGame);
        if (this._isTesting) {
            this._testScene.Update(deltaTime);
            if (this.parentGame.keysPressed[17] && this._control_testLevel.IsTriggered()) {
                this.EndTesting();
            }
        }
        else {
            this.EditorUpdate(deltaTime);
            if (this.parentGame.keysPressed[17] && this._control_testLevel.IsTriggered()) {
                this.BeginTesting();
            }
        }
    };
    /** @inheritdoc */
    LevelEditor.prototype.Draw = function (renderer) {
        if (this._isTesting) {
            this._testScene.Draw(renderer);
            return;
        }
        else {
            this.DrawSelectedObjects();
            renderer.render(this._objectRoot, {
                clear: false,
                transform: this._camTransform
            });
            renderer.render(this._editorRoot, {
                clear: false,
                transform: this._camTransform
            });
            if (this.isUIEnabled) {
                this._uiRoot.DrawUI(renderer);
            }
        }
    };
    //----------------------------------------------------------------------------------------------
    /** save the level to LocalStorage under the temporary storage key */
    LevelEditor.prototype.SaveLevelCurrent = function () {
        var lvl = this.GetSerializedLevel();
        console.log("Saving level to local storage:");
        console.log(lvl);
        localStorage.setItem(LevelEditor.STORAGEKEY_RECENTLEVEL, lvl);
    };
    /** load the level from the current local storage temporary key */
    LevelEditor.prototype.LoadLevelCurrent = function () {
        var sLev = localStorage.getItem(LevelEditor.STORAGEKEY_RECENTLEVEL);
        console.log("Loading Level...");
        console.log(sLev);
        var lvl = _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.GetDeserializedObject(JSON.parse(sLev));
        this.EditGameSene(lvl);
    };
    /** save the level to a specific key on the local storage */
    LevelEditor.prototype.SaveLevelLocalStorage = function () {
        var saveKey = prompt("Enter Level Save Name:");
        var lvl = this.GetSerializedLevel();
        localStorage.setItem(LevelEditor.STORAGEKEY_BASE + saveKey, lvl);
        this.parentGame.ResetKeyboardInput();
    };
    /** load the level from a specific key on the local storage */
    LevelEditor.prototype.LoadLevelLocalStorage = function () {
        var saveKey = prompt("Enter Level Load Name:");
        var sLev = localStorage.getItem(LevelEditor.STORAGEKEY_BASE + saveKey);
        var lvl = _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.GetDeserializedObject(JSON.parse(sLev));
        this.EditGameSene(lvl);
        this.parentGame.ResetKeyboardInput();
    };
    /** download level file */
    LevelEditor.prototype.SaveLevelAs = function () {
        var filename = prompt("Enter File Name:", "level") + ".lvl";
        var lvlData = this.GetSerializedLevel();
        var blob = new Blob([lvlData], { type: 'text/plain' });
        var a = document.createElement("a");
        a.download = filename;
        a.href = URL.createObjectURL(blob);
        a.click();
        this.parentGame.ResetKeyboardInput();
    };
    /** upload level file */
    LevelEditor.prototype.LoadLevelFile = function () {
        var fileInput = document.createElement("input");
        fileInput.type = "file";
        var ths = this;
        fileInput.addEventListener("change", function (e) {
            this.files[0].text().then(function (value) {
                ths.LoadLevelFromJSON(value);
            });
        });
        fileInput.click();
        this.parentGame.ResetKeyboardInput();
    };
    LevelEditor.prototype.LoadLevelFromJSON = function (json) {
        var lvl = _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.GetDeserializedObject(JSON.parse(json));
        this.EditGameSene(lvl);
    };
    /**
     * clones the game scene into the level editor's object root so that it can be edited
     * @param scene the scene to start editing
     */
    LevelEditor.prototype.EditGameSene = function (scene) {
        this._objectRoot = scene.Clone();
    };
    /**
     * Serializes the current level state and runs the game from the current level that is open in
     * the level editor. The level is then returned to the original state when EndTesting()
     * is called
     */
    LevelEditor.prototype.BeginTesting = function () {
        if (this._isTesting)
            return;
        this._testScene = this._objectRoot.Clone();
        this._isTesting = true;
        this._testScene.parentGame = this.parentGame;
        this._requestPointerLock = true;
    };
    /** Returns the level editor back to the edit mode if it's running from BeginTesting() */
    LevelEditor.prototype.EndTesting = function () {
        if (!this._isTesting)
            return;
        this._isTesting = false;
        this._testScene.RemoveFromScene();
        this._testScene = null;
        this._requestPointerLock = false;
        document.exitPointerLock();
    };
    /**
     * copy the specified entities into the clipboard
     * @param entities the entitys to copy
     */
    LevelEditor.prototype.CopyToClipboard = function () {
        var entities = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            entities[_i] = arguments[_i];
        }
        // do nothing if no entities specified
        if (entities.length <= 0) {
            return;
        }
        // clear the clipboard
        this._clipboardEntities.splice(0);
        // clone each entity into the clipboard in order
        for (var i = 0; i < entities.length; i++) {
            var ent = entities[i];
            var obj = ent.Clone();
            if (ent.parentEntity instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.Terrain) {
                obj._parentEntity = ent.parentEntity;
            }
            this._clipboardEntities.push(obj);
        }
    };
    /**
     * paste the entities from the clipboard
     * @param whether or not the pasted objects should be selected when they are created
     */
    LevelEditor.prototype.PasteFromClipboard = function (selectPastedObjects) {
        // clear the selected objects if necessary
        if (selectPastedObjects) {
            this._selectedObjects.splice(0);
        }
        // iterate through the clipboard and add each entity to the level
        for (var i = 0; i < this._clipboardEntities.length; i++) {
            // clone the object
            var ent = this._clipboardEntities[i].Clone();
            // if a parent is specified, add it to that parent's children
            if (this._clipboardEntities[i].parentEntity != null) {
                this._clipboardEntities[i].parentEntity.addChild(ent);
            }
            // if a parent is not specified, add it to the level's object root
            else {
                this._objectRoot.addChild(ent);
            }
            // select the object if necessary
            if (selectPastedObjects) {
                this._selectedObjects.push(ent);
            }
        }
    };
    //----------------------------------------------------------------------------------------------
    /** Update logic for level editor when in edit mode */
    LevelEditor.prototype.EditorUpdate = function (deltaTime) {
        this._objectRoot.Update(0);
        this._editorRoot.Update(deltaTime);
        this._uiRoot.Update(deltaTime);
        this.HandleEditorMouse();
        this.HandleEditorControls();
    };
    LevelEditor.prototype.InitializeInputControls = function () {
        this._control_testLevel = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.keyboardButton, 13, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.onPress); // ENTER
        this._control_mousePrimary = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.mouseButton, 0, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.whileHeld); // mouse l
        this._control_mouseSecondary = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.mouseButton, 2, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.whileHeld); // mouse r
        this._control_toggleUI = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.keyboardButton, 9, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.onPress); // TAB
        this._control_delete = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.keyboardButton, 46, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.onPress); // DEL
        this._control_selectMode = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.keyboardButton, 87, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.onPress); // W
        this._control_terrainMode = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.keyboardButton, 81, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.onPress); // Q
        this._control_entityMode = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.keyboardButton, 69, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.onPress); // E
        this._control_quickSave = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.keyboardButton, 83, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.onPress); // S
        this._control_quickLoad = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.keyboardButton, 76, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.onPress); // L
        this._control_selectionModPositive = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.keyboardButton, 17, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.whileHeld); // CTRL
        this._control_selectionModNegative = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.keyboardButton, 18, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.whileHeld); // ALT
        this._control_copy = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.keyboardButton, 67, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.onPress); // C
        this._control_paste = _internal__WEBPACK_IMPORTED_MODULE_1__.InputControl.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.InputType.keyboardButton, 86, _internal__WEBPACK_IMPORTED_MODULE_1__.InputCondition.onPress); // V
    };
    LevelEditor.prototype.HandleEditorControls = function () {
        // update the state of each control
        this._control_mousePrimary.Update(this.parentGame);
        this._control_mouseSecondary.Update(this.parentGame);
        this._control_toggleUI.Update(this.parentGame);
        this._control_delete.Update(this.parentGame);
        this._control_selectMode.Update(this.parentGame);
        this._control_terrainMode.Update(this.parentGame);
        this._control_entityMode.Update(this.parentGame);
        this._control_quickSave.Update(this.parentGame);
        this._control_quickLoad.Update(this.parentGame);
        this._control_selectionModPositive.Update(this.parentGame);
        this._control_selectionModNegative.Update(this.parentGame);
        this._control_copy.Update(this.parentGame);
        this._control_paste.Update(this.parentGame);
        // selection modifier keys
        if (this._control_selectionModPositive.IsTriggered()) {
            this._selectionMod = SelectionModifier.positive;
        }
        else if (this._control_selectionModNegative.IsTriggered()) {
            this._selectionMod = SelectionModifier.negative;
        }
        else {
            this._selectionMod = SelectionModifier.neutral;
        }
        // set the selection modifier to mimic the level editor modifier
        this._uiRoot.uiModifier = this._selectionMod;
        // don't perform any control actions if a ui element is in focus
        if (this._uiRoot.GetFocusedElements().length > 0) {
            return;
        }
        // show / hide ui key
        if (this._control_toggleUI.IsTriggered()) {
            this.SetUIEnabled(!this.isUIEnabled);
        }
        // delete key
        if (this._control_delete.IsTriggered()) {
            this.DeleteSelectedObjects();
        }
        // edit mode selections:
        if (this._control_selectMode.IsTriggered()) {
            this.CloseContextMenu();
            this._editMode = EditMode.select;
            this.UpdateEditModeText();
        }
        if (this._control_terrainMode.IsTriggered()) {
            this.CloseContextMenu();
            this._editMode = EditMode.terrain;
            this.UpdateEditModeText();
        }
        if (this._control_entityMode.IsTriggered()) {
            this.CloseContextMenu();
            this._editMode = EditMode.entities;
            this.UpdateEditModeText();
        }
        // if CTRL is pressed
        if (this.parentGame.keysPressed[17]) {
            // if SHIFT is pressed
            if (this.parentGame.keysPressed[16]) {
                // local storage save
                if (this._control_quickSave.IsTriggered()) {
                    this.SaveLevelLocalStorage();
                }
                // local storage load
                if (this._control_quickLoad.IsTriggered()) {
                    this.LoadLevelLocalStorage();
                }
            }
            // if ALT is pressed
            else if (this.parentGame.keysPressed[18]) {
                // file save
                if (this._control_quickSave.IsTriggered()) {
                    this.SaveLevelAs();
                }
                // file load
                if (this._control_quickLoad.IsTriggered()) {
                    this.LoadLevelFile();
                }
            }
            // if SHIFT and ALT are not pressed
            else {
                // quick save
                if (this._control_quickSave.IsTriggered()) {
                    this.SaveLevelCurrent();
                }
                // quick load
                if (this._control_quickLoad.IsTriggered()) {
                    this.LoadLevelCurrent();
                }
            }
            // copy key
            if (this._control_copy.IsTriggered()) {
                this.CopyToClipboard.apply(this, this._selectedObjects);
            }
            // paste key
            if (this._control_paste.IsTriggered()) {
                this.PasteFromClipboard(true);
            }
        }
    };
    LevelEditor.prototype.HandleEditorMouse = function () {
        // on mouse is pressed
        if (this.parentGame.mousePressed) {
            // on mouse click
            if (!this._isMouseHeld) {
                this.HandleEditorClick(this.parentGame.mouseButton);
                this._dragStart = this._camTransform.applyInverse(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Clone(this.parentGame.mousePos));
                this._lastDragPos = this._camTransform.applyInverse(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Clone(this.parentGame.mousePos));
                this._isMouseHeld = true;
            }
            // on mouse held
            else {
                this.HandleEditorMouseDrag();
            }
        }
        // on mouse release
        else if (this._isMouseHeld) {
            this.HandleEditorReleaseClick();
            this._isMouseHeld = false;
        }
        // on mouse hover
        else {
            this.HandleEditorMouseHover();
        }
        // on mouse scroll
        if (this.parentGame.mouseScroll != 0) {
            var zoom = _internal__WEBPACK_IMPORTED_MODULE_1__.Matrix.GetScale(this._camTransform).x;
            var mod = -this.parentGame.mouseScroll > 0 ? 1.414213562373095 : 0.7071067811865475;
            this.SetZoomLevel(zoom * mod);
        }
    };
    //----------------------------------------------------------------------------------------------
    LevelEditor.prototype.HandleEditorClick = function (button) {
        var uiElemsClicked = this._uiRoot.GetUIElementsAtPoint(this.parentGame.mousePos);
        if (uiElemsClicked.length > 0) {
            // if context menu focus is lost, close it
            if (this._uiContextMenu != null && !uiElemsClicked.includes(this._uiContextMenu)) {
                this.CloseContextMenu();
            }
            // handle the action for the ui elements that were clicked
            this.HandleUIElementAction(uiElemsClicked, button);
            return;
        }
        else {
            this.CloseContextMenu();
        }
        if (button == 0) {
            // if the mouse is over a selected object
            var selObjsHover = this.GetSelectedObjectsOnMouse();
            if (selObjsHover.length > 0) {
                this._isMoving = true;
            }
            // if the mouse isn't over a selected object
            else {
                switch (this._editMode) {
                    case EditMode.select:
                    case EditMode.terrain:
                        this.StartMouseSelecting();
                        break;
                    case EditMode.entities:
                        this.PlaceEntity();
                        break;
                }
            }
        }
        else if (button == 1) {
            this._isPanning = true;
        }
        else if (button == 2) {
            this.StartMouseRectEditing();
        }
    };
    LevelEditor.prototype.HandleEditorMouseDrag = function () {
        var mousePos = this.worldMousePos;
        var delta = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Subtract(mousePos, this._lastDragPos);
        if (delta.x == 0 && delta.y == 0)
            return;
        // handle selection rect
        if (this._isSelecting) {
            this._dragGraphic.clear();
            this._dragGraphic.lineStyle({
                color: 0x00FF00,
                width: 1
            });
            var rect = this.GetMouseDragArea(this._editMode == EditMode.terrain);
            this._dragGraphic.drawRect(rect.position.x, rect.position.y, rect.size.x, rect.size.y);
        }
        // handle moving the selected objects
        if (this._isMoving) {
            // snap mouse delta to grid
            delta = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(delta, 1 / this._snapGridSize);
            delta.x = Math.round(delta.x);
            delta.y = Math.round(delta.y);
            delta = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(delta, this._snapGridSize);
            // move selected objects if necessary
            if (delta.x != 0 || delta.y != 0) {
                for (var i = this._selectedObjects.length - 1; i >= 0; i--) {
                    // get a reference to the object and the object that will be moved
                    var obj = this._selectedObjects[i];
                    var movedObj = obj;
                    // move the object 
                    var pos = movedObj.position;
                    movedObj.position.set(pos.x + delta.x, pos.y + delta.y);
                }
            }
        }
        // handle editing the collider rects
        if (this._isRectEditing) {
            // snap mouse delta to grid
            delta = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(delta, 1 / this._snapGridSize);
            delta.x = Math.round(delta.x);
            delta.y = Math.round(delta.y);
            delta = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(delta, this._snapGridSize);
            this.HandleRectEditing(this._rectEditSide, delta);
        }
        // handle camera pan
        if (this._isPanning) {
            this.PanCamera(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(delta.x, delta.y));
            this._dragStart.x -= delta.x;
            this._dragStart.y -= delta.y;
            this._lastDragPos.x -= delta.x;
            this._lastDragPos.y -= delta.y;
        }
        this._lastDragPos = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Add(this._lastDragPos, delta);
    };
    LevelEditor.prototype.HandleEditorReleaseClick = function () {
        this._dragGraphic.visible = false;
        this._isPanning = false;
        switch (this._editMode) {
            case EditMode.terrain:
                if (this.parentGame.mouseButton == 0)
                    this.AddSelectionRectTerrain(this.GetMouseDragArea(true));
                break;
            case EditMode.select:
                if (this._isSelecting) {
                    this.SelectArea(this.GetMouseDragArea(false), this._selectionMod);
                    this._isSelecting = false;
                }
                break;
        }
        if (this._isMoving) {
            this._isMoving = false;
        }
        if (this._isRectEditing) {
            this._isRectEditing = false;
        }
    };
    LevelEditor.prototype.HandleEditorMouseHover = function () {
        var elems = this._uiRoot.GetUIElementsAtPoint(this.parentGame.mousePos);
        // iterate through each element that the mouse is hovering over
        var didSet = false;
        for (var i = elems.length - 1; i >= 0; i--) {
            // if the element is a child of the ui selection element, emphasize this element
            var elem = elems[i];
            if (elem instanceof UIElemEntityLink &&
                elem.parentEntity == this._uiSelectedObjectsRoot) {
                this._emphasizedSelectionEntity = elem.linkedEntity;
                didSet = true;
                break;
            }
        }
        // unset the emphasized object if mouse is not over a corresponding element
        if (!didSet) {
            this._emphasizedSelectionEntity = null;
        }
    };
    //----------------------------------------------------------------------------------------------
    LevelEditor.prototype.StartMouseSelecting = function () {
        this._isSelecting = true;
        this._dragGraphic.clear();
        this._dragGraphic.lineStyle({
            color: 0xFFFFFF,
            width: 1
        });
        this._dragGraphic.visible = true;
    };
    LevelEditor.prototype.StartMouseRectEditing = function () {
        var worldMouse = this.worldMousePos;
        // iterate through each selected object
        var editRect = null;
        var closestPoint = null;
        var closest = Number.POSITIVE_INFINITY;
        for (var i = this._selectedObjects.length - 1; i >= 0; i--) {
            // if the object is a aabbcollider
            var sel = this._selectedObjects[i];
            if (_internal__WEBPACK_IMPORTED_MODULE_1__.IEditorResizable.ImplementedIn(sel)) {
                // if the the mouse is closer to this one than the others, pick it
                var point = sel.ClosestPoint(worldMouse);
                var dist = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Distance(worldMouse, point);
                if (dist < closest) {
                    editRect = sel;
                    closest = dist;
                    closestPoint = point;
                }
            }
        }
        // if no editable rects were found, do nothing
        if (editRect == null) {
            return;
        }
        // set the appropriate edit side based on the which side the mouse is closest to
        this._rectEditSide = _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.ClosestSide(editRect.GetResizeRect(), closestPoint);
        this._isRectEditing = true;
    };
    LevelEditor.prototype.PlaceEntity = function () {
        var ent = this._selectedCreationObject.Clone();
        var pos = this.worldMousePos;
        pos.x = Math.round(pos.x / this._snapGridSize) * this._snapGridSize;
        pos.y = Math.round(pos.y / this._snapGridSize) * this._snapGridSize;
        ent.position.set(pos.x, pos.y);
        this._objectRoot.addChild(ent);
    };
    LevelEditor.prototype.HandleRectEditing = function (side, delta) {
        // do nothing if no mouse movement
        if (Math.abs(delta.x) <= 0 && Math.abs(delta.y) <= 0)
            return;
        // iterate through each selected AABBCollider
        for (var i = this._selectedObjects.length - 1; i >= 0; i--) {
            var sel = this._selectedObjects[i];
            if (_internal__WEBPACK_IMPORTED_MODULE_1__.IEditorResizable.ImplementedIn(sel)) {
                var rect = sel.GetResizeRect();
                // calculate the delta size and position from the mouse delta based on the specified
                // edit side
                var deltaSize = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
                var deltaPos = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
                switch (side) {
                    case _internal__WEBPACK_IMPORTED_MODULE_1__.Side.up:
                        deltaSize.y = -delta.y;
                        deltaPos.y = delta.y;
                        break;
                    case _internal__WEBPACK_IMPORTED_MODULE_1__.Side.down:
                        deltaSize.y = delta.y;
                        deltaPos.y = 0;
                        break;
                    case _internal__WEBPACK_IMPORTED_MODULE_1__.Side.left:
                        deltaSize.x = -delta.x;
                        deltaPos.x = delta.x;
                        break;
                    case _internal__WEBPACK_IMPORTED_MODULE_1__.Side.right:
                        deltaSize.x = delta.x;
                        deltaPos.x = 0;
                        break;
                }
                rect.position.x += deltaPos.x;
                rect.position.y += deltaPos.y;
                rect.size.x += deltaSize.x;
                rect.size.y += deltaSize.y;
                sel.SetResizeRect(rect);
            }
        }
    };
    //----------------------------------------------------------------------------------------------
    /** build the UI elements for the level editor scene */
    LevelEditor.prototype.InitializeUI = function () {
        // create the initial UI objects and parent them appropriately
        this._uiRoot = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIContainer(this.parentGame);
        this.addChild(this._uiRoot);
        // create right panel
        var rpanelWidth = 200;
        this._uiPanelRight = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIElement();
        this._uiRoot.addChild(this._uiPanelRight);
        this._uiPanelRight.uiOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this.parentGame.renderer.width - rpanelWidth, 0);
        this._uiPanelRight.uiSize = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, 1);
        this._uiPanelRight.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(rpanelWidth, 0);
        this._uiPanelRight.UpdatePanelGraphic();
        this._uiPanelRight.UpdatePanelCollider();
        // create bottom panel
        var bpanelWidth = 250;
        var bpanelHeight = 40;
        this._uiPanelBottom = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIElement();
        this._uiRoot.addChild(this._uiPanelBottom);
        this._uiPanelBottom.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(bpanelWidth, bpanelHeight);
        this._uiPanelBottom.uiOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this.parentGame.renderer.width - rpanelWidth - bpanelWidth, this.parentGame.renderer.height - bpanelHeight);
        this._uiPanelBottom.UpdatePanelGraphic();
        this._uiPanelBottom.UpdatePanelCollider();
        // entity selection button
        var entitySelection = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIInteractiveElement();
        this._uiPanelBottom.addChild(entitySelection);
        entitySelection.uiAnchor = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(1, 0);
        entitySelection.uiPivot = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(1, 0);
        entitySelection.uiOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(-5, 5);
        entitySelection.uiSize = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0.5, 1);
        entitySelection.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(-7, -10);
        entitySelection.UpdatePanelCollider();
        entitySelection.UpdatePanelGraphic();
        var entSelText = _internal__WEBPACK_IMPORTED_MODULE_1__.UITextElement.CreateFromString(this._selectedCreationObject.name);
        entitySelection.addChild(entSelText.AlignedCenter());
        var ths = this;
        entitySelection.submitAction.AddListener(function (action) {
            ths._uiContextMenu = ths.CreateContextMenu_EntitySelection(entSelText);
        });
        // edit mode selection button
        var editModeButton = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIInteractiveElement();
        this._uiEditModeButton = editModeButton;
        this._uiPanelBottom.addChild(editModeButton);
        editModeButton.uiAnchor = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        editModeButton.uiPivot = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        editModeButton.uiOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(5, 5);
        editModeButton.uiSize = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0.5, 1);
        editModeButton.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(-7, -10);
        editModeButton.UpdatePanelCollider();
        editModeButton.UpdatePanelGraphic();
        this.UpdateEditModeText();
        editModeButton.submitAction.AddListener(function (action) {
            ths._uiContextMenu = ths.CreateContextMenu_EditModeSelection();
        });
        // create the selected objects list
        this._uiSelectedObjectsRoot = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIElement();
        this._uiSelectedObjectsRoot.position.set(50, 25);
        this._uiPanelRight.addChild(this._uiSelectedObjectsRoot);
        var selectionListText = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Text("Selection:", {
            fontFamily: "Consolas",
            fontSize: 12,
            fill: 0xFFFFFF,
            stroke: 0x000000,
            strokeThickness: 1
        });
        selectionListText.position.set(-20, -14);
        this._uiSelectedObjectsRoot.addChild(selectionListText);
    };
    LevelEditor.prototype.UpdateEditModeText = function () {
        var textElem = this._uiEditModeButton.GetChildOfType(_internal__WEBPACK_IMPORTED_MODULE_1__.UITextElement);
        textElem === null || textElem === void 0 ? void 0 : textElem.destroy();
        var editSelText = _internal__WEBPACK_IMPORTED_MODULE_1__.UITextElement.CreateFromString(EditMode.ToString(this._editMode));
        this._uiEditModeButton.addChild(editSelText.AlignedCenter());
    };
    LevelEditor.prototype.SetUIEnabled = function (enabled) {
        if (enabled === void 0) { enabled = false; }
        // if enabled
        if (this._uiRoot.parentEntity == this) {
            if (!enabled) {
                this.removeChild(this._uiRoot);
            }
        }
        // if not enabled
        else {
            if (enabled) {
                this.addChild(this._uiRoot);
            }
        }
    };
    LevelEditor.prototype.HandleUIElementAction = function (elements, action) {
        // iterate through each specified element
        for (var i = elements.length - 1; i >= 0; i--) {
            var elem = elements[i];
            if (!(elem instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.UIElement))
                break;
            // if the element has a custom action
            if (_internal__WEBPACK_IMPORTED_MODULE_1__.IUISubmittable.ImplementedIn(elem)) {
                elem.Submit(action);
                break;
            }
            // if the element is part of the selected objects list
            if (elem instanceof UIElemEntityLink &&
                elem.parentEntity == this._uiSelectedObjectsRoot) {
                // get the index of the entity
                var index = this._selectedObjects.indexOf(elem.linkedEntity);
                // if the level editor is in entity property selection mode
                if (this.isSelectingEntity) {
                    this._selectEntityProp.SetValue(this._selectEntityPropTarget, elem.linkedEntity);
                    this._selectEntityPropText.text.text = elem.linkedEntity.name;
                    this._selectEntityProp = null;
                    this._selectEntityPropTarget = null;
                    return;
                }
                // if right click
                if (action == 2) {
                    this.CreateObjectPropertiesWindow(elem.linkedEntity);
                    break;
                }
                // if not right click, deselect the entity that corresponds to the element in 
                // the list
                else if (index >= 0) {
                    this._selectedObjects.splice(index, 1);
                    this.RefreshSelectedObjectsUI();
                    break;
                }
            }
        }
    };
    LevelEditor.prototype.RefreshSelectedObjectsUI = function () {
        // remove all selection list ui entities
        var curChildren = this._uiSelectedObjectsRoot.children;
        for (var i = curChildren.length - 1; i >= 1; i--) {
            curChildren[i].destroy();
        }
        // define text style and line spacing for list
        var lineSpacing = 14;
        // create a ui element for each selected object
        for (var i = 0; i < this._selectedObjects.length; i++) {
            var obj = this._selectedObjects[i];
            var uiEntity = new UIElemEntityLink(obj);
            this._uiSelectedObjectsRoot.addChild(uiEntity);
            uiEntity.uiOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, lineSpacing * i);
            uiEntity.MatchSizeOffsetToText(10, 0);
            uiEntity.uiSizeOffset.y = lineSpacing;
            uiEntity.UpdatePanelCollider();
        }
    };
    LevelEditor.prototype.CreateObjectPropertiesWindow = function (obj) {
        if (this._uiObjectPropertyWindow != null) {
            this._uiObjectPropertyWindow.destroy();
            this._uiObjectPropertyWindow = null;
        }
        var uiWindow = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIElement();
        uiWindow.uiAnchor = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(1, 0);
        uiWindow.uiPivot = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(1, 0);
        var bpanelRect = this._uiPanelBottom.GetUiRect();
        uiWindow.uiOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(bpanelRect.position.x + bpanelRect.size.x - 5, 1);
        var title = _internal__WEBPACK_IMPORTED_MODULE_1__.UITextElement.CreateFromString(obj.name + " properties:").AlignedCenter();
        title.uiAnchor.y = 0;
        title.uiPivot.y = 0;
        title.uiOffset.y = 10;
        uiWindow.UpdatePanelGraphic();
        uiWindow.addChild(title);
        // TODO add object properties
        var selectedObject = obj;
        var propsContainer = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIElement();
        propsContainer.uiOffset.y = 30;
        propsContainer.uiSize.x = 1;
        propsContainer.uiSizeOffset.y = 10;
        var yOff = 0;
        var props = Object.getPrototypeOf(selectedObject).constructor.GetEditorProperties();
        for (var i = 0; i < props.length; i++) {
            // create a text element for the property name
            var propElem = props[i].GetUIElement(selectedObject);
            propElem.uiOffset.y = yOff;
            propsContainer.addChild(propElem);
            propElem.ExpandToFitChildren();
            yOff += propElem.GetUiRect().size.y;
        }
        var closeButton = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIInteractiveElement();
        closeButton.uiAnchor = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(1, 1);
        closeButton.uiPivot = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(1, 1);
        closeButton.uiOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(-5, -5);
        closeButton.uiSize.x = 0.5;
        closeButton.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(-7, 25);
        closeButton.UpdatePanelGraphic();
        closeButton.UpdatePanelCollider();
        closeButton.addChild(_internal__WEBPACK_IMPORTED_MODULE_1__.UITextElement.CreateFromString("Close").AlignedCenter());
        var applyButton = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIInteractiveElement();
        applyButton.uiAnchor = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, 1);
        applyButton.uiPivot = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, 1);
        applyButton.uiOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(5, -5);
        applyButton.uiSize.x = 0.5;
        applyButton.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(-7, 25);
        applyButton.UpdatePanelGraphic();
        applyButton.UpdatePanelCollider();
        applyButton.addChild(_internal__WEBPACK_IMPORTED_MODULE_1__.UITextElement.CreateFromString("apply").AlignedCenter());
        uiWindow.addChild(propsContainer, applyButton, closeButton);
        uiWindow.ExpandToFitChildren(20, 0);
        // expand to fit the properties block
        propsContainer.ExpandToFitChildren(0, 0);
        uiWindow.uiSizeOffset.y += propsContainer.GetUiRect().size.y;
        var rect = uiWindow.GetUiRect();
        var bottom = rect.position.y + rect.size.y;
        var hOff = this._uiPanelBottom.GetUiRect().position.y - bottom;
        if (hOff < 0)
            uiWindow.uiSizeOffset.y += hOff;
        uiWindow.UpdateUiPosition(true);
        uiWindow.UpdatePanelCollider();
        uiWindow.UpdatePanelGraphic();
        // props.UpdatePanelGraphic();
        applyButton.UpdatePanelGraphic();
        applyButton.UpdatePanelCollider();
        closeButton.UpdatePanelGraphic();
        closeButton.UpdatePanelCollider();
        var ths = this;
        var closeAction = function (action) {
            ths._uiObjectPropertyWindow.destroy();
            ths._uiObjectPropertyWindow = null;
        };
        closeButton.submitAction.AddListener(closeAction);
        this._uiRoot.addChild(uiWindow);
        this._uiObjectPropertyWindow = uiWindow;
        return uiWindow;
    };
    //----------------------------------------------------------------------------------------------
    LevelEditor.prototype.CloseContextMenu = function () {
        if (this._uiContextMenu != null) {
            this._uiContextMenu.destroy();
            this._uiContextMenu = null;
        }
    };
    LevelEditor.prototype.CreateContextMenu_EntitySelection = function (textModify) {
        this.CloseContextMenu();
        var panRect = this._uiPanelBottom.GetUiRect();
        var height = 10;
        var width = panRect.size.x - 10;
        var r = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIElement();
        r.UpdatePanelGraphic();
        r.uiPivot = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0.5, 1);
        r.uiOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(panRect.position.x + panRect.size.x * 0.5, panRect.position.y - 5);
        r.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(width, height);
        this._uiRoot.addChild(r);
        var ths = this;
        var func_selectEnt = function (entity) {
            ths.CloseContextMenu();
            ths._selectedCreationObject = entity;
            textModify.SetTextString(entity.name);
        };
        var spacing = 15;
        var _loop_1 = function (i) {
            var ent = this_1._entityCreationPool[i];
            var elem = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIInteractiveElement();
            elem.uiOffset.y = i * spacing + 5;
            elem.uiAnchor.x = 0.5;
            elem.uiPivot.x = 0.5;
            elem.submitAction.AddListener(function (action) { func_selectEnt(ent); });
            r.addChild(elem);
            elem.addChild(_internal__WEBPACK_IMPORTED_MODULE_1__.UITextElement.CreateFromString(ent.name).AlignedCenter());
            elem.ExpandToFitChildren();
            elem.UpdatePanelCollider();
        };
        var this_1 = this;
        for (var i = 0; i < this._entityCreationPool.length; i++) {
            _loop_1(i);
        }
        r.ExpandToFitChildren(0, 10);
        r.UpdateUiPosition(true);
        r.UpdatePanelCollider();
        r.UpdatePanelGraphic();
        this._uiContextMenu = r;
        return r;
    };
    LevelEditor.prototype.CreateContextMenu_EditModeSelection = function () {
        this.CloseContextMenu();
        var panRect = this._uiPanelBottom.GetUiRect();
        var height = 10;
        var width = panRect.size.x - 10;
        var r = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIElement();
        r.UpdatePanelGraphic();
        r.uiPivot = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0.5, 1);
        r.uiOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(panRect.position.x + panRect.size.x * 0.5, panRect.position.y - 5);
        r.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(width, height);
        this._uiRoot.addChild(r);
        var ths = this;
        var func_selectMode = function (mode) {
            ths.CloseContextMenu();
            ths._editMode = mode;
            ths.UpdateEditModeText();
        };
        var spacing = 15;
        var _loop_2 = function (i) {
            var mode = i;
            var elem = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIInteractiveElement();
            elem.uiOffset.y = (i - 1) * spacing + 5;
            elem.uiAnchor.x = 0.5;
            elem.uiPivot.x = 0.5;
            elem.submitAction.AddListener(function (action) { func_selectMode(mode); });
            r.addChild(elem);
            elem.addChild(_internal__WEBPACK_IMPORTED_MODULE_1__.UITextElement.CreateFromString(EditMode.ToString(mode)).AlignedCenter());
            elem.ExpandToFitChildren();
            elem.UpdatePanelCollider();
        };
        for (var i = 1; i <= 3; i++) {
            _loop_2(i);
        }
        r.ExpandToFitChildren(0, 10);
        r.UpdateUiPosition(true);
        r.UpdatePanelCollider();
        r.UpdatePanelGraphic();
        this._uiContextMenu = r;
        return r;
    };
    //----------------------------------------------------------------------------------------------
    /** puts the editor into entity selection mode */
    LevelEditor.prototype.StartEntitySelection = function (property, target, text) {
        this._selectEntityProp = property;
        this._selectEntityPropTarget = target;
        this._selectEntityPropText = text;
    };
    LevelEditor.prototype.CancelEntitySelection = function () {
        if (!this.isSelectingEntity)
            return;
        this._selectEntityProp.SetValue(this._selectEntityPropTarget, null);
        this._selectEntityProp = null;
        this._selectEntityPropTarget = null;
        this._selectEntityPropText = null;
    };
    /**
     * select all the objects in a given area in the level
     * @param rect the selection rect to select all objects who overlap this rectangle
     * @param modifier the selection modifier to apply (neutral is default)
     */
    LevelEditor.prototype.SelectArea = function (rect, modifier) {
        // create an axis aligned box collider over the area of the mouse drag
        var center = _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.Center(rect);
        var col = _internal__WEBPACK_IMPORTED_MODULE_1__.AABBCollider.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(rect.size, 0.5));
        col.position.set(center.x, center.y);
        col.layer = _internal__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.none;
        // find all the colliders that intersect the mouse drag box
        var obRoot = this._objectRoot;
        var cols = obRoot.colliders.FindOverlapColliders(col);
        // reset the selected object array and add each selected collider to the array
        if (modifier == SelectionModifier.neutral) {
            this._selectedObjects.splice(0, this._selectedObjects.length);
        }
        // iterate through each selected object and add or remove it
        for (var i = cols.length - 1; i >= 0; i--) {
            var obj = cols[i];
            if (obj instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.Collider) {
                if (!(obj.parentEntity instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.Terrain)) {
                    obj = obj.parentEntity;
                }
            }
            // get the array of the object in the selection
            var selIndex = this._selectedObjects.indexOf(obj);
            // if modifier is set to remove the object and it's in the array, remove the object
            if (modifier == SelectionModifier.negative) {
                if (selIndex >= 0) {
                    this._selectedObjects.splice(selIndex, 1);
                }
            }
            // if the object does not exist in the selection already and modifier is set to add it
            else if (selIndex < 0) {
                this._selectedObjects.push(obj);
            }
        }
        this.RefreshSelectedObjectsUI();
    };
    LevelEditor.prototype.GetSelectedObjectsOnMouse = function () {
        var r = new Array();
        var mpos = this.worldMousePos;
        for (var i = this._selectedObjects.length - 1; i >= 0; i--) {
            var obj = this._selectedObjects[i];
            if (_internal__WEBPACK_IMPORTED_MODULE_1__.ICollidable.ImplementedIn(obj)) {
                if (obj.collider.OverlapsPoint(mpos)) {
                    r.push(obj);
                    continue;
                }
            }
        }
        return r;
    };
    LevelEditor.prototype.GetMouseDragArea = function (snapToGrid) {
        if (snapToGrid === void 0) { snapToGrid = false; }
        // calculate the mouse drag start and end position
        var rectPos = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Clone(this._dragStart);
        var dragEnd = this._camTransform.applyInverse(this.parentGame.mousePos);
        // snap to the grid if specified to do so
        if (snapToGrid) {
            rectPos = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(rectPos, 1 / this._snapGridSize);
            rectPos.x = Math.round(rectPos.x);
            rectPos.y = Math.round(rectPos.y);
            rectPos = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(rectPos, this._snapGridSize);
            dragEnd = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(dragEnd, 1 / this._snapGridSize);
            dragEnd.x = Math.round(dragEnd.x);
            dragEnd.y = Math.round(dragEnd.y);
            dragEnd = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(dragEnd, this._snapGridSize);
        }
        // difference between drag end and start
        var dif = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(dragEnd.x - rectPos.x, dragEnd.y - rectPos.y);
        // ensure x size is not negative
        if (dif.x < 0) {
            dif.x *= -1;
            rectPos.x -= dif.x;
        }
        // ensure y size is not negative
        if (dif.y < 0) {
            dif.y *= -1;
            rectPos.y -= dif.y;
        }
        // get the area from the mouse drag start and end positions
        var rect = _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.Create(rectPos, dif);
        return rect;
    };
    LevelEditor.prototype.DrawSelectedObjects = function () {
        var graphic = this._selectedObjectsGraphic;
        graphic.clear();
        graphic.lineStyle({
            color: 0x00FF00,
            alpha: 0.65,
            width: 1
        });
        for (var i = this._selectedObjects.length - 1; i >= 0; i--) {
            // if the emphasized selection is current iteration object, skip it so that we can
            // draw it later with a different style
            if (this._selectedObjects[i] == this._emphasizedSelectionEntity)
                continue;
            this.DrawSelectedEntity(this._selectedObjects[i]);
        }
        graphic.lineStyle({
            color: 0xFFFF00,
            alpha: 0.9,
            width: 2
        });
        // draw empasized selection
        var emphasis = this._emphasizedSelectionEntity;
        if (emphasis != null) {
            this.DrawSelectedEntity(emphasis);
        }
    };
    LevelEditor.prototype.DrawSelectedEntity = function (entity) {
        var graphic = this._selectedObjectsGraphic;
        var pos = entity.globalPosition;
        var rect = _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(pos.x - 10, pos.y - 10), _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(20));
        if (_internal__WEBPACK_IMPORTED_MODULE_1__.ICollidable.ImplementedIn(entity)) {
            var selCol = entity.collider;
            rect = selCol.GetColliderAABB();
            rect.position.x -= 5,
                rect.position.y -= 5;
            rect.size.x += 10;
            rect.size.y += 10;
        }
        var cSize = 7;
        // draw top left corner
        graphic.moveTo(rect.position.x, rect.position.y + cSize);
        graphic.lineTo(rect.position.x, rect.position.y);
        graphic.lineTo(rect.position.x + cSize, rect.position.y);
        // top right
        graphic.moveTo(rect.position.x + rect.size.x - cSize, rect.position.y);
        graphic.lineTo(rect.position.x + rect.size.x, rect.position.y);
        graphic.lineTo(rect.position.x + rect.size.x, rect.position.y + cSize);
        // bottom right
        graphic.moveTo(rect.position.x + rect.size.x - cSize, rect.position.y + rect.size.y);
        graphic.lineTo(rect.position.x + rect.size.x, rect.position.y + rect.size.y);
        graphic.lineTo(rect.position.x + rect.size.x, rect.position.y + rect.size.y - cSize);
        // bottom left
        graphic.moveTo(rect.position.x, rect.position.y + rect.size.y - cSize);
        graphic.lineTo(rect.position.x, rect.position.y + rect.size.y);
        graphic.lineTo(rect.position.x + cSize, rect.position.y + rect.size.y);
    };
    LevelEditor.prototype.DeleteSelectedObjects = function () {
        // iterate throug each object
        for (var i = this._selectedObjects.length - 1; i >= 0; i--) {
            // get a reference to the object that's selected
            var obj = this._selectedObjects[i];
            // remove the object
            console.log("Deleted " + obj.name);
            obj.RemoveFromScene();
        }
        // clear selected objects so we dont get null refs in the array
        this._selectedObjects.splice(0, this._selectedObjects.length);
        this.RefreshSelectedObjectsUI();
    };
    LevelEditor.prototype.AddSelectionRectTerrain = function (rect, terrain) {
        if (terrain === void 0) { terrain = null; }
        // don't add empty terrains
        if (rect.size.x == 0 || rect.size.y == 0)
            return;
        // create a collider from the selection rect to add to the terrain
        var col = _internal__WEBPACK_IMPORTED_MODULE_1__.AABBCollider.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create());
        col.layer = _internal__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.terrain;
        // if no terrain object was specified then we'll need to find one in the scene
        if (terrain == null) {
            // iterate thorugh each object in the scene until we find a terrain object
            // to add the collider to
            for (var i = this._objectRoot.children.length - 1; i >= 0; i--) {
                if (this._objectRoot.children[i] instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.Terrain) {
                    terrain = this._objectRoot.children[i];
                    break;
                }
            }
            // no terrain in level object root
            if (terrain == null) {
                throw "No Terrain object found in object root";
            }
        }
        terrain.addChild(col);
        // position the terrain collider to match the selection rect
        col.SetRect(rect);
        col.CreateWireframe(0xFFFFFF, 1);
        console.log("Terrain collider added to " + terrain.name);
        var serialized = terrain.GetSerializedObject();
        console.log(terrain);
        console.log(serialized);
        console.log(_internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.GetDeserializedObject(serialized));
        // so that the selection rect is removed without modifying current selection
        this._isSelecting = false;
    };
    //----------------------------------------------------------------------------------------------
    LevelEditor.prototype.SetZoomLevel = function (zoom) {
        var vpX = this.parentGame.renderer.view.width;
        var vpY = this.parentGame.renderer.view.height;
        var curZoom = _internal__WEBPACK_IMPORTED_MODULE_1__.Matrix.GetScale(this._camTransform).x;
        var curPos = _internal__WEBPACK_IMPORTED_MODULE_1__.Matrix.GetTranslation(this._camTransform);
        var dZoom = zoom / curZoom;
        var p0 = this.worldMousePos;
        this._camTransform.scale(dZoom, dZoom);
        var p1 = this.worldMousePos;
        var dx = p1.x - p0.x;
        var dy = p1.y - p0.y;
        this._camTransform.translate(dx * zoom, dy * zoom);
    };
    LevelEditor.prototype.PanCamera = function (translation) {
        var zoom = _internal__WEBPACK_IMPORTED_MODULE_1__.Matrix.GetScale(this._camTransform);
        this._camTransform.translate(translation.x * zoom.x, translation.y * zoom.y);
    };
    //----------------------------------------------------------------------------------------------
    /**
     * serializes the level in base64 encoded json and returns the string
     */
    LevelEditor.prototype.GetSerializedLevel = function () {
        var r = this._objectRoot.GetSerializedObject();
        // TODO: convert to base 64
        console.log(r);
        return JSON.stringify(r, null, '\t');
    };
    LevelEditor.STORAGEKEY_BASE = "kinesisTS_lvlEdit_";
    LevelEditor.STORAGEKEY_RECENTLEVEL = LevelEditor.STORAGEKEY_BASE + "Current";
    return LevelEditor;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.Scene));

/**
 * an object meant to be used as a ui element in the level editor that has a link to an entity in
 * the level
 */
var UIElemEntityLink = /** @class */ (function (_super) {
    __extends(UIElemEntityLink, _super);
    function UIElemEntityLink(linkedEntity) {
        var _this = _super.call(this, new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Text(linkedEntity.name, _internal__WEBPACK_IMPORTED_MODULE_1__.UITextElement.DEFAULT_STYLE)) || this;
        _this.linkedEntity = null;
        _this.linkedEntity = linkedEntity;
        return _this;
    }
    return UIElemEntityLink;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.UITextElement));


/***/ }),

/***/ 1426:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GroundHazard": () => (/* binding */ GroundHazard)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var GroundHazard = /** @class */ (function (_super) {
    __extends(GroundHazard, _super);
    function GroundHazard() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._upDirection = _internal__WEBPACK_IMPORTED_MODULE_0__.Side.up;
        _this._length = 10;
        _this._thickness = 10;
        _this._sprite = null;
        _this._collider = null;
        return _this;
    }
    Object.defineProperty(GroundHazard.prototype, "upDirection", {
        get: function () { return this._upDirection; },
        set: function (val) {
            this._upDirection = val;
            this.UpdateColliderToLength();
            this.UpdateSprite();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(GroundHazard.prototype, "length", {
        get: function () { return this._length; },
        set: function (val) {
            this._length = val;
            this.UpdateColliderToLength();
            this.UpdateSprite();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(GroundHazard.prototype, "collider", {
        get: function () { return this._collider; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(GroundHazard.prototype, "isHorizontal", {
        get: function () { return !_internal__WEBPACK_IMPORTED_MODULE_0__.Side.IsHorizontal(this._upDirection); },
        enumerable: false,
        configurable: true
    });
    /// --------------------------------------------------------------------------------------------
    /** @inheritdoc */
    GroundHazard.GetEditorProperties = function () {
        var r = _super.GetEditorProperties.call(this);
        r.push(_internal__WEBPACK_IMPORTED_MODULE_0__.EditorProperty.CreateKVProp("upDirection", "Up Direction", [
            { key: "up", value: _internal__WEBPACK_IMPORTED_MODULE_0__.Side.up },
            { key: "right", value: _internal__WEBPACK_IMPORTED_MODULE_0__.Side.right },
            { key: "down", value: _internal__WEBPACK_IMPORTED_MODULE_0__.Side.down },
            { key: "left", value: _internal__WEBPACK_IMPORTED_MODULE_0__.Side.left }
        ]), _internal__WEBPACK_IMPORTED_MODULE_0__.EditorProperty.CreateNumberProp("length", "Length"));
        return r;
    };
    /// --------------------------------------------------------------------------------------------
    /** @inheritdoc */
    GroundHazard.prototype.ClosestPoint = function (point) {
        return this.collider.ClosestPoint(point);
    };
    /** @inheritdoc */
    GroundHazard.prototype.GetResizeRect = function () {
        return this.collider.GetColliderAABB();
    };
    /** @inheritdoc */
    GroundHazard.prototype.SetResizeRect = function (rect) {
        var r = _internal__WEBPACK_IMPORTED_MODULE_0__.Rect.Clone(rect);
        // set length to rect width if it's a horizontal spike strip
        if (this.isHorizontal) {
            r.size.y = this._thickness;
            this._length = rect.size.x;
        }
        // set length to rect height if it's a vertical spike strip
        else {
            r.size.x = this._thickness;
            this._length = rect.size.y;
        }
        this.UpdateColliderToLength(_internal__WEBPACK_IMPORTED_MODULE_0__.Rect.Center(r));
        this.collider.RedrawWireframe();
        this.UpdateSprite();
    };
    /// --------------------------------------------------------------------------------------------
    GroundHazard.prototype.UpdateSprite = function () {
        if (this._sprite == null)
            return;
        var rect = this.collider.GetColliderAABB();
        var length = this.isHorizontal ? rect.size.x : rect.size.y;
        this._sprite.width = length;
        this._sprite.height = this._sprite.texture.height;
        switch (this._upDirection) {
            case _internal__WEBPACK_IMPORTED_MODULE_0__.Side.up:
                this._sprite.rotation = 0;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.Side.right:
                this._sprite.rotation = Math.PI * 0.5;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.Side.down:
                this._sprite.rotation = Math.PI;
                break;
            case _internal__WEBPACK_IMPORTED_MODULE_0__.Side.left:
                this._sprite.rotation = Math.PI * -0.5;
                break;
        }
    };
    GroundHazard.prototype.UpdateColliderToLength = function (center) {
        if (center === void 0) { center = null; }
        var oRect = this.collider.GetColliderAABB();
        var r = _internal__WEBPACK_IMPORTED_MODULE_0__.Rect.Clone(oRect);
        // only resize along the x axis if it's a horizontal spike strip
        if (this.isHorizontal) {
            r.position.y = oRect.position.y;
            r.size.x = this._length;
            r.size.y = this._thickness;
        }
        // only resize along the y axis if it's a vertical spike strip
        else {
            r.position.x = oRect.position.x;
            r.size.x = this._thickness;
            r.size.y = this._length;
        }
        center = center !== null && center !== void 0 ? center : this.globalPosition;
        this.collider.SetRect(r);
        this.globalPosition = center;
        this.collider.globalPosition = center;
    };
    /// --------------------------------------------------------------------------------------------
    /** @inheritdoc */
    GroundHazard.prototype.GetSerializedChildren = function () {
        var r = new Array();
        for (var i = 0; i < this.entities.length; i++) {
            // dont copy the collider
            if (this.entities[i] == this._collider)
                continue;
            var serEnt = this.entities[i].GetSerializedObject();
            if (serEnt != null)
                r.push(serEnt);
        }
        return r;
    };
    /** @inheritdoc */
    GroundHazard.prototype.Deserialize = function (data) {
        _super.prototype.Deserialize.call(this, data);
        this.UpdateColliderToLength();
        this.UpdateSprite();
    };
    return GroundHazard;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.SceneEntity));



/***/ }),

/***/ 4810:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "JumpPad": () => (/* binding */ JumpPad)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var JumpPad = /** @class */ (function (_super) {
    __extends(JumpPad, _super);
    function JumpPad() {
        var _this = _super.call(this) || this;
        _this._particleSystem = null;
        _this._particleEmitter = null;
        _this.force = 200;
        _this.Initialize();
        _this._name = "Jump Pad";
        return _this;
    }
    /// --------------------------------------------------------------------------------------------
    JumpPad.prototype.Update = function (deltaTime) {
        _super.prototype.Update.call(this, deltaTime);
        if (this._sprite.texture == null) {
            this.InitGraphic();
        }
        if (this._particleEmitter == null) {
            this.CreateParticleEffects();
        }
    };
    JumpPad.prototype.Initialize = function () {
        this._collider = new _internal__WEBPACK_IMPORTED_MODULE_1__.AABBCollider();
        this._collider.layer = _internal__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.projectiles;
        this._collider.halfExtents = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this._thickness * 0.5);
        this._collider.CreateWireframe(0xFF0000, 1);
        var ths = this;
        this.collider.onCollision.AddListener(function (col) { ths.OnCollision(col); });
        this.InitGraphic();
        this.addChild(this._collider);
        this.UpdateSprite();
        this.CreateParticleEffects();
    };
    JumpPad.prototype.InitGraphic = function () {
        if (!JumpPad.SPRITESHEET.isLoaded)
            return;
        var plane = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.NineSlicePlane(JumpPad.SPRITESHEET.sprites[0]);
        plane.topHeight = 0;
        plane.bottomHeight = 0;
        plane.leftWidth = 3;
        plane.rightWidth = 3;
        this._sprite = plane;
        this._sprite.pivot.set(this._sprite.width * 0.5, this._sprite.height);
        this.addChild(this._sprite);
        this.UpdateSprite();
    };
    JumpPad.prototype.CreateParticleEffects = function () {
        if (!(this.parentScene instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.GameScene))
            return;
        var pSystem = new _internal__WEBPACK_IMPORTED_MODULE_1__.ParticleSystem(_internal__WEBPACK_IMPORTED_MODULE_1__.LineParticle, 25);
        this.parentScene.addChild(pSystem);
        this._particleSystem = pSystem;
        var emitter = new _internal__WEBPACK_IMPORTED_MODULE_1__.ParticleEmitter(pSystem);
        this.addChild(emitter);
        this._particleEmitter = emitter;
        emitter.particleLife = 0.5;
        emitter.particleLifeVar = 0.1;
        emitter.speed = 0;
        emitter.speedVar = 0;
        emitter.emissionOverDist = 0;
        emitter.radius = 0;
        emitter.colorA = 0x44AAFF;
        emitter.colorB = 0x88FFFF;
        this.UpdateParticleEmitterShape();
    };
    JumpPad.prototype.UpdateParticleEmitterShape = function () {
        if (this._particleEmitter == null)
            return;
        this._particleEmitter.halfExtents = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Clone(this.collider.halfExtents);
        this._particleEmitter.startVelocity = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.MultiplyScalar(_internal__WEBPACK_IMPORTED_MODULE_1__.Side.GetVector(this.upDirection), 50);
        this._particleEmitter.emissionOverTime =
            this._particleEmitter.halfExtents.x * this._particleEmitter.halfExtents.y * 0.25;
    };
    JumpPad.prototype.OnCollision = function (col) {
        var other = _internal__WEBPACK_IMPORTED_MODULE_1__.Collision.OtherCollider(col, this.collider);
        var entity = other.parentEntity;
        if (entity instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.PhysicsEntity) {
            var vect = _internal__WEBPACK_IMPORTED_MODULE_1__.Side.GetVector(this.upDirection);
            vect.x *= this.force;
            vect.y *= this.force;
            if (!this.isHorizontal) {
                if (Math.sign(vect.x) != Math.sign(entity.velocity.x))
                    entity.velocity.x = vect.x;
            }
            else {
                if (Math.sign(vect.y) != Math.sign(entity.velocity.y))
                    entity.velocity.y = vect.y;
            }
        }
    };
    /// --------------------------------------------------------------------------------------------
    JumpPad.prototype.UpdateSprite = function () {
        if (this._sprite == null)
            return;
        _super.prototype.UpdateSprite.call(this);
        this._sprite.pivot.set(this._sprite.width * 0.5, this._sprite.height);
    };
    JumpPad.SPRITESHEET = new _internal__WEBPACK_IMPORTED_MODULE_1__.SpriteSheet("./dist/assets/graphics/JumpPad.png");
    return JumpPad;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.GroundHazard));



/***/ }),

/***/ 7810:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Spikes": () => (/* binding */ Spikes)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var Spikes = /** @class */ (function (_super) {
    __extends(Spikes, _super);
    function Spikes() {
        var _this = _super.call(this) || this;
        _this._name = "Spikes";
        return _this;
    }
    /// --------------------------------------------------------------------------------------------
    Spikes.prototype.Initialize = function () {
        this._collider = new _internal__WEBPACK_IMPORTED_MODULE_1__.AABBCollider();
        this._collider.layer = _internal__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.projectiles;
        this._collider.halfExtents = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this._thickness * 0.5);
        this._collider.CreateWireframe(0xFF0000, 1);
        var ths = this;
        this.collider.onCollision.AddListener(function (col) { ths.OnCollision(col); });
        var spr = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.TilingSprite(null);
        spr.anchor.set(0.5, 1);
        this._sprite = spr;
        Spikes.SPRITESHEET.DoOnLoaded(function () {
            ths._sprite.texture = Spikes.SPRITESHEET.sprites[0];
        });
        this.addChild(this._collider);
        this.addChild(this._sprite);
        this.UpdateSprite();
    };
    Spikes.prototype.OnCollision = function (col) {
        var other = _internal__WEBPACK_IMPORTED_MODULE_1__.Collision.OtherCollider(col, this.collider);
        var entity = other.parentEntity;
        if (_internal__WEBPACK_IMPORTED_MODULE_1__.ITarget.ImplementedIn(entity)) {
            if (_internal__WEBPACK_IMPORTED_MODULE_1__.Allegiance.HAZARD.AggresionToward(entity) == _internal__WEBPACK_IMPORTED_MODULE_1__.Aggression.hostile) {
                entity.Hit(10, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(), col.collisionPoint, this);
            }
        }
    };
    /// --------------------------------------------------------------------------------------------
    Spikes.GetEditorProperties = function () {
        var r = _super.GetEditorProperties.call(this);
        r.push(_internal__WEBPACK_IMPORTED_MODULE_1__.EditorProperty.CreateKVProp("upDirection", "Up Direction", [
            { key: "up", value: _internal__WEBPACK_IMPORTED_MODULE_1__.Side.up },
            { key: "right", value: _internal__WEBPACK_IMPORTED_MODULE_1__.Side.right },
            { key: "down", value: _internal__WEBPACK_IMPORTED_MODULE_1__.Side.down },
            { key: "left", value: _internal__WEBPACK_IMPORTED_MODULE_1__.Side.left }
        ]), _internal__WEBPACK_IMPORTED_MODULE_1__.EditorProperty.CreateNumberProp("length", "Length"));
        return r;
    };
    Spikes.SPRITESHEET = new _internal__WEBPACK_IMPORTED_MODULE_1__.SpriteSheet("./dist/assets/graphics/Spikes.png");
    return Spikes;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.GroundHazard));



/***/ }),

/***/ 4993:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _core_iSerializable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6818);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_iSerializable__WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_iSerializable__WEBPACK_IMPORTED_MODULE_0__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_raycastResult__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2930);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_raycastResult__WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_raycastResult__WEBPACK_IMPORTED_MODULE_1__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_iActivate__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3945);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_iActivate__WEBPACK_IMPORTED_MODULE_2__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_iActivate__WEBPACK_IMPORTED_MODULE_2__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_iTarget__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5928);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_iTarget__WEBPACK_IMPORTED_MODULE_3__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_iTarget__WEBPACK_IMPORTED_MODULE_3__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_iCollider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4103);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_iCollider__WEBPACK_IMPORTED_MODULE_4__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_iCollider__WEBPACK_IMPORTED_MODULE_4__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_iCollision__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8379);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_iCollision__WEBPACK_IMPORTED_MODULE_5__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_iCollision__WEBPACK_IMPORTED_MODULE_5__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_iSolid__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2386);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_iSolid__WEBPACK_IMPORTED_MODULE_6__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_iSolid__WEBPACK_IMPORTED_MODULE_6__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_iAllied__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(9960);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_iAllied__WEBPACK_IMPORTED_MODULE_7__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_iAllied__WEBPACK_IMPORTED_MODULE_7__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_iDisplayTintable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(11);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_iDisplayTintable__WEBPACK_IMPORTED_MODULE_8__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_iDisplayTintable__WEBPACK_IMPORTED_MODULE_8__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_iSizeableDisplayObject__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(2494);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_iSizeableDisplayObject__WEBPACK_IMPORTED_MODULE_9__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_iSizeableDisplayObject__WEBPACK_IMPORTED_MODULE_9__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_iControllable__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(4556);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_iControllable__WEBPACK_IMPORTED_MODULE_10__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_iControllable__WEBPACK_IMPORTED_MODULE_10__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _editor_iEditorResizable__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(4630);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _editor_iEditorResizable__WEBPACK_IMPORTED_MODULE_11__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _editor_iEditorResizable__WEBPACK_IMPORTED_MODULE_11__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _ui_iUiSubmittable__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(2137);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _ui_iUiSubmittable__WEBPACK_IMPORTED_MODULE_12__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _ui_iUiSubmittable__WEBPACK_IMPORTED_MODULE_12__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _ui_iUiFocusable__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(6663);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _ui_iUiFocusable__WEBPACK_IMPORTED_MODULE_13__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _ui_iUiFocusable__WEBPACK_IMPORTED_MODULE_13__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_math__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(1287);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_math__WEBPACK_IMPORTED_MODULE_14__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_math__WEBPACK_IMPORTED_MODULE_14__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_ray__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(116);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_ray__WEBPACK_IMPORTED_MODULE_15__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_ray__WEBPACK_IMPORTED_MODULE_15__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_gameEvent__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(7757);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_gameEvent__WEBPACK_IMPORTED_MODULE_16__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_gameEvent__WEBPACK_IMPORTED_MODULE_16__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _editor_editorProperty__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(3668);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _editor_editorProperty__WEBPACK_IMPORTED_MODULE_17__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _editor_editorProperty__WEBPACK_IMPORTED_MODULE_17__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_colliderPartitions__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(3466);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_colliderPartitions__WEBPACK_IMPORTED_MODULE_18__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_colliderPartitions__WEBPACK_IMPORTED_MODULE_18__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_inputControl__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(1529);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_inputControl__WEBPACK_IMPORTED_MODULE_19__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_inputControl__WEBPACK_IMPORTED_MODULE_19__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_allegiance__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(9125);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_allegiance__WEBPACK_IMPORTED_MODULE_20__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_allegiance__WEBPACK_IMPORTED_MODULE_20__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_spriteSheet__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(7714);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_spriteSheet__WEBPACK_IMPORTED_MODULE_21__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_spriteSheet__WEBPACK_IMPORTED_MODULE_21__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_sceneReference__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(9238);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_sceneReference__WEBPACK_IMPORTED_MODULE_22__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_sceneReference__WEBPACK_IMPORTED_MODULE_22__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_game__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(4544);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_game__WEBPACK_IMPORTED_MODULE_23__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_game__WEBPACK_IMPORTED_MODULE_23__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_statModifier__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(4606);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_statModifier__WEBPACK_IMPORTED_MODULE_24__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_statModifier__WEBPACK_IMPORTED_MODULE_24__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_stat__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(6896);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_stat__WEBPACK_IMPORTED_MODULE_25__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_stat__WEBPACK_IMPORTED_MODULE_25__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_stats__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(4561);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_stats__WEBPACK_IMPORTED_MODULE_26__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_stats__WEBPACK_IMPORTED_MODULE_26__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_particle__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(2288);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_particle__WEBPACK_IMPORTED_MODULE_27__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_particle__WEBPACK_IMPORTED_MODULE_27__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_particlePool__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(3781);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_particlePool__WEBPACK_IMPORTED_MODULE_28__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_particlePool__WEBPACK_IMPORTED_MODULE_28__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_sceneEntity__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(5477);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_sceneEntity__WEBPACK_IMPORTED_MODULE_29__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_sceneEntity__WEBPACK_IMPORTED_MODULE_29__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _ui_uiElement__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(2465);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _ui_uiElement__WEBPACK_IMPORTED_MODULE_30__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _ui_uiElement__WEBPACK_IMPORTED_MODULE_30__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _ui_uiInteractiveElement__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(6064);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _ui_uiInteractiveElement__WEBPACK_IMPORTED_MODULE_31__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _ui_uiInteractiveElement__WEBPACK_IMPORTED_MODULE_31__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _ui_uiCheckboxElement__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(864);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _ui_uiCheckboxElement__WEBPACK_IMPORTED_MODULE_32__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _ui_uiCheckboxElement__WEBPACK_IMPORTED_MODULE_32__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _ui_uiTextElement__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(5159);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _ui_uiTextElement__WEBPACK_IMPORTED_MODULE_33__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _ui_uiTextElement__WEBPACK_IMPORTED_MODULE_33__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _ui_uiTextInputField__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(9454);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _ui_uiTextInputField__WEBPACK_IMPORTED_MODULE_34__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _ui_uiTextInputField__WEBPACK_IMPORTED_MODULE_34__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_particleSystem__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(6026);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_particleSystem__WEBPACK_IMPORTED_MODULE_35__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_particleSystem__WEBPACK_IMPORTED_MODULE_35__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_particleEmitter__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(9745);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_particleEmitter__WEBPACK_IMPORTED_MODULE_36__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_particleEmitter__WEBPACK_IMPORTED_MODULE_36__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_objectController__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(8551);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_objectController__WEBPACK_IMPORTED_MODULE_37__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_objectController__WEBPACK_IMPORTED_MODULE_37__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _actors_playerController__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(3358);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _actors_playerController__WEBPACK_IMPORTED_MODULE_38__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _actors_playerController__WEBPACK_IMPORTED_MODULE_38__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_aiController__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(5962);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_aiController__WEBPACK_IMPORTED_MODULE_39__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_aiController__WEBPACK_IMPORTED_MODULE_39__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_aiBehavior__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(2750);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_aiBehavior__WEBPACK_IMPORTED_MODULE_40__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_aiBehavior__WEBPACK_IMPORTED_MODULE_40__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _actors_enemies_enemyBehaviors__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(3783);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _actors_enemies_enemyBehaviors__WEBPACK_IMPORTED_MODULE_41__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _actors_enemies_enemyBehaviors__WEBPACK_IMPORTED_MODULE_41__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _actors_enemies_gunnerAi__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(5860);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _actors_enemies_gunnerAi__WEBPACK_IMPORTED_MODULE_42__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _actors_enemies_gunnerAi__WEBPACK_IMPORTED_MODULE_42__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _actors_enemies_seekerAi__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(1128);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _actors_enemies_seekerAi__WEBPACK_IMPORTED_MODULE_43__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _actors_enemies_seekerAi__WEBPACK_IMPORTED_MODULE_43__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_scene__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(803);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_scene__WEBPACK_IMPORTED_MODULE_44__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_scene__WEBPACK_IMPORTED_MODULE_44__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_gameScene__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(1570);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_gameScene__WEBPACK_IMPORTED_MODULE_45__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_gameScene__WEBPACK_IMPORTED_MODULE_45__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _ui_uiContainer__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(9099);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _ui_uiContainer__WEBPACK_IMPORTED_MODULE_46__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _ui_uiContainer__WEBPACK_IMPORTED_MODULE_46__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _editor_levelEditor__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(3141);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _editor_levelEditor__WEBPACK_IMPORTED_MODULE_47__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _editor_levelEditor__WEBPACK_IMPORTED_MODULE_47__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_terrain__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(3322);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_terrain__WEBPACK_IMPORTED_MODULE_48__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_terrain__WEBPACK_IMPORTED_MODULE_48__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_camera__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(9263);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_camera__WEBPACK_IMPORTED_MODULE_49__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_camera__WEBPACK_IMPORTED_MODULE_49__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_physicsEntity__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(8267);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_physicsEntity__WEBPACK_IMPORTED_MODULE_50__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_physicsEntity__WEBPACK_IMPORTED_MODULE_50__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _items_weapons_projectile__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(9112);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _items_weapons_projectile__WEBPACK_IMPORTED_MODULE_51__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _items_weapons_projectile__WEBPACK_IMPORTED_MODULE_51__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _environment_groundHazard__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(1426);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _environment_groundHazard__WEBPACK_IMPORTED_MODULE_52__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _environment_groundHazard__WEBPACK_IMPORTED_MODULE_52__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _environment_jumpPad__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(4810);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _environment_jumpPad__WEBPACK_IMPORTED_MODULE_53__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _environment_jumpPad__WEBPACK_IMPORTED_MODULE_53__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _environment_spikes__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(7810);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _environment_spikes__WEBPACK_IMPORTED_MODULE_54__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _environment_spikes__WEBPACK_IMPORTED_MODULE_54__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _items_weapons_weapon__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(2737);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _items_weapons_weapon__WEBPACK_IMPORTED_MODULE_55__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _items_weapons_weapon__WEBPACK_IMPORTED_MODULE_55__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _actors_actor__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(8091);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _actors_actor__WEBPACK_IMPORTED_MODULE_56__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _actors_actor__WEBPACK_IMPORTED_MODULE_56__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _actors_enemies_enemy__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(1470);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _actors_enemies_enemy__WEBPACK_IMPORTED_MODULE_57__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _actors_enemies_enemy__WEBPACK_IMPORTED_MODULE_57__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _actors_enemies_seeker__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(4517);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _actors_enemies_seeker__WEBPACK_IMPORTED_MODULE_58__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _actors_enemies_seeker__WEBPACK_IMPORTED_MODULE_58__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _actors_enemies_gunner__WEBPACK_IMPORTED_MODULE_59__ = __webpack_require__(3144);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _actors_enemies_gunner__WEBPACK_IMPORTED_MODULE_59__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _actors_enemies_gunner__WEBPACK_IMPORTED_MODULE_59__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _actors_player__WEBPACK_IMPORTED_MODULE_60__ = __webpack_require__(9331);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _actors_player__WEBPACK_IMPORTED_MODULE_60__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _actors_player__WEBPACK_IMPORTED_MODULE_60__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_effect__WEBPACK_IMPORTED_MODULE_61__ = __webpack_require__(3224);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_effect__WEBPACK_IMPORTED_MODULE_61__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_effect__WEBPACK_IMPORTED_MODULE_61__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_animatedEffect__WEBPACK_IMPORTED_MODULE_62__ = __webpack_require__(6033);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_animatedEffect__WEBPACK_IMPORTED_MODULE_62__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_animatedEffect__WEBPACK_IMPORTED_MODULE_62__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
/* harmony import */ var _core_collider__WEBPACK_IMPORTED_MODULE_63__ = __webpack_require__(7354);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _core_collider__WEBPACK_IMPORTED_MODULE_63__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _core_collider__WEBPACK_IMPORTED_MODULE_63__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
/// ------------------------------------------------------------------------------------------------
/// Interfaces do not do not depend on inheritance and therefore should always be exported first














/// ------------------------------------------------------------------------------------------------
/// classes that have no inheritence dependencies (base classes)















/// ------------------------------------------------------------------------------------------------
/// classes that have inheritence dependencies MUST be loaded in the order that they are inherit
/// their peers. Base classes must be loaded first, and then classes that inherit from it can
/// be loaded afterwards





































/***/ }),

/***/ 9112:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Projectile": () => (/* binding */ Projectile),
/* harmony export */   "BeamSegment": () => (/* binding */ BeamSegment)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var Projectile = /** @class */ (function (_super) {
    __extends(Projectile, _super);
    function Projectile() {
        var _this = _super.call(this) || this;
        _this._size = 2.5;
        _this._owner = null;
        _this._graphic = null;
        _this._lifetime = 1;
        _this._lastPhysPos = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        _this._lastDrawPos = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        _this.damage = 5;
        _this.color = 0xFFFF00;
        _this._onPreRenderAction = null;
        _this._name = "Projectile";
        return _this;
    }
    Object.defineProperty(Projectile.prototype, "size", {
        get: function () { return this._size; },
        set: function (val) {
            this._size = val;
            if (this._collider != null)
                this._collider.radius = this.size;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Projectile.prototype, "owner", {
        get: function () { return this._owner; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Projectile.prototype, "lastPos", {
        set: function (val) {
            this._lastPhysPos.x = val.x;
            this._lastPhysPos.y = val.y;
            this._lastDrawPos.x = val.x;
            this._lastDrawPos.y = val.y;
        },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    Projectile.prototype.OnAddedToScene = function (scene) {
        _super.prototype.OnAddedToScene.call(this, scene);
        this.CreateGraphic();
        if (this._onPreRenderAction != null)
            throw "pre render action not cleared";
        var ths = this;
        this._onPreRenderAction = function () {
            ths.OnPreRender();
        };
        scene.onPreRender.AddListener(this._onPreRenderAction);
    };
    Projectile.prototype.OnRemovedFromScene = function (scene) {
        _super.prototype.OnRemovedFromScene.call(this, scene);
        scene.onPreRender.RemoveListener(this._onPreRenderAction);
        this._onPreRenderAction = null;
    };
    // ---------------------------------------------------------------------------------------------
    Projectile.prototype.GetCurrentImpulse = function () {
        return this.momentum;
    };
    Projectile.prototype.CreateCollider = function () {
        var col = _internal__WEBPACK_IMPORTED_MODULE_1__.CircleCollider.Create(this._size);
        col.layer = _internal__WEBPACK_IMPORTED_MODULE_1__.CollisionLayer.projectiles;
        var ths = this;
        col.onCollision.AddListener(function (data) {
            ths.OnCollision(data);
        });
        this._collider = col;
        //this._collider.CreateWireframe(0xFF00FF, 1, true);
        this.addChild(this._collider);
    };
    Projectile.prototype.CheckCollisions = function () {
        var ray = _internal__WEBPACK_IMPORTED_MODULE_1__.Ray.FromPoints(this._lastPhysPos, this.globalPosition);
        var results = this.parentScene.colliders.CircleCast(ray, this._size, this.collider.layer);
        if (results.length <= 0) {
            return;
        }
        // get the projectile's allegience
        var allegiance = null;
        if (_internal__WEBPACK_IMPORTED_MODULE_1__.IAllied.ImplementedIn(this.owner)) {
            allegiance = this.owner.allegiance;
        }
        // iterate through each result
        var closestResult = null;
        var closestDistSq = Number.POSITIVE_INFINITY;
        for (var i = results.length - 1; i >= 0; i--) {
            var res = results[i];
            // skip any that did not enter
            if (!res.didEnter)
                continue;
            // skip a result if it's friendly with this projectile
            var resParent = res.collider.parentEntity;
            if (allegiance != null) {
                if (_internal__WEBPACK_IMPORTED_MODULE_1__.ITarget.ImplementedIn(resParent)) {
                    var aggr = allegiance.AggresionToward(resParent);
                    if (aggr == _internal__WEBPACK_IMPORTED_MODULE_1__.Aggression.friendly) {
                        continue;
                    }
                }
            }
            // if there are any closer results, use those instead
            var distSq = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.DistanceSquared(results[i].entryPoint, this._lastPhysPos);
            if (distSq < closestDistSq) {
                closestResult = res;
                closestDistSq = distSq;
            }
        }
        // ensure that there was at least one collider that the ray entered
        if (closestResult == null)
            return;
        // set the projectile position to the raycast result entry point
        this.globalPosition = closestResult.entryPoint;
        this.parentScene.colliders.ForceCollision(this._collider, closestResult.collider);
    };
    Projectile.prototype.OnCollision = function (collision) {
        var otherCol = _internal__WEBPACK_IMPORTED_MODULE_1__.Collision.OtherCollider(collision, this._collider);
        // get the projectile's allegience
        var allegiance = null;
        if (_internal__WEBPACK_IMPORTED_MODULE_1__.IAllied.ImplementedIn(this.owner)) {
            allegiance = this.owner.allegiance;
        }
        // skip a collision if it's friendly with this projectile
        var resParent = otherCol.parentEntity;
        if (allegiance != null) {
            if (_internal__WEBPACK_IMPORTED_MODULE_1__.ITarget.ImplementedIn(resParent)) {
                var aggr = allegiance.AggresionToward(resParent);
                if (aggr == _internal__WEBPACK_IMPORTED_MODULE_1__.Aggression.friendly) {
                    return;
                }
            }
        }
        var normal = collision.directionNormal;
        if (otherCol.parentEntity == this) {
            normal = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Invert(normal);
        }
        // if the hit object is implements the target interface, apply a projecile hit call on it
        if (_internal__WEBPACK_IMPORTED_MODULE_1__.ITarget.ImplementedIn(otherCol.parentEntity)) {
            var target = otherCol.parentEntity;
            target.Hit(this.damage, this.GetCurrentImpulse(), collision.collisionPoint, this.owner);
        }
        if (this._lifetime > 0) {
            _internal__WEBPACK_IMPORTED_MODULE_1__.AnimatedEffect.HitEffect(this.parentScene, collision.collisionPoint, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(), _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Direction(normal) + Math.PI, 0xFFEE88);
        }
        this.RemoveFromScene(true);
    };
    Projectile.prototype.CreateGraphic = function () {
        this._graphic = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Graphics();
        this.addChild(this._graphic);
    };
    Projectile.prototype.UpdateGraphic = function () {
        var pos = this.globalPosition;
        var dif = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Subtract(pos, this._lastDrawPos);
        this._graphic.clear();
        this._graphic.lineStyle({
            color: this.color,
            width: this.size
        });
        this._graphic.moveTo(0, 0);
        this._graphic.lineTo(-dif.x, -dif.y);
    };
    Projectile.prototype.OnPreRender = function () {
        this._lastDrawPos = this.globalPosition;
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    Projectile.prototype.Update = function (deltaTime) {
        this._lastPhysPos = this.globalPosition;
        _super.prototype.Update.call(this, deltaTime);
        this.CheckCollisions();
        this._lifetime -= deltaTime;
        this.UpdateGraphic();
        if (this._lifetime <= 0) {
            this.RemoveFromScene();
        }
    };
    /** @inheritdoc */
    Projectile.prototype.destroy = function () {
        this._collider.destroy();
        _super.prototype.destroy.call(this);
    };
    // ---------------------------------------------------------------------------------------------
    Projectile.prototype.FireFrom = function (owner, position, velocity) {
        this._owner = owner;
        this._lastDrawPos.x = position.x;
        this._lastDrawPos.y = position.y;
        this._lastPhysPos.x = position.x;
        this._lastPhysPos.y = position.y;
        this.position.set(position.x, position.y);
        this._velocity = velocity;
    };
    /**
     * a method to quickly clone this object, meant to be used at runtime in place of the slower
     * `clone` method. Must be overriden for each different projectile class that inherits from
     * projectile
     */
    Projectile.prototype.FastClone = function () {
        var r = new (Object.getPrototypeOf(this).constructor)();
        r.damage = this.damage;
        r._size = this._size;
        r.mass = this.mass;
        r.color = this.color;
        r.CreateCollider();
        return r;
    };
    return Projectile;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.PhysicsEntity));

var BeamSegment = /** @class */ (function (_super) {
    __extends(BeamSegment, _super);
    function BeamSegment(texture) {
        if (texture === void 0) { texture = null; }
        var _this = _super.call(this) || this;
        _this._sprite = null;
        _this.texture = null;
        _this.textureWidthFactor = 2;
        _this._name = "Beam Segment";
        return _this;
    }
    Object.defineProperty(BeamSegment.prototype, "trailingSegment", {
        get: function () { return this._trailingSegment; },
        set: function (val) { this._trailingSegment = val; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    BeamSegment.prototype.CreateGraphic = function () {
        if (this.texture != null) {
            this._sprite = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Sprite(this.texture);
            this.addChild(this._sprite);
        }
        else {
            _super.prototype.CreateGraphic.call(this);
        }
    };
    BeamSegment.prototype.UpdateGraphic = function () { };
    BeamSegment.prototype.UpdateGraphic_Self = function () {
        // if it has no texture, just use vector graphics
        if (this._sprite == null) {
            _super.prototype.UpdateGraphic.call(this);
            return;
        }
        // if there is a texture
        var wScale = this.size / this.texture.height;
        wScale *= this.textureWidthFactor;
        // set sprite texture and position
        this._sprite.texture = this.texture;
        this._sprite.anchor.set(0, 0.5);
        // calculate the difference between the last draw position and current position
        var dif = this.globalPosition;
        _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Subtract(dif, this._lastDrawPos, dif);
        this._sprite.position.set(-dif.x, -dif.y);
        this._sprite.scale.x = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Magnitude(dif) / this.texture.width;
        this._sprite.scale.y = wScale;
        this._sprite.rotation = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Direction(dif);
    };
    BeamSegment.prototype.OnCollision = function (collision) {
        var otherCol = _internal__WEBPACK_IMPORTED_MODULE_1__.Collision.OtherCollider(collision, this._collider);
        // get the projectile's allegience
        var allegiance = null;
        if (_internal__WEBPACK_IMPORTED_MODULE_1__.IAllied.ImplementedIn(this.owner)) {
            allegiance = this.owner.allegiance;
        }
        // skip a collision if it's friendly with this projectile
        var resParent = otherCol.parentEntity;
        if (allegiance != null) {
            if (_internal__WEBPACK_IMPORTED_MODULE_1__.ITarget.ImplementedIn(resParent)) {
                var aggr = allegiance.AggresionToward(resParent);
                if (aggr == _internal__WEBPACK_IMPORTED_MODULE_1__.Aggression.friendly) {
                    return;
                }
            }
        }
        var normal = collision.directionNormal;
        if (otherCol.parentEntity == this) {
            normal = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Invert(normal);
        }
        // if the hit object is implements the target interface, apply a projecile hit call on it
        if (_internal__WEBPACK_IMPORTED_MODULE_1__.ITarget.ImplementedIn(otherCol.parentEntity)) {
            var target = otherCol.parentEntity;
            target.Hit(this.damage, this.GetCurrentImpulse(), collision.collisionPoint, this.owner);
        }
        if (this._lifetime > 0) {
            _internal__WEBPACK_IMPORTED_MODULE_1__.AnimatedEffect.HitEffect(this.parentScene, collision.collisionPoint, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(), _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Direction(normal) + Math.PI, 0xFFEE88);
        }
        this.TrailToTrailingSegment();
        this.RemoveFromScene(true);
    };
    // ---------------------------------------------------------------------------------------------
    BeamSegment.prototype.OnPreRender = function () {
        var _a;
        if ((_a = this._trailingSegment) === null || _a === void 0 ? void 0 : _a.removed)
            this._trailingSegment = null;
        this.TrailToTrailingSegment();
    };
    BeamSegment.prototype.TrailToTrailingSegment = function () {
        var pos;
        if (this._trailingSegment != null)
            pos = this._trailingSegment.globalPosition;
        else
            pos = this._lastPhysPos;
        this._lastDrawPos.x = pos.x;
        this._lastDrawPos.y = pos.y;
        this.UpdateGraphic_Self();
    };
    // ---------------------------------------------------------------------------------------------
    BeamSegment.prototype.FastClone = function () {
        var r = _super.prototype.FastClone.call(this);
        r.texture = this.texture;
        r.textureWidthFactor = this.textureWidthFactor;
        return r;
    };
    return BeamSegment;
}(Projectile));



/***/ }),

/***/ 2737:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Weapon": () => (/* binding */ Weapon),
/* harmony export */   "Firearm": () => (/* binding */ Firearm),
/* harmony export */   "Handgun": () => (/* binding */ Handgun),
/* harmony export */   "Shotgun": () => (/* binding */ Shotgun),
/* harmony export */   "BeamLaser": () => (/* binding */ BeamLaser)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var _a, _b, _c, _d;
var _this = undefined;
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var Weapon = /** @class */ (function (_super) {
    __extends(Weapon, _super);
    function Weapon() {
        var _e;
        var _this = _super.call(this) || this;
        _this._barrelPosition = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        _this._name = (_e = _this.constructor) === null || _e === void 0 ? void 0 : _e.name;
        return _this;
    }
    Object.defineProperty(Weapon.prototype, "barrelPosition", {
        get: function () {
            return this.toGlobal(this._barrelPosition);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Weapon.prototype, "readyAlternate", {
        get: function () { return false; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Weapon.prototype, "readyTertiary", {
        get: function () { return false; },
        enumerable: false,
        configurable: true
    });
    return Weapon;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity));

var Firearm = /** @class */ (function (_super) {
    __extends(Firearm, _super);
    function Firearm() {
        var _this = _super.call(this) || this;
        _this._payload = null;
        _this._projectileSpeed = 2000;
        _this._spread = 0;
        _this._speedSpread = 0;
        _this._fireDelay = 0.2;
        _this._semiAuto = false;
        _this._clipSize = 12;
        _this._reloadTime = 2.5;
        _this._recoil = 350;
        _this._barrelPosition = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, 0);
        _this._lastTriggeredTick = 0;
        _this._fireWait = 0;
        _this._roundsLeft = null;
        _this._isReloading = false;
        _this.CreateGraphic();
        return _this;
    }
    Object.defineProperty(Firearm.prototype, "readyPrimary", {
        get: function () { return !this._isReloading; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    Firearm.CreateDefault = function () {
        throw "illegal call";
    };
    Firearm.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.MakeSerializable(this, function (x) { return [
            //x._payload,
            x._projectileSpeed,
            x._spread,
            x._speedSpread,
            x._fireDelay,
            x._semiAuto,
            x._clipSize,
            x._reloadTime,
            x._recoil,
            x._barrelPosition
        ]; });
    };
    // ---------------------------------------------------------------------------------------------
    Firearm.prototype.CreateGraphic = function () {
        this._sprite = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Sprite(null);
        this._sprite.anchor.set(0, 0.5);
        this.addChild(this._sprite);
        var ths = this;
        this._spriteSheet = new _internal__WEBPACK_IMPORTED_MODULE_1__.SpriteSheet('./dist/assets/graphics/Handgun.png');
        this._spriteSheet.GenerateSprites(1, 1, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, 0.5));
        this._spriteSheet.DoOnLoaded(function () {
            ths._sprite.texture = ths._spriteSheet.sprites[0];
        });
    };
    Firearm.prototype.Update = function (deltaTime) {
        _super.prototype.Update.call(this, deltaTime);
        if (this._roundsLeft == null)
            this._roundsLeft = this._clipSize;
        // if reloading, do nothing else
        if (this.HandleReload(deltaTime)) {
            this._lastTriggeredTick++;
            return;
        }
        // deduct the fire timer
        if (this._fireWait > 0)
            this._fireWait -= deltaTime;
        else if (this._fireDelay < 0)
            this._fireWait = 0;
        // increment triggered tickstamp
        this._lastTriggeredTick++;
    };
    Firearm.prototype.HandleReload = function (deltaTime) {
        // if reloading, do nothing else
        if (this._isReloading) {
            // deduct reload timer
            if (this._fireWait > 0)
                this._fireWait -= deltaTime;
            else {
                this.FinishReload();
            }
            return true;
        }
        return false;
    };
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    Firearm.prototype.Use = function (user, aim) {
        // weapon can fire if fire wait is up
        var canFire = this._fireWait <= 0;
        // if it was not triggered last frame, we can fire (to make it semi-auto)
        if (this._semiAuto)
            canFire = canFire && this._lastTriggeredTick > 1;
        // fire if possible
        if (canFire) {
            if (this._roundsLeft > 0)
                this.Fire(user, aim);
        }
        // reload if empty
        if (this._roundsLeft <= 0)
            this.Reload(user, aim);
        // reset the triggered tickstamp
        this._lastTriggeredTick = 0;
    };
    Firearm.prototype.Fire = function (user, aim) {
        if (this._roundsLeft <= 0)
            return;
        this._fireWait = this._fireDelay;
        this._roundsLeft--;
        this.ApplyRecoil(user, aim);
        var tpos = this.barrelPosition;
        this.ReleasePayload(user, aim, tpos);
    };
    Firearm.prototype.Reload = function (user, aim) {
        // don't reload if you don't gotta
        if (this._isReloading || this._roundsLeft >= this._clipSize) {
            return;
        }
        this._sprite.visible = false;
        this._fireWait = this._reloadTime;
        this._isReloading = true;
    };
    Firearm.prototype.ReleasePayload = function (user, aim, position) {
        if (this._payload == null)
            throw "no projectile payload";
        var direction = aim + (Math.random() - 0.5) * this._spread;
        var speed = this._projectileSpeed + (Math.random() - 0.5) * this._speedSpread;
        var vel = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.FromDirection(direction, speed);
        //let proj = Projectile.CreateAt(position, direction, speed, this._projectileDamage, user);
        var proj = this._payload.FastClone();
        proj.FireFrom(user, position, vel);
        this.parentScene.addChild(proj);
    };
    Firearm.prototype.FinishReload = function () {
        this._sprite.visible = true;
        this._fireWait = 0;
        this._isReloading = false;
        this._roundsLeft = this._clipSize;
    };
    Firearm.prototype.ApplyRecoil = function (user, aim) {
        if (!(user instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.PhysicsEntity))
            return;
        user.ApplyImpulse(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.FromDirection(aim + Math.PI, this._recoil));
    };
    return Firearm;
}(Weapon));

_a = Firearm;
(function () {
    _a.SetSerializableFields();
})();
var Handgun = /** @class */ (function (_super) {
    __extends(Handgun, _super);
    function Handgun() {
        var _this = _super.call(this) || this;
        _this._spread = 0.05;
        _this._speedSpread = 350;
        _this._recoil = 350;
        _this._fireDelay = 0.15;
        _this._semiAuto = true;
        _this._clipSize = 12;
        _this._reloadTime = 1.65;
        _this._barrelPosition = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(3, -1);
        _this._payload = new _internal__WEBPACK_IMPORTED_MODULE_1__.Projectile();
        _this._payload.damage = 7;
        _this._payload.size = 1;
        _this._payload.mass = 0.75;
        return _this;
    }
    // ---------------------------------------------------------------------------------------------
    Handgun.CreateDefault = function () {
        return new this();
    };
    // ---------------------------------------------------------------------------------------------
    Handgun.prototype.CreateGraphic = function () {
        this._sprite = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Sprite(null);
        this._sprite.anchor.set(0, 0.5);
        this.addChild(this._sprite);
        var ths = this;
        this._spriteSheet = new _internal__WEBPACK_IMPORTED_MODULE_1__.SpriteSheet('./dist/assets/graphics/Handgun.png');
        this._spriteSheet.GenerateSprites(1, 1, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, 0.5));
        this._spriteSheet.DoOnLoaded(function () {
            ths._sprite.texture = ths._spriteSheet.sprites[0];
        });
    };
    return Handgun;
}(Firearm));

_b = Handgun;
(function () {
    _b.SetSerializableFields();
})();
var Shotgun = /** @class */ (function (_super) {
    __extends(Shotgun, _super);
    function Shotgun() {
        var _this = _super.call(this) || this;
        _this._projectileCount = 8;
        _this._semiAuto = true;
        _this._projectileSpeed = 700;
        _this._spread = 0.35;
        _this._speedSpread = 800;
        _this._recoil = 5250;
        _this._fireDelay = 0.4;
        _this._clipSize = 6;
        _this._reloadTime = 0.55;
        _this._barrelPosition = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(5.5, -1);
        _this._payload = new _internal__WEBPACK_IMPORTED_MODULE_1__.Projectile();
        _this._payload.damage = 4;
        _this._payload.size = 1;
        _this._payload.mass = 1;
        return _this;
    }
    // ---------------------------------------------------------------------------------------------
    Shotgun.CreateDefault = function () {
        return new this();
    };
    Shotgun.SetSerializableFields = function () {
        _super.SetSerializableFields.call(this);
        _internal__WEBPACK_IMPORTED_MODULE_1__.ISerializable.MakeSerializable(this, function (x) { return [
            x._projectileCount
        ]; });
    };
    // ---------------------------------------------------------------------------------------------
    Shotgun.prototype.CreateGraphic = function () {
        this._sprite = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Sprite(null);
        this._sprite.anchor.set(0.25, 0.5);
        this.addChild(this._sprite);
        var ths = this;
        this._spriteSheet = new _internal__WEBPACK_IMPORTED_MODULE_1__.SpriteSheet('./dist/assets/graphics/Shotgun.png');
        this._spriteSheet.GenerateSprites(1, 1, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0.25, 0.5));
        this._spriteSheet.DoOnLoaded(function () {
            ths._sprite.texture = ths._spriteSheet.sprites[0];
        });
    };
    // ---------------------------------------------------------------------------------------------
    Shotgun.prototype.Use = function (user, aim) {
        if (this._isReloading) {
            if (this._roundsLeft > 0) {
                this.FinishReload();
                this._lastTriggeredTick = 0;
            }
            return;
        }
        _super.prototype.Use.call(this, user, aim);
    };
    Shotgun.prototype.ReleasePayload = function (user, aim, position) {
        for (var i = this._projectileCount; i > 0; i--) {
            _super.prototype.ReleasePayload.call(this, user, aim, position);
        }
    };
    Shotgun.prototype.HandleReload = function (deltaTime) {
        // if reloading, do nothing else
        if (this._isReloading) {
            // deduct reload timer
            if (this._fireWait > 0)
                this._fireWait -= deltaTime;
            else {
                this._roundsLeft += 1;
                if (this._roundsLeft >= this._clipSize)
                    this.FinishReload();
                else {
                    this._fireWait = this._reloadTime;
                }
            }
            return true;
        }
        return false;
    };
    Shotgun.prototype.FinishReload = function () {
        this._sprite.visible = true;
        this._fireWait = 0;
        this._isReloading = false;
    };
    return Shotgun;
}(Firearm));

_c = Shotgun;
(function () {
    _c.SetSerializableFields();
})();
var BeamLaser = /** @class */ (function (_super) {
    __extends(BeamLaser, _super);
    function BeamLaser() {
        var _this = _super.call(this) || this;
        _this._payload = null;
        _this._projectileSpeed = 1400;
        _this._lastBeamSeg = null;
        _this._lastTriggeredTick = 1;
        _this._lastTriggeredAim = 0;
        _this._lastTriggeredUser = null;
        _this._barrelPosition = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(9, -1.5);
        _this._payload = new _internal__WEBPACK_IMPORTED_MODULE_1__.BeamSegment();
        _this._name = "Proton Beam";
        _this._payload.damage = 80;
        _this._payload.mass = 10;
        _this._payload.size = 3;
        _this._payload.color = 0xFF4422;
        _this.CreateGraphic();
        return _this;
    }
    Object.defineProperty(BeamLaser.prototype, "readyPrimary", {
        get: function () { return true; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    BeamLaser.CreateDefault = function () {
        return new this();
    };
    // ---------------------------------------------------------------------------------------------
    BeamLaser.prototype.CreateGraphic = function () {
        this._sprite = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Sprite(null);
        this._sprite.anchor.set(0.1, 0.625);
        this.addChild(this._sprite);
        var ths = this;
        this._spriteSheet = new _internal__WEBPACK_IMPORTED_MODULE_1__.SpriteSheet('./dist/assets/graphics/BeamLaser.png');
        this._spriteSheet.GenerateSprites(1, 1, this._sprite.anchor);
        this._spriteSheet.DoOnLoaded(function () {
            ths._sprite.texture = ths._spriteSheet.sprites[0];
        });
        var sheet = new _internal__WEBPACK_IMPORTED_MODULE_1__.SpriteSheet('./dist/assets/graphics/ProtonBeam.png');
        sheet.GenerateSprites(1, 1);
        sheet.DoOnLoaded(function () {
            ths._payload.texture = sheet.sprites[0];
            ths._payload.textureWidthFactor = 7 / 4;
        });
    };
    // ---------------------------------------------------------------------------------------------
    BeamLaser.prototype.Update = function (deltaTime) {
        _super.prototype.Update.call(this, deltaTime);
        if (this._lastTriggeredTick <= 0) {
            this.Fire(deltaTime, this._lastTriggeredUser, this._lastTriggeredAim);
        }
        else {
            this._lastBeamSeg = null;
        }
        // increment triggered tickstamp
        this._lastTriggeredTick++;
    };
    BeamLaser.prototype.Use = function (user, aim) {
        this._lastTriggeredAim = aim;
        this._lastTriggeredUser = user;
        // reset the triggered tickstamp
        this._lastTriggeredTick = 0;
    };
    BeamLaser.prototype.Fire = function (deltaTime, owner, aim) {
        var tpos = this.barrelPosition;
        var proj = this._payload.FastClone();
        proj.damage = this._payload.damage * deltaTime;
        proj.mass = this._payload.mass * deltaTime;
        proj.FireFrom(owner, tpos, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.FromDirection(aim, this._projectileSpeed));
        owner.parentScene.addChild(proj);
        if (this._lastBeamSeg != null)
            this._lastBeamSeg.trailingSegment = proj;
        this._lastBeamSeg = proj;
    };
    return BeamLaser;
}(Weapon));

_d = BeamLaser;
(function () {
    _d.SetSerializableFields();
})();


/***/ }),

/***/ 6663:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IUIFocusable": () => (/* binding */ IUIFocusable)
/* harmony export */ });
var IUIFocusable;
(function (IUIFocusable) {
    /** Type checking discriminator for IUISelectable */
    function ImplementedIn(obj) {
        var r = obj;
        return (r.isFocused !== undefined &&
            r.SetFocus !== undefined);
    }
    IUIFocusable.ImplementedIn = ImplementedIn;
})(IUIFocusable || (IUIFocusable = {}));


/***/ }),

/***/ 2137:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IUISubmittable": () => (/* binding */ IUISubmittable)
/* harmony export */ });
var IUISubmittable;
(function (IUISubmittable) {
    /** Type checking discriminator for IUISelectable */
    function ImplementedIn(obj) {
        var r = obj;
        return (r.submitAction !== undefined &&
            r.Submit !== undefined);
    }
    IUISubmittable.ImplementedIn = ImplementedIn;
})(IUISubmittable || (IUISubmittable = {}));


/***/ }),

/***/ 864:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UICheckboxElement": () => (/* binding */ UICheckboxElement)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


var UICheckboxElement = /** @class */ (function (_super) {
    __extends(UICheckboxElement, _super);
    function UICheckboxElement(value) {
        var _this = _super.call(this) || this;
        // ---------------------------------------------------------------------------------------------
        _this._checkGraphic = null;
        _this._checkGraphicContainer = null;
        _this._value = false;
        _this.submitAction = new _internal__WEBPACK_IMPORTED_MODULE_1__.GameEvent();
        var gfxSize = UICheckboxElement.TICK_SIZE;
        _this.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(gfxSize + 8);
        _this._checkGraphic = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Graphics();
        _this._checkGraphic.beginFill(0xFFFFFF, 1);
        _this._checkGraphic.drawRect(0, 0, gfxSize, gfxSize);
        _this._checkGraphic.endFill();
        _this._checkGraphicContainer = new _internal__WEBPACK_IMPORTED_MODULE_1__.UIElement();
        _this._checkGraphicContainer.addChild(_this._checkGraphic);
        _this._checkGraphicContainer.uiOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(-1, 0);
        _this._checkGraphicContainer.uiAnchor = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0.5, 0.5);
        _this._checkGraphicContainer.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(gfxSize, gfxSize);
        _this._checkGraphicContainer.uiPivot = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0.5, 0.5);
        _this.addChild(_this._checkGraphicContainer);
        _this._checkGraphicContainer.visible = value;
        _this._value = value;
        return _this;
    }
    Object.defineProperty(UICheckboxElement.prototype, "value", {
        get: function () {
            return this._value;
        },
        set: function (val) {
            this._value = val;
            this._checkGraphicContainer.visible = val;
        },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    UICheckboxElement.prototype.Submit = function (action) {
        this.value = !this.value;
        this.submitAction.Invoke(action);
    };
    UICheckboxElement.TICK_SIZE = 7;
    return UICheckboxElement;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.UIElement));



/***/ }),

/***/ 9099:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UIContainer": () => (/* binding */ UIContainer)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var UIContainer = /** @class */ (function (_super) {
    __extends(UIContainer, _super);
    function UIContainer(game) {
        var _this = _super.call(this, game) || this;
        _this.uiClipboard = null;
        _this.focusables = new Array();
        _this._uiRect = _internal__WEBPACK_IMPORTED_MODULE_0__.Rect.Create(_internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(), _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(game.renderer.view.width, game.renderer.view.height));
        return _this;
    }
    Object.defineProperty(UIContainer.prototype, "uiRect", {
        get: function () { return this._uiRect; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UIContainer.prototype, "parentRootScene", {
        get: function () { return this._parentScene; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    UIContainer.prototype.addChild = function () {
        var childs = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            childs[_i] = arguments[_i];
        }
        var r = _super.prototype.addChild.apply(this, childs);
        // iterate through each child added
        for (var i = 0; i < childs.length; i++) {
            // if the child is a ui element
            var child = childs[i];
            if (child instanceof _internal__WEBPACK_IMPORTED_MODULE_0__.UIElement) {
                // if it also implementents IUIFocusable, add it to the focusable array
                if (_internal__WEBPACK_IMPORTED_MODULE_0__.IUIFocusable.ImplementedIn(child)) {
                    this.focusables.push(child);
                }
            }
        }
        return r;
    };
    // ---------------------------------------------------------------------------------------------
    UIContainer.prototype.DrawUI = function (renderer) {
        renderer.render(this, {
            clear: false
        });
    };
    // ---------------------------------------------------------------------------------------------
    UIContainer.prototype.GetUIElementsAtPoint = function (point) {
        var col = new _internal__WEBPACK_IMPORTED_MODULE_0__.AABBCollider();
        col.SetRect(_internal__WEBPACK_IMPORTED_MODULE_0__.Rect.Create(point, _internal__WEBPACK_IMPORTED_MODULE_0__.Vect.Create(1, 1)));
        return this.GetUIElementsOnCollider(col);
    };
    UIContainer.prototype.GetUIElementsOnCollider = function (collider) {
        var r = new Array();
        var cols = this.colliders.FindOverlapColliders(collider);
        for (var i = 0; i < cols.length; i++) {
            if (cols[i].parentEntity instanceof _internal__WEBPACK_IMPORTED_MODULE_0__.UIElement) {
                r.push(cols[i].parentEntity);
            }
        }
        return r;
    };
    UIContainer.prototype.GetFocusedElements = function () {
        var r = new Array();
        // iterate through each focusable element
        for (var i = this.focusables.length - 1; i >= 0; i--) {
            // if it has been removed from the scene, remove it from the focusable array and skip 
            // the rest of the logic for this iteration
            if (this.focusables[i].removed) {
                this.focusables[i].SetFocus(false);
                this.focusables.splice(i, 1);
                continue;
            }
            // if the element is focused, add it to the return array
            if (this.focusables[i].isFocused) {
                r.push(this.focusables[i]);
            }
        }
        return r;
    };
    return UIContainer;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.Scene));



/***/ }),

/***/ 2465:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UIElement": () => (/* binding */ UIElement)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


/** a specialized scene entity used for creating ui panels and controls */
var UIElement = /** @class */ (function (_super) {
    __extends(UIElement, _super);
    function UIElement() {
        var _this = _super.call(this) || this;
        _this._panelGraphic = null;
        _this._panelCollider = null;
        /** how the element calculates it's position relative to it's parent */
        _this.uiAnchor = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, 0);
        /** how the element positions it's rect relative to it's own position */
        _this.uiPivot = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, 0);
        /** the offset in pixels of the element from it's anchored position */
        _this.uiOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, 0);
        /** how the element calculate's it's rect's size relative to it's parent's rect size */
        _this.uiSize = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, 0);
        /** the offset in pixels of the element's rect size from it's size calculated from uiSize */
        _this.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, 0);
        _this._name = "Ui Element";
        return _this;
    }
    Object.defineProperty(UIElement.prototype, "ParentContainer", {
        /** the ui container object that this element belongs to */
        get: function () { return this.parentScene; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    /**
     * set the element properties so that it is anchored to the center of it's parent, then return
     * itself after properties are adjusted
     */
    UIElement.prototype.AlignedCenter = function () {
        this.uiAnchor.x = 0.5;
        this.uiAnchor.y = 0.5;
        this.uiPivot.x = 0.5;
        this.uiPivot.y = 0.5;
        return this;
    };
    /**
     * set the element ui properties so that it's anchor and pivot are set to the specified
     * alignment parameters, then return itself
     * @param x the value to set anchor and pivot x components
     * @param y value to apply to anchor and pivot y components
     */
    UIElement.prototype.Aligned = function (x, y) {
        if (x === void 0) { x = 0; }
        if (y === void 0) { y = x; }
        this.uiAnchor.x = x;
        this.uiAnchor.y = y;
        this.uiPivot.x = x;
        this.uiPivot.y = y;
        return this;
    };
    /**
     * updates the object position to represent the appropriate ui position relative to parents
     * @param deepUpdate whether or not all children should also recursively update their positions
     */
    UIElement.prototype.UpdateUiPosition = function (deepUpdate) {
        if (deepUpdate === void 0) { deepUpdate = true; }
        // update to appropriate position according to the ui rect
        this.GetUiPosition();
        if (!deepUpdate)
            return;
        // deep recursive update through children
        var entities = this.entities;
        for (var i = 0; i < entities.length; i++) {
            var ent = entities[i];
            if (ent instanceof UIElement) {
                ent.UpdateUiPosition(deepUpdate);
            }
        }
    };
    // ---------------------------------------------------------------------------------------------
    /** gets and updates the ui position according to all it's ancestor's rects */
    UIElement.prototype.GetUiPosition = function () {
        // get the parent rect
        var parentRect = this.GetParentUiRect();
        // calculate the screenspace coordinates from parent rect
        var tpos = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Multiply(this.uiAnchor, parentRect.size);
        tpos.x += this.uiOffset.x;
        tpos.y += this.uiOffset.y;
        tpos.x += parentRect.position.x;
        tpos.y += parentRect.position.y;
        var tsiz = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Multiply(this.uiSize, parentRect.size);
        tsiz.x += this.uiSizeOffset.x;
        tsiz.y += this.uiSizeOffset.y;
        tpos.x -= tsiz.x * this.uiPivot.x;
        tpos.y -= tsiz.y * this.uiPivot.y;
        // update local entity position
        this.globalPosition = tpos;
        return tpos;
    };
    /** gets the current size of the element's ui rect calculated from it's parent size */
    UIElement.prototype.GetUiSize = function () {
        // get the parent rect
        var parentRect = this.GetParentUiRect();
        var tsiz = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Multiply(this.uiSize, parentRect.size);
        tsiz.x += this.uiSizeOffset.x;
        tsiz.y += this.uiSizeOffset.y;
        return tsiz;
    };
    /** gets the ui rect of this element's direct parent */
    UIElement.prototype.GetParentUiRect = function () {
        var parentRect = null;
        if (this.parentEntity instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.UIContainer) {
            parentRect = this.ParentContainer.uiRect;
        }
        else if (this.parentEntity instanceof UIElement) {
            parentRect = this.parentEntity.GetUiRect();
        }
        else if (this.parentEntity == null) {
            parentRect = _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.Create(_internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(), _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create());
        }
        else {
            parentRect = _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.Create(this.parentEntity.globalPosition, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, 0));
        }
        return parentRect;
    };
    /** gets the ui rect of this element */
    UIElement.prototype.GetUiRect = function () {
        // get the parent rect
        var parentRect = this.GetParentUiRect();
        // calculate the scrrenspace coordinates from parent rect
        var tpos = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Multiply(this.uiAnchor, parentRect.size);
        tpos.x += this.uiOffset.x;
        tpos.y += this.uiOffset.y;
        tpos.x += parentRect.position.x;
        tpos.y += parentRect.position.y;
        // calculate size from parent rect size
        var tsiz = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Multiply(this.uiSize, parentRect.size);
        tsiz.x += this.uiSizeOffset.x;
        tsiz.y += this.uiSizeOffset.y;
        // calculate top left of rect based on pivot
        var topLeft = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Subtract(tpos, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Multiply(this.uiPivot, tsiz));
        // update local entity position
        tpos.x -= tsiz.x * this.uiPivot.x;
        tpos.y -= tsiz.y * this.uiPivot.y;
        this.globalPosition = tpos;
        // create the rect from the calculated coordinates
        var rect = _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.Create(topLeft, tsiz);
        return rect;
    };
    /** @inheritdoc */
    UIElement.prototype.addChild = function () {
        var childs = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            childs[_i] = arguments[_i];
        }
        var r = _super.prototype.addChild.apply(this, childs);
        for (var i = childs.length - 1; i >= 0; i--) {
            var child = childs[i];
            if (child instanceof UIElement) {
                child.UpdateUiPosition(true);
            }
        }
        return r;
    };
    UIElement.prototype.OnAddedToScene = function (scene) {
        _super.prototype.OnAddedToScene.call(this, scene);
        // add self to container focusables array if necessary
        if (scene instanceof _internal__WEBPACK_IMPORTED_MODULE_1__.UIContainer) {
            if (_internal__WEBPACK_IMPORTED_MODULE_1__.IUIFocusable.ImplementedIn(this)) {
                // TODO fix this element getting added multiple times so this check can be removed
                var index = scene.focusables.indexOf(this);
                if (index < 0)
                    scene.focusables.push(this);
            }
        }
    };
    UIElement.prototype.OnRemovedFromScene = function (scene) {
        _super.prototype.OnRemovedFromScene.call(this, scene);
        // remove self from container focusable array if necessary
        if (_internal__WEBPACK_IMPORTED_MODULE_1__.IUIFocusable.ImplementedIn(this)) {
            var index = this.ParentContainer.focusables.indexOf(this);
            this.ParentContainer.focusables.splice(index, 1);
        }
    };
    // ---------------------------------------------------------------------------------------------
    /**
     * expands the sizeOffset of the UI element to fit all the UI element children inside it
     * @param paddingX how much horizontal padding between children and edge of element
     * @param paddingY how much vertical padding between children and edge of element
     */
    UIElement.prototype.ExpandToFitChildren = function (paddingX, paddingY) {
        if (paddingX === void 0) { paddingX = 0; }
        if (paddingY === void 0) { paddingY = paddingX; }
        var oRect = this.GetUiRect();
        var tRect = _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.Clone(oRect);
        var ents = this.entities;
        for (var i = ents.length - 1; i >= 0; i--) {
            var ent = ents[i];
            if (ent instanceof UIElement) {
                var childRect = ent.GetUiRect();
                _internal__WEBPACK_IMPORTED_MODULE_1__.Rect.ExpandToOverlap(tRect, childRect);
            }
        }
        var difX = tRect.size.x - oRect.size.x;
        var difY = tRect.size.y - oRect.size.y;
        if (oRect.position.x > tRect.position.x)
            difX += paddingX;
        if (oRect.position.x + oRect.size.x < tRect.position.x + tRect.size.x)
            difX += paddingX;
        if (oRect.position.y > tRect.position.y)
            difY += paddingY;
        if (oRect.position.y + oRect.size.y < tRect.position.y + tRect.size.y)
            difY += paddingY;
        this.uiSizeOffset.x += difX;
        this.uiSizeOffset.y += difY;
    };
    /**
     * creates a basic panel graphic that represents the element's ui rect and adds it to the
     * scene hierarchy
     */
    UIElement.prototype.UpdatePanelGraphic = function () {
        // create or get the graphics object
        var graphics = null;
        if (this._panelGraphic != null) {
            graphics = this._panelGraphic;
            graphics.clear();
        }
        else {
            graphics = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Graphics();
            this.addChildAt(graphics, 0);
            this._panelGraphic = graphics;
        }
        var uiRect = this.GetUiRect();
        var tpos = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Subtract(uiRect.position, this.globalPosition);
        graphics.lineStyle({
            color: 0xFFFFFF,
            width: 1
        });
        graphics.beginFill(0x000000, 0.5);
        graphics.drawRect(tpos.x, tpos.y, uiRect.size.x - 1, uiRect.size.y - 1);
        graphics.endFill();
        return graphics;
    };
    /**
     * creates an AABBCollider that matches the element's ui rect and adds it to the scene hierarchy
     * and collider partitions
     * @returns
     */
    UIElement.prototype.UpdatePanelCollider = function () {
        var collider = null;
        if (this._panelCollider == null) {
            collider = new _internal__WEBPACK_IMPORTED_MODULE_1__.AABBCollider();
            this.addChild(collider);
        }
        else {
            collider = this._panelCollider;
        }
        collider.SetRect(this.GetUiRect());
        return collider;
    };
    return UIElement;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity));



/***/ }),

/***/ 6064:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UIInteractiveElement": () => (/* binding */ UIInteractiveElement)
/* harmony export */ });
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/** a ui basic ui element which implements the subittable interface */
var UIInteractiveElement = /** @class */ (function (_super) {
    __extends(UIInteractiveElement, _super);
    function UIInteractiveElement() {
        var _this = _super.call(this) || this;
        _this._selectAction = new _internal__WEBPACK_IMPORTED_MODULE_0__.GameEvent();
        _this._name = "Ui Interactive Element";
        return _this;
    }
    Object.defineProperty(UIInteractiveElement.prototype, "submitAction", {
        /** @inheritdoc */
        get: function () { return this._selectAction; },
        enumerable: false,
        configurable: true
    });
    /** @inheritdoc */
    UIInteractiveElement.prototype.Submit = function (action) {
        this.submitAction.Invoke(action);
    };
    return UIInteractiveElement;
}(_internal__WEBPACK_IMPORTED_MODULE_0__.UIElement));



/***/ }),

/***/ 5159:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UITextElement": () => (/* binding */ UITextElement)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


/** a ui element that is used to display text */
var UITextElement = /** @class */ (function (_super) {
    __extends(UITextElement, _super);
    /** create from pixi text object */
    function UITextElement(text) {
        var _this = _super.call(this) || this;
        /** how the internal text object is anchored relative to this element's rect */
        _this.textAnchor = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(0, 0);
        _this.addChild(text);
        _this._text = text;
        _this.MatchSizeOffsetToText(0, 0);
        _this._name = "Ui Text '" + text.text + "'";
        return _this;
    }
    /**
     * create a text element from the specified string
     * @param text the string that the text element will contain
     * @param style the style to format the text by
     */
    UITextElement.CreateFromString = function (text, style) {
        if (style === void 0) { style = this.DEFAULT_STYLE; }
        var txt = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Text(text, style);
        return new UITextElement(txt);
    };
    Object.defineProperty(UITextElement.prototype, "text", {
        /** getter for the internal PIXI text object which is used to render the text string*/
        get: function () { return this._text; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    UITextElement.prototype.UpdateUiPosition = function (deepUpdate) {
        if (deepUpdate === void 0) { deepUpdate = true; }
        _super.prototype.UpdateUiPosition.call(this, deepUpdate);
        this.UpdateTextPosition();
    };
    /** @inheritdoc */
    UITextElement.prototype.AlignedCenter = function () {
        _super.prototype.AlignedCenter.call(this);
        this.textAnchor.x = 0.5;
        this.textAnchor.y = 0.5;
        return this;
    };
    /** update the internal text object's position according to textAnchor */
    UITextElement.prototype.UpdateTextPosition = function () {
        // get some data about the text and ui space for calculations
        var rect = this.GetUiRect();
        var textSize = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this._text.width, this._text.height);
        // calculate the offset from the top left of the ui rect, based on the text achor
        var toff = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create();
        toff.x = this.textAnchor.x * (rect.size.x - textSize.x);
        toff.y = this.textAnchor.y * (rect.size.y - textSize.y);
        // apply the position
        _internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity.SetGlobalPosition(this._text, _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(rect.position.x + toff.x, rect.position.y + toff.y));
    };
    // ---------------------------------------------------------------------------------------------
    /**
     * Set the size offset of the ui element so it matches the width and height of the text
     * @param paddingX the amount of space between the text and the element's leftright edges
     * @param paddingY the amount of space between the text and the element's top/bottom edges
     */
    UITextElement.prototype.MatchSizeOffsetToText = function (paddingX, paddingY) {
        this.uiSizeOffset = _internal__WEBPACK_IMPORTED_MODULE_1__.Vect.Create(this._text.width + paddingX, this._text.height + paddingY);
    };
    /**
     * sets the element to render the specified string, and updates the text position
     * @param text
     */
    UITextElement.prototype.SetTextString = function (text) {
        this._text.text = text;
        this.UpdateTextPosition();
    };
    /**
     * convert the static text element to a text input field that the user can modify the text of
     * @param inputMode what text is allowed in the field
     */
    UITextElement.prototype.ToInputField = function (inputMode) {
        if (inputMode === void 0) { inputMode = _internal__WEBPACK_IMPORTED_MODULE_1__.TextInputMode.allowEverything; }
        this.removeChild(this._text);
        var r = new _internal__WEBPACK_IMPORTED_MODULE_1__.UITextInputField(this._text);
        r._name = this._name;
        r.uiAnchor = this.uiAnchor;
        r.uiOffset = this.uiOffset;
        r.uiSize = this.uiSize;
        r.uiSizeOffset = this.uiSizeOffset;
        r.uiPivot = this.uiPivot;
        r.inputMode = inputMode;
        return r;
    };
    /** default style for the in game text */
    UITextElement.DEFAULT_STYLE = {
        fontFamily: "Consolas",
        fontSize: 12,
        fill: 0xFFFFFF,
        stroke: 0x000000,
        strokeThickness: 1
    };
    return UITextElement;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.UIElement));



/***/ }),

/***/ 9454:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TextInputMode": () => (/* binding */ TextInputMode),
/* harmony export */   "UITextInputField": () => (/* binding */ UITextInputField)
/* harmony export */ });
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3049);
/* harmony import */ var pixi_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(pixi_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4993);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


/** specifies what text is valid in a text input field */
var TextInputMode;
(function (TextInputMode) {
    TextInputMode[TextInputMode["allowEverything"] = 0] = "allowEverything";
    TextInputMode[TextInputMode["alphaNumerals"] = 1] = "alphaNumerals";
    TextInputMode[TextInputMode["alphabetOnly"] = 2] = "alphabetOnly";
    TextInputMode[TextInputMode["decimal"] = 3] = "decimal";
    TextInputMode[TextInputMode["integer"] = 4] = "integer";
})(TextInputMode || (TextInputMode = {}));
/**
 * a ui element that is used as a text input field, allowing the user to modify the text inside of
 * this element that's displayed on screen
 */
var UITextInputField = /** @class */ (function (_super) {
    __extends(UITextInputField, _super);
    function UITextInputField(text) {
        var _this = _super.call(this, text) || this;
        // ---------------------------------------------------------------------------------------------
        _this._prevText = "";
        _this._focused = false;
        _this._cursor = null;
        /** whether or not the text input field will allow the user to enter an empty string */
        _this.allowEmpty = false;
        /** restrict input of the user based on this character set */
        _this.inputMode = TextInputMode.allowEverything;
        _this._keyEventListener = null;
        _this._selectAction = new _internal__WEBPACK_IMPORTED_MODULE_1__.GameEvent();
        _this.CreateCursor();
        return _this;
    }
    Object.defineProperty(UITextInputField.prototype, "isFocused", {
        /** if the user is currently editing the input field */
        get: function () { return this._focused; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(UITextInputField.prototype, "submitAction", {
        /** @inheritdoc */
        get: function () { return this._selectAction; },
        enumerable: false,
        configurable: true
    });
    // ---------------------------------------------------------------------------------------------
    /** @inheritdoc */
    UITextInputField.prototype.Update = function (deltaTime) {
        _super.prototype.Update.call(this, deltaTime);
        if (this._focused)
            this._cursor.visible = performance.now() % 1000 < 500;
    };
    // Cursor: -------------------------------------------------------------------------------------
    UITextInputField.prototype.CreateCursor = function () {
        var _a;
        this._cursor = new pixi_js__WEBPACK_IMPORTED_MODULE_0__.Graphics();
        var cursorSize = (_a = this.text.style.fontSize) !== null && _a !== void 0 ? _a : 12;
        this._cursor.lineStyle({
            color: 0xFFFFFF,
            width: 1
        });
        this._cursor.moveTo(0, cursorSize / -2);
        this._cursor.lineTo(0, cursorSize / 2);
        this._cursor.visible = false;
        this.addChild(this._cursor);
        this.PositionCursor();
    };
    UITextInputField.prototype.PositionCursor = function () {
        var pos = _internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity.GetGlobalPosition(this.text);
        pos.y += this.text.height * 0.5;
        if (this.text.text.length > 0)
            pos.x += this.text.width + 2;
        else
            pos.x += 3;
        _internal__WEBPACK_IMPORTED_MODULE_1__.SceneEntity.SetGlobalPosition(this._cursor, pos);
    };
    // User Control: -------------------------------------------------------------------------------
    /** @inheritdoc */
    UITextInputField.prototype.Submit = function (action) {
        // if the element is in and has already been edited, the result can be submitted
        if (this._focused) {
            if (!this.allowEmpty) {
                if (this.text.text.length <= 0) {
                    this.text.text = this._prevText;
                }
            }
            this.submitAction.Invoke(action);
            this.OffFocus();
        }
        // if the element has not gotten focus yet, allow the user to edit it before submitting
        else {
            this.OnFocus();
            if (action == 2) {
                this.text.text = "";
                this.PositionCursor();
            }
        }
    };
    /**
     * set the focus state of the element
     * @param focus whether or not the element should be in focus
     */
    UITextInputField.prototype.SetFocus = function (focus) {
        if (focus === this._focused)
            return;
        if (focus)
            this.OnFocus();
        else {
            this.submitAction.Invoke(0);
            this.OffFocus();
        }
    };
    UITextInputField.prototype.OnFocus = function () {
        // de-focus any other elements that are currently focused if necessary
        if (this.ParentContainer.uiModifier == _internal__WEBPACK_IMPORTED_MODULE_1__.SelectionModifier.neutral) {
            var others = this.ParentContainer.GetFocusedElements();
            for (var i = others.length - 1; i >= 0; i--) {
                others[i].SetFocus(false);
            }
        }
        this._prevText = this.text.text;
        this._focused = true;
        this.PositionCursor();
        this._cursor.visible = true;
        // add keyboard event listener
        var ths = this;
        var listener = function (e) {
            ths.OnKeyDown(e);
        };
        window.addEventListener('keydown', listener);
        this._keyEventListener = listener;
    };
    UITextInputField.prototype.OffFocus = function () {
        this._focused = false;
        this._cursor.visible = false;
        // remove keyboard event listener
        if (this._keyEventListener != null) {
            window.removeEventListener('keydown', this._keyEventListener);
            this._keyEventListener = null;
        }
    };
    UITextInputField.prototype.OnKeyDown = function (e) {
        // get the currently displayed text
        var txt = this.text.text;
        // if it's a keystroke with a typable character
        if (e.key.length == 1) {
            var char = e.key.toLowerCase();
            // if ctrl is pressed
            if (e.ctrlKey) {
                // copy
                if (char == 'c' && e.ctrlKey) {
                    this.ParentContainer.uiClipboard = txt;
                }
                // paste
                else if (char == 'v' && e.ctrlKey) {
                    if (this.ParentContainer.uiClipboard !== null) {
                        txt = this.ParentContainer.uiClipboard;
                    }
                }
            }
            // ctrl is not pressed
            else
                switch (this.inputMode) {
                    case TextInputMode.allowEverything:
                        txt += e.key;
                        break;
                    case TextInputMode.alphaNumerals:
                        if (UITextInputField.ALPHABETCHARS.includes(char) ||
                            UITextInputField.NUMERALCHARS.includes(char))
                            txt += e.key;
                        break;
                    case TextInputMode.alphabetOnly:
                        if (UITextInputField.ALPHABETCHARS.includes(char))
                            txt += e.key;
                        break;
                    case TextInputMode.decimal:
                        if (UITextInputField.NUMERALCHARS.includes(char))
                            txt += e.key;
                        if (char === '.') {
                            if (!txt.includes('.'))
                                txt += char;
                        }
                        break;
                    case TextInputMode.integer:
                        if (UITextInputField.NUMERALCHARS.includes(char))
                            txt += e.key;
                        break;
                }
        }
        // if it's a functional keystroke
        else {
            switch (e.key.toLowerCase()) {
                case "backspace":
                    if (e.ctrlKey) {
                        txt = "";
                    }
                    txt = txt.substring(0, txt.length - 1);
                    break;
                case "enter":
                    this.Submit(0);
                    break;
                case "escape":
                    txt = this._prevText;
                    this.text.text = txt;
                    this.Submit(0);
                    break;
            }
        }
        // apply the text to be displayed and position the cursor
        this.text.text = txt;
        this.PositionCursor();
    };
    /** used for enforcing text input mode */
    UITextInputField.NUMERALCHARS = "01234567890";
    UITextInputField.ALPHABETCHARS = "abcdefghijklmnopqrstuvwxyz";
    return UITextInputField;
}(_internal__WEBPACK_IMPORTED_MODULE_1__.UITextElement));



/***/ }),

/***/ 3049:
/***/ ((module) => {

module.exports = window["PIXI"];

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _internal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(4993);
///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/**
 * entry point of the program
 */
function initialize() {
    document.body.style.textAlign = "center";
    document.body.style.backgroundColor = "#000";
    document.body.style.color = "#CCC";
    document.body.style.padding = "0px";
    document.body.style.margin = "0px";
    _internal__WEBPACK_IMPORTED_MODULE_0__.Effect.Load();
    var game = new _internal__WEBPACK_IMPORTED_MODULE_0__.Game(800, 600);
    window.game = game;
    window.ISerializable = _internal__WEBPACK_IMPORTED_MODULE_0__.ISerializable;
    var lvlEdit = new _internal__WEBPACK_IMPORTED_MODULE_0__.LevelEditor(game);
    lvlEdit.EditGameSene(_internal__WEBPACK_IMPORTED_MODULE_0__.GameScene.BasicScene(game));
    game.currentScene = lvlEdit;
    // game.currentScene = GameScene.BasicScene(game);
    document.body.appendChild(game.renderer.view);
    game.renderer.backgroundColor = 0x202020;
    game.AttachKeyboardEvents();
    game.AttachMouseEvents();
    game.StartGameLoop();
}
window.addEventListener('load', initialize);

})();

window.Kinesis = __webpack_exports__;
/******/ })()
;
//# sourceMappingURL=bundle.js.map