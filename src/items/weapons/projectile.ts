///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	AnimatedEffect,
	AABBCollider, CircleCollider, Collider, CollisionLayer,
	Collision, ICollision,
	Effect,
	ITarget,
	IVect, Vect,
	PhysicsEntity,
	Ray,
	SceneEntity, 
	Aggression,
	IAllied,
	Allegiance,
	IReadonlyVect,
	Scene,
	SpriteSheet
} from '../../internal';

export class Projectile extends PhysicsEntity{

	public constructor() {
		super(); 
		this._name = "Projectile";
	}

	public get size() { return this._size; }
	public set size(val) { this._size = val; 
		if(this._collider != null) 
		(this._collider as CircleCollider).radius = this.size; 
	}
	protected _size: number = 2.5;
	
	public get owner() { return this._owner; }
	protected _owner: SceneEntity = null;
	
	protected _graphic: PIXI.Graphics = null;
	protected _lifetime = 1;

	protected _lastPhysPos: IVect = Vect.Create();
	protected _lastDrawPos: IVect = Vect.Create();

	protected set lastPos(val: IReadonlyVect){
		this._lastPhysPos.x = val.x;
		this._lastPhysPos.y = val.y;
		this._lastDrawPos.x = val.x;
		this._lastDrawPos.y = val.y;
	}

	public damage: number = 5;
	public color: number = 0xFFFF00;

	private _onPreRenderAction: () => void = null;
	
	// ---------------------------------------------------------------------------------------------

	protected override OnAddedToScene(scene: Scene){
		super.OnAddedToScene(scene);

		this.CreateGraphic();

		if(this._onPreRenderAction != null) throw "pre render action not cleared";

		let ths = this;
		this._onPreRenderAction = function(){
			ths.OnPreRender();
		};
		scene.onPreRender.AddListener(this._onPreRenderAction);
	}

	protected override OnRemovedFromScene(scene: Scene){
		super.OnRemovedFromScene(scene);
	
		scene.onPreRender.RemoveListener(this._onPreRenderAction);
		this._onPreRenderAction = null;
	}

	// ---------------------------------------------------------------------------------------------

	public GetCurrentImpulse(): IVect{
		return this.momentum;
	}

	protected CreateCollider(){

		let col = CircleCollider.Create(this._size);
		col.layer = CollisionLayer.projectiles;

		let ths = this;
		col.onCollision.AddListener(function(data: ICollision){
			ths.OnCollision(data);
		});

		this._collider = col;
		//this._collider.CreateWireframe(0xFF00FF, 1, true);
		this.addChild(this._collider);
	}

	protected CheckCollisions(){

		let ray = Ray.FromPoints(this._lastPhysPos, this.globalPosition);
		let results = this.parentScene.colliders.CircleCast(ray, this._size, this.collider.layer);

		if(results.length <= 0){
			return;
		}

		// get the projectile's allegience
		let allegiance: Allegiance = null;
		if(IAllied.ImplementedIn(this.owner)){
			allegiance = (this.owner as IAllied).allegiance;
		}

		// iterate through each result
		let closestResult = null;
		let closestDistSq = Number.POSITIVE_INFINITY;
		for(let i = results.length - 1; i >= 0; i--){
			let res = results[i];
			
			// skip any that did not enter
			if(!res.didEnter)
				continue;
			
			// skip a result if it's friendly with this projectile
			let resParent = res.collider.parentEntity;
			if(allegiance != null){
				if(ITarget.ImplementedIn(resParent)){
					let aggr = allegiance.AggresionToward(resParent);
					if(aggr == Aggression.friendly){
						continue;
					}
				}
			}

			// if there are any closer results, use those instead
			let distSq = Vect.DistanceSquared(results[i].entryPoint, this._lastPhysPos);
			if(distSq < closestDistSq){
				closestResult = res;
				closestDistSq = distSq;
			}
		}

		// ensure that there was at least one collider that the ray entered
		if(closestResult == null)
			return;

		// set the projectile position to the raycast result entry point
		this.globalPosition = closestResult.entryPoint;
		this.parentScene.colliders.ForceCollision(this._collider, closestResult.collider);
	}

	protected OnCollision(collision: ICollision){

		let otherCol = Collision.OtherCollider(collision, this._collider);

		// get the projectile's allegience
		let allegiance: Allegiance = null;
		if(IAllied.ImplementedIn(this.owner)){
			allegiance = (this.owner as IAllied).allegiance;
		}
		
		// skip a collision if it's friendly with this projectile
		let resParent = otherCol.parentEntity;
		if(allegiance != null){
			if(ITarget.ImplementedIn(resParent)){
				let aggr = allegiance.AggresionToward(resParent);
				if(aggr == Aggression.friendly){
					return;
				}
			}
		}

		let normal = collision.directionNormal;
		if(otherCol.parentEntity == this){
			normal = Vect.Invert(normal);
		}
		
		// if the hit object is implements the target interface, apply a projecile hit call on it
		if(ITarget.ImplementedIn(otherCol.parentEntity)){
			let target = otherCol.parentEntity as ITarget;
			target.Hit(this.damage, this.GetCurrentImpulse(), collision.collisionPoint, this.owner);
		}

		if(this._lifetime > 0){
			AnimatedEffect.HitEffect(
				this.parentScene, 
				collision.collisionPoint, 
				Vect.Create(), 
				Vect.Direction(normal) + Math.PI, 
				0xFFEE88
			);
		}

		this.RemoveFromScene(true);
	}

	protected CreateGraphic(): void{

		this._graphic = new PIXI.Graphics();
		this.addChild(this._graphic);
	}

	protected UpdateGraphic(): void{

		let pos = this.globalPosition;
		let dif = Vect.Subtract(pos, this._lastDrawPos);

		this._graphic.clear();
		this._graphic.lineStyle({
			color: this.color,
			width: this.size
		});
		this._graphic.moveTo(0, 0);
		this._graphic.lineTo(-dif.x, -dif.y);
	}

	
	protected OnPreRender(): void{
		this._lastDrawPos = this.globalPosition;	
	}

	// ---------------------------------------------------------------------------------------------

	/** @inheritdoc */
	public override Update(deltaTime: number){
		
		this._lastPhysPos = this.globalPosition;
		super.Update(deltaTime);
		this.CheckCollisions();

		this._lifetime -= deltaTime;

		this.UpdateGraphic();

		if(this._lifetime <= 0){
			this.RemoveFromScene();
		}
	}

	/** @inheritdoc */
	public override destroy(){
		this._collider.destroy();
		super.destroy();
	}

	// ---------------------------------------------------------------------------------------------

	public FireFrom(owner: SceneEntity, position: IReadonlyVect, velocity: IVect){
		this._owner = owner;
		this._lastDrawPos.x = position.x;
		this._lastDrawPos.y = position.y;
		this._lastPhysPos.x = position.x;
		this._lastPhysPos.y = position.y;
		this.position.set(position.x, position.y);
		this._velocity = velocity;
	}

	/**
	 * a method to quickly clone this object, meant to be used at runtime in place of the slower 
	 * `clone` method. Must be overriden for each different projectile class that inherits from
	 * projectile
	 */
	public FastClone(): Projectile{
		
		let r = new (Object.getPrototypeOf(this).constructor)();
		r.damage = this.damage;
		r._size = this._size;
		r.mass = this.mass;
		r.color = this.color;

		r.CreateCollider();
		return r;
	}
}

export class BeamSegment extends Projectile{

	public constructor(texture: PIXI.Texture = null){
		super();
		this._name = "Beam Segment";
	}

	protected _sprite: PIXI.Sprite = null;

	public get trailingSegment() { return this._trailingSegment; }
	public set trailingSegment(val) { this._trailingSegment = val; }
	protected _trailingSegment: BeamSegment;

	public texture: PIXI.Texture = null;
	public textureWidthFactor: number = 2;
	
	// ---------------------------------------------------------------------------------------------
	
	protected override CreateGraphic(){
		if(this.texture != null){

			this._sprite = new PIXI.Sprite(this.texture);
			this.addChild(this._sprite);
		}
		else{
			super.CreateGraphic();
		}
	}

	protected override UpdateGraphic(): void{ }

	private UpdateGraphic_Self(): void{

		// if it has no texture, just use vector graphics
		if(this._sprite == null){
			super.UpdateGraphic();
			return;
		}
	
		// if there is a texture
		let wScale = this.size / this.texture.height;
		wScale *= this.textureWidthFactor;

		// set sprite texture and position
		this._sprite.texture = this.texture;
		this._sprite.anchor.set(0, 0.5);

		// calculate the difference between the last draw position and current position
		let dif = this.globalPosition;
		Vect.Subtract(dif, this._lastDrawPos, dif);

		this._sprite.position.set(-dif.x, -dif.y);
		this._sprite.scale.x = Vect.Magnitude(dif) / this.texture.width;
		this._sprite.scale.y = wScale;
		this._sprite.rotation = Vect.Direction(dif);
	}

	protected override OnCollision(collision: ICollision){

		let otherCol = Collision.OtherCollider(collision, this._collider);

		// get the projectile's allegience
		let allegiance: Allegiance = null;
		if(IAllied.ImplementedIn(this.owner)){
			allegiance = (this.owner as IAllied).allegiance;
		}
		
		// skip a collision if it's friendly with this projectile
		let resParent = otherCol.parentEntity;
		if(allegiance != null){
			if(ITarget.ImplementedIn(resParent)){
				let aggr = allegiance.AggresionToward(resParent);
				if(aggr == Aggression.friendly){
					return;
				}
			}
		}

		let normal = collision.directionNormal;
		if(otherCol.parentEntity == this){
			normal = Vect.Invert(normal);
		}
		
		// if the hit object is implements the target interface, apply a projecile hit call on it
		if(ITarget.ImplementedIn(otherCol.parentEntity)){
			let target = otherCol.parentEntity as ITarget;
			target.Hit(this.damage, this.GetCurrentImpulse(), collision.collisionPoint, this.owner);
		}

		if(this._lifetime > 0){
			AnimatedEffect.HitEffect(
				this.parentScene, 
				collision.collisionPoint, 
				Vect.Create(), 
				Vect.Direction(normal) + Math.PI, 
				0xFFEE88
			);
		}

		this.TrailToTrailingSegment();
		this.RemoveFromScene(true);
	}

	// ---------------------------------------------------------------------------------------------

	protected override OnPreRender(){
		
		if(this._trailingSegment?.removed) this._trailingSegment = null;
		this.TrailToTrailingSegment();
	}

	protected TrailToTrailingSegment(){

		let pos: IVect;
		if(this._trailingSegment != null) pos = this._trailingSegment.globalPosition;
		else pos = this._lastPhysPos;

		this._lastDrawPos.x = pos.x;
		this._lastDrawPos.y = pos.y;
		this.UpdateGraphic_Self();
	}

	// ---------------------------------------------------------------------------------------------

	public override FastClone(): Projectile{
		
		let r = super.FastClone() as BeamSegment;

		r.texture = this.texture;
		r.textureWidthFactor = this.textureWidthFactor;

		return r;
	}
	
}