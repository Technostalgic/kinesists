///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	IVect, Vect, IAllied,
	Player,
	Projectile,
	SceneEntity, 
	SpriteSheet,
	ISerializable,
	ISerializableType,
	PhysicsEntity,
	IReadonlyVect,
	BeamSegment
} from '../../internal';

export abstract class Weapon extends SceneEntity{

	public constructor() { 
		super(); 
		this._name = (this as any).constructor?.name;
	}

	protected _barrelPosition: IVect = Vect.Create();

	public get barrelPosition(): IVect{
		return this.toGlobal(this._barrelPosition);
	}

	public abstract get readyPrimary(): boolean;
	public get readyAlternate(): boolean { return false; }
	public get readyTertiary(): boolean { return false; }

	/**
	 * Uses by the specified user
	 * @param user the user of the weapon
	 * @param aim the direction the weapon is pointing in, in radians
	 */
	public abstract Use(user: IAllied, aim: number): void;
}

export abstract class Firearm extends Weapon {
	
	public constructor(){
		super();
		this.CreateGraphic();
	}

	protected _payload: Projectile = null;
	protected _projectileSpeed: number = 2000;
	protected _spread: number = 0;
	protected _speedSpread: number = 0;
	protected _fireDelay: number = 0.2;
	protected _semiAuto: boolean = false;
	protected _clipSize: number = 12;
	protected _reloadTime: number = 2.5;
	protected _recoil: number = 350;
	
	protected _barrelPosition: IVect = Vect.Create(0, 0);
	protected _sprite: PIXI.Sprite;
	protected _spriteSheet: SpriteSheet;
	
	protected _lastTriggeredTick: number = 0;
	protected _fireWait: number = 0;
	protected _roundsLeft: number = null;
	protected _isReloading: boolean = false;
	
	public get readyPrimary(): boolean { return !this._isReloading; }

	// ---------------------------------------------------------------------------------------------

	public static CreateDefault(): Firearm{
		throw "illegal call";
	}

	protected static SetSerializableFields(): void{

		super.SetSerializableFields();
		ISerializable.MakeSerializable((this as unknown as ISerializableType<Firearm>), x => [
			//x._payload,
			x._projectileSpeed,
			x._spread,
			x._speedSpread,
			x._fireDelay,
			x._semiAuto,
			x._clipSize,
			x._reloadTime,
			x._recoil,
			x._barrelPosition
		]);
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------
	
	protected CreateGraphic(): void{
		
		this._sprite = new PIXI.Sprite(null);
		this._sprite.anchor.set(0, 0.5);
		this.addChild(this._sprite);
		
		let ths = this;
		this._spriteSheet = new SpriteSheet('./dist/assets/graphics/Handgun.png');
		this._spriteSheet.GenerateSprites(1, 1, Vect.Create(0, 0.5));
		this._spriteSheet.DoOnLoaded(function(){
			ths._sprite.texture = ths._spriteSheet.sprites[0];
		});
	}

	public override Update(deltaTime: number){
		super.Update(deltaTime);

		if(this._roundsLeft == null) this._roundsLeft = this._clipSize;

		// if reloading, do nothing else
		if(this.HandleReload(deltaTime)){
			this._lastTriggeredTick++;
			return;
		}

		// deduct the fire timer
		if(this._fireWait > 0)
			this._fireWait -= deltaTime;
		else if(this._fireDelay < 0)
			this._fireWait = 0;

		// increment triggered tickstamp
		this._lastTriggeredTick++;
	}

	protected HandleReload(deltaTime: number): boolean{

		// if reloading, do nothing else
		if(this._isReloading){

			// deduct reload timer
			if(this._fireWait > 0)
				this._fireWait -= deltaTime;
			else {
				this.FinishReload();
			}

			return true;
		}

		return false;
	}

	// ---------------------------------------------------------------------------------------------

	/** @inheritdoc */
	public override Use(user: IAllied, aim: number): void{

		// weapon can fire if fire wait is up
		let canFire = this._fireWait <= 0;

		// if it was not triggered last frame, we can fire (to make it semi-auto)
		if(this._semiAuto) canFire = canFire && this._lastTriggeredTick > 1;

		// fire if possible
		if(canFire) {
			if(this._roundsLeft > 0) this.Fire(user, aim);
		}

		// reload if empty
		if(this._roundsLeft <= 0) this.Reload(user, aim);

		// reset the triggered tickstamp
		this._lastTriggeredTick = 0;
	}

	public Fire(user: IAllied, aim: number): void{

		if(this._roundsLeft <= 0) return;

		this._fireWait = this._fireDelay;
		this._roundsLeft--;

		this.ApplyRecoil(user, aim);

		let tpos = this.barrelPosition;
		this.ReleasePayload(user, aim, tpos);
	}

	public Reload(user: IAllied, aim: number): void{

		// don't reload if you don't gotta
		if(this._isReloading || this._roundsLeft >= this._clipSize){
			return;
		}

		this._sprite.visible = false;
		this._fireWait = this._reloadTime;
		this._isReloading = true;
	}

	protected ReleasePayload(user: IAllied, aim: number, position: IReadonlyVect){

		if(this._payload == null) throw "no projectile payload";

		let direction = aim + (Math.random() - 0.5) * this._spread;
		let speed = this._projectileSpeed + (Math.random() - 0.5) * this._speedSpread;
		let vel = Vect.FromDirection(direction, speed);

		//let proj = Projectile.CreateAt(position, direction, speed, this._projectileDamage, user);
		let proj = this._payload.FastClone();
		proj.FireFrom(user, position, vel);

		this.parentScene.addChild(proj);
	}

	protected FinishReload(): void{

		this._sprite.visible = true;
		this._fireWait = 0;
		this._isReloading = false;
		this._roundsLeft = this._clipSize;
	}

	protected ApplyRecoil(user: IAllied, aim: number){
		if(!(user instanceof PhysicsEntity)) return;
		user.ApplyImpulse(Vect.FromDirection(aim + Math.PI, this._recoil));
	}
}

export class Handgun extends Firearm{

	public constructor(){
		super();
		this._payload = new Projectile();
		this._payload.damage = 7;
		this._payload.size = 1;
		this._payload.mass = 0.75;
	}
	
	protected _spread = 0.05;
	protected _speedSpread = 350;
	protected _recoil = 350;
	protected _fireDelay = 0.15;
	protected _semiAuto = true;
	protected _clipSize = 12;
	protected _reloadTime = 1.65;
	protected _barrelPosition = Vect.Create(3, -1);

	// ---------------------------------------------------------------------------------------------

	public static CreateDefault() {
		return new this();
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------

	protected override CreateGraphic(): void{
		
		this._sprite = new PIXI.Sprite(null);
		this._sprite.anchor.set(0, 0.5);
		this.addChild(this._sprite);
		
		let ths = this;
		this._spriteSheet = new SpriteSheet('./dist/assets/graphics/Handgun.png');
		this._spriteSheet.GenerateSprites(1, 1, Vect.Create(0, 0.5));
		this._spriteSheet.DoOnLoaded(function(){
			ths._sprite.texture = ths._spriteSheet.sprites[0];
		});
	}
}

export class Shotgun extends Firearm{

	public constructor(){
		super();
		this._payload = new Projectile();
		this._payload.damage = 4;
		this._payload.size = 1;
		this._payload.mass = 1;
	}

	protected _projectileCount = 8;

	protected _semiAuto = true;
	protected _projectileSpeed = 700;
	protected _spread = 0.35;
	protected _speedSpread = 800;
	protected _recoil = 5250;
	protected _fireDelay = 0.4;
	protected _clipSize = 6;
	protected _reloadTime = 0.55;
	protected _barrelPosition = Vect.Create(5.5, -1);

	// ---------------------------------------------------------------------------------------------

	public static CreateDefault(){
		return new this();
	}

	protected static SetSerializableFields(): void{

		super.SetSerializableFields();
		ISerializable.MakeSerializable(this , x => [
			x._projectileCount
		]);
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------

	protected override CreateGraphic(): void{
		
		this._sprite = new PIXI.Sprite(null);
		this._sprite.anchor.set(0.25, 0.5);
		this.addChild(this._sprite);
		
		let ths = this;
		this._spriteSheet = new SpriteSheet('./dist/assets/graphics/Shotgun.png');
		this._spriteSheet.GenerateSprites(1, 1, Vect.Create(0.25, 0.5));
		this._spriteSheet.DoOnLoaded(function(){
			ths._sprite.texture = ths._spriteSheet.sprites[0];
		});
	}

	// ---------------------------------------------------------------------------------------------

	public override Use(user: IAllied, aim: number){

		if(this._isReloading){
			if(this._roundsLeft > 0){
				this.FinishReload();
				this._lastTriggeredTick = 0;
			}
			return;
		}

		super.Use(user, aim);
	}
	
	protected override ReleasePayload(user: IAllied, aim: number, position: IReadonlyVect){
		for(let i = this._projectileCount; i > 0; i--){
			super.ReleasePayload(user, aim, position);
		}
	}

	protected override HandleReload(deltaTime: number): boolean{

		// if reloading, do nothing else
		if(this._isReloading){

			// deduct reload timer
			if(this._fireWait > 0)
				this._fireWait -= deltaTime;
			else {
				this._roundsLeft += 1;
				if(this._roundsLeft >= this._clipSize) this.FinishReload();
				else {
					this._fireWait = this._reloadTime;
				}
			}

			return true;
		}

		return false;
	}
	
	protected override FinishReload(): void{

		this._sprite.visible = true;
		this._fireWait = 0;
		this._isReloading = false;
	}
}

export class BeamLaser extends Weapon{

	public constructor(){
		super();
		this._payload = new BeamSegment();
		this._name = "Proton Beam";
		this._payload.damage = 80;
		this._payload.mass = 10;
		this._payload.size = 3;
		this._payload.color = 0xFF4422
		this.CreateGraphic();
	}

	protected _payload: BeamSegment = null;
	protected _projectileSpeed = 1400;
	
	protected _sprite: PIXI.Sprite;
	protected _spriteSheet: SpriteSheet;

	protected _lastBeamSeg: BeamSegment = null;
	protected _lastTriggeredTick: number = 1;
	protected _lastTriggeredAim: number = 0;
	protected _lastTriggeredUser: IAllied = null;
	protected _barrelPosition = Vect.Create(9, -1.5);

	public get readyPrimary(): boolean { return true; }	
	
	// ---------------------------------------------------------------------------------------------

	public static CreateDefault() {
		return new this();
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------

	protected CreateGraphic(): void{
		
		this._sprite = new PIXI.Sprite(null);
		this._sprite.anchor.set(0.1, 0.625);
		this.addChild(this._sprite);
		
		let ths = this;
		this._spriteSheet = new SpriteSheet('./dist/assets/graphics/BeamLaser.png');
		this._spriteSheet.GenerateSprites(1, 1, this._sprite.anchor);
		this._spriteSheet.DoOnLoaded(function(){
			ths._sprite.texture = ths._spriteSheet.sprites[0];
		});

		let sheet = new SpriteSheet('./dist/assets/graphics/ProtonBeam.png');
		sheet.GenerateSprites(1, 1);
		sheet.DoOnLoaded(function(){
			ths._payload.texture = sheet.sprites[0];
			ths._payload.textureWidthFactor = 7 / 4;
		});
	}

	// ---------------------------------------------------------------------------------------------

	public override Update(deltaTime: number){
		super.Update(deltaTime);
		
		if(this._lastTriggeredTick <= 0){
			this.Fire(deltaTime, this._lastTriggeredUser, this._lastTriggeredAim);
		}

		else{
			this._lastBeamSeg = null;
		}

		// increment triggered tickstamp
		this._lastTriggeredTick++;
	}

	public override Use(user: IAllied, aim: number): void{

		this._lastTriggeredAim = aim;
		this._lastTriggeredUser = user;

		// reset the triggered tickstamp
		this._lastTriggeredTick = 0;
	}

	public Fire(deltaTime: number, owner: IAllied, aim: number){

		let tpos = this.barrelPosition;
		let proj = this._payload.FastClone() as BeamSegment;
		proj.damage = this._payload.damage * deltaTime;
		proj.mass = this._payload.mass * deltaTime;
		proj.FireFrom(owner, tpos, Vect.FromDirection(aim, this._projectileSpeed));

		owner.parentScene.addChild(proj);

		if(this._lastBeamSeg != null) this._lastBeamSeg.trailingSegment = proj;
		this._lastBeamSeg = proj;
	}
	
}