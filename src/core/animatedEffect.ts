///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { Effect, 
	IVect,
	Scene, SpriteSheet
} from '../internal';

export class AnimatedEffect extends Effect{

	constructor() { super(); }

	private _spriteAnim: SpriteSheet;
	private _sprite: PIXI.Sprite;
	private _frameTimer: number = 0;
	private _frameDelay: number = 0;
	private _currentFrame: number = 0;

	public static HitEffect(scene: Scene, pos: IVect, vel: IVect, direction: number, color: number): 
	AnimatedEffect{

		// TODO velocity
		let r = AnimatedEffect.Create(Effect.Sprite_HitEffect, 0.03333, color);
		scene.addChild(r);
		r.globalPosition = pos;
		r.rotation = direction;

		return r;
	}

	/**
	 * create an animated effect from the provided information
	 * @param spriteAnim the sprite for the effect to animate
	 * @param frameDelay the amount of seconds that each frame will be shown for
	 */
	public static Create(spriteAnim: SpriteSheet, frameDelay: number, color: number = 0xFFFFFF):
	AnimatedEffect{

		let r = new AnimatedEffect();
		
		r._spriteAnim = spriteAnim;
		r._sprite = new PIXI.Sprite(null);
		if(r._spriteAnim.isLoaded) {
			r._sprite.texture = r._spriteAnim.sprites[0];
			r._sprite.anchor.set(r._sprite.texture.defaultAnchor.x, r._sprite.texture.defaultAnchor.y);
		}
		r._sprite.tint = color;

		r._frameDelay = frameDelay;
		r._frameTimer = frameDelay;
		r.addChild(r._sprite);
		
		return r;
	}

	public override Update(deltaTime: number): void {
		super.Update(deltaTime);

		// deduct frame timer and test to see if it has fully elapsed
		this._frameTimer -= deltaTime;
		if(this._frameTimer <= 0){

			// reset frame timer and get next frame
			this._frameTimer = this._frameDelay;
			this._currentFrame += 1;

			if(this._spriteAnim.isLoaded){
				
				// if next frame exceeds total frame count, animaiton is complete, so we remove the 
				// effect
				if(this._currentFrame >= this._spriteAnim.sprites.length){
					this.RemoveFromScene();
				}
			
				// increment the frame
				else{
					let tex = this._spriteAnim.sprites[this._currentFrame];
					this._sprite.texture = tex;
					this._sprite.anchor.set(tex.defaultAnchor.x, tex.defaultAnchor.y);
				}
			}
		}
	}
}