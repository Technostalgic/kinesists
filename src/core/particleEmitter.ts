///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from "pixi.js";
import { 
	IVect, Vect, Maths,
	SceneEntity,
	Particle, ParticleSystem, ISerializable
} from "../internal";

export class ParticleEmitter extends SceneEntity{
	constructor(particleSystem: ParticleSystem<Particle>){ 
		super(); 
		this._particleSystem = particleSystem;
	}

	private _particleSystem: ParticleSystem<Particle> = null;
	private _emissionDelta: number = 0;
	private _lastPos: IVect = null;

	public emitterVelocityRatio: number = 0;
	public emissionOverTime: number = 10;
	public emissionOverDist: number = 0;
	public minSpeed: number = 0;
	public maxSpeed: number = 100;
	public startVelocityMin: IVect = Vect.Create();
	public startVelocityMax: IVect = Vect.Create();
	public minParticleLife: number = 0.5;
	public maxParticleLife: number = 1;
	public colorA: number = 0xFF0000;
	public colorB: number = 0xFFFF00;
	public halfExtents: IVect = Vect.Create();
	public radius: number = 0;

	public get particleSystem() { return  this._particleSystem; }

	// ---------------------------------------------------------------------------------------------

	/** midpoint between min and max speed */
	public get speed(){ return (this.minSpeed + this.maxSpeed) * 0.5; }
	public set speed(val) { 
		let off = this.speedVar * 0.5;
		this.minSpeed = val - off;
		this.maxSpeed = val + off;
	}

	/** difference of min and max speed */
	public get speedVar() { return this.maxSpeed - this.minSpeed; }
	public set speedVar(val) { 
		let spd = this.speed;
		let halfVal = val * 0.5;
		this.minSpeed = spd - halfVal; 
		this.maxSpeed = spd + halfVal; 
	}

	/** 
	 * The starting velocity that each particle will have. Also a convenient getter and setter for 
	 * the midpoint between velocity offset min and max 
	 */
	public get startVelocity(){ 
		return Vect.Lerp(this.startVelocityMax, this.startVelocityMin, 0.5); 
	}
	public set startVelocity(val){
		let voDif = Vect.Subtract(val, this.startVelocity);
		this.startVelocityMin.x += voDif.x;
		this.startVelocityMin.y += voDif.y;
		this.startVelocityMax.x += voDif.x;
		this.startVelocityMax.y += voDif.y;
	}

	/** the amount of variation between the min and max start velocity */
	public get startVelocityVar() { 
		return Vect.Subtract(this.startVelocityMax, this.startVelocityMin); 
	}
	public set startVelocityVar(val) {
		let vel = this.startVelocity;
		let hval = Vect.MultiplyScalar(val, 0.5);
		this.startVelocityMin.x = vel.x - hval.x;
		this.startVelocityMin.y = vel.y - hval.y;
		this.startVelocityMax.x = vel.x + hval.x;
		this.startVelocityMax.y = vel.y + hval.y;
	}

	/** midpoint between min and max life */
	public get particleLife() { return (this.minParticleLife + this.maxParticleLife) * 0.5; }
	public set particleLife(val){
		let off = this.particleLifeVar * 0.5;
		this.minParticleLife = val - off;
		this.maxParticleLife = val + off;
	}
	
	/** difference between min and max life */
	public get particleLifeVar() { return this.maxParticleLife - this.minParticleLife; }
	public set particleLifeVar(val){
		let lf = this.particleLife;
		let halfVal = val * 0.5;
		this.minParticleLife = lf - halfVal;
		this.maxParticleLife = lf + halfVal;
	}
	
	/// --------------------------------------------------------------------------------------------

	public static CreateDefault() {
		return new ParticleEmitter(null);
	}

	static {
		ISerializable.MakeSerializable(this, x => [
			x._particleSystem,
			x.emitterVelocityRatio,
			x.emissionOverTime,
			x.emissionOverDist,
			x.minSpeed,
			x.maxSpeed,
			x.startVelocityMin,
			x.startVelocityMax,
			x.minParticleLife,
			x.maxParticleLife,
			x.colorA,
			x.colorB,
			x.halfExtents,
			x.radius
		]);
	}

	// ---------------------------------------------------------------------------------------------

	public override Update(deltaTime: number): void {
		super.Update(deltaTime);

		if(this._lastPos == null) this._lastPos = this.globalPosition;
		this.HandleEmission(deltaTime);
	}

	public HandleEmission(deltaTime: number): void{

		// calculate distance moved fro last position
		let gpos = this.globalPosition;
		let dist = Vect.Distance(this._lastPos, gpos);

		// increase emission amount by emission parameters
		this._emissionDelta += this.emissionOverTime * deltaTime;
		this._emissionDelta += this.emissionOverDist * dist;

		// get particle amount to emit
		let count = Math.floor(this._emissionDelta);
		this._emissionDelta -= count;

		// emit the particles
		if(count > 0) this.EmitAt(this._lastPos, gpos, count, deltaTime);
		
		this._lastPos = gpos;
	}

	/**
	 * emits the specified amount of particles from the emitter as if it were currently at the 
	 * specified position with the specified velocity
	 * @param posA the position of the emitter
	 * @param posB where the emitter was at the previous frame
	 * @param count the amount of particles to emit along the path from a to b
	 * @param emitterVel the velocity of the emitter as it emits these particles
	 */
	public EmitAt(posA: IVect, posB: IVect, count: number, deltaTime: number){

		// warn if particle system is gone
		if(this._particleSystem.removed){
			console.error("Attempting to add particles to a system that no longer exists");
			return;
		}

		// get the particles from the particle system
		let parts = this._particleSystem.GetParticles(count);
	
		// shortcut for min/max velocity offset
		let voMin = this.startVelocityMin;
		let voMax = this.startVelocityMax;
		
		// use emitter velocity if necessary
		let evx = null;
		let evy = null;
		if(this.emitterVelocityRatio > 0){
			let dpos = Vect.Subtract(posB, posA);
			evx = dpos.x / deltaTime * this.emitterVelocityRatio;
			evy = dpos.y / deltaTime * this.emitterVelocityRatio;
		}

		// iterate through each particle and set their properties
		for(let i = parts.length - 1; i >= 0; i--){
	
			// calculate some values used on the particle
			let tspdFactor = Math.random();
			let tspd = Maths.LerpValue(this.minSpeed, this.maxSpeed, tspdFactor);
			let tvel = Vect.FromDirection(Math.PI * 2 * Math.random(), 1);
			let tpos = Vect.Add(
				Vect.Lerp(posA, posB, Math.random()), 
				Vect.MultiplyScalar(tvel, tspdFactor * this.radius)
			);
			let toff = Vect.Create(
				Maths.LerpValue(-this.halfExtents.x, this.halfExtents.x, Math.random()), 
				Maths.LerpValue(-this.halfExtents.y, this.halfExtents.y, Math.random())
			);
			tpos.x += toff.x;
			tpos.y += toff.y;
			tvel.x *= tspd;
			tvel.y *= tspd;
			tvel.x += Maths.LerpValue(voMin.x, voMax.x, Math.random());
			tvel.y += Maths.LerpValue(voMin.y, voMax.y, Math.random());
			let tcol = Maths.LerpColor(this.colorA, this.colorB, Math.random());

			// add emitter velocity if necessary
			if(evx !== null){
				tvel.x += evx;
				tvel.y += evy;
			}
			
			// set it's properties
			let part = parts[i];
			part.maxLifetime = Maths.LerpValue(this.minParticleLife, this.maxParticleLife, Math.random());
			part.globalPosition = tpos;
			part.velocity = tvel;
			part.displayObj.tint = tcol;
		}
	}
}