///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { ISerializable, SceneEntity, Serializable } from '../internal';

export class SceneReference extends Serializable{

	public constructor(suid: number){
		super();
		this._targetSUid = suid;
	}

	private _targetSUid: number = null;
	public get targetSUid() { return this._targetSUid; }
	public set targetSUid(val) { this._targetSUid = val; this._targetEntity = null; }

	private _targetEntity: SceneEntity = null;
	public get targetEntity() { return this._targetEntity; }
	
	// ---------------------------------------------------------------------------------------------

	public static CreateDefault(){ return new this(0); }

	static {
		ISerializable.MakeSerializable(this, x => [
			x._targetSUid
		]);
	}

	// ---------------------------------------------------------------------------------------------

	/**
	 * finds the reference to the entity in the specified scene, if it exists
	 * @param scene the scene to search for the reference in
	 */
	GetDereferencedEntity(scene: SceneEntity): SceneEntity{

		// if the scene is a container for the reference, find it and return it
		if(this._targetEntity == null) this._targetEntity = scene.GetEntityBySUid(this.targetSUid);
		return this._targetEntity;
	}
}