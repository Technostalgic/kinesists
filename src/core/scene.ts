///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { isReturnStatement } from 'typescript';
import { 
	Game, SceneEntity, ColliderPartitions,
	ParticleSystem, Particle, ISerializable, GameEvent
} from '../internal';

/**
 * data structure that contains game entities
 */
export class Scene extends SceneEntity{

	public constructor(game: Game) {

		super();

		if(game !== undefined){
			this._parentGame = game;
		}
		
		this._parentScene = this;
		this._name = "Scene";
		
		this._colliders = new ColliderPartitions();
	}

	// ---------------------------------------------------------------------------------------------

	private _entitiesToRemove: Array<SceneEntity> = new Array();
	private _entitiesToRemovePostRender: Array<SceneEntity> = new Array();
	private _particleSystems: Array<ParticleSystem<Particle>> = new Array();
	private _colliders: ColliderPartitions = null;
	private _currentTime: number = 0;
	private _initializationQuery: SceneEntity[] = [];
	
	
	protected _requestPointerLock: boolean = false;
	protected _parentGame: Game = null;
	protected _particleRenderer: PIXI.ParticleRenderer = null;

	public override get parentScene(): Scene { return this; }
	public override get parentGame(): Game { return this._parentGame; }
	public set parentGame(val){ if(this.parentGame != null) return; this._parentGame = val; }
	
	public onPreRender: GameEvent<void> = new GameEvent();
	
	public get sceneRefQuery() {return this._sceneRefQuery; }
	public get entitiesToRemovePreRender(){ return this._entitiesToRemove; }
	public get entitiesToRemovePostRender(){ return this._entitiesToRemovePostRender; }
	public get colliders() { return this._colliders; }
	public get particleSystems() { return this._particleSystems; }
	public get currentTime() { return this._currentTime; }
	public get requestPointerLock() { return this._requestPointerLock; }

	// ---------------------------------------------------------------------------------------------

	public static CreateDefault(){
		return new Scene(null);
	}

	protected static SetSerializableFields(): void{

		super.SetSerializableFields();
		ISerializable.MakeSerializable(this, x => [
			x._requestPointerLock,
		]);
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------
	/**
	 * should be called when scene is switched to
	 */
	public OnEnter(): void{ }

	/**
	 * should be called when scene is switched away from
	 */
	public OnExit(): void{ }

	// ---------------------------------------------------------------------------------------------

	public override addChild<T extends PIXI.DisplayObject>
	(...childs: [T, ...PIXI.DisplayObject[]]): T{
		let r = super.addChild(... childs);
		
		for(let i = 0; i < childs.length; i++){

			// if it is not a scene entity, only perform base class addChild functionality
			if(!(childs[i] instanceof SceneEntity))
				continue;

			this._initializationQuery.push(childs[i] as SceneEntity);
		}
		return r;
	}

	// ---------------------------------------------------------------------------------------------
	/**
	 * Check collisions between all the colliders in the scene
	 */
	public CheckCollisions(): void{

		let collisions = this._colliders.FindCollisions();

		for(let i = collisions.length - 1; i >= 0; i--){
			collisions[i].colliderA.onCollision.Invoke(collisions[i]);
			collisions[i].colliderB.onCollision.Invoke(collisions[i]);
		}
	}

	private RemoveFlaggedEntities(): void {
		if(this._entitiesToRemove.length <= 0) return;

		// iterate through each entity flagged for removal and remove them all
		for(let i = this._entitiesToRemove.length - 1; i >= 0; i--){
			this._entitiesToRemove[i].destroy();
		}

		// remove them from the list
		this._entitiesToRemove.splice(0, this._entitiesToRemove.length);
	}

	private RemoveFlaggedEntitiesPostRender(): void {
		if(this._entitiesToRemovePostRender.length <= 0) return;

		// iterate through each entity flagged for removal and remove them all
		for(let i = this._entitiesToRemovePostRender.length - 1; i >= 0; i--){
			this._entitiesToRemovePostRender[i].destroy();
		}

		// remove them from the list
		this._entitiesToRemovePostRender.splice(0, this._entitiesToRemovePostRender.length);
	}

	/**
	 * override to implement update functionality for the scene
	 * @param deltaTime 
	 */
	public Update(deltaTime: number){

		// initialize any entities that have not yet been initialized
		if(this._initializationQuery.length > 0) this.InitializeEntities();

		// update all direct children
		super.Update(deltaTime);

		// check collisions
		this.CheckCollisions();

		// remove entities that are to be removed
		this.RemoveFlaggedEntities();

		// increment the amount of time the scene has been active for
		this._currentTime += deltaTime;
	}

	/**
	 * override to implement rendering functionality for the scene
	 */
	public Draw(renderer: PIXI.Renderer): void{
		this.onPreRender.Invoke();
		this.DrawTransformed(renderer, PIXI.Matrix.IDENTITY);
	}

	public DrawTransformed(renderer: PIXI.Renderer, transformation: PIXI.Matrix){

		// render the scene graph
		renderer.render(this, {
			transform: transformation
		});

		if(this._particleRenderer == null){
			this._particleRenderer = new PIXI.ParticleRenderer(renderer);
		}
		for(let i = 0; i < this._particleSystems.length; i++){
			this._particleRenderer.render(this._particleSystems[i].container);
		}

		this.RemoveFlaggedEntitiesPostRender();
	}

	// ---------------------------------------------------------------------------------------------

	public InitializeEntities(){
		for(let i = this._initializationQuery.length - 1; i >= 0; i--){
			this._initializationQuery[i].InitializationCheck();
			this._initializationQuery.splice(i, 1);
		}
	}

	/** @inheritdoc */
	public override OnPostDeserialize(){
		this.ParseSceneRefs();
	}
}