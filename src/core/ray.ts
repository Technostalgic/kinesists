///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { 
	IRect, IVect, Vect, IRaycastResult, RaycastResult, ISerializable, Serializable 
} from '../internal';

export class Ray extends Serializable{
	
	public static CastAgainstRect(ray: Ray, rect: IRect): IRaycastResult{

		let rectMin = rect.position;
		let rectMax = Vect.Add(rectMin, rect.size);
		let rayOrigin = ray.rayStart;
		let dirFrac = ray.directionFrac;

		let t1 = (rectMin.x - rayOrigin.x) * dirFrac.x; // left side
		let t2 = (rectMax.x - rayOrigin.x) * dirFrac.x; // right side
		let t3 = (rectMin.y - rayOrigin.y) * dirFrac.y; // top side
		let t4 = (rectMax.y - rayOrigin.y) * dirFrac.y; // bottom side

		// the distance from entry point to ray origin
		let tmin = Math.max(Math.min(t1, t2), Math.min(t3, t4)); 

		// no hit
		if(tmin > ray.magnitude || tmin < 0){
			return null;
		}
		
		// the distance from exit point to ray origin
		let tmax = Math.min(Math.max(t1, t2), Math.max(t3, t4)); 
		
		// no hit
		if(tmin > tmax){
			return null;
		}

		let entryPoint = Vect.Add(rayOrigin, Vect.MultiplyScalar(ray.directionNormal, tmin));
		let exitPoint = Vect.Add(rayOrigin, Vect.MultiplyScalar(ray.directionNormal, tmax));

		let didEnter = tmin >= 0 && tmin <= ray.magnitude;
		let didExit = tmax >= 0 && tmax <= ray.magnitude;
		
		// calculate the penetration based on which parts of the ray entered and exited the collider
		let penetration = 0;
		if(!didExit && !didEnter){
			penetration = ray.magnitude;
		}
		else if (!didExit){
			penetration = ray.magnitude - tmin;
		}
		else if (!didEnter){
			penetration = tmax;
		}
		else{
			penetration = tmax - tmin;
		}

		// calculate normals by seeing which side of the AABB collider the ray hit
		let entryNorm = null;
		switch(tmin){
			case t1: entryNorm = Vect.Create(-1, 0); break;
			case t2: entryNorm = Vect.Create(1, 0); break;
			case t3: entryNorm = Vect.Create(0, -1); break;
			case t4: entryNorm = Vect.Create(0, 1); break;
		}
		let exitNorm = null;
		switch(tmax){
			case t1: exitNorm = Vect.Create(-1, 0); break;
			case t2: exitNorm = Vect.Create(1, 0); break;
			case t3: exitNorm = Vect.Create(0, -1); break;
			case t4: exitNorm = Vect.Create(0, 1); break;
		}

		// construct raycast result object and return it
		let r = RaycastResult.PrecalculatedRaycastResult(
			ray, null, 
			didEnter, didExit,
			entryPoint, exitPoint, 
			entryNorm, exitNorm, penetration
		);
		return r;
	}

	// TODO test this function
	public static CastAgainstCircle(ray: Ray, circlePos: IVect, circleRadius: number): 
	IRaycastResult{

		// offset the line to be relative to the circle
		let rayStart = Vect.Subtract(ray.rayStart, circlePos);
		let rayEnd = Vect.Subtract(ray.rayEnd, circlePos);
		
		// calculate the dot products between the origin and target vectors
		let aoa = Vect.Dot(rayStart, rayStart);
		let aob = Vect.Dot(rayStart, rayEnd);
		let bob = Vect.Dot(rayEnd, rayEnd);
	
		// calculate the ratios used to find the determinant and the t-values
		let qa = aoa - 2.0 * aob + bob;
		let qb = -2.0 * aoa + 2.0 * aob;
		let qc = aoa - circleRadius * circleRadius;
	
		// caclulate the determinant used to determine if there was an intersection or not
		let determinant = qb * qb - 4.0 * qa * qc;
		
		// if the determinant is negative, the ray completely misses
		if(determinant < 0){
			return null;
		}
			 
		// compute the nearest of the two t values
		let tmin = (-qb - Math.sqrt(determinant)) / (2.0 * qa);

		// compute the furthest of the t values
		let tmax = (-qb + Math.sqrt(determinant)) / (2.0 * qa);

		// calculate raycast results from tmin and tmax
		let entered = tmin >= 0 && tmin <= 1;
		let exited = tmax >= 0 && tmin <= 1;
		let entryPoint = Vect.Lerp(ray.rayStart, ray.rayEnd, tmin);
		let exitPoint = Vect.Lerp(ray.rayStart, ray.rayEnd, tmax);
		let entryNorm = Vect.Normalize(Vect.Subtract(entryPoint, circlePos));
		let exitNorm = Vect.Normalize(Vect.Subtract(exitPoint, circlePos));

		// calculate penetration based on where and whether the ray enters and exits the circle
		let penetration = 0;
		if(!entered && !exited){
			penetration = ray.magnitude;
		}
		else if (!exited){
			penetration = ray.magnitude - (tmin * ray.magnitude);
		}
		else if (!entered){
			penetration = tmax * ray.magnitude;
		}
		else{
			penetration = (tmax - tmax) * ray.magnitude;
		}

		// create and return the raycast result
		let r = RaycastResult.PrecalculatedRaycastResult(
			ray, null,
			entered, exited, entryPoint, exitPoint,
			entryNorm, exitNorm, penetration
		);
		return r;
	}

	/**
	 * Create a ray object from the specified data
	 * @param start the starting point of the ray
	 * @param directionNormal the normal direction that the ray is facing (MUST be normalized)
	 * @param length the length of the ray from start to end
	 */
	public static Create(start: IVect, directionNormal: IVect, length: number): Ray{
		let r = new Ray();
		r._rayStart = start;
		r._rayEnd = Vect.Add(start, Vect.MultiplyScalar(directionNormal, length));
		r._directionNormal = directionNormal;
		r._magnitude = length;
		return r;
	}

	/**
	 * Create a ray object from the specified start and end points
	 * @param start the start of the ray
	 * @param end the end of the ray
	 */
	public static FromPoints(start: IVect, end: IVect): Ray{
		let r = new Ray();
		r._rayStart = start;
		r._rayEnd = end;
		return r;
	}

	private _rayStart: IVect = Vect.Create();
	private _rayEnd: IVect = Vect.Create();
	private _difference: IVect;
	private _magnitude: number;
	private _directionNormal: IVect;
	private _directionFrac: IVect;
	
	public get rayStart() { return this._rayStart; }
	public get rayEnd() { return this._rayEnd; }
	
	/** the vector from the start of the ray to the end of the ray */
	public get difference() 
	{ return this._difference ??= Vect.Subtract(this._rayEnd, this._rayStart); }
	
	/** the length of the ray from start to end */
	public get magnitude() { return this._magnitude ??= Vect.Magnitude(this.difference); }
	
	/** the normalized vector that points in the same direction as the ray */
	public get directionNormal() 
	{ return this._directionNormal ??= Vect.Normalize(this.difference); }

	/** the multplicative inverse of the direction normal */
	public get directionFrac() { 
		return this._directionFrac ??= 
			Vect.Create(1 / this.directionNormal.x, 1 / this.directionNormal.y); 
	}
	
	/// --------------------------------------------------------------------------------------------

	public static CreateDefault(){ return new this(); }
	
	static {
		ISerializable.MakeSerializable(this, x => [
			x._rayStart,
			x._rayEnd
		]);
	}
}