///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	Game,
	Scene,
	AABBCollider, CollisionLayer,
	Terrain,
	Vect,
	Player,
	Camera,
	Seeker, Gunner,
	LineParticle, ParticleSystem, ParticleEmitter, ISerializable, GameEvent
} from '../internal';

export class GameScene extends Scene{

	constructor(game: Game){
		super(game);
		this._name = "GameScene";

		// for testing
		// let psystem = new ParticleSystem(LineParticle, 100);
		// let emitter = new ParticleEmitter(psystem);
		// this.addChild(psystem);
		// this._camera.addChild(emitter);
		// emitter.emissionOverTime = 10;
		// emitter.emissionOverDist = 10;
		// emitter.minSpeed = 0;
		// emitter.maxSpeed = 0;
		// emitter.particleLife = 0.5;
		// emitter.particleLifeVar = 1;
		// emitter.radius = 10;
		// emitter.halfExtents = Vect.Create(20, 5)
		// emitter.emitterVelocityRatio = 1;
	}

	public static BasicScene(game: Game): GameScene{

		let r = new GameScene(game);

		r.generateObjects();

		return r;
	}

	private _camera: Camera = null;
	private _player: Player = null;
	
	protected _requestPointerLock: boolean = true;

	public get camera() { return this._camera; }

	// ---------------------------------------------------------------------------------------------

	public static CreateDefault(){
		return new GameScene(null);
	}

	protected static SetSerializableFields(): void{

		super.SetSerializableFields();
		ISerializable.MakeSerializable(this, x => [
			x._camera,
			x._player
		]);
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------

	protected override Initialize(){
		super.Initialize();

		if(this._camera == null){
			this._camera = new Camera();
			this._camera.zoomScalar = 2;
			this.addChild(this._camera);
		}
	}

	private generateObjects(): void{

		let terrain = new Terrain();

		let groundBottom = new AABBCollider();
		groundBottom.layer = CollisionLayer.terrain;
		groundBottom.halfExtents = Vect.Create(150, 10);
		groundBottom.position.set(200, 250);
		groundBottom.CreateWireframe(0xFFFFFF, 1);
		
		let groundLeft = new AABBCollider();
		groundLeft.layer = CollisionLayer.terrain;
		groundLeft.halfExtents = Vect.Create(10, 75);
		groundLeft.position.set(50, 175);
		groundLeft.CreateWireframe(0xFFFFFF, 1);
		
		let groundRight = new AABBCollider();
		groundRight.layer = CollisionLayer.terrain;
		groundRight.halfExtents = Vect.Create(10, 75);
		groundRight.position.set(350, 175);
		groundRight.CreateWireframe(0xFFFFFF, 1);

		let groundTop = new AABBCollider();
		groundTop.layer = CollisionLayer.terrain;
		groundTop.halfExtents = Vect.Create(150, 10);
		groundTop.position.set(200, 100);
		groundTop.CreateWireframe(0xFFFFFF, 1);
		
		let groundMid = new AABBCollider();
		groundMid.layer = CollisionLayer.terrain;
		groundMid.halfExtents = Vect.Create(20, 20);
		groundMid.position.set(200, 175);
		groundMid.CreateWireframe(0xFFFFFF, 1);

		terrain.addChild(groundBottom);
		terrain.addChild(groundLeft);
		terrain.addChild(groundRight);
		terrain.addChild(groundTop);
		terrain.addChild(groundMid);

		let plr = new Player();
		plr.position.set(200);
		this._player = plr;
		
		let enemy = new Gunner();
		enemy.position.set(200, 125);
		
		this.addChild(terrain);
		this.addChild(plr);
		this.addChild(enemy);
	}

	public override Draw(renderer: PIXI.Renderer): void {
		this.InitializationCheck();
		this.onPreRender.Invoke();
		this.DrawTransformed(renderer, this._camera.GetViewMatrix());
	}
}