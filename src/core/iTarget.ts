///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	ICollidable,
	Collider,
	IVect,
	SceneEntity,
	Team
} from '../internal';

/** 
 * interface that should be implemented in every scene object that is able to be hit by an attack or
 * otherwise take damage 
 */
export interface ITarget extends ICollidable{

	/**
	 * apply hit damage to the target
	 * @param damage the amount of damage to apply
	 * @param impulse the amount of force to apply to the target
	 * @param damager the entity that applied the damage
	 */
	Hit(damage: number, impulse: IVect, hitPoint: IVect, damager: SceneEntity): void;

	/** the team that the entity belongs to */
	get team(): Team;
}

export namespace ITarget{

	/** Type checking discriminator for ITarget */
	export function ImplementedIn(obj: any): obj is ITarget{
		if(obj == null) return false;
		let r = obj as ITarget;
		return(
			r.Hit !== undefined &&
			ICollidable.ImplementedIn(obj)
		);
	}
}