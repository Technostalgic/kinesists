///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { ISerializable, Serializable, Stat } from '../internal';

/**
 * data structure used to modify individual stats
 */
export class StatModifier extends Serializable{

	public constructor(add: number, multiply: number = 1){
		super();
		this._add = add;
		this._multiply = multiply;
	}

	private _add: number = 0;
	private _multiply: number = 1;

	public get add() { return this._add; }
	public get multiply() { return this._multiply; }

	private _stat: Stat = null;
	public get stat() { return this._stat; }
	public set stat(val: Stat){
		if(this._stat != null) throw "this modifier has already been applied to a stat";
		this._stat = val;
	}
	
	/// --------------------------------------------------------------------------------------------

	public static CreateDefault(){ return new this(0, 1); }

	static {
		ISerializable.MakeSerializable(this, x => [
			x._add, 
			x._multiply
		]);
	}

	/// --------------------------------------------------------------------------------------------
}