///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { ConsumableStat, ISerializable, Serializable, Stat, StaticStat } from '../internal';

/**
 * data structure used to keep track of individual character stats
 */
export class Stats extends Serializable implements IReadonlyStats{
	
	/** the default stats set for the player */
	public static get DEFAULT(): IReadonlyStats {
		if(this._DEFAULT == null) this.InitializeDefault();
		return this._DEFAULT; 
	}
	private static _DEFAULT: IReadonlyStats = null;

	/// --------------------------------------------------------------------------------------------

	public static CreateDefault(){ return new this(); }
	
	static {
		ISerializable.MakeSerializable(this, x => [
			x._health,
			x._energy,
			x._attack,
			x._speed
		]);
	}

	/// --------------------------------------------------------------------------------------------

	/**
	 * creates the stats.DEFAULT stats object
	 */
	public static InitializeDefault(): void{
		let def = new Stats();
		
		def._health = new ConsumableStat(10);
		def._energy = new ConsumableStat(10);
		def._attack = new StaticStat(0);
		def._speed = new StaticStat(15);

		Stats._DEFAULT = def;
	}

	private _health: ConsumableStat = new ConsumableStat(10);
	private _energy: ConsumableStat = new ConsumableStat(10);
	private _attack: StaticStat = new StaticStat(0);
	private _speed: StaticStat = new StaticStat(15);

	public get health(): ConsumableStat{ return this._health; }
	public get energy(): ConsumableStat{ return this._energy; }
	public get attack(): StaticStat{ return this._attack ?? Stats.DEFAULT.attack as StaticStat; }
	public get speed(): StaticStat{ return this._speed ?? Stats.DEFAULT.speed as StaticStat; }
}

/**
 * use this to pass an immutable stats object as a reference
 */
export interface IReadonlyStats{
	get health(): Readonly<ConsumableStat>;
	get energy(): Readonly<ConsumableStat>;
	get attack(): Readonly<StaticStat>;
	get speed(): Readonly<StaticStat>;
}