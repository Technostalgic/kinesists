///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { Allegiance, SceneEntity } from '../internal';

export interface IAllied extends SceneEntity{
	
	get allegiance(): Allegiance;
}

export namespace IAllied{
	
	/** type checking discriminator fro IAllied interface */
	export function ImplementedIn(obj: SceneEntity): obj is IAllied {
		if(obj == null) return false;
		let r = obj as IAllied;
		return(
			r.allegiance !== undefined
		);
	}
}