///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { 
	Actor, Aggression,
	AiController, CombatMode, IControllable, ISerializable, ISerializableType, Serializable
} from '../internal';

export abstract class AiBehavior extends Serializable{
	public abstract Update(
		controller: AiController, deltaTime: number, actor: IControllable & Actor
	): void;
}