///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

export class GameEvent<T>{
	
	/**
	 * all the listeners that will be invoked when the event is fired
	 */
	public listeners: Array<(data: T) => void> = new Array<(data: T) => void>();

	/**
	 * add a listener to be invoked when the event is invoked, listeners can not be added multiple
	 * times
	 * @param func the funciton to invoke when the event is fired
	 */
	AddListener(func: (data: T) => void): void{

		// ensure listener is not already added
		let index = this.listeners.indexOf(func);
		if(index >= 0)
			return;

		this.listeners.push(func);
	}

	/**
	 * remove the specified listener from the event so it is no longer invoked
	 * @param func 
	 */
	RemoveListener(func: (data: T) => void): void{
		let index = this.listeners.indexOf(func);
		if(index >= 0)
			this.listeners.splice(index, 1);
	}

	/** removes all the event listeners from the event */
	ClearListeners(){
		this.listeners.splice(0, this.listeners.length);
	}

	/**
	 * fire the event, invoke all listeners
	 * @param data the event data to pass to the listener functions
	 */
	Invoke(data: T): void{
		for(let i = 0; i < this.listeners.length; i++){
			this.listeners[i](data);
		}
	}
}