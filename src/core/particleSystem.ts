///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	IVect, Vect,
	SceneEntity, Scene,
	Particle, ParticlePool, ISerializable
} from '../internal';

export class ParticleSystem<T extends Particle> extends SceneEntity{

	/**
	 * create a particle system of the specified particle type
	 * @param particleType the particle type
	 * @param count the amount of particles to initialize the particle pool with
	 */
	public constructor(particleType: { new(): T }, count: number = 0){
		super();
		this._container = new PIXI.ParticleContainer();
		this._pool = new ParticlePool<T>(particleType, count);
	}

	private _container: PIXI.ParticleContainer = null;
	private _pool: ParticlePool<T> = null;
	private _particles: Array<Particle> = new Array();

	public removeOnEmpty: boolean = false;

	/** the particle container that the particles will be added to */
	public get container() { return this._container; }
	public get pool() { return this._pool; }

	/** an array of particles created from the particle system that are currently in the scene */
	public get particles() { return this._particles; }

	protected OnAddedToScene(scene: Scene): void {
		super.OnAddedToScene(scene);
		scene.particleSystems.push(this);
	}

	public RemoveFromScene(): void{

		// iterate through all particles and remove them from the scene
		for(let i = this._particles.length - 1; i >= 0; i--){
			let part = this._particles[i];
			this._pool.PoolParticle(part);
		}

		// remove the particle refs from the particles array
		this._particles.splice(0, this._particles.length);

		// free memory allocated for this particle system (?)
		this._pool.Dispose();
		this._container.destroy();

		// remove particle system from parent scene
		let ind = this.parentScene.particleSystems.indexOf(this);
		if(ind >= 0) this.parentScene.particleSystems.splice(ind, 1);

		super.RemoveFromScene();
	}

	// ---------------------------------------------------------------------------------------------

	public Update(deltaTime: number): void {
		super.Update(deltaTime);

		// remove particle system from scene if necessary
		if(this._particles.length <= 0){
			if(this.removeOnEmpty) this.RemoveFromScene();
		}

		// iterate through all particles and update them
		else{
			for(let i = this._particles.length - 1; i >= 0; i--){
				let part = this._particles[i];
				part.Update(deltaTime);
				if(!part.isAlive){
					this._pool.PoolParticle(part);
					this._particles.splice(i, 1);
				}
			}
		}
	}

	/** creates an array of particles */
	public GetParticles(count: number): Array<Particle>{

		let r = new Array<Particle>();
		for(let i = count - 1; i >= 0; i--){
			let part = this._pool.GetParticle(this._container);
			this._particles.push(part);
			r.push(part);
		}
		
		return r;
	}
}