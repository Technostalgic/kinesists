///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';

export interface IVect { x: number, y: number }
export interface IReadonlyVect extends Readonly<IVect> { }

export interface IRect {
	position: IVect;
	size: IVect;
}
export interface IReadonlyRect {
	get position(): IReadonlyVect;
	get size(): IReadonlyVect;
}

export enum Side{
	none = 0,
	left = 1,
	right = 2,
	up = 4,
	top = 4,
	down = 8,
	bottom = 8,
	all = 15
}

export namespace Side{

	export function RotateCW(side: Side): Side{
		switch(side){
			case Side.left: return Side.up;
			case Side.right: return Side.down;
			case Side.up: return Side.right;
			case Side.down: return Side.left;
		}
		return Side.none;
	}
	
	export function RotateCCW(side: Side): Side{
		switch(side){
			case Side.left: return Side.down;
			case Side.right: return Side.up;
			case Side.up: return Side.left;
			case Side.down: return Side.right;
		}
		return Side.none;
	}

	export function Opposite(side: Side): Side{
		switch(side){
			case Side.left: return Side.right;
			case Side.right: return Side.left;
			case Side.up: return Side.down;
			case Side.down: return Side.up;
		}
		return Side.none;
	}

	export function FromDirection(direction: IVect): Side{

		if(direction.x == 0 && direction.y == 0){
			console.error("No direction from zero vector");
			return Side.none;
		}

		if(Math.abs(direction.x) > Math.abs(direction.y)){
			return direction.x < 0 ? Side.left : Side.right;
		}

		else{
			return direction.y < 0 ? Side.up : Side.down;
		}
	}

	export function GetAngle(side: Side): number{
		switch(side){
			case Side.left: return Math.PI;
			case Side.right: return 0;
			case Side.up: return - Math.PI / 2;
			case Side.down: return Math.PI / 2;
		}

		console.error("Invalid side for angle value");
	}

	export function GetVector(side: Side): IVect{
		switch(side){
			case Side.left: return Vect.Create(-1, 0);
			case Side.right: return Vect.Create(1, 0);
			case Side.up: return Vect.Create(0, -1);
			case Side.down: return Vect.Create(0, 1);
		}
		return Vect.Create(0);
	}

	export function IsHorizontal(side: Side): boolean{
		return (
			side == Side.right ||
			side == Side.left
		);
	}
}

export namespace Maths{

	/**
	 * calculate the modulus properly
	 * @param num number to get the modulus of
	 * @param mod the divisor
	 */
	export function Mod(num: number, mod: number){

		let r = num % mod;
		if(r < 0) r += mod;

		return r;
	}

	/**
	 * calculates the smallest signed distance between two angles in radians
	 * @param target the target angle to subtract from the source
	 * @param source the source angle to be subtracted from
	 */
	export function SignedAngleDif(target: number, source:number): number{

		let twoPi = Math.PI * 2;
		let pi = Math.PI;
	
		let r = target - source;
		r = ((r + pi) % twoPi + twoPi) % twoPi;
		r -= pi;
		return r;
	}

	/**
	 * interpolate between two numerical values
	 * @param a number to lerp from
	 * @param b number to lerp to
	 * @param delta percentage from a to be value returned is
	 */
	export function LerpValue(a: number, b: number, delta: number){
		let dif = b - a;
		return a + dif * delta;
	}

	/**
	 * interpolate between two colors (where the colors are represented as numbers from hexadecimal
	 * values - i.e. 0xFFFFFF is white, 0x00FF00, is green, etc)
	 * @param a the color to lerp from
	 * @param b the color to lerp to
	 * @param delta how far from a toward b the result will be
	 */
	export function LerpColor(a: number, b: number, delta: number): number{

		var MASK1:number = 0xff00ff; 
		var MASK2:number = 0x00ff00; 
	 
		var f2:number = Math.floor(256 * delta);
		var f1:number = 256 - f2;
	 
		return( 
			((((( a & MASK1 ) * f1 ) + ( ( b & MASK1 ) * f2 )) >> 8 ) & MASK1 ) |
			((((( a & MASK2 ) * f1 ) + ( ( b & MASK2 ) * f2 )) >> 8 ) & MASK2 )
		);
	}

	/**
	 * generates a psuedo-unique hash number from the specified string
	 * @param str the string to generate a hash number from
	 */
	export function StringToHash(str: string): number{
		let hash = 0;
		if (str.length == 0)
			return hash;
		for (let i = 0; i < str.length; i++) {
			let charCode = str.charCodeAt(i);
			hash = ((hash << 7) - hash) + charCode;
			hash = hash & hash;
		}
		return hash;
	}
}

export namespace Vect{

	/**
	 * create a vector with the specified components
	 * @param x the x component of the vector
	 * @param y the y component of the vector
	 */
	export function Create(x: number = 0, y: number = x): IVect{
		return{
			x: x,
			y: y
		}
	}

	export function Clone(vect: IVect){
		return{
			x: vect.x,
			y: vect.y
		}
	}

	/**
	 * create a vector that points in the specified direction
	 * @param direction the direcion the vector will point in
	 * @param magnitude the length of the vector
	 * @param vector the vector to store the value in, new vector created if not specified
	 * @returns 
	 */
	export function FromDirection(direction: number, magnitude: number = 1, vector: IVect = Create()){
		vector.x = Math.cos(direction) * magnitude;
		vector.y = Math.sin(direction) * magnitude;
		return vector;
	}

	/**
	 * interpolate from point a to b, returns the interpolated value
	 * @param from the vector to interpolate from, vector A
	 * @param to the vector to interpolate to, vector B
	 * @param delta how much interpolation from A to B has elapsed; 0 for none, 1 for 100%
	 */
	export function Lerp(from: IReadonlyVect, to: IReadonlyVect, delta: number, vector: IVect = Create()): IVect{

		let invDelta = 1 - delta;

		vector.x = from.x * invDelta + to.x * delta;
		vector.y = from.y * invDelta + to.y * delta;
		return vector;
	}

	/**
	 * calculate the euclidean distance between two vectors
	 * @param a the first vector
	 * @param b the second vector
	 */
	export function Distance(a: IReadonlyVect, b: IReadonlyVect): number{

		let dx = a.x - b.x;
		let dy = a.y - b.y;

		return Math.sqrt(dx * dx + dy * dy);
	}

	/**
	 * returns the distance between two vectors, squared (more performant than calculating distance)
	 * @param a the first vector
	 * @param b the second vector
	 * @returns 
	 */
	export function DistanceSquared(a: IReadonlyVect, b: IReadonlyVect): number{

		let dx = a.x - b.x;
		let dy = a.y - b.y;

		return dx * dx + dy * dy;
	}

	/** finds the midpoint of the specified vectors */
	export function MidPoint(... vects: IReadonlyVect[]): IVect{

		let total = Vect.Create();
		for(let i = vects.length - 1; i >= 0; i--){
			total.x += vects[i].x;
			total.y += vects[i].y;
		}

		let recip = 1 / vects.length;
		total.x *= recip;
		total.y *= recip;
		return total;
	}

	/**
	 * Add together two vectors
	 */
	export function Add(a: IReadonlyVect, b: IReadonlyVect, vector: IVect = Create()): IVect{
		vector.x = a.x + b.x;
		vector.y = a.y + b.y;
		return vector;
	}

	/**
	 * subtract b from a
	 */
	export function Subtract(a: IReadonlyVect, b: IReadonlyVect, vector: IVect = Create()): IVect{
		vector.x = a.x - b.x;
		vector.y = a.y - b.y;
		return vector;
	}

	/**
	 * multiply each component of a with each component of b
	 */
	export function Multiply(a: IReadonlyVect, b: IReadonlyVect, vector: IVect = Create()): IVect{
		vector.x = a.x * b.x;
		vector.y = a.y * b.y;
		return vector;
	}

	/**
	 * scale multuply each component a vector by a scalar
	 */
	export function MultiplyScalar(a: IReadonlyVect, scalar: number, vector: IVect = Create()): IVect{
		vector.x = a.x * scalar;
		vector.y = a.y * scalar;
		return vector;
	}

	export function Magnitude(a: IReadonlyVect): number{
		return Math.sqrt(a.x * a.x + a.y * a.y);
	}

	export function Equal(a: IReadonlyVect, b: IReadonlyVect, leniency: number = 0.001): boolean{
		return (
			Math.abs(a.x - b.x) <= leniency &&
			Math.abs(a.y - b.y) <= leniency
		);
	}

	/**
	 * Calculate the dot product between two vectors
	 * @param a the first vector
	 * @param b the second vector
	 */
	export function Dot(a: IReadonlyVect, b: IReadonlyVect): number{
		return a.x * b.x + a.y * b.y;
	}

	/** 
	 * calculate the projection of the specified vector onto another vector
	 * @param a the vector to project
	 * @param target the vector that a will be projected onto
	 */
	export function Project(a: IReadonlyVect, target: IReadonlyVect, vector: IVect = Create()): IVect{
		
		let dot = Dot(a, target);
		let quotient = Dot(target, target);
		let factor = dot / quotient;

		return MultiplyScalar(target, factor, vector);
	}

	export function Direction(a: IReadonlyVect): number{
		return Math.atan2(a.y, a.x);
	}

	export function Normalize(a: IReadonlyVect, vector: IVect = Create()): IVect{

		let mag = Magnitude(a);

		if(mag <= 0)
			return {x: 0, y: 0};

		return MultiplyScalar(a, 1/mag, vector);
	}

	export function Rotate(a: IReadonlyVect, rotation: number, vector: IVect = Create()): IVect{

		let mag = Magnitude(a);
		let dir = Direction(a);
		dir += rotation;

		return FromDirection(dir, mag, vector);
	}

	export function Invert(a: IReadonlyVect, vector: IVect = Create()): IVect{
		vector.x = -a.x;
		vector.y = -a.y;
		return vector;
	}

	/** Type checking discriminator for IVect */
	export function ImplementedIn(obj: Object): obj is IVect{
		if(!(obj instanceof Object)) return false;
		return(
			"x" in obj && "y" in obj
		);
	}
}

export namespace Matrix{

	export function GetTranslation(mat: PIXI.Matrix): IVect{
		return { x: mat.tx, y: mat.ty };
	}

	export function GetScale(mat: PIXI.Matrix): IVect{
		return {
			x: Math.sqrt(mat.a * mat.a + mat.c * mat.c),
			y: Math.sqrt(mat.b * mat.b + mat.d * mat.d)
		};
	}

}

export namespace Rect{

	export function ImplementedIn(obj: any): obj is IRect{
		let r = obj as IRect;
		return (
			r.position != undefined &&
			r.size != undefined
		);
	}

	export function Create(position: IVect = Vect.Create(), size: IVect = Vect.Create()): IRect{
		return {
			position: position,
			size: size
		};
	}

	export function Clone(rect: IReadonlyRect): IRect{ 
		return{
			position: Vect.Clone(rect.position),
			size: Vect.Clone(rect.size)
		}
	}

	export function Center(rect: IReadonlyRect): IVect{
		return{
			x: rect.position.x + rect.size.x * 0.5,
			y: rect.position.y + rect.size.y * 0.5
		};
	}

	export function Overlaps(a: IReadonlyRect, b: IReadonlyRect): Boolean{
		
		return !(
			a.position.x > b.position.x + b.size.x ||
			a.position.x + a.size.x < b.position.x ||
			a.position.y > b.position.y + b.size.y ||
			a.position.y + a.size.y < b.position.y
		);
	}

	/**
	 * expands the rect a to fully overlap rect b, returns the modified rect a
	 * @param a the rect to expand (this rect is modified)
	 * @param b the rect that this rect should overlap
	 */
	export function ExpandToOverlap(a: IRect, b: IReadonlyRect): IRect{

		let minX = Math.min(a.position.x, b.position.x);
		let minY = Math.min(a.position.y, b.position.y);
		let maxX = Math.max(a.position.x + a.size.x, b.position.x + b.size.x);
		let maxY = Math.max(a.position.y + b.size.y, b.position.y + b.size.y);

		if(minX < a.position.x){
			let dif = a.position.x - minX;
			a.position.x -= dif;
			a.size.x += dif;
		}
		if(minY < a.position.y){
			let dif = a.position.y - minY;
			a.position.y -= dif;
			a.size.y += dif;
		}
		if(maxX > a.position.x + a.size.x){
			a.size.x += maxX - (a.position.x + a.size.x);
		}
		if(maxY > a.position.y + a.size.y){
			a.size.y += maxY - (a.position.y + a.size.y);
		}

		return a;
	}

	/** Return a rect that overlaps each specified rect */
	export function OverlapRect(a: IReadonlyRect, b: IReadonlyRect): IRect{

		let minX = a.position.x;
		let minY = a.position.y;
		let maxX = a.position.x + a.size.x;
		let maxY = a.position.y + a.size.y;

		minX = Math.max(minX, b.position.x);
		minY = Math.max(minY, b.position.y);
		maxX = Math.min(maxX, b.position.x + b.size.x);
		maxY = Math.min(maxY, b.position.y + b.size.y);

		return Rect.Create(Vect.Create(minX, minY), Vect.Create(maxX - minX, maxY - minY));
	}

	/**
	 * Returns the side of the rect that is closest to the specified point
	 * @param a the rect to check the sides of
	 * @param b the point to compare the sides to
	 */
	export function ClosestSide(rect: IReadonlyRect, point: IReadonlyVect): Side{

		let aspect = rect.size.x / rect.size.y;
		let dif = Vect.Subtract(point, Rect.Center(rect));

		dif.x /= aspect;

		if(Math.abs(dif.x) > Math.abs(dif.y)){
			return dif.x > 0 ? Side.right : Side.left;
		}
		return dif.y > 0 ? Side.down : Side.up;
	}
}