///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import {
	IVect, Vect, SceneEntity, ISerializable 
} from '../internal';

export class Camera extends SceneEntity{

	public constructor() { super(); }
	
	private _viewMatrix: PIXI.Matrix = PIXI.Matrix.IDENTITY.clone();

	public zoomScalar: number = 1;
	public zoomVector: IVect = Vect.Create(1, 1);

	// ---------------------------------------------------------------------------------------------

	public static CreateDefault(){
		return new Camera();
	}

	protected static SetSerializableFields(): void{

		super.SetSerializableFields();
		ISerializable.MakeSerializable(this, x => [
			x.zoomScalar,
			x.zoomVector
		]);
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------

	public CenterAt(target: IVect): void{
		this.globalPosition = target;
	}

	/**
	 * gets the view matrix of the camer to transform the graphics by
	 * @param rtSize the size of the render target that the camer will be drawing to, the game's 
	 * 	main renderer canvas size will be used by default if not specified
	 */
	public GetViewMatrix(rtSize: IVect = null): PIXI.Matrix{

		let trot = 0;
		let tscl = Vect.Create(
			this.zoomVector.x * this.zoomScalar, 
			this.zoomVector.y * this.zoomScalar
		);

		rtSize = rtSize || Vect.Create(
			this.parentGame.renderer.view.width, 
			this.parentGame.renderer.view.height
		);
		
		let tpos = this.globalPosition;
		tpos.x = tpos.x * tscl.x;
		tpos.y = tpos.y * tscl.y;

		let tpiv = Vect.Create(
			-((rtSize.x / tscl.x) * 0.5),
			-((rtSize.y / tscl.y) * 0.5)
		);

		this._viewMatrix.setTransform(
			-tpos.x, -tpos.y, 
			tpiv.x, tpiv.y, 
			tscl.x, tscl.y, 
			trot, 0, 0
		);
		return this._viewMatrix;
	}
}