///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { Game, ITarget } from '../internal';

export enum InputCondition{
	whileHeld,
	onPress,
	onRelease
}

export enum InputType{
	keyboardButton = 0,
	mouseButton = 1,
	gamepadButton = 2,
	mouseAnalogueX = 3,
	mouseAnalogueY = 4,
	gamepadAnalogue = 5,
}

export namespace InputType{
	export function IsAnalogue(itype: InputType){
		return itype > InputType.gamepadButton;
	}
}

export class InputControl{

	public constructor(){ }

	/**
	 * Create an input control with the specified input
	 * @param type what piece of hardware the input is coming from
	 * @param code the id of the button on the hardware (ie keyCode if from keyboard)
	 * @param condition controls when the control is triggered
	 */
	public static Create(type: InputType, code: number, condition: InputCondition): InputControl{

		let r = new InputControl();
		r._type = type;
		r._inputCode = code;
		r._triggerCondition = condition;

		return r;
	}

	public static CreateAnalogue(type: InputType, code: number){
		let r = new InputControl();
		r._type = type;
		r._inputCode = code;
		r._triggerCondition = InputCondition.whileHeld;
		return r;
	}

	private _triggerCondition: InputCondition = InputCondition.whileHeld;
	private _inputCode: number = 0;
	private _type: InputType = InputType.keyboardButton;
	private _isHeld: boolean = false;
	private _heldLastTick: boolean = false;
	private _inputValue: number = 0;
	private _lastMouseAnalogue: number = 0;
	
	public get triggerCondition() { return this._triggerCondition; }
	public set triggerCondition(val){ this._triggerCondition = val; }

	public get inputCode() { return this._inputCode; }
	public set inputCode(val) { this._inputCode = val; }

	public get type() { return this._type; }
	public set type(val) { this._type = val; }

	// ---------------------------------------------------------------------------------------------

	/** 
	 * update the control to reflect thr current gamestate 
	 * @param game the game instance that this control is reporting input for
	 */
	public Update(game: Game): void{
		
		let held = false;
		
		if(InputType.IsAnalogue(this._type)){
			this.UpdateAnalogue(game);
		}

		// for digital inputs
		else {

			this._inputValue = 0;
			switch(this._type){
				case InputType.keyboardButton:
					held = game.keysPressed[this._inputCode];
					this._inputValue = 1;
					break;
				case InputType.mouseButton:
					held = game.mousePressed;
					this._inputValue = 1;
					break;
				case InputType.gamepadButton:
					// TODO
					break;
			}
		}

		this._heldLastTick = this._isHeld;
		this._isHeld = held;
	}

	private UpdateAnalogue(game: Game): void{

		if(this._type == InputType.gamepadAnalogue){
			// TODO
		}

		// if mouse
		else{

			let mouseval = 0;
			if(this._type == InputType.mouseAnalogueX){
				mouseval = game.mousePos.x;
			}
			else if(this._type == InputType.mouseAnalogueY){
				mouseval = game.mousePos.y;
			}

			let mouseDelta = mouseval - this._lastMouseAnalogue;
			this._inputValue += mouseDelta * 0.01;
			if(this._inputValue > 1) this._inputValue = 1;
			else if (this._inputValue < -1) this._inputValue = -1;
			this._lastMouseAnalogue = mouseval;
		}
	}

	/** determines whether or not the control has been triggered based on it's trigger condition */
	public IsTriggered(): boolean{

		switch(this._triggerCondition){
			case InputCondition.whileHeld:
				return this._isHeld;
			case InputCondition.onPress:
				return (!this._heldLastTick) && this._isHeld;
			case InputCondition.onRelease:
				return this._heldLastTick && (!this._isHeld);
		}

		return false;
	}

	/** returns true if the input is currently held down */
	public IsHeld(): boolean{
		return this._isHeld;
	}

	/** 
	 * the input value from -1 to 1 for analogue inputs. For digital inputs it will be either 0 
	 * or 1 depending on if it's held or not 
	 */
	public GetValue(): number{
		return this._inputValue;
	}

	public SetValue(val: number): void{
		this._inputValue = val;
	}
}