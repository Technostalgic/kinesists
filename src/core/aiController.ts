///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { 
	ObjectController, IControllable, ActorController, PhysicsEntity, Actor, ITarget, AiBehavior, ISerializable
} from '../internal';

export enum CombatMode{
	none = 0,
	passive,
	suspicious,
	engaged,
	retreating
}

export abstract class AiController extends ActorController{

	public abstract get combatMode(): CombatMode;

	protected _currentBehavior: AiBehavior;
	public get currentBehavior() { return this._currentBehavior; }

	protected _target: ITarget;
	public get target() { return this._target; }
	
	public Update(deltaTime: number, actor: IControllable & Actor){
		this.currentBehavior.Update(this, deltaTime, actor);
	}
}