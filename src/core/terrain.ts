///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	Collider,
	IActivate,
	ISerializable,
	SceneEntity 
} from '../internal';

export class Terrain extends SceneEntity implements IActivate{

	public constructor(){
		super(); 
		this._name = "Terrain";
	}

	// ---------------------------------------------------------------------------------------------
	private _isActivated: boolean = true;
	private _colliders: Array<Collider> = new Array<Collider>();

	/** @inheritdoc */
	public get isActivated() { return this._isActivated; }

	// ---------------------------------------------------------------------------------------------

	public static CreateDefault(){ return new this(); }

	protected static SetSerializableFields(): void{

		super.SetSerializableFields();
		ISerializable.MakeSerializable(this, x => [
			x._isActivated,
			x._colliders
		]);
	}

	static{ this.SetSerializableFields(); }

	// ---------------------------------------------------------------------------------------------
	public override addChild<T extends PIXI.DisplayObject>
	(...childs: [T, ...PIXI.DisplayObject[]]): T {
		
		let r = super.addChild(... childs);

		// iterate through each collider in the children being added to the terrain
		for(let i = childs.length - 1; i >= 0; i--){

			let child = childs[i];
			if(!(child instanceof Collider)){
				continue;
			}

			// add the collider to the terrain collider array
			this._colliders.push(child);
			child.isSolid = true;
		}

		return r;
	}

	public override removeChild<T extends PIXI.DisplayObject[]>(...childs: T): T[0] {
		
		let r = super.removeChild(...childs);

		// iterate through each collider in the children being removed from the terrain
		for(let i = childs.length - 1; i >= 0; i--){
			
			if(!(childs[i] instanceof Collider)){
				continue;
			}
			
			// get the index of the collider in the collider array and remove it
			let colIndex = this._colliders.indexOf(childs[i] as Collider);
			if(colIndex >= 0){
				this._colliders.splice(colIndex, 1);
			}
		}

		return r;
	}

	// ---------------------------------------------------------------------------------------------
	public SetActivated(active: boolean): void{
		if(active) this.Activate();
		else this.Deactivate();
	}

	public Activate(): void{
		if(this.isActivated)
			return;
		
		for(let i = this._colliders.length - 1; i >= 0; i--){
			this._colliders[i].SetActivated(true);
		}

		this._isActivated = true;
	}

	public Deactivate(): void{
		if(!this.isActivated)
			return;

		for(let i = this._colliders.length - 1; i >= 0; i--){
			this._colliders[i].SetActivated(false);
		}

		this._isActivated = false;
	}
}