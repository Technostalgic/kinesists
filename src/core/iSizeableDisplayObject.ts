///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from "pixi.js";
import { IVect } from "../internal";

export interface ISizeableDisplayObject extends PIXI.DisplayObject{
	width: number;
	height: number;
	texture: PIXI.Texture;
}