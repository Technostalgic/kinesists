///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { 
	PhysicsEntity
} from '../internal';

export interface IControllable{

	/**
	 * Whether or not theyy is currently able to perform the specified action 
	 * @param action 
	 */
	CanDoAction(action: IControllable.Action): boolean;

	/**
	 * Perform an action if it is available
	 * @param action the action to perform
	 * @param value the value to pass if analogue, or if digital action will not perform with
	 * 	value of 0
	 */
	DoAction(action: IControllable.Action, value: number): void;

	/**
	 * What action type the specified action is, return None if it is not available at all on this
	 * @param action the action to check for
	 */
	GetActionType(action: IControllable.Action): IControllable.ActionMode;
}

export namespace IControllable{

	export enum Action{
		None,
		XMove,
		YMove,
		AimRotate,
		AimMagnitude,
		Jump,
		UseItemPrimary,
		UseItemAlternate,
		UseItemTertiary,
		AttackPrimary,
		AttackAlternate,
		AttackTertiary,
		Defend,
		InteractSend,
		InteractReceive,
		Misc1,
		Misc2,
		Other
	}

	export enum ActionMode{
		None,
		Digital,
		Analogue
	}

	/** type discriminator for IControllable */
	export function ImplementedIn(obj: any): obj is IControllable{
		if(obj == null) return false;
		let cont = obj as IControllable;
		return (
			cont.CanDoAction !== undefined &&
			cont.DoAction !== undefined &&
			cont.GetActionType !== undefined
		);
	}
}