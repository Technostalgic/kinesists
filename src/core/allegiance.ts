///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { ITarget, ISerializable, Serializable } from '../internal';

export enum Team{
	none = 0,
	player = 1 << 0,
	neutral = 1 << 1,
	hazard = 1 << 2,
	enemy = 1 << 3,
	all = 255
}

export enum Aggression{
	neutral = 0,
	friendly,
	hostile
}

export class Allegiance extends Serializable{

	public team: Team = Team.neutral;
	public hostileToward: Team = Team.none;
	public friendlyToward: Team = Team.none;
	public hostileExceptions: Array<ITarget> = [];

	/// --------------------------------------------------------------------------------------------

	public static readonly HAZARD: Allegiance = this.Hazard();

	private static Hazard(): Allegiance{
		let r = new Allegiance();

		r.team = Team.hazard;
		r.hostileToward = Team.player | Team.enemy;

		return r;
	}

	public static PlayerDefault(): Allegiance{
		let r = new Allegiance();

		r.team = Team.player;
		r.hostileToward = Team.enemy;
		r.friendlyToward = Team.player;

		return r;
	}

	public static EnemyDefault(): Allegiance{
		let r = new Allegiance();

		r.team = Team.enemy;
		r.hostileToward = Team.player;
		r.friendlyToward = Team.enemy;

		return r;
	}

	/// --------------------------------------------------------------------------------------------

	public static CreateDefault(){ return new this(); }

	static {
		ISerializable.MakeSerializable(this, x => [
			x.team,
			x.hostileToward,
			x.friendlyToward,
			x.hostileExceptions
		]);
	}

	/// --------------------------------------------------------------------------------------------

	/**
	 * determines whether this allegiance finds the specified target hostile, friendly, or nuetral
	 * @param target 
	 */
	public AggresionToward(target: ITarget): Aggression{
		let r = Aggression.neutral;

		if((this.hostileToward & target.team) > 0 || this.hostileExceptions.includes(target)){
			return Aggression.hostile;
		}

		if((this.friendlyToward & target.team) > 0){
			return Aggression.friendly;
		}

		return r;
	}
}