///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	Collider, CollisionLayer,
	ICollision,
	Ray,
	ICirclecastResult, IRaycastResult 
} from '../internal';

export class ColliderPartitions {

	constructor() { }

	private _forcedCols: Array<Collider>;
	public any: Array<Collider> = new Array<Collider>();
	public terrain: Array<Collider> = new Array<Collider>();
	public actors: Array<Collider> = new Array<Collider>();
	public props: Array<Collider> = new Array<Collider>();
	public projectiles: Array<Collider> = new Array<Collider>();
	public effects: Array<Collider> = new Array<Collider>();

	public GetCompatibleCollisionLayers(layer: CollisionLayer): Array<Array<Collider>>{
		let r = new Array<Array<Collider>>();

		// TODO finish implementation
		switch(layer){
			case CollisionLayer.none:
				r.push(this.any, this.terrain, this.actors, this.props, this.projectiles, this.effects);
				break;
			case CollisionLayer.actors: 
				r.push(this.terrain, this.actors, this.props, this.projectiles);
				break;
			case CollisionLayer.projectiles: 
				r.push(this.terrain, this.actors, this.props);
				break;
			case CollisionLayer.terrain: 
				r.push(this.any, this.actors, this.projectiles, this.props);
				break;
		}

		return r;
	}

	public GetCollisionLayers(layers: CollisionLayer): Array<Array<Collider>>{
		let r = new Array<Array<Collider>>();

		if((layers & CollisionLayer.none) > 0) r.push(this.any);
		if((layers & CollisionLayer.actors) > 0) r.push(this.actors);
		if((layers & CollisionLayer.terrain) > 0) r.push(this.terrain);
		if((layers & CollisionLayer.props) > 0) r.push(this.props);
		if((layers & CollisionLayer.projectiles) > 0) r.push(this.projectiles);
		if((layers & CollisionLayer.effects) > 0) r.push(this.effects);

		return r;
	}

	public AddCollider(collider: Collider, layer: CollisionLayer = null): void{
		if(layer != null){
			collider.layer = layer;
		}

		let array = this.any;
		switch(collider.layer){
			case CollisionLayer.terrain: array = this.terrain; break;
			case CollisionLayer.actors: array = this.actors; break;
			case CollisionLayer.props: array = this.props; break;
			case CollisionLayer.projectiles: array = this.projectiles; break;
			case CollisionLayer.effects: array = this.effects; break;
		}

		array.push(collider);
	}

	public RemoveCollider(collider: Collider): void{

		let array = this.any;
		switch(collider.layer){
			case CollisionLayer.terrain: array = this.terrain; break;
			case CollisionLayer.actors: array = this.actors; break;
			case CollisionLayer.props: array = this.props; break;
			case CollisionLayer.projectiles: array = this.projectiles; break;
			case CollisionLayer.effects: array = this.effects; break;
		}

		let index = array.indexOf(collider);
		if(index >= 0){
			array.splice(index, 1);
		}
	}

	public ForceCollision(colliderA: Collider, colliderB: Collider){

		if(this._forcedCols == null){
			this._forcedCols = new Array<Collider>();
		}

		this._forcedCols.push(colliderA, colliderB);
	}

	private ContainsForcedCollision(colA: Collider, colB: Collider){
		
		// iterate through each forced collision pair
		if(this._forcedCols != null) for(let i = this._forcedCols.length - 1; i > 0; i -= 2){
			
			if(
				(this._forcedCols[i - 1] == colA && this._forcedCols[i] == colB) ||
				(this._forcedCols[i - 1] == colB && this._forcedCols[i] == colA) 
				)
				return true;
		}

		return false;
	}

	public FindOverlapColliders(col: Collider): Array<Collider>{
		return this.FindOverlapCollidersOnLayerArrs(col, this.GetCompatibleCollisionLayers(col.layer));
	}

	public FindOverlapCollidersOnLayers(col: Collider, layers: CollisionLayer): Array<Collider>{
		return this.FindOverlapCollidersOnLayerArrs(col, this.GetCollisionLayers(layers));
	}

	private FindOverlapCollidersOnLayerArrs(
		col: Collider, layers: Array<Array<Collider>>): 
	Array<Collider>{

		let r = new Array<Collider>();
		
		for(let i0 = layers.length - 1; i0 >= 0; i0--){
			for(let i1 = layers[i0].length - 1; i1 >= 0; i1--){
				if(col.OverlapsCollider(layers[i0][i1])){
					r.push(layers[i0][i1]);
				}
			}
		}

		return r;
	}

	public FindCollisions(): Array<ICollision>{

		let r = new Array<ICollision>();

		// iterate through each terrain collider
		for(let i0 = this.terrain.length - 1; i0 >= 0; i0--){

			let colA = this.terrain[i0];

			// check collisions for terrain vs actors
			for(let i1 = this.actors.length - 1; i1 >= 0; i1--){

				let colB = this.actors[i1];
				
				if(this.ContainsForcedCollision(colA, colB))
					continue;

				if(colA.OverlapsCollider(colB))
					r.push(colA.GetCollision(colB));
			}
			
			// check collisions for terrain vs projectiles
			for(let i1 = this.projectiles.length - 1; i1 >= 0; i1--){

				let colB = this.projectiles[i1];
				
				if(this.ContainsForcedCollision(colA, colB))
					continue;
					
				if(colA.OverlapsCollider(colB))
					r.push(colA.GetCollision(colB));
			}
		}

		// iterate through all actor colliders
		for(let i0 = this.actors.length - 1; i0 >= 0; i0--){

			let colA = this.actors[i0];
	
			// check collisions for projectiles vs actors
			for(let i1 = this.projectiles.length - 1; i1 >= 0; i1--){

				let colB = this.projectiles[i1];
				
				if(this.ContainsForcedCollision(colA, colB))
					continue;
					
				if(colA.OverlapsCollider(colB))
					r.push(colA.GetCollision(colB));
			}
			
			// check collisions for actors vs actors
			for(let i1 = this.actors.length - 1; i1 >= 0; i1--){

				let colB = this.actors[i1];
				
				// no self collision >:c
				if(colA == colB) continue;

				if(this.ContainsForcedCollision(colA, colB))
					continue;
					
				if(colA.OverlapsCollider(colB))
					r.push(colA.GetCollision(colB));
			}
		}

		// iterate through each forced collision
		if(this._forcedCols != null) {
			for(let i = this._forcedCols.length - 1; i > 0; i -= 2){

				let colA = this._forcedCols[i - 1];
				let colB = this._forcedCols[i];
				r.push(colA.GetCollision(colB));
			}

			// clear the forced collisions
			this._forcedCols.splice(0, this._forcedCols.length);
		}


		return r;
	}

	/**
	 * Casts a ray as if the ray was a collider on the specified layer
	 * @param ray the ray to cast
	 * @param fromLayer the layer that the ray should be on, and uses collision matrix from that 
	 * 	layer to determine which layers to cast against
	 * @returns 
	 */
	public RaycastFromLayer(ray: Ray, fromLayer: CollisionLayer): Array<IRaycastResult>{

		// each layer array that the raycast can collide with
		let layers = this.GetCompatibleCollisionLayers(fromLayer);
		return this.RaycastInArrays(ray, layers);
	}

	/**
	 * Raycast against all colliders in specified layer or layers
	 * @param ray the ray to cast
	 * @param layers the layers to cast against
	 */
	public RaycastAgainstLayers(ray: Ray, layers: CollisionLayer): Array<IRaycastResult>{

		let arrs = this.GetCollisionLayers(layers);
		return this.RaycastInArrays(ray, arrs);
	}

	private RaycastInArrays(ray: Ray, arrays: Array<Array<Collider>>): Array<IRaycastResult>{
		let r = new Array<IRaycastResult>();

		// iterate through each layer that the raycast can collide with
		for(let i0 = arrays.length - 1; i0 >= 0; i0--){

			// iterate through each collider array
			let layer = arrays[i0];
			for(let i1 = layer.length - 1; i1 >= 0; i1--){

				// calculate raycast and append to results if necessary
				let collider = layer[i1];
				let result = collider.RayCast(ray);
				if(result != null && (result.didEnter || result.didExit)){
					r.push(result);
				}
			}
		}

		return r;
	}

	public CircleCast(ray: Ray, radius: number, layer: CollisionLayer): Array<ICirclecastResult>{
		let r = new Array<ICirclecastResult>();

		// iterate through each layer that the raycast can collide with
		let layers = this.GetCompatibleCollisionLayers(layer);
		for(let i0 = layers.length - 1; i0 >= 0; i0--){

			// iterate through each collider layer
			let layer = layers[i0];
			for(let i1 = layer.length - 1; i1 >= 0; i1--){

				// calculate circlecast and append to results if necessary
				let collider = layer[i1];
				let result = collider.CircleCast(ray, radius);
				if(result != null && (result.didEnter || result.didExit)){
					r.push(result);
				}
			}
		}

		return r;
	}
}