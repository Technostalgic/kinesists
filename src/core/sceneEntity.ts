///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	EditorProperty, AiController,
	Game,
	GameEvent,
	IVect, Vect,
	Scene, Player, ISerializable, SceneReference
} from '../internal';
import { ISerializableType } from './iSerializable';

export class SceneEntity extends PIXI.Container implements ISerializable{

	public static readonly TYPE_CHILDREF = "_childRef";
	public static readonly SFIELD_CHILDREN = "_entities";
	public static readonly SFIELD_RECURSIVE_INDEX = "childIndex";

	public constructor(){
		super(); 

		this._entities = new Array<SceneEntity>();
	}

	// Set Serializable Metadata -------------------------------------------------------------------

	public static CreateDefault(){
		return new this();
	}

	protected static SetSerializableFields(): void{

		ISerializable.MakeSerializable(this, x => [
			x._name,
			x._sUid,
			x.localPosition,
			x.localRotation,
			x.localScale,
			x.alpha,
			x.visible
		]);
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------
	
	private static _curSUid: number = 0;

	protected _sceneRefQuery: PropertyKey[] = [];

	private _initialized: boolean = false;
	public get initialized() { return this._initialized; }

	protected static get nextSUid(): number{ return this._curSUid++; }
	private _sUid: number = SceneEntity.nextSUid;
	private _sUidsChecked: boolean = false;

	private _removed: boolean = false;
	protected _name: string = null;
	protected _parentScene: Scene = null;
	protected _parentEntity: SceneEntity = null;
	protected _entities: Array<SceneEntity>;

	public get SUid() { return this._sUid; }
	
	public get name(): string{

		if(this._name != null)
			return this._name;

		if(this.parent != null && this.parent instanceof SceneEntity)
			return this.parent.name + "_chld";
		
		return "Entity";
	}
	public set name(nm){
		this._name = nm;
	}

	public get removed() { return this._removed; }
	public get parentGame() { return this._parentScene.parentGame; }
	public get parentScene() { return this._parentScene; }
	public get parentEntity(): SceneEntity { 
		return this._parentEntity ?? (this._parentEntity = (this.parent as SceneEntity)); 
	}
	public get entities() { return this._entities; }

	public get globalPosition(): IVect { return this.toGlobal(Vect.Create()); }
	public set globalPosition(pos) {
		let localPos = this.parent?.toLocal(pos) ?? Vect.Clone(pos);
		this.position.set(localPos.x, localPos.y);
	}

	public get globalRotation(): number{ return SceneEntity.GetGlobalRotation(this); }
	public set globalRotation(val) { SceneEntity.SetGlobalRotation(this, val); }

	public get GlobalScale(): IVect{ return SceneEntity.GetGlobalScale(this); }

	public static GetGlobalRotation(obj: PIXI.Container): number{
		
		let ent: PIXI.Container = obj;
		let r = obj.rotation;
		while(ent.parent != null){
			r += ent.parent.rotation;
			ent = ent.parent;
		}
		return r;
	}
	public static SetGlobalRotation(obj: PIXI.Container, rot: number): void{
		
		let parRot = 0;
		if(obj.parent != null){
			let parent: PIXI.Container = obj.parent;
			parRot = parent.rotation;
			while(parent.parent != null){
				parRot += parent.parent.rotation;
				parent = parent.parent;
			}
		}

		obj.rotation = rot - parRot;
	}

	public static GetGlobalScale(obj: PIXI.Container): IVect{

		let ent:PIXI.Container = obj;
		let r = Vect.Clone(obj.scale);
		while(ent.parent != null){
			r.x *= ent.parent.scale.x;
			r.y *= ent.parent.scale.y;
			ent = ent.parent;
		}
		return r;
	}

	public get localPosition(): IVect { return Vect.Create(this.position.x, this.position.y); }
	public set localPosition(val) { this.position.set(val.x, val.y); }

	public get localRotation(): number { return this.rotation; }
	public set localRotation(val) { this.rotation = val; }

	public get localScale(): IVect { return Vect.Create(this.scale.x, this.scale.y); }
	public set localScale(val) { this.scale.set(val.x, val.y); }

	// ---------------------------------------------------------------------------------------------
	
	public static GetGlobalPosition(obj: PIXI.DisplayObject){
		return obj.toGlobal(Vect.Create());
	}
	public static SetGlobalPosition(obj: PIXI.DisplayObject, pos: IVect){
		let localPos = obj.parent.toLocal(pos);
		obj.position.set(localPos.x, localPos.y);
	}

	// ---------------------------------------------------------------------------------------------
	/// Hierarchy

	/** called when this entity is added to another scene entity as a child */
	protected OnAddedToParent(parent: SceneEntity): void{
		this._parentEntity = parent;
	}

	/** called when added to a new scene */
	protected OnAddedToScene(scene: Scene): void{
		this._parentScene = scene;
	}

	protected Initialize(){ }

	public InitializationCheck(){
		if(!this._initialized){
			this.Initialize();
			this._initialized = true;
		}
	}

	/** called when removed from a scene */
	protected OnRemovedFromScene(scene: Scene): void{ }

	/**
	 * for all child entities, ensure that they are also removed from the scene if this entity is
	 * removed from a scene
	 * @param scene 
	 */
	private UpdateChildrenRemovedFromScene(scene: Scene): void{

		// iterate through each child entity
		for(let i = 0; i < this.entities.length; i++){

			let ent = this.entities[i];
			ent.OnRemovedFromScene(scene);
			ent.UpdateChildrenRemovedFromScene(scene);
		}
	}

	/**
	 * for all the entities that are children of this object, ensure that they share the
	 * same parent scene as this object
	 */
	private UpdateChildrenParentRef(): void{
		for(let i = this._entities.length - 1; i >= 0; i--){

			// update entity new parent scene
			let ent = this._entities[i];
			ent.OnAddedToScene(this.parentScene);
			ent.OnAddedToParent(this);
			ent.UpdateChildrenParentRef();
		}
	}

	/** @inheritdoc */
	public override addChild<T extends PIXI.DisplayObject>
	(...childs: [T, ...PIXI.DisplayObject[]]): T{
		
		// base functionality
		let t = super.addChild<T>(...childs);

		// iterate through each child provided
		for(let i = 0; i < childs.length; i++){

			// if it is not a scene entity, only perform base class addChild functionality
			if(!(childs[i] instanceof SceneEntity))
				continue;
			
			// otherwise, provide scene entity data
			let child = childs[i] as SceneEntity;

			// parent the child entity to our scene, and all of it's children too
			child.OnAddedToScene(this.parentScene);
			child.OnAddedToParent(this);
			child.UpdateChildrenParentRef();
			
			// add to child entities
			this._entities.push(child);
		}

		// mark the flag to ensure SUids of children are checked when serialized
		this._sUidsChecked = false;

		return t;
	}

	/** @inheritdoc */
	public override removeChild<T extends PIXI.DisplayObject[]>
	(...childs: T): T[0]{

		// iterate through each child provided
		for(let i = 0; i < childs.length; i++){

			// if it is not a scene entity, only perform base class removeChild functionality
			if(!(childs[0] instanceof SceneEntity))
				continue;
	
			let child = childs[0] as SceneEntity;
			let oscn = child.parentScene;

			child._parentEntity = null;
			child._parentScene = null;

			if(oscn != null){
				child.OnRemovedFromScene(oscn);
				child.UpdateChildrenRemovedFromScene(oscn);
			}
			
			// remove from child entities
			let index = this._entities.indexOf(child);
			this._entities.splice(index, 1);
		}
		
		// base functionality
		return super.removeChild(...childs);
	}

	/** @inheritdoc */
	public override destroy(options?: PIXI.IDestroyOptions | boolean): void{

		if(!this._removed){
			this._removed = true;
			if(this.parentScene != null) this.OnRemovedFromScene(this.parentScene);
		}

		let pent = this.parentEntity;

		this._parentEntity = null;
		super.destroy(options);

		if(pent == null)
			return;
		
		let pind = pent._entities.indexOf(this);
		if(pind >= 0)
			pent._entities.splice(pind, 1);
		
		this._parentScene = null;
	}

	/**
	 * Removes the entity from the scene at the end of the update cycle (before being rendered)
	 * @param removePostRender whether or not the object should render at the end of this frame
	 */
	public RemoveFromScene(removePostRender: boolean = false){

		if(this.parentScene != null) {
			this.OnRemovedFromScene(this.parentScene);
			let removeArray = removePostRender ? 
				this.parentScene.entitiesToRemovePostRender :
				this.parentScene.entitiesToRemovePreRender;
			removeArray.push(this);
		}
		this._removed = true;
	}

	/**
	 * returns the first child entity of the specified type
	 * @param type the type (constructor function) value to look for, must be the same type passed 
	 * 	in as the generic parameter
	 */
	public GetChildOfType<T extends SceneEntity>(type: Function): T{

		for(let i = 0; i < this.entities.length; i++){
			if(this.entities[i] instanceof type){
				return this.entities[i] as T;
			}
		}

		return null;
	}

	/**
	 * search all the child entities recursively for the specified child, return an array
	 * of each child's index until the target is found
	 * @param child the child to search for
	 */
	public FindRecursiveChildIndex(child: SceneEntity): Array<number>{

		let r = new Array<number>();
		let chRef = child;

		// (it's secretly actually a backwards-up search, not a recursive downward traversal)
		// iterate through the child parents until it reaches this object, or null
		while(chRef != null && chRef instanceof SceneEntity){

			if(chRef.parent == null) return null;

			let chRefParent = chRef.parentEntity;
			let index = chRefParent._entities.indexOf(chRef);

			// if the specified child is actually not a child of this entity, return null
			if(index < 0) return null;

			// insert index at beginning of array
			r.splice(0, 0, index);

			// if the parent was reached, break out of the loop
			if(chRefParent == this) return r;

			chRef = chRefParent;
		}

		return null;
	}

	/**
	 * return the nested child with the specified indices
	 * @param deepIndex the sequential indices of the nested child
	 */
	public GetDeepChild(deepIndex: Array<number>): SceneEntity{

		let r: SceneEntity = this;

		for(let i = 0; i < deepIndex.length; i++){
			r = r.entities[deepIndex[i]];
		}

		return r;
	}

	/**
	 * finds the child (or self) of this entity that matches the specified suid, return null if 
	 * none found
	 * @param suid the suid of the entity to search for
	 */
	public GetEntityBySUid(suid: number): SceneEntity{
		
		if(this._sUid == suid) return this;

		let rec = null;
		for(let i = this.entities.length - 1; i >= 0; i--){
			rec = this.entities[i].GetEntityBySUid(suid);
			if(rec != null) break;
		}

		return rec;
	}

	///---------------------------------------------------------------------------------------------
	/// Scene Methods

	public Update(deltaTime: number): void{ 

		// iterate through each scene entity child in the entity container
		for(let i = 0; i < this._entities.length; i++){

			// update the scene entity's game logic
			this._entities[i].Update(deltaTime);
		}
	}

	public Draw(renderer: PIXI.Renderer): void{ }

	///---------------------------------------------------------------------------------------------

	public static GetDefaultEditorProperties(): Array<EditorProperty<SceneEntity, any>>{
		
		let r = new Array<EditorProperty<SceneEntity, any>>(
			EditorProperty.CreateStringProp("_name", "Name"),
			EditorProperty.CreateBoolProp("visible", "Visible"),
			EditorProperty.CreateVectProp("globalPosition", "Position")
		);

		return r;
	}

	public static GetEditorProperties(): Array<EditorProperty<SceneEntity, any>>{
		return this.GetDefaultEditorProperties();
	}

	///---------------------------------------------------------------------------------------------
	/// Cloning

	public CopyFields(source: SceneEntity): void{

		let serObj = SceneEntity.GetSerializedObject_NoChilds(source);
		this.Deserialize(serObj);

		// let pos = source.position;
		// this.position.set(pos.x, pos.y);
		// this._name = source._name;
		// // this._parentEntity = source._parentEntity;
	}

	protected CopyChildren(source: SceneEntity): void{

		let childs = source.entities;
		for(let i = 0; i < childs.length; i++){
			
			let childClone = childs[i].Clone();
			if(childClone != null) this.addChild(childClone);
		}
	}

	/** creates a clone of the object and returns it */
	public Clone(): SceneEntity{

		let r = (Object.getPrototypeOf(this).constructor).CreateDefault() as SceneEntity;
		r.CopyChildren(this);
		r.CopyFields(this);

		r.ParseSceneRefs();
		return r;
	}

	///---------------------------------------------------------------------------------------------
	/// Serialization

	/**
	 * default implementation for getting a serialized scene entity object without it's 
	 * child entities 
	 * @param ent the scene entity to serialize
	 */
	protected static GetSerializedObject_NoChilds(ent: SceneEntity): Object{
		let r: any = ISerializable.GetSerializedObject_DefaultImplementation(ent, false);

		// get the fields marked as serializable
		let type: ISerializableType<typeof ent> = Object.getPrototypeOf(ent).constructor;
		let untypedObj = ent as any;
		let fields = ISerializable.serializedFields.get(type);

		// individually serialize each field
		let ths = this;
		fields.forEach(function(field){
			let fieldData = untypedObj[field];
			if(fieldData != null){

				// handle field serialization with custom GetSerializedField function specifically
				// for fields on SceneEntities
				r[field] = ths.GetSerializedField(fieldData);
			}
		});

		return r;
	}

	/**
	 * default implementation for turning a field on a scene entity into a serialized object
	 * @param data the field data object
	 */
	protected static GetSerializedField(data: any): any{

		if(data instanceof Object){

			// serialize scene entities as scene references
			if(data instanceof SceneEntity) {
				return (new SceneReference(data.SUid)).GetSerializedObject();
			}

			// serialize each array entry seperately so that each scene entity can be converted to
			// a scene reference
			if(data instanceof Array){
				let r = [];
				for(let i = 0; i < data.length; i++){
					r.push(this.GetSerializedField(data[i]));
				}
				return r;
			}

			// serialize it normally if it's ISerializable but not a Scene Entity
			if(ISerializable.ImplementedIn(data)) ISerializable.GetSerializedField(data);

			// recursively iterate through each field of an anonymous object type to check for 
			// scene entities
			else{
				let r: any = {};
				for(let innerField in data){
					r[innerField] = this.GetSerializedField(data[innerField]);
				}
				return r;
			}
		}

		// if it's not an object, we can just serialize as it normally would be serialized
		return ISerializable.GetSerializedField(data);
	}

	/** 
	 * recursive call for EnsureNoRedundantChildSUids to gather data for finding matchin SUids,
	 * do not use this function outside of 'EnsureNoRedundantChildSUids' function call
	 */
	private EnsureNoRedundantChildSUids_Recursive(
		passIn: {low: number, high: number},
		ids: number[],
		matchingIds: SceneEntity[]
	): void {

		this._sUidsChecked = true;

		// append ranges to passin if provided
		passIn.low = Math.min(this._sUid, passIn.low);
		passIn.high = Math.max(this._sUid, passIn.low);
		
		// if the id of this entity is redundant, add it to the matchingIDs array
		if(ids.includes(this.SUid))
			matchingIds.push(this);

		// add their own id to the id array if not redundant
		else ids.push(this.SUid);

		// iterate thourgh children and recursively call this func
		for(let i = this.entities.length - 1; i >= 0; i--){
			this.entities[i].EnsureNoRedundantChildSUids_Recursive(
				passIn, ids, matchingIds);
		}
	}

	/** 
	 * ensure that this entity or any of it's recursive children do not share the same SUid, this
	 * should be called before an object is serialized
	 */
	public EnsureNoRedundantChildSUids(){

		// create passin if not provided
		let passIn = {
			low: this._sUid, 
			high: this._sUid
		}
		
		// create id and matchingID arrays if not passed in
		let ids: number[] = [];
		let matchingIds: SceneEntity[] = [];

		// recursively call on self and children to get matchin uids
		this.EnsureNoRedundantChildSUids_Recursive(passIn, ids, matchingIds);

		// enforce new SUids on the children who have matchin IDs
		SceneEntity._curSUid = Math.max(passIn.high + 1, SceneEntity._curSUid);
		for(let i = matchingIds.length - 1; i >= 0; i--){
			matchingIds[i]._sUid = SceneEntity.nextSUid;
		}
	}

	/** returns a JSON serializable object that has all the necessary data to recreate itself */
	public GetSerializedObject(): Object{

		// check children SUids to ensure none of them have the same SUid
		if(!this._sUidsChecked) this.EnsureNoRedundantChildSUids();
		
		// iterate through each serialized field, serialize it's value and append it to the return object
		let r = SceneEntity.GetSerializedObject_NoChilds(this) as any;

		// serialize the children
		let childs = this.GetSerializedChildren();
		r[SceneEntity.SFIELD_CHILDREN] = childs;

		return r;
	}

	/** get all the serialized child data in the form of an array of anonymous serialized objects */
	protected GetSerializedChildren(): Array<any>{

		// get all the children that are serializable and put them in an array
		let entities = this.entities;
		let toSerialize: SceneEntity[] = [];
		for(let i = 0; i < entities.length; i++){
			let cType: ISerializableType = Object.getPrototypeOf(entities[i]).constructor;
			if(ISerializable.IsTypeRegistered(cType)) toSerialize.push(entities[i]);
		}

		// iterate through the array of serializable children serialize each one		
		let r: any[] = [];
		for(let i = 0; i < toSerialize.length; i++){
			let serEnt = null;
			serEnt = toSerialize[i].GetSerializedObject();
			r.push(serEnt);
		}

		return r;
	}

	/**
	 * copy all the fields of the entity into an anonymous object for serialization
	 * @param obj the object to copy our fields into
	 */
	protected SerializeFieldsInto(obj: any, omittedTypes: Array<any> = []){
		
		let dif = new SceneEntity() as any;
		delete dif._sUid;

		let untypedSelf = this as any;

		omittedTypes.push( ... [
			Function,
			GameEvent,
			AiController
		]);

		// iterate though each field we have and copy the necesary ones over to our 
		// serialized object
		fieldCopy: for(let field in untypedSelf){

			// we don't want to copy any of these fields or
			// any field if it's shared with the base entity type (so we don't copy over a
			// bunch of useless redundant garbage into the serialized object)
			if(
				untypedSelf[field] === undefined || 
				field === "_parentGame" ||
				field === "_parentScene" ||
				dif[field] !== undefined
			){
				continue fieldCopy;
			}

			// skip if it's a native pixi object
			let isPixi = (
				untypedSelf[field] instanceof PIXI.DisplayObject && 
				!(untypedSelf[field] instanceof SceneEntity)
			);
			if(isPixi) continue fieldCopy;

			// dont copy omitted types
			for(let i = omittedTypes.length - 1; i >= 0; i--){
				if(untypedSelf[field] instanceof omittedTypes[i]){
					continue fieldCopy;
				}
			}

			// if it's a scene entity field, it must be a child or scene reference
			let ent = untypedSelf[field];
			if(ent instanceof SceneEntity){

				// TODO find recursive child index
				// if the field references an entity outside this object's children, don't store it
				let crIndex = this.FindRecursiveChildIndex(ent);
				if(crIndex == null){

					// look for scene reference
					if(this.parentScene !== null){

						// store the scene reference
						let sceneRef = new SceneReference(this.SUid);
						obj[field] = sceneRef.GetSerializedObject();
					}

					continue fieldCopy;
				}

				// serialize a field to the reference into the object
				let childRef: any = {};
				childRef.type = SceneEntity.TYPE_CHILDREF;
				childRef[SceneEntity.SFIELD_RECURSIVE_INDEX] = crIndex;
				obj[field] = childRef;

				continue fieldCopy;
			}

			// if the field value is serializable, get and store the serialized value
			let fieldVal = untypedSelf[field]
			if(ISerializable.ImplementedIn(fieldVal)){
				let val = fieldVal.GetSerializedObject();
				obj[field] = val;
				continue fieldCopy;
			}

			obj[field] = untypedSelf[field];
		}

		dif.destroy();
	}

	/**
	 * Deserialize a field from a serialized data object, can be any type, number, string, obj, etc
	 * @param fieldData JSON parsed the data for the field
	 */
	protected GetDeserializedField(fieldData: any): any{
		let r = ISerializable.GetDeserializedField(fieldData);
		return r;
	}

	/**
	 * copy the field values from a serialized object
	 * @param obj the serialized object to parse from
	 */
	protected CopyFieldsFromSerialized(obj: any): void{

		// cast self to anonymous object
		let untypedSelf = this as any;
		let selfType: ISerializableType<typeof this> = Object.getPrototypeOf(this).constructor;
		let serializedFields = ISerializable.serializedFields.get(selfType);
		
		// iterate through each property marked as serializable
		let ths = this;
		serializedFields.forEach(function(field){

			// get the deserialized and typed value from the serialzed data
			let fieldVal = ths.GetDeserializedField(obj[field]);
			untypedSelf[field] = fieldVal;

			// add the field to the scene ref query if it is a scene reference
			if(fieldVal instanceof SceneReference){
				ths._sceneRefQuery.push(field);
			}
		});

		// // iterate through each of the object's fields
		// for(let fieldName in obj){

		// 	// don't copy the children
		// 	if(fieldName == SceneEntity.SFIELD_CHILDREN){
		// 		continue;
		// 	}

		// 	// copy the field from the serialized object if we have a field of the same name
		// 	if(serializedFields.has(fieldName)){

		// 		// destroy the entity if it already exists
		// 		if(untypedSelf[fieldName] != null){
		// 			let selfFieldObj = untypedSelf[fieldName];
		// 			if(selfFieldObj instanceof SceneEntity){
		// 				selfFieldObj.destroy();
		// 			}
		// 		}

		// 		let fieldVal = ISerializable.GetDeserializedField(obj[fieldName]);
		// 		if(fieldVal instanceof SceneReference){
		// 			this._sceneRefQuery.push({fieldName: fieldName });
		// 			untypedSelf[fieldName] = fieldVal.targetSUid;
		// 			continue;
		// 		}

		// 		untypedSelf[fieldName] = fieldVal;
		// 	}
		// 
	}

	/**
	 * copy the children into this object from a serialized object
	 * @param obj the serialized object whose child list to parse and copy
	 */
	protected CopyChildrenFromSerialized(obj: any): void{
		if(obj[SceneEntity.SFIELD_CHILDREN] == null) return;

		let childs = obj[SceneEntity.SFIELD_CHILDREN];
		for(let i = 0; i < childs.length; i++){

			let child = ISerializable.GetDeserializedObject(
				childs[i]
			);

			if(child instanceof SceneEntity) {
				// if(this instanceof Scene) child._parentScene = this;
				this.addChild(child);
			}
		}
	}

	/** @inheritdoc */
	public Deserialize(data: any): void {
		this.CopyChildrenFromSerialized(data);
		this.CopyFieldsFromSerialized(data);
		this.OnPostDeserialize();
	}
	
	/** called after an object is deserialized */
	protected OnPostDeserialize(){ }

	/** iterate through all the non parsed scene references and dereference them */
	protected ParseSceneRefs(): void{

		// iterate through each entry in the scene reference query
		for(let i = this._sceneRefQuery.length - 1; i >= 0; i--){

			let sceneRefField = this._sceneRefQuery[i];
			let untypedTarget = this as any;

			// try parsing the scene reference
			let sceneRef = untypedTarget[sceneRefField] as SceneReference;
			let ent = sceneRef.GetDereferencedEntity(this.parentScene || this);
			
			// if the parse is successful, remove the field from the query
			if(ent != null){
				untypedTarget[sceneRefField] = ent;
				this._sceneRefQuery.splice(i, 1);
			}
		}

		// recursive scene refs
		for(let i = this.entities.length - 1; i >= 0; i--){
			this.entities[i].ParseSceneRefs();
		}
	}
}
