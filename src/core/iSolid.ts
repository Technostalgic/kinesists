///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { ICollidable, SceneEntity } from "../internal";

export interface ISolid extends ICollidable{

	get isSolid(): boolean;
	get materialFriction(): number;
	get materialRestitution(): number;
}

export namespace ISolid{

	/** type checking discriminator for ISolid interface */
	export function ImplementedIn(obj: SceneEntity): obj is ISolid{
		let r = obj as ISolid;
		return(
			r.isSolid !== undefined &&
			r.materialFriction !== undefined &&
			r.materialRestitution !== undefined &&
			ICollidable.ImplementedIn(r)
		);
	}

	/**
	 * discriminator and type checker just like ISolid.ImplementedIn, but only returns true if the
	 * object also has 'isSolid' set to true
	 */
	export function ImplementedValue(obj: SceneEntity): obj is ISolid{
		if(obj == null) return false;
		let r = obj as ISolid;
		return(
			ImplementedIn(r) &&
			r.isSolid
		);
	}
}