///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { Vect, SceneEntity, SpriteSheet } from '../internal';

export abstract class Effect extends SceneEntity{
	constructor(){ super(); }

	public static Sprite_HitEffect: SpriteSheet;

	public static Load(): void{

		// load the hit effect graphic
		// let texture = PIXI.BaseTexture.from('./dist/assets/graphics/SmallHit.png', {
		// 	scaleMode: PIXI.SCALE_MODES.NEAREST
		// });
		// let texSize = Vect.Create(35, 8);
		// let sprTextures: Array<PIXI.Texture> = [];
		// let frameSize = Vect.Create(5, 8);
		// for(let x = 0; x < texSize.x; x += frameSize.x){
		// 	let tex = new PIXI.Texture(texture, new PIXI.Rectangle(
		// 		x, 0, frameSize.x, frameSize.y
		// 	));
		// 	sprTextures.push(tex);
		// }

		this.Sprite_HitEffect = new SpriteSheet('./dist/assets/graphics/SmallHit.png');
		this.Sprite_HitEffect.GenerateSprites(7, 1, Vect.Create(1, 0.5));
	}
}