///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { GameEvent, ISerializable, Serializable, StatModifier, Stats } from '../internal';

/** 
 * base type for any type of character stat
 */
export abstract class Stat extends Serializable{

	private _modifiers: StatModifier[] = [];

	public get modifiers(): ReadonlyArray<Readonly<StatModifier>> { return this._modifiers; }
	
	/** the value of the stat, with modifers applied */
	public abstract get value(): number;

	/** the base value of the stat, without modifiers */
	public abstract get baseValue(): number;
	public abstract set baseValue(val: number);
	
	/** applies the specified stat modifier to this stat */
	public ApplyModifier(mod: StatModifier): StatModifier{
		if(mod.stat != null) throw "modifier is already applied to a stat";
		this._modifiers.push(mod);
		mod.stat = this;
		return mod;
	}

	/** calculates the total amount of stat modification from all the modifiers applied to this stat*/
	public GetCumulativeModifiers(): {add: number, multiply: number} {
		let r = {add: 0, multiply: 1};

		for(let i = this._modifiers.length - 1; i >= 0; i++){
			r.add += this._modifiers[i].add;
			r.multiply *= this._modifiers[i].multiply;
		}

		return r;
	}
}

/**
 * a stat that has a separate max value and current state value, i.e. heath has a "max health" and
 * a "current health" value
 */
export class ConsumableStat extends Stat{

	public constructor(baseMax: number){
		super();

		this._baseMaxValue = baseMax;
		this._currentValue = this.maxValue;
	}

	private _baseMaxValue: number = 0;
	private _currentValue: number = 0;

	private _cachedCurMaxVal: number = null;

	public get value(): number {
		return this._currentValue;
	}

	/** @inheritdoc */
	public set value(val: number) {
		this._currentValue = val;
		if(this._currentValue > this.maxValue) this._currentValue = this.maxValue;
		else if( this._currentValue < 0) this._currentValue = 0;
	}

	/** the max possible value of the stat's 'current' state, with modifiers applied */
	public get maxValue(): number{
		if(this._cachedCurMaxVal == null) this.CalculateMaxValue();
		return this._cachedCurMaxVal;
	}

	/** the base max value of the stat's 'current' state, WITHOUT modifiers applied */
	public get baseValue(): number {
		return this._baseMaxValue;
	}
	public set baseValue(val: number) {

		let percent = this.value / this.maxValue;

		this._cachedCurMaxVal = null;
		this._baseMaxValue = val;
		this._currentValue = percent * this.maxValue;
	}


	/// --------------------------------------------------------------------------------------------

	public static CreateDefault(){ return new this(1); }

	static {
		ISerializable.MakeSerializable(this, x => [
			x._baseMaxValue,
			x._currentValue
		]);
	}

	public Deserialize(data: any): void {
		ISerializable.DeserializeObject_DefaultImplementation(this, data);
		this._cachedCurMaxVal = null;
	}

	/// --------------------------------------------------------------------------------------------

	private CalculateMaxValue(){
		let cum = this.GetCumulativeModifiers();
		this._cachedCurMaxVal = (this._baseMaxValue * cum.multiply) + cum.add;
	}
}

/**
 * a generic stat, such as speed or attack, that can be used to calculate other things, such as 
 * the damage of an attack, or how fast a character can move
 */
export class StaticStat extends Stat{

	public constructor(baseVal: number){
		super();
		this._baseValue = baseVal;
	}

	private _baseValue: number = 0;
	private _cachedVal: number = null;

	/** @inheritdoc */
	public get value(): number {
		if(this._cachedVal == null) this.CalculateValue();
		return this._cachedVal;
	}

	/** @inheritdoc */
	public get baseValue(): number {
		return this._baseValue;
	}
	public set baseValue(val: number) {
		this._baseValue = val;
	}

	/// --------------------------------------------------------------------------------------------

	public static CreateDefault(){ return new this(1); }

	static {
		ISerializable.MakeSerializable(this, x => [
			x._baseValue
		]);
	}

	/// --------------------------------------------------------------------------------------------

	private CalculateValue(){
		let cum = this.GetCumulativeModifiers();
		this._cachedVal = (this._baseValue * cum.multiply) + cum.add;
	}
}