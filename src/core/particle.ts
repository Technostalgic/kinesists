///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from "pixi.js";
import {
	IVect, Vect, SpriteSheet,
	IDisplayTintable,
	SceneEntity
} from "../internal";

export abstract class Particle{

	constructor(){ }

	protected _displayObj: IDisplayTintable;
	
	public static readonly DEFAULT_SHEET: SpriteSheet = new SpriteSheet("./dist/assets/graphics/Pixel.png");

	public lifetime: number = 0;
	public maxLifetime: number = 1;
	public velocity: IVect = Vect.Create();
	public fadeStart: number = 0.5;

	public get displayObj() { return this._displayObj; }
	public get isAlive() { return this.lifetime < this.maxLifetime; }
	public get isRemoved() { return this._displayObj?.parent == null; }
	public get deltaLifetime() { return this.lifetime / this.maxLifetime; }

	public get globalPosition(): IVect{ return SceneEntity.GetGlobalPosition(this._displayObj); }
	public set globalPosition(val){ SceneEntity.SetGlobalPosition(this._displayObj, val); }

	// ---------------------------------------------------------------------------------------------

	public Update(deltaTime: number){

		// do nothing if the particle is not alive
		if(!this.isAlive) return;

		let lifedelta = this.deltaLifetime;

		this.HandelVelocity(deltaTime);
		this.HandleFade(lifedelta);

		this.lifetime += deltaTime;
	}

	public HandleFade(lifedelta: number){
		
		let fadeStart = this.maxLifetime * this.fadeStart;
		let fadeDelta = lifedelta - fadeStart;
		if(fadeDelta < 0){
			fadeDelta = 0;
		}
		else{
			let fadeTime = this.maxLifetime - fadeStart;
			fadeDelta /= fadeTime;
			if(fadeDelta > 1) fadeDelta = 1;
		}

		this._displayObj.alpha = 1 - fadeDelta;
	}

	public HandelVelocity(deltaTime: number){

		// apply velocity
		let opos = this._displayObj.position;
		this._displayObj.position.set(
			opos.x + this.velocity.x * deltaTime,
			opos.y + this.velocity.y * deltaTime
		);
	}

	/**
	 * removes the particle's display object from the scene - note this does not destroy the display
	 * object, so it may cause memory leaks if used improperly. You can manually call 
	 * this._displayObj.destroy() if calling this function outside it's intended scope
	 */
	public Remove(){
		if(this._displayObj == null) return;
		this._displayObj.parent?.removeChild(this._displayObj);
	}

	/** resets the particle so it is ready to be re-used as if it were new */
	public Reset(){
		this.lifetime = 0;
		this._displayObj.position.set(0, 0);
		this._displayObj.rotation = 0;
		this._displayObj.scale.set(1, 1);
		this._displayObj.alpha = 1;
		this._displayObj.tint = 0xFFFFFF;
	}
}

export class LineParticle extends Particle{

	public constructor(){
		super();
		this._displayObj = new PIXI.Sprite(Particle.DEFAULT_SHEET.sprites[0]);
	}

	private _lastPos: IVect = null;

	public lineWidth: number = 2;
	public minLength: number = 2;

	public get displayObj(): PIXI.Sprite { return this._displayObj as PIXI.Sprite; }

	public override Update(deltaTime: number): void {
		if(this._lastPos == null) this._lastPos = this.globalPosition;
		super.Update(deltaTime);

		let curPos = this.globalPosition;
		let dif = Vect.Subtract(curPos, this._lastPos);
		let difMag = Vect.Magnitude(dif);
		let difDir = Vect.Direction(dif);

		let fx = this.displayObj;
		fx.scale.set(Math.max(this.minLength, difMag), this.lineWidth);
		fx.rotation = difDir;

		this._lastPos = curPos;
	}

	public override Reset(): void {
		super.Reset();
		this._lastPos = null;
		this.lineWidth = 1;
	}
}