///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from "pixi.js";

/** interface for PIXI displayObjects that have the 'tint' field */ 
export interface IDisplayTintable extends PIXI.DisplayObject{
	get tint(): number;
	set tint(val: number);
}

export namespace IDisplayTintable {

	/** type discriminator for IDisplayable */
	export function ImplementedIn(obj: PIXI.DisplayObject): obj is IDisplayTintable {
		if(obj == null) return;
		let r = obj as IDisplayTintable;
		return(
			r.tint !== undefined
		);
	}
}