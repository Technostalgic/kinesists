///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { Game, SceneEntity, Rect, Vect, SceneReference } from '../internal';

/** 
 * interface for specifying constraints of the class for a serializable instance (for enforcing 
 * static methods) 
 */
export interface ISerializableType<T extends ISerializable = ISerializable>{
	new(... args: unknown[]): T;
	CreateDefault(): T;
}

/** used to serialize and deserialize objects to/from anonymous data objects */
export interface ISerializable{
	GetSerializedObject(): any;
	Deserialize(data: any): void;
}

export namespace ISerializable{

	export const SFIELD_TYPE: string = "_serializedType";

	/**
	 * Deserialize an object based on it's 'type' property in the scope of the specified game
	 * @param obj the object to deserialized
	 * @param gameRef the reference to the game who's type registry will be used
	 */
	export function GetDeserializedObject(obj: any): ISerializable{

		if(obj == null) return null;
		if(obj[ISerializable.SFIELD_TYPE] === undefined) return null;
		if(obj[ISerializable.SFIELD_TYPE] === this.TYPE_CHILDREF){
			throw (
				"Objects cannot be parsed as children without a reference to their sibling list," + 
				"use SceneEntity.GetDeserializedField(...) instead"
			);
		}

		// search for the serialized type
		let type = typeRegistry.get(obj[SFIELD_TYPE]);
		let ur = type.CreateDefault();
		ur.Deserialize(obj);

		return ur;
	}

	/**
	 * Type discriminator for ISerializable type
	 * @param obj the object to check and see if it is ISerializable
	 */
	export function ImplementedIn(obj: any): obj is ISerializable{
		if(obj == null) return false;
		let r = obj as ISerializable;
		return r instanceof SceneEntity || (
			r.GetSerializedObject !== undefined &&
			r.Deserialize !== undefined
		);
	}

	// ---------------------------------------------------------------------------------------------

	/** gets a safe serialized object from any reference type object if possible */
	export function GetSerializedObject(obj: ISerializable): any{ return obj.GetSerializedObject(); }

	/**
	 * serializes an object according to default serialization algorithm, it is meant for use in 
	 * ISerializable.GetSerializedObject specific class implementations and to be expanded upon if
	 * needed
	 * @param obj the ISerializable instance to serialize
	 * @param doFields whether or not the fields of the object should be parsed (this should pretty 
	 * 	much always be true, only false when you're trying to implement a custom solution for the 
	 * 	field serialization)
	 */
	export function GetSerializedObject_DefaultImplementation(
		obj: ISerializable, 
		doFields: boolean = true): 
	any{

		let type: ISerializableType<typeof obj> = Object.getPrototypeOf(obj).constructor;
		if(typeRegistry.get(type.name) == null){
			throw "Type for specified object is not registered for serialization";
		}

		// define the return type and store the type data in it
		let r: any = {};
		r[SFIELD_TYPE] = type.name;
		
		// early return without serialized fields if specified
		if(!doFields) return r;
		
		// serialize fields based on fields marked as serializable
		let untypedObj = obj as any;
		let fields = serializedFields.get(type);
		fields.forEach(function(field){
			let fieldData = untypedObj[field];
			if(fieldData != null) r[field] = GetSerializedField(fieldData);
		});

		return r;
	}

	/** 
	 * gets a safe serialized object from any type, including value types such as strings 
	 * and numbers 
	 */
	export function GetSerializedField(data: any): any{

		if(data instanceof Object){

			// recursively serialize elements
			if(data instanceof Array){
				let r = [];
				for(let i = 0; i < data.length; i++){
					r.push(GetSerializedField(data[i]));
				}
				return r;
			}

			// serialize it according to serialization technique if it implements ISerializable
			if(ImplementedIn(data)) return data.GetSerializedObject();

			// recursively iterate through each field of an anonymous object type
			else{
				let r: any = {};
				for(let innerField in data){
					r[innerField] = GetSerializedField(data[innerField]);
				}
				return r;
			}
		}

		// basic math types to serialize
		// if(Vect.ImplementedIn(data)) return Vect.Create(data.x, data.y);
		// if(Rect.ImplementedIn(data)) return Rect.Create(Vect.Clone(data.position), Vect.Clone(data.size));

		return data;
	}

	/**
	 * Deserialize serialized object data into a new object of the specified type
	 * @param type the type to parse the serialized object as
	 * @param data the serialized object
	 * @param gameRef a reference to the game (unused - will be removed)
	 */
	export function DeserializeObject<T extends ISerializable>(
		type: ISerializableType<T>, 
		data: any ): 
	T {
		
		// create a new instance of specified type
		let t = typeRegistry.get(data[SFIELD_TYPE] as string);
		let target = t.CreateDefault() as T;

		// deserialize the target as an instance of the specified type
		target.Deserialize(data as any);
		return target;

		// let fields = serializedFields.get(type);
		// if(fields == null) return target;
		// 
		// let untypedData = data as any;
		// fields.forEach(function(field){
		// 	
		// 	if(untypedData[SFIELD_TYPE] != null){
		// 		// TODO Deserialize by type
		// 	}
		//
		// 	else target[field as keyof T] = untypedData[field];
		// });
		//
		// return target;
	}

	/**
	 * the default implementation for deserialization behavior, meant to be expanded upon for 
	 * individual serialization cases
	 * @param target the target object to deserialize the data into
	 * @param data the serialized data
	 */
	export function DeserializeObject_DefaultImplementation<T extends ISerializable = ISerializable>(
		target: T,
		data: Object ):
	void {
		let untypedData = data as any;
		let serializedType = typeRegistry.get(untypedData[SFIELD_TYPE] as string);
		let targetType: ISerializableType<typeof target> = Object.getPrototypeOf(target).constructor;

		// check to see if type signature on the serialized type is the same as the type specified
		let sType = untypedData[SFIELD_TYPE];
		if(sType != null){
			if(serializedType != targetType)
				throw (
					"Type mismatch, specified type is " + targetType.name + 
					", but serialized type is" + sType 
				);
		}

		// if there is no type field, we can't really deserialize it
		else{ throw "Type not specified in serialized data"; }

		// get the serialized fields registered to the type
		let fields = serializedFields.get(targetType);

		// iterate through each field marked as serializable and grab the data from them
		fields.forEach(function(field){
			target[field as keyof T] = GetDeserializedField(untypedData[field]);
		});

		return;
	}

	/**
	 * Deserialize a field from a serialized data object, can be any type, number, string, obj, etc
	 * @param fieldData JSON parsed the data for the field
	 */
	export function GetDeserializedField(fieldData: any): any{

		// if the field data is an Object type, it may be an array or ISerializable type
		if(fieldData instanceof Object){

			// if the field is an array parse each individual entry
			if(fieldData instanceof Array){
				let r = [];
				for(let i = 0; i < fieldData.length; i++){
					r.push(GetDeserializedField(fieldData[i]));
				}
				return r;
			}

			// if the field is an ISerializable object, deserialize it according to it's type
			let fieldTypeKey = fieldData[SFIELD_TYPE];
			if(fieldTypeKey !== undefined){
				let fieldType = typeRegistry.get(fieldTypeKey);
				let r = DeserializeObject(fieldType, fieldData);

				// references should be handled differently somehow
				// if(r instanceof SceneReference){
				// 	// TODO
				// 	// push to some reference query on fieldTarget
				// 	throw "Not Implemented";
				// }

				return r;
			}

			// if the field is an anonymous object type, we need to check each inner field
			else{
				let r: any = {};
				for(let innerField in fieldData){
					let innerData = fieldData[innerField];
					r[innerField] = GetDeserializedField(innerData);
				}
				return r;
			}
		}

		return fieldData;
	}

	// ---------------------------------------------------------------------------------------------

	/** holds meta data about which types the type names represent */
	export const typeRegistry = new Map<string, ISerializableType>();

	/** the meta data object that holds info about serialized fields for each serializable class */
	export const serializedFields = new Map<ISerializableType, Set<PropertyKey>>();
	const keyProxy = new Proxy({}, { get(_, p) { return p; } });

	/**
	 * returns true if the specified type is registered and marked as serializable
	 * @param ctor the type to check for
	 */
	export function IsTypeRegistered<T extends ISerializable>(ctor: ISerializableType<T>): boolean{
		return serializedFields.has(ctor);
	}

	/**
	 * marks a certain field on the specified type to be serialized
	 * @param ctor the constructor object of the specified type (the class object)
	 * @param makeFields the proxy for the fields to make serializable (ie x=>[x.*fieldName*, ...])
	 */
	export function MakeSerializable<T extends ISerializable>(
		ctor: ISerializableType<T>, 
		makeFields: (value: T) => unknown[] = () => []
	): void {

		// register the type if it has not yet been registered
		let type = typeRegistry.get(ctor.name);
		if(type == null) typeRegistry.set(ctor.name, ctor);

		// get the set of fields that are already marked as serialized from the specified type
		let props = (function(){
			let res = serializedFields.get(ctor);
			if (!res) {
				res = new Set();
				serializedFields.set(ctor, res);
			}
			return res;
		})();

		// add the specified fields to the set to mark them as serialized
		let fields = makeFields(keyProxy as T) as string[];
		for (const field of fields) {
			props.add(field);
		}
	}

	//	|// example on how to make serializable fields within a class
	//	|class Foo implements ISerializable {
	//	|	
	//	|	GetSerializedObject(): any { throw "not impl"; }
	//	|	Deserialize(data: any, gameRef: Game): void { throw "not impl"; }
	//	|	
	//	|	someProp: string = "";
	//	|	static SetSerializableFields(): void {
	//	|		MakeSerializable(this, x => [x.someProp]);
	//	|	}
	//	|	static {
	//	|		this.SetSerializableFields();
	//	|	}
	//	|}
	//	|class Bar extends Foo{
	//	|	anotherProp: string = "";
	//	|	thirdProp: string = "";
	//	|	static SetSerializableFields(): void {
	//	|		super.SetSerializableFields();
	//	|		MakeSerializable(this, x => [x.anotherProp, x.thirdProp]);
	//	|	}
	//	|	static{
	//	|		this.SetSerializableFields();
	//	|		// console.log(metaData.get(Bar)); // set of "someProp", "anotherProp", and "thirdProp"
	//	|	}
	//	|}

}

export abstract class Serializable implements ISerializable{
	
	public static CreateDefault(){
		return new (this as unknown as ISerializableType<Serializable>)();
	}

	protected static SetSerializableFields(): void{
		ISerializable.MakeSerializable(this as unknown as ISerializableType<Serializable>);
	}

	// To mark as serializable, use:
	// static { this.SetSerializableFields(); }

	// ---------------------------------------------------------------------------------------------

	public GetSerializedObject(): Object { 
		return ISerializable.GetSerializedObject_DefaultImplementation(this); 
	}

	public Deserialize(data: any): void {
		ISerializable.DeserializeObject_DefaultImplementation(this, data);
	}
}