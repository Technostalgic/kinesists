///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	AABBCollider, CircleCollider, SpriteSheet,
	Seeker,
	GameScene, ISerializable,
	IVect, Vect,
	Player,
	Scene,
	SceneEntity,
	Terrain,
	Spikes, JumpPad,
	Handgun, 
	Gunner
} from '../internal';

/**
 * data structure that handles the core game logic and loop
 */
export class Game{

	/**
	 * creates a game with the specified render resolution
	 * @param width the x resolution of the game renderer
	 * @param height the y resolution of the game renderer, equal to width if not specified
	 */
	public constructor(width: number = 0, height: number = width){
		this._renderer = new PIXI.Renderer({
			clearBeforeRender: false,
			backgroundAlpha: 1,
			width: width,
			height: height
		});

		if(Game._instance != null){
			throw "Multiple singleton instances not allowed";
		}
		Game._instance = this;

		this._currentScene = GameScene.BasicScene(this);
		this._currentScene.OnEnter();

		console.log(this);
	}

	private static _instance: Game;
	public static get instance() {return this._instance; }

	private _animFrameRequestID: number = -1;
	private _renderer: PIXI.Renderer;
	private _lastStepTimestamp: DOMHighResTimeStamp;
	private _frameRateCap = 11.11111; // 90 FPS
	private _currentScene: Scene;
	private _keysPressed: Array<boolean> = new Array(256);
	private _mousePos: IVect = Vect.Create();
	private _mouseButton: number = 0;
	private _mouseScroll: number = 0;
	private _mouseButtonsPressed: Array<boolean> = new Array(3);
	private _mousePressed: boolean = false;

	/**
	 * The pixijs renderer object used for rendering everything in the game
	 */
	public get renderer(){
		return this._renderer;
	}

	public get mouseScroll() { return this._mouseScroll; }
	public get mousePos() { return this._mousePos; }
	public get mouseButton() { return this._mouseButton; }
	public get mousePressed() { return this._mousePressed; }
	public get pointerIsLocked() { return document.pointerLockElement == this.renderer.view; }

	public get maxFramesPerSecond(){
		return 1000 / this._frameRateCap;
	}
	public set maxFramesPerSecond(fps){
		this._frameRateCap = 1000 / fps;
	}

	public get currentScene(){ return this._currentScene; }
	public set currentScene(scene){

		this._currentScene.OnExit();
		this._currentScene = scene;
		this._currentScene.OnEnter();
		
		if(!scene.requestPointerLock){
			document.exitPointerLock();
		}
	}

	public get keysPressed(): Array<boolean>{ return this._keysPressed; }
	public get mouseButtonsPressed(): Array<boolean>{ return this._mouseButtonsPressed; }

	/** starts listening to mouse events for the game */
	public AttachMouseEvents(): void{

		let ths = this;

		// NO RIGHT CLIK MENU >:C
		this.renderer.view.addEventListener('contextmenu', function(e){ 
			e.preventDefault(); 
			return false; 
		});

		this.renderer.view.addEventListener('wheel', function(e){
			ths.OnScroll(e);
		});
		this.renderer.view.addEventListener('mousedown', function(e){
			ths.OnMouseDown(e);
		});
		this.renderer.view.addEventListener('mouseup', function(e){
			ths.OnMouseUp(e);
		});
		this.renderer.view.addEventListener('mouseout', function(e){
			ths.OnMouseOut(e);
		});
		this.renderer.view.addEventListener('mousemove', function(e){
			ths.OnMouseMove(e);
		})
	}

	/**
	 * starts the game listening to keyboard events
	 */
	public AttachKeyboardEvents(): void{

		let ths = this;

		window.addEventListener('keydown', function(e){
			//console.log(e.key + ": " + e.keyCode);
			if(e.keyCode !== 73 && e.keyCode !== 123){
				e.preventDefault();
			}
			ths.OnKeyDown(e);
		});
		window.addEventListener('keyup', function(e){
			ths.OnKeyUp(e)
		});
	}

	/**
	 * stops listening for keyboard events
	 */
	public DetachKeyboardEvents(): void{

		// TODO
	}

	/**
	 * resets all the keyboard input flags to false
	 */
	public ResetKeyboardInput(): void{

		for(let i = this._keysPressed.length - 1; i >= 0; i--){
			this._keysPressed[i] = false;
		}
	}

	private OnKeyDown(e: KeyboardEvent){
		console.log(e.key + ": " + e.keyCode);
		this._keysPressed[e.keyCode] = true;
	}

	private OnKeyUp(e: KeyboardEvent){
		this._keysPressed[e.keyCode] = false;
	}

	private OnScroll(e: WheelEvent){
		this._mouseScroll = Math.sign(e.deltaY);
	}

	private OnMouseDown(e: MouseEvent){

		if(this.currentScene?.requestPointerLock){
			this.renderer.view.requestPointerLock();
		}

		// console.log(e.button);
		this._mouseButtonsPressed[e.button] = true;
		this._mouseButton = e.button;
		this._mousePressed = true;

		if(!this.pointerIsLocked){
			this._mousePos = {
				x: e.offsetX,
				y: e.offsetY
			};
		}
	}

	private OnMouseMove(e: MouseEvent){

		if(this.pointerIsLocked){
			this._mousePos.x += e.movementX;
			this._mousePos.y += e.movementY;
			return;
		}

		this._mousePos = {
			x: e.offsetX,
			y: e.offsetY
		};
	}

	private OnMouseUp(e: MouseEvent){
		
		this._mouseButtonsPressed[e.button] = false;
		this._mousePressed = false;

		if(!this.pointerIsLocked){
			this._mousePos = {
				x: e.offsetX,
				y: e.offsetY
			};
		}
	}

	private OnMouseOut(e: MouseEvent){
		
		for(let i = this._mouseButtonsPressed.length - 1; i >= 0; i--) {
			this._mouseButtonsPressed[i] = false;
		}

		this._mousePressed = false;
		
		if(!this.pointerIsLocked){
			this._mousePos = {
				x: e.offsetX,
				y: e.offsetY
			};
		}
	}

	/**
	 * sets the game to start continuously performing update and draw cycles
	 */
	public StartGameLoop(): void{

		if(this._animFrameRequestID >= 0)
			return;

		this._lastStepTimestamp = performance.now();
		let ths = this;
		this._animFrameRequestID = requestAnimationFrame(function(time){
			ths.Step(time);
		});
	}

	/**
	 * stops the game from performing anymore update/draw cycles automatically
	 */
	public StopGameLoop(): void{
		cancelAnimationFrame(this._animFrameRequestID);
		this._animFrameRequestID = -1;
	}

	private Step(time: DOMHighResTimeStamp): void{

		let ths = this;
		this._animFrameRequestID = requestAnimationFrame(function(time){
			ths.Step(time);
		})

		let deltaTime = time - this._lastStepTimestamp;
		if(deltaTime <= this._frameRateCap)
			return;

		this._lastStepTimestamp = time;

		this.Update(deltaTime * 0.001);
		this.Draw();

		this._mouseScroll = 0;
	}

	/**
	 * Simulate gameplay logic for the specified amount of time
	 * @param deltaTime the ampunt of time in seconds that will be simulated in this update cycle
	 */
	public Update(deltaTime: number): void{

		this._currentScene?.Update(deltaTime);
	}

	/**
	 * Render the current game state to the render canvas
	 */
	public Draw(): void{
		
		// clear the render canvas to the default background color
		this._renderer.clear();

		this._currentScene?.Draw(this.renderer);
	}
}