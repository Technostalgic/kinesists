///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { 
	Collider,
	ICollision,
	ICollidable,
	IVect, Vect,
	SceneEntity, 
	ISerializable
} from '../internal';

export class PhysicsEntity extends SceneEntity implements ICollidable{

	public constructor(){ super(); }

	protected _collider: Collider = null;
	protected _velocity: IVect = Vect.Create();
	protected _rotationalVelocity: number = 0;
	private _mass: number = 1;
	private _inertia: number = null;
	
	public get collider() { return this._collider; }

	public get velocity(){ return this._velocity; }
	public set velocity(val){ 
		this._velocity.x = val.x;
		this._velocity.y = val.y;
	}

	/** be careful with this, not all collision shapes support rotation */
	public get rotationalVelocity() { return this._rotationalVelocity; }
	public set rotationalVelocity(val) { this._rotationalVelocity = val; }

	public get mass(){ return this._mass; }
	public set mass(val){
		this._mass = val;

		if(val > 0) this._inertia = 1 / this.mass;
		else this._inertia = Number.POSITIVE_INFINITY;
	}

	public get inertia(){ 
		if(this._inertia == null) this.mass = this.mass;
		return this._inertia; 
	}
	
	public get momentum(){
		return Vect.MultiplyScalar(this.velocity, this.mass);
	}
	
	// ---------------------------------------------------------------------------------------------

	public static CreateDefault(){ return new this(); }

	protected static SetSerializableFields(): void{

		super.SetSerializableFields();
		ISerializable.MakeSerializable(this, x => [
			x._velocity,
			x._rotationalVelocity,
			x._mass,
			x._collider
		]);
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------
	
	public override Update(deltaTime: number): void{

		this.Simulate(deltaTime);
		super.Update(deltaTime);
	}

	/**
	 * accelerate the object toward a specified velocity
	 * @param velocity the velocity to approach
	 * @param maxAccel the maximum magnitude of acceleration to apply toward the velocity
	 */
	public AccelerateToVelocity(vel: IVect, maxAccel: number){
		
		// if target velocity is reachable in this step, set velocity to target velocity
		let dif = Vect.Subtract(vel, this.velocity);
		if(Vect.Magnitude(dif) < maxAccel){
			this._velocity.x = vel.x;
			this._velocity.y = vel.y;
			return;
		}

		// apply maximum possible acceleration if velocity is not reachable
		let acc = Vect.MultiplyScalar(Vect.Normalize(dif), maxAccel);
		this._velocity.x += acc.x;
		this._velocity.y += acc.y;
	}

	/**
	 * applies an impulse to to the physics object
	 * @param impulse the impulse to apply
	 */
	public ApplyImpulse(impulse: IVect){

		let acc = Vect.MultiplyScalar(impulse, this._inertia);
		this._velocity = Vect.Add(this._velocity, acc);
	}

	/**
	 * applies a force to the object over a specified time
	 * @param force the force to apply
	 * @param deltaTime the amount of time to apply the force over (note, the cumulative force is 
	 * 	applied instantly, even if a long deltaTime is specified)
	 */
	public ApplyForce(force: IVect, deltaTime: number){

		let acc = Vect.MultiplyScalar(force, this.inertia * deltaTime);
		this._velocity = Vect.Add(this._velocity, acc);
	}

	public ApplyTorqueImpulse(torque: number){
		let rotAcc = torque * this.inertia;
		this._rotationalVelocity += rotAcc;
	}

	public ApplyTorque(torque: number, deltaTime: number){
		let rotAcc = torque * this.inertia * deltaTime;
		this._rotationalVelocity += rotAcc;
	}

	public ResolveSolidCollision(collision: ICollision): void{

		let norm = collision.directionNormal;
		if(this._collider == collision.colliderB){
			norm = Vect.Invert(norm);
		}

		// add velocity against collision normal
		let dot = Vect.Dot(this._velocity, norm);
		if(dot < 0){
			let dVel = Vect.Project(this._velocity, norm);
			dVel = Vect.Invert(dVel);
			this._velocity = Vect.Add(this._velocity, dVel);
		}

		// modify position to move entity outside of collider
		let pos = this.globalPosition;
		this.position.set(
			pos.x + norm.x * collision.penetration,
			pos.y + norm.y * collision.penetration
		);
	}

	public Simulate(deltaTime: number){

		// translate position by velocity applied over delta time
		let pos = Vect.Clone(this.globalPosition);
		pos = Vect.Add(pos, Vect.MultiplyScalar(this._velocity, deltaTime));

		// rotate the object by it's rotational velocity
		this.rotation += this._rotationalVelocity * deltaTime;

		// applying the new position
		this.globalPosition = pos;
	}
}