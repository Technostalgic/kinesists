///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

import { Collider, SceneEntity } from '../internal';

/** Interface to allow a scene entity to be able to be activated or deactivated */
export interface ICollidable extends SceneEntity{
	
	/** Whether or not the bject is currently active in the scene */
	get collider(): Collider;
}

export namespace ICollidable{
	
	/** Type checking discriminator for IActivate */
	export function ImplementedIn(obj: SceneEntity): obj is ICollidable{
		if(obj == null) return false;
		let r = obj as ICollidable;
		return(
			r.collider !== undefined
		);
	}
}