///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { 
	IControllable, SceneEntity, PhysicsEntity, Actor, ISerializable, Serializable
} from '../internal';

export abstract class ObjectController extends Serializable{
	public abstract Update(deltaTime: number, controlled: IControllable): void;
}

export class IdleController extends ObjectController{
	static{ this.SetSerializableFields(); }

	public Update(deltTime: number, controlled: IControllable): void { }
}

export abstract class EntityController extends ObjectController{
	
	public Update(deltaTime: number, entity: IControllable & SceneEntity): void {
		if(!(entity instanceof SceneEntity)) throw "Invalid control target";
	}

}

export abstract class ActorController extends ObjectController{
	
	public Update(deltaTime: number, actor: IControllable & Actor): void {
		if(!(actor instanceof Actor)) throw "Invalid control target";
	}
}