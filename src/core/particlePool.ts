///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	Particle 
} from '../internal';

/**
 * an object used to recycle and reuse particle instances instead of constantly creating new ones
 */
export class ParticlePool<T extends Particle>{

	/**
	 * create a new particle pool for the specified type of particle
	 */
	constructor(particleType: { new(): T }, count: number){
		this._particleType = particleType;
		this.ReserveParticles(count);
	}

	private _particleType: { new(): T } = null;
	private _unusedParticles: Array<Particle> = new Array();

	// ---------------------------------------------------------------------------------------------

	/**
	 * create the specified amount of particles and store them without adding them to the scene
	 * @param count the amount of particles that will be created
	 */
	public ReserveParticles(count: number){
		for(let i = count - 1; i >= 0; i--){
			let part = new this._particleType();
			this._unusedParticles.push(part);
		}
	}

	/** gets an unused particle from the pool and returns it */
	public GetParticle(container: PIXI.ParticleContainer): Particle{

		let r = this._unusedParticles.length > 0 ?
			this._unusedParticles.pop() :
			new this._particleType();

		r.Reset();
		container.addChild(r.displayObj);
		return r;
	}

	/** removes a particle from the scene, puts it in the pool and reserves it for later use */
	public PoolParticle(particle: Particle): void{
		this._unusedParticles.push(particle);
		particle.Remove();
	}

	/** dispose of all the particles in the pool from memory (?) */
	public Dispose(){
		for(let i = this._unusedParticles.length - 1; i >= 0; i--){
			this._unusedParticles[i].displayObj.destroy();
		}
		this._unusedParticles = null;
	}
}