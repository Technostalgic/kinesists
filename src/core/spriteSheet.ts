///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	GameEvent, IVect, Vect, Maths, ISerializable, IRect, Rect, Serializable
} from '../internal';

export class SpriteSheet extends Serializable{

	public constructor(path: string) {
		super();
		if(path != null) this.SetTexture(path);
	}

	public static GetFromPath(path: string){
		let r = new SpriteSheet(path);
		return r;
	}

	// ---------------------------------------------------------------------------------------------
	private static _sheetID: number = 0;
	private _uid: number = SpriteSheet._sheetID++;
	private _texPath: string = "./dist/assets/graphics/Pixel.png";
	private _resource: PIXI.LoaderResource = null;
	private _isLoaded: boolean = false;
	private _baseTexture: PIXI.BaseTexture;
	private _onLoad: GameEvent<PIXI.LoaderResource> = new GameEvent();
	private _sprites: Array<PIXI.Texture>;
	private _isInQueue: boolean = false;
	private _pathHash: number = 0;
	
	public get uid(){ return this._uid; }
	public get uniqueHashString() { return "_sheet" + this._pathHash; }
	public get isLoaded() { return this._isLoaded; }
	public get baseTexture() { return this._baseTexture; }
	public get sprites() { return this._sprites; }


	/// --------------------------------------------------------------------------------------------

	public static CreateDefault(){ return new SpriteSheet(null); }

	static { this.SetSerializableFields(); }

	/// --------------------------------------------------------------------------------------------
	public SetTexture(path: string): void{
		this._uid = SpriteSheet._sheetID++;
		this._texPath = path;
		this._pathHash = Maths.StringToHash(this._texPath);
		this._baseTexture = null;
		this._isLoaded = false;
		this._resource = null;

		this.GetTexture();
		if(this._resource == null){
			this.TryLoad();
		}
	}

	public GetTexture(): void{
		
		let loader = PIXI.Loader.shared;
		let resource = loader.resources[this.uniqueHashString];
		if(resource == null){
			this._isLoaded = false;
			this._resource = null;
			return;
		}
		
		// delay get texture until next step if it's not yet loaded (can't use resource.onComplete 
		// because PIXI is broken)
		this._resource = resource;
		if(!resource.isComplete){
			this._isLoaded = false;
			let ths = this;
			setTimeout(function(){ ths.GetTexture(); });
			return;
		}

		// delay get texture until next step if it's not yet loaded and it's an image type (can't 
		// use resource.onComplete because PIXI is broken)
		if(resource.texture == null && resource.type == PIXI.LoaderResource.TYPE.IMAGE){
			this._isLoaded = false;
			let ths = this;
			setTimeout(function(){ ths.GetTexture(); });
			return;
		}

		this.OnGetResource(resource);
	}

	public TryLoad(): void{

		console.log("requested: " + this._texPath);

		let loader = PIXI.Loader.shared;
		if(!loader.loading){
			this.DoLoad(loader);
		}
		else{
			let ths = this;
			if(!ths._isInQueue){
				loader.onLoad.add(function(loader, resource){
					
					// TODO if getting weird resource issues, try not fucking with this flag:
					loader.loading = false;
					console.log("dequeued: " + ths._texPath);
					ths.TryLoad();
				});
				ths._isInQueue = true;
				console.log("queuing: " + this._texPath);
			}
		}
	}

	// ---------------------------------------------------------------------------------------------
	private DoLoad(loader: PIXI.Loader): void{
		
		// check to see if resource is already loaded
		let resource = loader.resources[this.uniqueHashString];
		if(resource != null){
			let ths = this;
			setTimeout(function(){
				ths.GetTexture();
			});
			return;
		}

		let ths = this;
		loader.add(this.uniqueHashString, this._texPath, {}, function(resource){
			ths.OnLoad(resource);
		});

		this._isInQueue = false;

		setTimeout(function(){
			console.log("LOADING: " + ths._texPath);
			if(!loader.loading) loader.load();
		}, 0);
	}

	private OnLoad(resource: PIXI.LoaderResource): void{

		this.OnGetResource(resource);
		console.log("LOADED: " + resource.url);
	}

	private OnGetResource(resource: PIXI.LoaderResource): void{

		if(resource?.texture == null){
			throw "texture not found at " + this._texPath;
		}

		this._baseTexture = resource.texture.baseTexture;
		this._baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
		this._resource = resource;
		this._isLoaded = true;

		if(this.sprites == null) this.GenerateSprites(1, 1, Vect.Create(0.5));
		this._onLoad.Invoke(resource);
		this._onLoad.ClearListeners();
	}

	// ---------------------------------------------------------------------------------------------
	public DoOnLoaded(listener: (data: PIXI.LoaderResource) => void){
		if(this.isLoaded)
			listener(this._resource);
		else{
			this._onLoad.AddListener(listener);
		}
	}

	public GenerateSprites(columns: number, rows: number, 
		anchor: IVect = Vect.Create(0.5), lastRowCount:number = columns): 
	void{

		if(!this.isLoaded){
			let ths = this;
			this._onLoad.AddListener(function(resource){
				ths.GenerateSprites(columns, rows, anchor, lastRowCount);
			});
			return;
		}

		// create new sprite array
		this._sprites = new Array<PIXI.Texture>();

		// firgure sprite dimensions
		let width = this._baseTexture.width / columns;
		let height = this._baseTexture.height / rows;

		// iterate through each row/column cell
		for(let y = 0; y < rows; y++){
			let yPos = Math.floor(y * height);
			for(let x = 0; x < columns; x++){
				let xPos = Math.floor(x * width);

				// create a sprite frame texture from the cell rect and add it to the sprite array
				let rect = new PIXI.Rectangle(xPos, yPos, Math.floor(width), Math.floor(height));
				let tex = new PIXI.Texture(this._baseTexture, rect);
				tex.defaultAnchor.set(anchor.x, anchor.y);
				this._sprites.push(tex);
			}
		}
	}
	
	// ---------------------------------------------------------------------------------------------

	public GetSerializedObject(): any{

		let sprites: IRect[] = [];
		for(let i = 0; i < this._sprites.length; i++){
			sprites.push(Rect.Create(
				Vect.Create(this._sprites[i]._frame.x, this._sprites[i]._frame.y),
				Vect.Create(this._sprites[i]._frame.width, this._sprites[i]._frame.height)
			));
		}

		let r: any = {
			_texPath: this._texPath,
			_sprites: sprites
		};

		r[ISerializable.SFIELD_TYPE] = "SpriteSheet";

		return r;
	}

	public Deserialize(data: any): void {

		this.SetTexture(data._texPath);

		let sprites: IRect[] = data._sprites;
		let ths = this;
		this.DoOnLoaded(function(){
			ths._sprites = [];
			for(let i = 0; i < sprites.length; i++){
				ths._sprites.push(new PIXI.Texture(
					ths._baseTexture, 
					new PIXI.Rectangle(
						sprites[i].position.x, sprites[i].position.y, 
						sprites[i].size.x, sprites[i].size.y
				)));
			}
		});
	}
}