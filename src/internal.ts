///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/// ------------------------------------------------------------------------------------------------
/// Interfaces do not do not depend on inheritance and therefore should always be exported first

export * from "./core/iSerializable";
export * from "./core/raycastResult";
export * from "./core/iActivate";
export * from "./core/iTarget";
export * from "./core/iCollider";
export * from "./core/iCollision";
export * from "./core/iSolid";
export * from "./core/iAllied";
export * from "./core/iDisplayTintable"
export * from "./core/iSizeableDisplayObject";
export * from "./core/iControllable";

export * from "./editor/iEditorResizable";

export * from "./ui/iUiSubmittable";
export * from "./ui/iUiFocusable";

/// ------------------------------------------------------------------------------------------------
/// classes that have no inheritence dependencies (base classes)

export * from "./core/math";
export * from "./core/ray";
export * from "./core/gameEvent";
export * from "./editor/editorProperty";
export * from "./core/colliderPartitions";
export * from "./core/inputControl";
export * from "./core/allegiance";
export * from "./core/spriteSheet";
export * from "./core/sceneReference";
export * from "./core/game";

export * from "./core/statModifier";
export * from "./core/stat";
export * from "./core/stats";

export * from "./core/particle";
export * from "./core/particlePool";

/// ------------------------------------------------------------------------------------------------
/// classes that have inheritence dependencies MUST be loaded in the order that they are inherit
/// their peers. Base classes must be loaded first, and then classes that inherit from it can
/// be loaded afterwards

export * from "./core/sceneEntity";

export * from "./ui/uiElement";
export * from "./ui/uiInteractiveElement";
export * from "./ui/uiCheckboxElement";
export * from "./ui/uiTextElement";
export * from "./ui/uiTextInputField";

export * from "./core/particleSystem";
export * from "./core/particleEmitter";

export * from "./core/objectController";
export * from "./actors/playerController"

export * from "./core/aiController";
export * from "./core/aiBehavior";
export * from "./actors/enemies/enemyBehaviors";
export * from "./actors/enemies/gunnerAi";
export * from "./actors/enemies/seekerAi";

export * from "./core/scene";
export * from "./core/gameScene";
export * from "./ui/uiContainer";
export * from "./editor/levelEditor";

export * from "./core/terrain"
export * from "./core/camera"

export * from "./core/physicsEntity";
export * from "./items/weapons/projectile";

export * from "./environment/groundHazard";
export * from "./environment/jumpPad";
export * from "./environment/spikes";

export * from "./items/weapons/weapon";

export * from "./actors/actor";
export * from "./actors/enemies/enemy";
export * from "./actors/enemies/seeker";
export * from "./actors/enemies/gunner";
export * from "./actors/player";

export * from "./core/effect";
export * from "./core/animatedEffect";

export * from "./core/collider";