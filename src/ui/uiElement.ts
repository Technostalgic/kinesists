///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from "pixi.js"
import { 
	AABBCollider, Collider,
	IRect, IVect, Rect, Vect,
	SceneEntity,
	UIContainer,
	IUIFocusable, 
	Scene
} from '../internal';

/** a specialized scene entity used for creating ui panels and controls */
export class UIElement extends SceneEntity{

	public constructor(){
		super();
		this._name = "Ui Element";
	}

	/** the ui container object that this element belongs to */
	public get ParentContainer(): UIContainer { return this.parentScene as UIContainer; }
	
	private _panelGraphic: PIXI.Graphics = null;
	protected _panelCollider: AABBCollider = null;

	/** how the element calculates it's position relative to it's parent */
	public uiAnchor: IVect = Vect.Create(0, 0);

	/** how the element positions it's rect relative to it's own position */
	public uiPivot: IVect = Vect.Create(0, 0);

	/** the offset in pixels of the element from it's anchored position */
	public uiOffset: IVect = Vect.Create(0, 0);

	/** how the element calculate's it's rect's size relative to it's parent's rect size */
	public uiSize: IVect = Vect.Create(0, 0);

	/** the offset in pixels of the element's rect size from it's size calculated from uiSize */
	public uiSizeOffset: IVect = Vect.Create(0, 0);

	// ---------------------------------------------------------------------------------------------

	/** 
	 * set the element properties so that it is anchored to the center of it's parent, then return
	 * itself after properties are adjusted
	 */
	public AlignedCenter(): UIElement{
		this.uiAnchor.x = 0.5;
		this.uiAnchor.y = 0.5;
		this.uiPivot.x = 0.5;
		this.uiPivot.y = 0.5;
		return this;
	}

	/**
	 * set the element ui properties so that it's anchor and pivot are set to the specified 
	 * alignment parameters, then return itself
	 * @param x the value to set anchor and pivot x components
	 * @param y value to apply to anchor and pivot y components
	 */
	public Aligned(x: number = 0, y: number = x): UIElement{
		this.uiAnchor.x = x;
		this.uiAnchor.y = y;
		this.uiPivot.x = x;
		this.uiPivot.y = y;
		return this;
	}

	/**
	 * updates the object position to represent the appropriate ui position relative to parents
	 * @param deepUpdate whether or not all children should also recursively update their positions
	 */
	public UpdateUiPosition(deepUpdate: boolean = true): void{

		// update to appropriate position according to the ui rect
		this.GetUiPosition();

		if(!deepUpdate) return;

		// deep recursive update through children
		let entities = this.entities;
		for(let i = 0; i < entities.length; i++){
			let ent = entities[i];
			if(ent instanceof UIElement){
				ent.UpdateUiPosition(deepUpdate);
			}
		}
	}

	// ---------------------------------------------------------------------------------------------

	/** gets and updates the ui position according to all it's ancestor's rects */
	public GetUiPosition(): IVect{
		
		// get the parent rect
		let parentRect = this.GetParentUiRect();

		// calculate the screenspace coordinates from parent rect
		let tpos = Vect.Multiply(this.uiAnchor, parentRect.size);
		tpos.x += this.uiOffset.x;
		tpos.y += this.uiOffset.y;
		tpos.x += parentRect.position.x;
		tpos.y += parentRect.position.y;

		let tsiz = Vect.Multiply(this.uiSize, parentRect.size);
		tsiz.x += this.uiSizeOffset.x;
		tsiz.y += this.uiSizeOffset.y;

		tpos.x -= tsiz.x * this.uiPivot.x;
		tpos.y -= tsiz.y * this.uiPivot.y;

		// update local entity position
		this.globalPosition = tpos;
		return tpos;
	}

	/** gets the current size of the element's ui rect calculated from it's parent size */
	public GetUiSize(): IVect{
		
		// get the parent rect
		let parentRect = this.GetParentUiRect();
		
		let tsiz = Vect.Multiply(this.uiSize, parentRect.size);
		tsiz.x += this.uiSizeOffset.x;
		tsiz.y += this.uiSizeOffset.y;

		return tsiz;
	}

	/** gets the ui rect of this element's direct parent */
	public GetParentUiRect(): IRect{
		
		let parentRect = null;
		if(this.parentEntity instanceof UIContainer){
			parentRect = this.ParentContainer.uiRect;
		}
		else if(this.parentEntity instanceof UIElement){
			parentRect = this.parentEntity.GetUiRect();
		}
		else if(this.parentEntity == null){
			parentRect = Rect.Create(Vect.Create(), Vect.Create());
		}
		else{
			parentRect = Rect.Create(this.parentEntity.globalPosition, Vect.Create(0, 0));
		}

		return parentRect;
	}

	/** gets the ui rect of this element */
	public GetUiRect(): IRect{
		
		// get the parent rect
		let parentRect = this.GetParentUiRect();

		// calculate the scrrenspace coordinates from parent rect
		let tpos = Vect.Multiply(this.uiAnchor, parentRect.size);
		tpos.x += this.uiOffset.x;
		tpos.y += this.uiOffset.y;
		tpos.x += parentRect.position.x;
		tpos.y += parentRect.position.y;

		// calculate size from parent rect size
		let tsiz = Vect.Multiply(this.uiSize, parentRect.size);
		tsiz.x += this.uiSizeOffset.x;
		tsiz.y += this.uiSizeOffset.y;
		
		// calculate top left of rect based on pivot
		let topLeft = Vect.Subtract(tpos, Vect.Multiply(this.uiPivot, tsiz));
		
		// update local entity position
		tpos.x -= tsiz.x * this.uiPivot.x;
		tpos.y -= tsiz.y * this.uiPivot.y;
		this.globalPosition = tpos;

		// create the rect from the calculated coordinates
		let rect = Rect.Create(topLeft, tsiz);
		return rect;
	}

	/** @inheritdoc */
	public override addChild<T extends PIXI.DisplayObject>
	(...childs: [T, ...PIXI.DisplayObject[]]): T{
		
		let r = super.addChild(...childs);

		for(let i = childs.length - 1; i >= 0; i--){
			let child = childs[i];
			if(child instanceof UIElement){
				child.UpdateUiPosition(true);
			}
		}

		return r;
	}

	protected override OnAddedToScene(scene: Scene): void {
		
		super.OnAddedToScene(scene);

		// add self to container focusables array if necessary
		if(scene instanceof UIContainer){
			if(IUIFocusable.ImplementedIn(this)){

				// TODO fix this element getting added multiple times so this check can be removed
				let index = scene.focusables.indexOf(this);
				if(index < 0) scene.focusables.push(this);
			}
		}
	}

	protected override OnRemovedFromScene(scene: Scene): void {
		
		super.OnRemovedFromScene(scene);

		// remove self from container focusable array if necessary
		if(IUIFocusable.ImplementedIn(this)){
			let index = this.ParentContainer.focusables.indexOf(this);
			this.ParentContainer.focusables.splice(index, 1);
		}
	}

	// ---------------------------------------------------------------------------------------------

	/**
	 * expands the sizeOffset of the UI element to fit all the UI element children inside it
	 * @param paddingX how much horizontal padding between children and edge of element
	 * @param paddingY how much vertical padding between children and edge of element 
	 */
	public ExpandToFitChildren(paddingX: number = 0, paddingY: number = paddingX): void{
		
		let oRect = this.GetUiRect();
		let tRect = Rect.Clone(oRect);

		let ents = this.entities;
		for(let i = ents.length - 1; i >= 0; i--){

			let ent = ents[i];
			if(ent instanceof UIElement){

				let childRect = ent.GetUiRect();
				Rect.ExpandToOverlap(tRect, childRect);
			}
		}

		let difX = tRect.size.x - oRect.size.x;
		let difY = tRect.size.y - oRect.size.y;
		if(oRect.position.x > tRect.position.x) difX += paddingX;
		if(oRect.position.x + oRect.size.x < tRect.position.x + tRect.size.x) difX += paddingX;
		if(oRect.position.y > tRect.position.y) difY += paddingY;
		if(oRect.position.y + oRect.size.y < tRect.position.y + tRect.size.y) difY += paddingY;
		
		this.uiSizeOffset.x += difX;
		this.uiSizeOffset.y += difY;
	}
	
	/**
	 * creates a basic panel graphic that represents the element's ui rect and adds it to the 
	 * scene hierarchy
	 */
	public UpdatePanelGraphic(): PIXI.Graphics{

		// create or get the graphics object
		let graphics = null;
		if(this._panelGraphic != null){
			graphics = this._panelGraphic;
			graphics.clear();
		}
		else{
			graphics = new PIXI.Graphics();
			this.addChildAt(graphics, 0);
			this._panelGraphic = graphics;
		}

		let uiRect = this.GetUiRect();
		let tpos = Vect.Subtract(uiRect.position, this.globalPosition);
		
		graphics.lineStyle({
			color: 0xFFFFFF,
			width: 1
		});
		graphics.beginFill(0x000000, 0.5);
		graphics.drawRect(tpos.x, tpos.y, uiRect.size.x - 1, uiRect.size.y - 1);
		graphics.endFill();

		return graphics;
	}

	/**
	 * creates an AABBCollider that matches the element's ui rect and adds it to the scene hierarchy
	 * and collider partitions
	 * @returns 
	 */
	public UpdatePanelCollider(): Collider{

		let collider = null;
		if(this._panelCollider == null){
			collider = new AABBCollider();
			this.addChild(collider);
		}
		else{
			collider = this._panelCollider;
		}
		
		collider.SetRect(this.GetUiRect());
		return collider;
	}
}