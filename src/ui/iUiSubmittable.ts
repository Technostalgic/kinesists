///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { 
	GameEvent,
	UIElement
} from '../internal';

/** interface for a UI element that can be submitted */
export interface IUISubmittable extends UIElement {

	/** the event object for the element's submit action, invoked when the element is submitted */
	get submitAction(): GameEvent<number>;

	/** invokes the element's submit event actions */
	Submit(action: number): void;
}

export namespace IUISubmittable {

	/** Type checking discriminator for IUISelectable */
	export function ImplementedIn(obj: UIElement): obj is IUISubmittable{
		let r = obj as IUISubmittable;
		return (
			r.submitAction !== undefined &&
			r.Submit !== undefined
		);
	}
}