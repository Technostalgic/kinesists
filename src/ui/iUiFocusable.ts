///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { 
	GameEvent,
	UIElement
} from '../internal';

/** interface that declares functionality for a ui element that can have focus */
export interface IUIFocusable extends UIElement{

	get isFocused(): boolean;

	SetFocus(focus: boolean): void;
}

export namespace IUIFocusable{
	
	/** Type checking discriminator for IUISelectable */
	export function ImplementedIn(obj: UIElement): obj is IUIFocusable{
		let r = obj as IUIFocusable;
		return (
			r.isFocused !== undefined &&
			r.SetFocus !== undefined
		);
	}
}