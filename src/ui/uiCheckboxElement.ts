///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { Vect, IVect, GameEvent, UIElement, IUISubmittable  } from '../internal';

export class UICheckboxElement extends UIElement implements IUISubmittable {
	
	constructor(value: boolean) {
		super();

		const gfxSize = UICheckboxElement.TICK_SIZE;
		this.uiSizeOffset = Vect.Create(gfxSize + 8);

		this._checkGraphic = new PIXI.Graphics();
		this._checkGraphic.beginFill(0xFFFFFF, 1);
		this._checkGraphic.drawRect(0, 0, gfxSize, gfxSize);
		this._checkGraphic.endFill();
		this._checkGraphicContainer = new UIElement();
		this._checkGraphicContainer.addChild(this._checkGraphic);
		this._checkGraphicContainer.uiOffset = Vect.Create(-1, 0);
		this._checkGraphicContainer.uiAnchor = Vect.Create(0.5, 0.5);
		this._checkGraphicContainer.uiSizeOffset = Vect.Create(gfxSize, gfxSize);
		this._checkGraphicContainer.uiPivot = Vect.Create(0.5, 0.5);
		this.addChild(this._checkGraphicContainer);

		this._checkGraphicContainer.visible = value;
		this._value = value;
	}

	public static readonly TICK_SIZE = 7;

	// ---------------------------------------------------------------------------------------------
	private _checkGraphic: PIXI.Graphics = null;
	private _checkGraphicContainer: UIElement = null;
	private _value: boolean = false;

	public submitAction: GameEvent<number> = new GameEvent<number>();

	public get value() { 
		return this._value;
	}
	public set value(val: boolean) {
		this._value = val;
		this._checkGraphicContainer.visible = val;
	}

	// ---------------------------------------------------------------------------------------------
	public Submit(action: number): void {
		this.value = !this.value;
		this.submitAction.Invoke(action);
	}
}