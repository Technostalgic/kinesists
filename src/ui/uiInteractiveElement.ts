///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { 
	GameEvent,
	UIElement, IUISubmittable
} from '../internal';

/** a ui basic ui element which implements the subittable interface */
export class UIInteractiveElement extends UIElement implements IUISubmittable{

	public constructor(){
		super();
		this._name = "Ui Interactive Element";
	}

	/** @inheritdoc */
	public get submitAction() { return this._selectAction; }
	private _selectAction: GameEvent<number> = new GameEvent<number>();

	/** @inheritdoc */
	public Submit(action: number){
		this.submitAction.Invoke(action);
	}
}