///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from "pixi.js"
import { 
	IVect, Vect,
	SceneEntity,
	UIElement, TextInputMode, UITextInputField
} from '../internal';

/** a ui element that is used to display text */
export class UITextElement extends UIElement{

	/** create from pixi text object */
	protected constructor(text: PIXI.Text){
		super();

		this.addChild(text);
		this._text = text;
		this.MatchSizeOffsetToText(0, 0);
		this._name = "Ui Text '" + text.text + "'";
	}

	/** default style for the in game text */
	public static readonly DEFAULT_STYLE: Partial<PIXI.TextStyle> = {
		fontFamily: "Consolas",
		fontSize: 12,
		fill: 0xFFFFFF,
		stroke: 0x000000,
		strokeThickness: 1
	}
	
	/**
	 * create a text element from the specified string
	 * @param text the string that the text element will contain
	 * @param style the style to format the text by
	 */
	public static CreateFromString(
		text: string, 
		style: Partial<PIXI.TextStyle> = this.DEFAULT_STYLE
	): UITextElement{
		
		let txt = new PIXI.Text(text, style);
		return new UITextElement(txt);
	}

	// ---------------------------------------------------------------------------------------------

	private _text: PIXI.Text;

	/** how the internal text object is anchored relative to this element's rect */
	public textAnchor: IVect = Vect.Create(0, 0);

	/** getter for the internal PIXI text object which is used to render the text string*/
	public get text() { return this._text; }

	// ---------------------------------------------------------------------------------------------

	/** @inheritdoc */
	public override UpdateUiPosition(deepUpdate: boolean = true): void {
		super.UpdateUiPosition(deepUpdate);
		this.UpdateTextPosition();
	}

	/** @inheritdoc */
	public override AlignedCenter(): UIElement {
		super.AlignedCenter();
		this.textAnchor.x = 0.5;
		this.textAnchor.y = 0.5;
		return this;
	}

	/** update the internal text object's position according to textAnchor */
	public UpdateTextPosition(){

		// get some data about the text and ui space for calculations
		let rect = this.GetUiRect();
		let textSize = Vect.Create(this._text.width, this._text.height);

		// calculate the offset from the top left of the ui rect, based on the text achor
		let toff = Vect.Create();
		toff.x = this.textAnchor.x * (rect.size.x - textSize.x);
		toff.y = this.textAnchor.y * (rect.size.y - textSize.y);

		// apply the position
		SceneEntity.SetGlobalPosition(this._text,
			Vect.Create(
				rect.position.x + toff.x, 
				rect.position.y + toff.y
		));
	}

	// ---------------------------------------------------------------------------------------------

	/**
	 * Set the size offset of the ui element so it matches the width and height of the text
	 * @param paddingX the amount of space between the text and the element's leftright edges
	 * @param paddingY the amount of space between the text and the element's top/bottom edges
	 */
	public MatchSizeOffsetToText(paddingX: number, paddingY: number	){
		this.uiSizeOffset = Vect.Create(
			this._text.width + paddingX, this._text.height + paddingY
		);
	}

	/**
	 * sets the element to render the specified string, and updates the text position
	 * @param text 
	 */
	public SetTextString(text: string){
		this._text.text = text;
		this.UpdateTextPosition();
	}

	/**
	 * convert the static text element to a text input field that the user can modify the text of
	 * @param inputMode what text is allowed in the field
	 */
	public ToInputField(inputMode: TextInputMode = TextInputMode.allowEverything): 
	UITextInputField{
		
		this.removeChild(this._text);

		let r = new UITextInputField(this._text);
		r._name = this._name;
		r.uiAnchor = this.uiAnchor;
		r.uiOffset = this.uiOffset;
		r.uiSize = this.uiSize;
		r.uiSizeOffset = this.uiSizeOffset;
		r.uiPivot = this.uiPivot;
		r.inputMode = inputMode;

		return r;
	}
}