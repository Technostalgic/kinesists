///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js'
import { 
	AABBCollider, Collider,
	Game,
	SelectionModifier,
	IRect, IVect, Rect, Vect,
	Scene,
	UIElement, IUIFocusable
} from '../internal';

export class UIContainer extends Scene{

	constructor(game: Game){ 
		super(game); 

		this._uiRect = Rect.Create(Vect.Create(), Vect.Create(
			game.renderer.view.width, 
			game.renderer.view.height
		));
	}
	
	private _uiRect: IRect;
	public get uiRect(){ return this._uiRect; }

	public uiModifier: SelectionModifier
	public uiClipboard: string = null;
	public focusables: Array<IUIFocusable> = new Array<IUIFocusable>();

	public get parentRootScene(): Scene { return this._parentScene; }

	// ---------------------------------------------------------------------------------------------
	public override addChild<T extends PIXI.DisplayObject>
	(...childs: [T, ...PIXI.DisplayObject[]]): T{
		let r = super.addChild(...childs);

		// iterate through each child added
		for(let i = 0; i < childs.length; i++){

			// if the child is a ui element
			let child = childs[i];
			if(child instanceof UIElement){

				// if it also implementents IUIFocusable, add it to the focusable array
				if(IUIFocusable.ImplementedIn(child)){
					this.focusables.push(child);
				}
			}
		}

		return r;
	}

	// ---------------------------------------------------------------------------------------------
	public DrawUI(renderer: PIXI.Renderer){
		renderer.render(this, {
			clear: false
		});
	}
	
	// ---------------------------------------------------------------------------------------------
	public GetUIElementsAtPoint(point: IVect): Array<UIElement>{

		let col = new AABBCollider();
		col.SetRect(Rect.Create(point, Vect.Create(1, 1)))

		return this.GetUIElementsOnCollider(col);
	}

	public GetUIElementsOnCollider(collider: Collider): Array<UIElement>{
		
		let r = new Array<UIElement>();
		let cols = this.colliders.FindOverlapColliders(collider);

		for(let i = 0; i < cols.length; i++){
			if(cols[i].parentEntity instanceof UIElement){
				r.push(cols[i].parentEntity as UIElement);
			}
		}

		return r;
	}

	public GetFocusedElements(): Array<IUIFocusable>{
		let r = new Array<IUIFocusable>();

		// iterate through each focusable element
		for(let i = this.focusables.length - 1; i >= 0; i--){

			// if it has been removed from the scene, remove it from the focusable array and skip 
			// the rest of the logic for this iteration
			if(this.focusables[i].removed){
				this.focusables[i].SetFocus(false);
				this.focusables.splice(i, 1);
				continue;
			}
			
			// if the element is focused, add it to the return array
			if(this.focusables[i].isFocused){
				r.push(this.focusables[i]);
			}
		}

		return r;
	}
}