///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from "pixi.js"
import { 
	GameEvent,
	SceneEntity,
	IUISubmittable, UITextElement, IUIFocusable,
	SelectionModifier
} from '../internal';

/** specifies what text is valid in a text input field */
export enum TextInputMode{
	allowEverything = 0,
	alphaNumerals = 1,
	alphabetOnly = 2,
	decimal = 3,
	integer = 4
}

/** 
 * a ui element that is used as a text input field, allowing the user to modify the text inside of
 * this element that's displayed on screen
 */
export class UITextInputField extends UITextElement implements IUISubmittable, IUIFocusable{

	public constructor(text: PIXI.Text){ 
		super(text); 
		
		this.CreateCursor();
	}

	/** used for enforcing text input mode */
	private static readonly NUMERALCHARS = "01234567890";
	private static readonly ALPHABETCHARS = "abcdefghijklmnopqrstuvwxyz";

	// ---------------------------------------------------------------------------------------------

	private _prevText: string = "";
	private _focused: boolean = false;
	private _cursor: PIXI.Graphics = null;

	/** whether or not the text input field will allow the user to enter an empty string */
	public allowEmpty: boolean = false;

	/** restrict input of the user based on this character set */
	public inputMode: TextInputMode = TextInputMode.allowEverything;

	private _keyEventListener:(e:KeyboardEvent) => void = null;

	/** if the user is currently editing the input field */
	public get isFocused() { return this._focused; }

	/** @inheritdoc */
	public get submitAction() { return this._selectAction; }
	private _selectAction: GameEvent<number> = new GameEvent<number>();

	// ---------------------------------------------------------------------------------------------

	/** @inheritdoc */
	public override Update(deltaTime: number): void {
		super.Update(deltaTime);
		
		if(this._focused) this._cursor.visible = performance.now() % 1000 < 500;
	}

	// Cursor: -------------------------------------------------------------------------------------

	private CreateCursor(): void{

		this._cursor = new PIXI.Graphics();
		let cursorSize = (this.text.style.fontSize as number)?? 12;

		this._cursor.lineStyle({
			color: 0xFFFFFF,
			width: 1
		});
		this._cursor.moveTo(0, cursorSize / -2);
		this._cursor.lineTo(0, cursorSize / 2);
		this._cursor.visible = false;
		this.addChild(this._cursor);

		this.PositionCursor();
	}

	private PositionCursor(): void{

		let pos = SceneEntity.GetGlobalPosition(this.text);
		pos.y += this.text.height * 0.5;

		if(this.text.text.length > 0) pos.x += this.text.width + 2;
		else pos.x += 3;

		SceneEntity.SetGlobalPosition(this._cursor, pos);
	}

	// User Control: -------------------------------------------------------------------------------

	/** @inheritdoc */
	public Submit(action: number): void {
		
		// if the element is in and has already been edited, the result can be submitted
		if(this._focused){
			if(!this.allowEmpty){
				if(this.text.text.length <= 0){
					this.text.text = this._prevText;
				}
			}
			this.submitAction.Invoke(action);
			this.OffFocus();
		}
		
		// if the element has not gotten focus yet, allow the user to edit it before submitting
		else{
			this.OnFocus();
			if(action == 2){
				this.text.text = "";
				this.PositionCursor();
			}
		}
	}

	/**
	 * set the focus state of the element
	 * @param focus whether or not the element should be in focus
	 */
	public SetFocus(focus: boolean): void {
		
		if(focus === this._focused)
			return;

		if(focus) this.OnFocus();
		else {
			this.submitAction.Invoke(0);
			this.OffFocus();
		}
	}

	private OnFocus(): void{

		// de-focus any other elements that are currently focused if necessary
		if(this.ParentContainer.uiModifier == SelectionModifier.neutral){
			let others = this.ParentContainer.GetFocusedElements();
			for(let i = others.length - 1; i >= 0; i--){
				others[i].SetFocus(false);
			}
		}

		this._prevText = this.text.text;
		this._focused = true;

		this.PositionCursor();
		this._cursor.visible = true;

		// add keyboard event listener
		let ths = this;
		let listener = function(e: KeyboardEvent){
			ths.OnKeyDown(e);
		};
		window.addEventListener('keydown', listener);
		this._keyEventListener = listener;
	}

	private OffFocus(): void{

		this._focused = false;
		this._cursor.visible = false;

		// remove keyboard event listener
		if(this._keyEventListener != null){
			window.removeEventListener('keydown', this._keyEventListener);
			this._keyEventListener = null;
		}
	}

	private OnKeyDown(e:KeyboardEvent){

		// get the currently displayed text
		let txt = this.text.text;

		// if it's a keystroke with a typable character
		if(e.key.length == 1){
			let char = e.key.toLowerCase();

			// if ctrl is pressed
			if(e.ctrlKey){

				// copy
				if(char == 'c' && e.ctrlKey){
					this.ParentContainer.uiClipboard = txt;
				}

				// paste
				else if(char == 'v' && e.ctrlKey){
					if(this.ParentContainer.uiClipboard !== null){
						txt = this.ParentContainer.uiClipboard;
					}
				}
			}

			// ctrl is not pressed
			else switch(this.inputMode){
				case TextInputMode.allowEverything: txt += e.key; break;
				case TextInputMode.alphaNumerals: 
					if(
						UITextInputField.ALPHABETCHARS.includes(char) || 
						UITextInputField.NUMERALCHARS.includes(char)
					)
						txt += e.key;
					break;
				case TextInputMode.alphabetOnly:
					if(UITextInputField.ALPHABETCHARS.includes(char))
						txt += e.key;
					break;
				case TextInputMode.decimal:
					if(UITextInputField.NUMERALCHARS.includes(char))
						txt += e.key;
					if(char === '.'){
						if(!txt.includes('.'))
							txt += char;
					}
					break;
				case TextInputMode.integer:
					if(UITextInputField.NUMERALCHARS.includes(char))
						txt += e.key;
					break;
			}
		}

		// if it's a functional keystroke
		else{
			switch(e.key.toLowerCase()){
				case "backspace":
					if(e.ctrlKey){
						txt = "";
					}
					txt = txt.substring(0, txt.length - 1);
					break;
				case "enter":
					this.Submit(0);
					break;
				case "escape":
					txt = this._prevText;
					this.text.text = txt;
					this.Submit(0);
					break;
			}
		}

		// apply the text to be displayed and position the cursor
		this.text.text = txt;
		this.PositionCursor();
	}
}