///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { 
	Effect,
	Game,
	GameScene,
	ISerializable,
	LevelEditor 
} from './internal';

/**
 * entry point of the program
 */
function initialize(): void {

	document.body.style.textAlign = "center";
	document.body.style.backgroundColor = "#000";
	document.body.style.color = "#CCC";
	document.body.style.padding = "0px";
	document.body.style.margin = "0px";

	Effect.Load();

	let game = new Game(800, 600);
	(window as any).game = game;
	(window as any).ISerializable = ISerializable;

	let lvlEdit = new LevelEditor(game);
	lvlEdit.EditGameSene(GameScene.BasicScene(game));
	game.currentScene = lvlEdit;
	// game.currentScene = GameScene.BasicScene(game);

	document.body.appendChild(game.renderer.view);
	game.renderer.backgroundColor = 0x202020;
	
	game.AttachKeyboardEvents();
	game.AttachMouseEvents();
	game.StartGameLoop();
}

window.addEventListener('load', initialize);