///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from "pixi.js";
import { parseIsolatedEntityName } from "typescript";
import { 
	IVect, Vect, SceneEntity, TextInputMode,
	UIElement, UITextElement, UIInteractiveElement,
	IUISubmittable, UITextInputField, UICheckboxElement, LevelEditor, Scene
} from '../internal';

/** the type that the object property is */
export enum PropertyType{
	bool,
	string,
	decimal,
	integer,
	vector2,
	keyValuePair,
	entityReference
}

/** used in level editor for changing properties of an object in a level */
export class EditorProperty<TTarget, TProp>{

	protected constructor(){ }

	public static Create<TTarget, TProp>(
		name: string,
		type: PropertyType,
		getter: (target: TTarget) => TProp, 
		setter: (target: TTarget, value: TProp) => void
	){

		let r = new EditorProperty<TTarget, TProp>();

		r._name = name;
		r._propertyType = type;
		r._funcGet = getter;
		r._funcSet = setter;

		return r;
	}

	/**
	 * create a vector property for the specified type
	 * @param field the name of the field in the type that contains the vector object
	 * @param name the name to call the property
	 */
	public static CreateVectProp<TTarget>(field: string, name: string){
		return this.Create<TTarget, IVect>(
			name, PropertyType.vector2,
			function(target: TTarget){ return (target as any)[field] as IVect; },
			function(target: TTarget, value: IVect){ (target as any)[field] = value; }
		);
	}

	/**
	 * create a string property for the specified type
	 * @param field the name of the field in the type that contains the string value
	 * @param name the name to call the property
	 */
	public static CreateStringProp<TTarget>(
		field: string, 
		name: string, 
		nullIfEmpty: boolean = false): 
	EditorProperty<TTarget, string>{

		let setFunc = nullIfEmpty ?
			function(target: TTarget, value: string) { 
				let v = value;
				if(v.length <= 0) v = null;
				(target as any)[field] = value; 
			}:
			function(target: TTarget, value: string) { 
				let v = value;
				(target as any)[field] = value; 
			};

		return this.Create<TTarget, string>(
			name, PropertyType.string,
			function(target: TTarget) { return (target as any)[field] as string },
			setFunc
		);
	}

	public static CreateNumberProp<TTarget>(field: string, name: string){
		return this.Create<TTarget, number>(
			name, PropertyType.decimal,
			function(target: TTarget) { return (target as any)[field] as number },
			function(target: TTarget, value: number) { (target as any)[field] = value; }
		);
	}

	public static CreateBoolProp<TTarget>(field: string, name: string){
		return this.Create<TTarget, boolean>(
			name, PropertyType.bool,
			function(target: TTarget) { return (target as any)[field] as boolean },
			function(target: TTarget, value: boolean) { (target as any)[field] = value; }
		);
	}

	public static CreateEntityProp<TTarget>(field: string, name: string){
		return this.Create<TTarget, SceneEntity>(
			name, PropertyType.entityReference,
			function(target: TTarget) { return (target as any)[field] as SceneEntity},
			function(target: TTarget, value: SceneEntity) {  (target as any)[field] = value; }
		);
	}

	public static CreateKVProp<TTarget>(
		field: string, 
		name: string, 
		pairs: Array<{key: string, value: any}>):
	EditorKVProperty<TTarget>{

		let r = EditorKVProperty.Create<TTarget>(
			name, PropertyType.keyValuePair,
			function(target: TTarget) { return (target as any)[field] },
			function(target: TTarget, value: any) {
				(target as any)[field] = this.pairs[this.index].value;
			}
		);
		r.pairs = pairs;

		return r;
	}

	/// --------------------------------------------------------------------------------------------

	protected _name: string = "";
	protected _funcGet: (target: TTarget) => TProp = null;
	protected _funcSet: (target: TTarget, value: TProp) => void = null;
	protected _propertyType: PropertyType;

	public get name() { return this._name; }
	public get propertyType() { return this._propertyType; }
	

	/// --------------------------------------------------------------------------------------------

	public GetValue(target: TTarget): TProp{
		return this._funcGet(target);
	}

	public SetValue(target:TTarget, value: TProp): void{
		this._funcSet(target, value);
	}

	/// --------------------------------------------------------------------------------------------

	public GetUIElement(instance: TTarget): UIElement{
		
		let ths = this;
		let r = new UIElement();
		r.uiSize.x = 1;
		
		let title = UITextElement.CreateFromString(this._name);
		r.addChild(title);
		let titleHeight = title.GetUiRect().size.y;
		r.uiSizeOffset.y += titleHeight;
		
		let propValue = this.GetValue(instance);
		let valElement: IUISubmittable = null;

		switch(this.propertyType){
			case PropertyType.string: 
				valElement = EditorProperty.GetUIStringField(propValue as any);
				valElement.submitAction.AddListener(function(action: number){
					let str = 
						valElement.GetChildOfType<UITextInputField>(UITextInputField).text.text;
					ths.SetValue(instance, str as any);
				});
				break;
			case PropertyType.decimal: 
				valElement = EditorProperty.GetUINumberField(propValue as any);
				valElement.submitAction.AddListener(function(action: number){
					let str = 
						valElement.GetChildOfType<UITextInputField>(UITextInputField).text.text;
					ths.SetValue(instance, Number.parseFloat(str) as any);
				});
				break;
			case PropertyType.integer: 
				valElement = EditorProperty.GetUINumberField(propValue as any);
				valElement.submitAction.AddListener(function(action: number){
					let str = 
						valElement.GetChildOfType<UITextInputField>(UITextInputField).text.text;
					ths.SetValue(instance, Number.parseFloat(str) as any);
				});
				break;
			case PropertyType.bool: 
				valElement = EditorProperty.GetUIBoolField(propValue as any);
				valElement.submitAction.AddListener(function(action: number){
					let val = valElement.GetChildOfType<UICheckboxElement>(UICheckboxElement).value;
					ths.SetValue(instance, val as any);
				});
				break;
			case PropertyType.vector2: 
				valElement = EditorProperty.GetUIVectorField(propValue as any);
				valElement.submitAction.AddListener(function(action:number){
					let xstr = (valElement.entities[0] as UITextInputField).text.text;
					let ystr = (valElement.entities[1] as UITextInputField).text.text;
					ths.SetValue(
						instance, 
						Vect.Create(
							Number.parseFloat(xstr), 
							Number.parseFloat(ystr)
						) as any
					);
				});
				break;
			case PropertyType.entityReference: 
				valElement = EditorProperty.GetUIEntityField<TTarget>(
					this as any,
					instance,
					propValue as any
				);
				break;
		}

		if(valElement != null){
			valElement.uiOffset.y += titleHeight;
			r.addChild(valElement);
			r.uiSizeOffset.y += valElement.GetUiRect().size.y + 5;
		}

		return r;
	}

	/// --------------------------------------------------------------------------------------------

	public static GetUIVectorField(value: IVect): IUISubmittable{

		if(value == null) value = Vect.Create();

		let xElem = 
			UITextElement.CreateFromString(
				value.x.toString()
			).ToInputField(TextInputMode.decimal); 
		xElem.name = "X Input";
		xElem.uiSizeOffset = Vect.Create(50, 15);
		xElem.UpdatePanelGraphic();
		xElem.UpdatePanelCollider();

		let yElem = 
			UITextElement.CreateFromString(
				value.y.toString()
			).ToInputField(TextInputMode.decimal);
		yElem.name = "Y Input";
		yElem.uiOffset.x = 65;
		yElem.uiSizeOffset = Vect.Create(50, 15);
		yElem.UpdatePanelGraphic();
		yElem.UpdatePanelCollider();

		let valElement = new UIInteractiveElement();
		valElement.uiOffset.x = 15;
		valElement.uiSizeOffset.x = -15
		valElement.uiSize.x = 1;
		valElement.addChild(xElem, yElem);

		let func_submit = function(action: number){
			valElement.Submit(action);
		}

		xElem.submitAction.AddListener(func_submit);
		yElem.submitAction.AddListener(func_submit);

		valElement.ExpandToFitChildren();

		return valElement;
	}

	public static GetUIStringField(value: string): IUISubmittable{

		if(value == null) value = "";

		let nameInput = 
			UITextElement.CreateFromString(value).ToInputField(TextInputMode.allowEverything); 
		nameInput.name = "Name Input";
		nameInput.uiSizeOffset = Vect.Create(100, 15);
		nameInput.UpdatePanelGraphic();
		nameInput.UpdatePanelCollider();

		let valElement = new UIInteractiveElement();
		valElement.uiOffset.x = 15;
		valElement.uiSizeOffset.x = -15
		valElement.uiSize.x = 1;
		valElement.addChild(nameInput);

		let func_submit = function(action: number){
			valElement.Submit(action);
		}

		nameInput.submitAction.AddListener(func_submit);

		valElement.ExpandToFitChildren();

		return valElement;
	}

	public static GetUINumberField(value: number, onlyInteger: boolean = false): IUISubmittable{

		if(value == null) value = 0;

		let numInput = 
			UITextElement.CreateFromString(value.toString()).ToInputField(TextInputMode.integer);
		numInput.name = "Number Input";
		numInput.uiSizeOffset = Vect.Create(100, 15);
		numInput.UpdatePanelGraphic();
		numInput.UpdatePanelCollider();

		let valElement = new UIInteractiveElement();
		valElement.uiOffset.x = 15;
		valElement.uiSizeOffset.x = -15
		valElement.uiSize.x = 1;
		valElement.addChild(numInput);
		
		let func_submit = function(action: number){
			valElement.Submit(action);
		}

		numInput.submitAction.AddListener(func_submit);

		valElement.ExpandToFitChildren();
		
		return valElement;
	}

	public static GetUIBoolField(value: boolean): IUISubmittable{

		if(value == null) value = false;

		let boolInput = new UICheckboxElement(value);
		boolInput.name = "Bool Input";

		let valElement = new UIInteractiveElement();
		valElement.uiOffset.x = 15;
		valElement.addChild(boolInput);
		valElement.ExpandToFitChildren();

		boolInput.UpdatePanelGraphic();
		boolInput.UpdatePanelCollider();

		let func_submit = function(action: number){
			valElement.Submit(action);
		}

		boolInput.submitAction.AddListener(func_submit);

		return valElement;
	}

	public static GetUIEntityField<TTarget>(
		prop: EditorProperty<TTarget, SceneEntity>, 
		target: TTarget,
		value: SceneEntity): 
	IUISubmittable{
		
		let buttonInput = new UIInteractiveElement();
		buttonInput.uiOffset.x = 15;
		buttonInput.uiSizeOffset = Vect.Create(100, 15);
		buttonInput.UpdatePanelGraphic();
		buttonInput.UpdatePanelCollider();

		let txtElem = UITextElement.CreateFromString(value?.name ?? "null");

		buttonInput.addChild(txtElem);
		buttonInput.ExpandToFitChildren();

		let func_submit = function(){
			
			txtElem.text.text = "<select entity>";
			let editor = buttonInput.ParentContainer.parentRootScene;
			if(!(editor instanceof LevelEditor)) {
				throw "property can only be modified within an editor scene";
			}
			
			editor.StartEntitySelection(prop, target, txtElem);
		};

		buttonInput.submitAction.AddListener(func_submit);

		return buttonInput;
	}
}

class EditorKVProperty<TTarget> extends EditorProperty<TTarget, any>{
	private constructor() { super(); }

	public static Create<TTarget>(
		name: string,
		type: PropertyType,
		getter: (target: TTarget) => any, 
		setter: (target: TTarget, value: any) => void
	){
		let r = new EditorKVProperty<TTarget>();

		r._name = name;
		r._propertyType = PropertyType.keyValuePair;
		r._funcGet = getter;
		r._funcSet = setter;

		return r;
	}

	private _index: number = 0;
	public pairs: Array<{key: string, value: any}> = new Array<{key: string, value: any}>();

	public get index() { return this._index; }

	public CycleIndexForward(): void{
		if(++this._index >= this.pairs.length) this._index = 0;
	}

	public CycleIndexBack(): void{
		if(--this._index < 0) this._index = this.pairs.length - 1;
	}

	public GetValueString(instance: TTarget): string{
		
		let val = this.GetValue(instance);
		let propString = val.toString();
		for(let i = this.pairs.length - 1; i >= 0; i--){
			if(this.pairs[i].value == val){
				propString = this.pairs[i].key;
				break;
			}
		}

		return propString;
	}

	public override GetUIElement(instance: TTarget): UIElement{
		
		let ths = this;
		let r = super.GetUIElement(instance);
		let oHeight = r.GetUiRect().size.y;
		
		let longestKeyStr = "";
		for(let i = this.pairs.length - 1; i >= 0; i--){
			if(this.pairs[i].key.length > longestKeyStr.length)
				longestKeyStr = this.pairs[i].key;
		}

		let propString = this.GetValueString(instance);
		let displayText = UITextElement.CreateFromString(longestKeyStr);
		displayText.AlignedCenter();
		displayText.text.text = propString;
		
		let cycleInput = new UIInteractiveElement();
		cycleInput.addChild(displayText);
		cycleInput.ExpandToFitChildren();
		cycleInput.name = "KV Cycle Input";

		let valElement = new UIInteractiveElement();
		valElement.uiOffset.x = 15;
		valElement.addChild(cycleInput);
		valElement.ExpandToFitChildren();

		cycleInput.UpdatePanelGraphic();
		cycleInput.UpdatePanelCollider();

		let func_submit = function(action: number){
			if(action == 0){
				ths.CycleIndexForward();
			}
			else{
				ths.CycleIndexBack();
			}

			let val = ths.pairs[ths.index].value;
			ths.SetValue(instance, val);
			displayText.text.text = ths.GetValueString(instance);

			valElement.Submit(action);
		}

		cycleInput.submitAction.AddListener(func_submit);

		if(valElement != null){
			valElement.uiOffset.y += oHeight;
			r.addChild(valElement);
			r.uiSizeOffset.y += valElement.GetUiRect().size.y + 5;
		}

		return r;
	}
}