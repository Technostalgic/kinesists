///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { 
	IVect, IRect,
	SceneEntity
} from '../internal';

/** implement into scene enetity objects that should be able to be resized in the level editor */
export interface IEditorResizable extends SceneEntity{
	ClosestPoint(point: IVect): IVect;
	GetResizeRect(): IRect;
	SetResizeRect(rect: IRect): void;
}

export namespace IEditorResizable{

	/** type checking and discriminator for IEditorResizable */
	export function ImplementedIn(obj: SceneEntity): obj is IEditorResizable{
		let r = obj as IEditorResizable;
		return (
			r.ClosestPoint !== undefined &&
			r.GetResizeRect !== undefined &&
			r.SetResizeRect !== undefined
		);
	}
}