///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	Game,
	SceneEntity,
	Scene, GameScene,
	AABBCollider, Collider, CollisionLayer, ICollidable,
	EditorProperty, IEditorResizable,
	InputCondition, InputControl, InputType,
	IRect, IVect, Rect, Side, Vect, Matrix,
	Terrain, Spikes, JumpPad,
	Player, Seeker, TestDummy,
	UIContainer,
	UIElement, UIInteractiveElement, UITextElement, IUISubmittable, Gunner, ISerializable 
} from '../internal';

export enum SelectionModifier{
	neutral = 0,
	positive = 1,
	negative = -1
}

enum EditMode{
	none = 0,
	select = 1,
	terrain = 2,
	entities = 3
}

namespace EditMode{
	export function ToString(mode: EditMode){
		switch(mode){
			case EditMode.select: return "selection";
			case EditMode.terrain: return "terrain";
			case EditMode.entities: return "entities";
		}
		return "none";
	}
}

export class LevelEditor extends Scene {

	private static readonly STORAGEKEY_BASE = "kinesisTS_lvlEdit_";
	private static readonly STORAGEKEY_RECENTLEVEL = LevelEditor.STORAGEKEY_BASE + "Current";

	constructor(game: Game){
		super(game); 

		this._testScene = new GameScene(game);
		this._editorRoot = new SceneEntity();
		this._objectRoot = new Scene(game);

		this.InitializeUI();
		this.InitializeInputControls();

		this._editorRoot.addChild(this._dragGraphic);
		this._editorRoot.addChild(this._selectedObjectsGraphic);
	}

	//----------------------------------------------------------------------------------------------

	private _isTesting: boolean = false;
	private _testScene: GameScene;
	private _editorRoot: SceneEntity;
	private _objectRoot: SceneEntity;
	
	private _uiRoot: UIContainer;
	private _uiPanelRight: UIElement;
	private _uiPanelBottom: UIElement;
	private _uiSelectedObjectsRoot: UIElement;
	private _uiContextMenu: UIElement = null;
	private _uiObjectPropertyWindow: UIElement = null;
	private _uiEditModeButton: UIInteractiveElement = null;
	private _selectEntityProp: EditorProperty<any, SceneEntity> = null;
	private _selectEntityPropText: UITextElement = null;
	private _selectEntityPropTarget: any = null;

	private _dragGraphic: PIXI.Graphics = new PIXI.Graphics();
	private _selectedObjectsGraphic: PIXI.Graphics = new PIXI.Graphics();
	private _emphasizedSelectionEntity: SceneEntity = null;
	private _selectedObjects: Array<SceneEntity> = new Array<SceneEntity>();
	private _clipboardEntities: Array<SceneEntity> = new Array<SceneEntity>();
	
	private _selectedCreationObject: SceneEntity = new Seeker();
	private _entityCreationPool: Array<SceneEntity> = [
		new Player(),
		new Seeker(),
		new Gunner(),
		new TestDummy(),
		new Spikes(),
		new JumpPad()
	];

	private _control_testLevel: InputControl;
	private _control_mousePrimary: InputControl;
	private _control_mouseSecondary: InputControl;
	private _control_toggleUI: InputControl;
	private _control_delete: InputControl;
	private _control_selectMode: InputControl;
	private _control_terrainMode: InputControl;
	private _control_entityMode: InputControl;
	private _control_quickSave: InputControl;
	private _control_quickLoad: InputControl;
	private _control_selectionModPositive: InputControl;
	private _control_selectionModNegative: InputControl;
	private _control_copy: InputControl;
	private _control_paste: InputControl;

	private _editMode: EditMode = EditMode.select;
	private _snapGridSize: number = 5;
	private _isMouseHeld: Boolean = false;
	private _isSelecting: Boolean = false;
	private _isMoving: Boolean = false;
	private _isRectEditing: Boolean = false;
	private _rectEditSide: Side = Side.none;
	private _isPanning: Boolean = false;
	private _selectionMod: SelectionModifier = SelectionModifier.neutral;
	private _dragStart: IVect = Vect.Create();
	private _lastDragPos: IVect = Vect.Create();

	private _camTransform: PIXI.Matrix = PIXI.Matrix.IDENTITY.clone();

	public get isSelectingEntity() { return this._selectEntityProp != null; }

	protected get worldMousePos(): IVect { 
		return this._camTransform.applyInverse(this.parentGame.mousePos); 
	}

	protected get isUIEnabled(): boolean { return this._uiRoot.parentEntity == this; }

	//----------------------------------------------------------------------------------------------

	/** @inheritdoc */
	public override Update(deltaTime: number): void {

		let keys = this.parentGame.keysPressed;
		this._control_testLevel.Update(this.parentGame);

		if(this._isTesting ) {

			this._testScene.Update(deltaTime);

			if(this.parentGame.keysPressed[17] && this._control_testLevel.IsTriggered()){
				this.EndTesting();
			}
		}
		else {

			this.EditorUpdate(deltaTime);

			if(this.parentGame.keysPressed[17] && this._control_testLevel.IsTriggered()){
				this.BeginTesting();
			}
		}
	}

	/** @inheritdoc */
	public override Draw(renderer: PIXI.Renderer): void {
		
		if(this._isTesting){
			this._testScene.Draw(renderer);
			return;
		}

		else{
			
			this.DrawSelectedObjects();

			renderer.render(this._objectRoot, {
				clear: false,
				transform: this._camTransform
			});
			renderer.render(this._editorRoot, { 
				clear: false,
				transform: this._camTransform
			});

			if(this.isUIEnabled){
				this._uiRoot.DrawUI(renderer);
			}
		}
	}

	//----------------------------------------------------------------------------------------------

	/** save the level to LocalStorage under the temporary storage key */
	public SaveLevelCurrent(){
		
		let lvl = this.GetSerializedLevel();

		console.log("Saving level to local storage:");
		console.log(lvl);
		localStorage.setItem(LevelEditor.STORAGEKEY_RECENTLEVEL, lvl);
	}

	/** load the level from the current local storage temporary key */
	public LoadLevelCurrent(){

		let sLev = localStorage.getItem(LevelEditor.STORAGEKEY_RECENTLEVEL);

		console.log("Loading Level...");
		console.log(sLev);

		let lvl = 
			ISerializable.GetDeserializedObject(
				JSON.parse(sLev)
			) as GameScene;
		this.EditGameSene(lvl);
	}

	/** save the level to a specific key on the local storage */
	public SaveLevelLocalStorage(){

		let saveKey = prompt("Enter Level Save Name:");
		let lvl = this.GetSerializedLevel();

		localStorage.setItem(LevelEditor.STORAGEKEY_BASE + saveKey, lvl);
		this.parentGame.ResetKeyboardInput();
	}

	/** load the level from a specific key on the local storage */
	public LoadLevelLocalStorage(){

		let saveKey = prompt("Enter Level Load Name:");

		let sLev = localStorage.getItem(LevelEditor.STORAGEKEY_BASE + saveKey);
		let lvl = 
			ISerializable.GetDeserializedObject(
				JSON.parse(sLev)
			) as GameScene;
		this.EditGameSene(lvl);
		this.parentGame.ResetKeyboardInput();
	}

	/** download level file */
	public SaveLevelAs(){

		let filename = prompt("Enter File Name:", "level") + ".lvl";
		let lvlData = this.GetSerializedLevel();

		let blob = new Blob([lvlData], {type: 'text/plain'});

		let a = document.createElement("a");
		a.download = filename;
		a.href = URL.createObjectURL(blob);
		a.click();

		this.parentGame.ResetKeyboardInput();
	}

	/** upload level file */
	public LoadLevelFile(){

		let fileInput = document.createElement("input");
		fileInput.type = "file";

		let ths = this;
		fileInput.addEventListener("change", function(this: HTMLInputElement, e: Event): any{
			this.files[0].text().then(function(value){ 
				ths.LoadLevelFromJSON(value);
			});
		})

		fileInput.click();
		this.parentGame.ResetKeyboardInput();
	}

	public LoadLevelFromJSON(json: string): void{
		let lvl = 
			ISerializable.GetDeserializedObject(
			JSON.parse(json)
			) as GameScene;
		this.EditGameSene(lvl);
	}

	/**
	 * clones the game scene into the level editor's object root so that it can be edited
	 * @param scene the scene to start editing
	 */
	public EditGameSene(scene: GameScene): void{
		
		this._objectRoot = scene.Clone();
	}

	/**
	 * Serializes the current level state and runs the game from the current level that is open in 
	 * the level editor. The level is then returned to the original state when EndTesting() 
	 * is called
	 */
	public BeginTesting(): void{

		if(this._isTesting) return;
		this._testScene = this._objectRoot.Clone() as GameScene;
		this._isTesting = true;
		this._testScene.parentGame = this.parentGame;

		this._requestPointerLock = true;
	}

	/** Returns the level editor back to the edit mode if it's running from BeginTesting() */
	public EndTesting(): void{
		
		if(!this._isTesting) return;
		this._isTesting = false;

		this._testScene.RemoveFromScene();
		this._testScene = null;

		this._requestPointerLock = false;
		document.exitPointerLock();
	}

	/**
	 * copy the specified entities into the clipboard
	 * @param entities the entitys to copy
	 */
	public CopyToClipboard(... entities: Array<SceneEntity>): void{

		// do nothing if no entities specified
		if(entities.length <= 0){
			return;
		}

		// clear the clipboard
		this._clipboardEntities.splice(0);

		// clone each entity into the clipboard in order
		for(let i = 0; i < entities.length; i++){
			let ent = entities[i];
			let obj = ent.Clone();

			if(ent.parentEntity instanceof Terrain){
				(obj as any)._parentEntity = ent.parentEntity;
			}

			this._clipboardEntities.push(obj);
		}
	}

	/**
	 * paste the entities from the clipboard
	 * @param whether or not the pasted objects should be selected when they are created
	 */
	public PasteFromClipboard(selectPastedObjects: boolean){

		// clear the selected objects if necessary
		if(selectPastedObjects){
			this._selectedObjects.splice(0);
		}

		// iterate through the clipboard and add each entity to the level
		for(let i = 0; i < this._clipboardEntities.length; i++){
			
			// clone the object
			let ent = this._clipboardEntities[i].Clone();

			// if a parent is specified, add it to that parent's children
			if(this._clipboardEntities[i].parentEntity != null){
				this._clipboardEntities[i].parentEntity.addChild(ent);
			}

			// if a parent is not specified, add it to the level's object root
			else{
				this._objectRoot.addChild(ent);
			}

			// select the object if necessary
			if(selectPastedObjects){
				this._selectedObjects.push(ent);
			}
		}
	}

	//----------------------------------------------------------------------------------------------

	/** Update logic for level editor when in edit mode */
	public EditorUpdate(deltaTime: number): void{

		this._objectRoot.Update(0);
		this._editorRoot.Update(deltaTime);
		this._uiRoot.Update(deltaTime);
		this.HandleEditorMouse();
		this.HandleEditorControls();
	}

	private InitializeInputControls(): void{
		this._control_testLevel = InputControl.Create(InputType.keyboardButton, 13, InputCondition.onPress); // ENTER
		this._control_mousePrimary = InputControl.Create(InputType.mouseButton, 0, InputCondition.whileHeld); // mouse l
		this._control_mouseSecondary = InputControl.Create(InputType.mouseButton, 2, InputCondition.whileHeld); // mouse r
		this._control_toggleUI = InputControl.Create(InputType.keyboardButton, 9, InputCondition.onPress); // TAB
		this._control_delete = InputControl.Create(InputType.keyboardButton, 46, InputCondition.onPress); // DEL
		this._control_selectMode = InputControl.Create(InputType.keyboardButton, 87, InputCondition.onPress); // W
		this._control_terrainMode = InputControl.Create(InputType.keyboardButton, 81, InputCondition.onPress); // Q
		this._control_entityMode = InputControl.Create(InputType.keyboardButton, 69, InputCondition.onPress); // E
		this._control_quickSave = InputControl.Create(InputType.keyboardButton, 83, InputCondition.onPress); // S
		this._control_quickLoad = InputControl.Create(InputType.keyboardButton, 76, InputCondition.onPress); // L
		this._control_selectionModPositive = InputControl.Create(InputType.keyboardButton, 17, InputCondition.whileHeld); // CTRL
		this._control_selectionModNegative = InputControl.Create(InputType.keyboardButton, 18, InputCondition.whileHeld); // ALT
		this._control_copy = InputControl.Create(InputType.keyboardButton, 67, InputCondition.onPress); // C
		this._control_paste = InputControl.Create(InputType.keyboardButton, 86, InputCondition.onPress); // V
	}

	private HandleEditorControls(): void{

		// update the state of each control
		this._control_mousePrimary.Update(this.parentGame);
		this._control_mouseSecondary.Update(this.parentGame);
		this._control_toggleUI.Update(this.parentGame);
		this._control_delete.Update(this.parentGame);
		this._control_selectMode.Update(this.parentGame);
		this._control_terrainMode.Update(this.parentGame);
		this._control_entityMode.Update(this.parentGame);
		this._control_quickSave.Update(this.parentGame);
		this._control_quickLoad.Update(this.parentGame);
		this._control_selectionModPositive.Update(this.parentGame);
		this._control_selectionModNegative.Update(this.parentGame);
		this._control_copy.Update(this.parentGame);
		this._control_paste.Update(this.parentGame);

		// selection modifier keys
		if(this._control_selectionModPositive.IsTriggered()){
			this._selectionMod = SelectionModifier.positive;
		}
		else if (this._control_selectionModNegative.IsTriggered()){
			this._selectionMod = SelectionModifier.negative;
		}
		else{
			this._selectionMod = SelectionModifier.neutral;
		}

		// set the selection modifier to mimic the level editor modifier
		this._uiRoot.uiModifier = this._selectionMod;

		// don't perform any control actions if a ui element is in focus
		if(this._uiRoot.GetFocusedElements().length > 0){
			return;
		}

		// show / hide ui key
		if(this._control_toggleUI.IsTriggered()){ 
			this.SetUIEnabled(!this.isUIEnabled);
		}

		// delete key
		if(this._control_delete.IsTriggered()){
			this.DeleteSelectedObjects();
		}

		// edit mode selections:
		if(this._control_selectMode.IsTriggered()){
			this.CloseContextMenu();
			this._editMode = EditMode.select;
			this.UpdateEditModeText();
		}
		if(this._control_terrainMode.IsTriggered()){
			this.CloseContextMenu();
			this._editMode = EditMode.terrain;
			this.UpdateEditModeText();
		}
		if(this._control_entityMode.IsTriggered()){
			this.CloseContextMenu();
			this._editMode = EditMode.entities;
			this.UpdateEditModeText();
		}

		// if CTRL is pressed
		if(this.parentGame.keysPressed[17]){

			// if SHIFT is pressed
			if(this.parentGame.keysPressed[16]){
				
				// local storage save
				if(this._control_quickSave.IsTriggered()){
					this.SaveLevelLocalStorage();
				}

				// local storage load
				if(this._control_quickLoad.IsTriggered()){
					this.LoadLevelLocalStorage();
				}
			}

			// if ALT is pressed
			else if(this.parentGame.keysPressed[18]){
				
				// file save
				if(this._control_quickSave.IsTriggered()){
					this.SaveLevelAs();
				}

				// file load
				if(this._control_quickLoad.IsTriggered()){
					this.LoadLevelFile();
				}
			}

			// if SHIFT and ALT are not pressed
			else{

				// quick save
				if(this._control_quickSave.IsTriggered()){
					this.SaveLevelCurrent();
				}

				// quick load
				if(this._control_quickLoad.IsTriggered()){
					this.LoadLevelCurrent();
				}
			}

			// copy key
			if(this._control_copy.IsTriggered()){
				this.CopyToClipboard(... this._selectedObjects);
			}

			// paste key
			if(this._control_paste.IsTriggered()){
				this.PasteFromClipboard(true);
			}
		}
	}

	private HandleEditorMouse(): void{
		
		// on mouse is pressed
		if(this.parentGame.mousePressed){

			// on mouse click
			if(!this._isMouseHeld){

				this.HandleEditorClick(this.parentGame.mouseButton);
				this._dragStart = this._camTransform.applyInverse(Vect.Clone(this.parentGame.mousePos));
				this._lastDragPos = this._camTransform.applyInverse(Vect.Clone(this.parentGame.mousePos));
				this._isMouseHeld = true;
			}

			// on mouse held
			else{
				this.HandleEditorMouseDrag();
			}
		}

		// on mouse release
		else if(this._isMouseHeld){

			this.HandleEditorReleaseClick();
			this._isMouseHeld = false;
		}

		// on mouse hover
		else{
			this.HandleEditorMouseHover();
		}

		// on mouse scroll
		if(this.parentGame.mouseScroll != 0){

			let zoom = Matrix.GetScale(this._camTransform).x;
			let mod = -this.parentGame.mouseScroll > 0 ? 1.414213562373095 : 0.7071067811865475;
			this.SetZoomLevel(zoom * mod);
		}
	}

	//----------------------------------------------------------------------------------------------

	private HandleEditorClick(button: number): void{
		
		let uiElemsClicked = this._uiRoot.GetUIElementsAtPoint(this.parentGame.mousePos);
		if(uiElemsClicked.length > 0){

			// if context menu focus is lost, close it
			if(this._uiContextMenu != null && !uiElemsClicked.includes(this._uiContextMenu)){
				this.CloseContextMenu();
			}

			// handle the action for the ui elements that were clicked
			this.HandleUIElementAction(uiElemsClicked, button);
			return;
		}
		else{
			this.CloseContextMenu();
		}

		if(button == 0){

			// if the mouse is over a selected object
			let selObjsHover = this.GetSelectedObjectsOnMouse();
			if(selObjsHover.length > 0){
				this._isMoving = true;
			}

			// if the mouse isn't over a selected object
			else{
				switch(this._editMode){
					case EditMode.select: 
					case EditMode.terrain:
						this.StartMouseSelecting(); 
						break;
					case EditMode.entities:
						this.PlaceEntity();
						break;
				}
			}
		}

		else if (button == 1){
			this._isPanning = true;
		}

		else if(button == 2){
			this.StartMouseRectEditing();
		}
	}

	private HandleEditorMouseDrag(): void{

		let mousePos = this.worldMousePos;
		let delta = Vect.Subtract(mousePos, this._lastDragPos);

		if(delta.x == 0 && delta.y == 0)
			return;

		// handle selection rect
		if(this._isSelecting){
			this._dragGraphic.clear();

			this._dragGraphic.lineStyle({
				color: 0x00FF00,
				width: 1
			});

			let rect = this.GetMouseDragArea(this._editMode == EditMode.terrain);
			this._dragGraphic.drawRect(rect.position.x, rect.position.y, rect.size.x, rect.size.y);
		}

		// handle moving the selected objects
		if(this._isMoving){

			// snap mouse delta to grid
			delta = Vect.MultiplyScalar(delta, 1 / this._snapGridSize);
			delta.x = Math.round(delta.x);
			delta.y = Math.round(delta.y);
			delta = Vect.MultiplyScalar(delta, this._snapGridSize);

			// move selected objects if necessary
			if(delta.x != 0 || delta.y != 0){
				for(let i = this._selectedObjects.length - 1; i >= 0; i--){

					// get a reference to the object and the object that will be moved
					let obj = this._selectedObjects[i];
					let movedObj = obj;

					// move the object 
					let pos = movedObj.position;
					movedObj.position.set(pos.x + delta.x, pos.y + delta.y);
				}
			}
		}

		// handle editing the collider rects
		if(this._isRectEditing){

			// snap mouse delta to grid
			delta = Vect.MultiplyScalar(delta, 1 / this._snapGridSize);
			delta.x = Math.round(delta.x);
			delta.y = Math.round(delta.y);
			delta = Vect.MultiplyScalar(delta, this._snapGridSize);

			this.HandleRectEditing(this._rectEditSide, delta);
		}

		// handle camera pan
		if(this._isPanning){
			
			this.PanCamera(Vect.Create(delta.x, delta.y));
			this._dragStart.x -= delta.x;
			this._dragStart.y -= delta.y;
			this._lastDragPos.x -= delta.x;
			this._lastDragPos.y -= delta.y;
		}

		this._lastDragPos = Vect.Add(this._lastDragPos, delta);
	}

	private HandleEditorReleaseClick(): void{
		
		this._dragGraphic.visible = false;
		this._isPanning = false;

		switch(this._editMode){
			case EditMode.terrain:
				if(this.parentGame.mouseButton == 0) 
					this.AddSelectionRectTerrain(this.GetMouseDragArea(true));
				break;
			case EditMode.select:
				if (this._isSelecting) {
					this.SelectArea(this.GetMouseDragArea(false), this._selectionMod);
					this._isSelecting = false;
				}
				break;
		}

		if(this._isMoving){
			this._isMoving = false;
		}
		if(this._isRectEditing){
			this._isRectEditing = false;
		}
	}

	private HandleEditorMouseHover(): void{

		let elems = this._uiRoot.GetUIElementsAtPoint(this.parentGame.mousePos);

		// iterate through each element that the mouse is hovering over
		let didSet = false;
		for(let i = elems.length - 1; i >= 0; i--){

			// if the element is a child of the ui selection element, emphasize this element
			let elem = elems[i]
			if(
				elem instanceof UIElemEntityLink &&
				elem.parentEntity == this._uiSelectedObjectsRoot
			){
				this._emphasizedSelectionEntity = elem.linkedEntity;
				didSet = true;
				break;
			}
		}

		// unset the emphasized object if mouse is not over a corresponding element
		if(!didSet){
			this._emphasizedSelectionEntity = null;
		}
	}

	//----------------------------------------------------------------------------------------------

	private StartMouseSelecting(){

		this._isSelecting = true;
		this._dragGraphic.clear();
		this._dragGraphic.lineStyle({
			color: 0xFFFFFF,
			width: 1
		});
		this._dragGraphic.visible = true;
	}

	private StartMouseRectEditing(){

		let worldMouse = this.worldMousePos;
		
		// iterate through each selected object
		let editRect:IEditorResizable = null;
		let closestPoint: IVect = null;
		let closest = Number.POSITIVE_INFINITY;
		for(let i = this._selectedObjects.length - 1; i >= 0; i--){

			// if the object is a aabbcollider
			let sel = this._selectedObjects[i];
			if(IEditorResizable.ImplementedIn(sel)){

				// if the the mouse is closer to this one than the others, pick it
				let point = sel.ClosestPoint(worldMouse);
				let dist = Vect.Distance(worldMouse, point)
				if(dist < closest){
					editRect = sel;
					closest = dist;
					closestPoint = point;
				}
			}
		}

		// if no editable rects were found, do nothing
		if(editRect == null){
			return;
		}
		
		// set the appropriate edit side based on the which side the mouse is closest to
		this._rectEditSide = Rect.ClosestSide(editRect.GetResizeRect(), closestPoint);
		this._isRectEditing = true;
	}

	private PlaceEntity(){

		let ent = this._selectedCreationObject.Clone();
		let pos = this.worldMousePos;
		pos.x = Math.round(pos.x / this._snapGridSize) * this._snapGridSize;
		pos.y = Math.round(pos.y / this._snapGridSize) * this._snapGridSize;

		ent.position.set(pos.x, pos.y);
		this._objectRoot.addChild(ent);
	}

	private HandleRectEditing(side: Side, delta: IVect){
		
		// do nothing if no mouse movement
		if(Math.abs(delta.x) <= 0 && Math.abs(delta.y) <= 0)
			return;

		// iterate through each selected AABBCollider
		for(let i = this._selectedObjects.length - 1; i >= 0; i--){
			let sel = this._selectedObjects[i];
			if(IEditorResizable.ImplementedIn(sel)){

				let rect = sel.GetResizeRect();

				// calculate the delta size and position from the mouse delta based on the specified
				// edit side
				let deltaSize = Vect.Create();
				let deltaPos = Vect.Create();
				switch(side){
					case Side.up:
						deltaSize.y = -delta.y;
						deltaPos.y = delta.y;
						break;
					case Side.down:
						deltaSize.y = delta.y;
						deltaPos.y = 0;
						break;
					case Side.left:
						deltaSize.x = -delta.x;
						deltaPos.x = delta.x;
						break;
					case Side.right:
						deltaSize.x = delta.x;
						deltaPos.x = 0;
						break;
				}

				rect.position.x += deltaPos.x;
				rect.position.y += deltaPos.y;
				rect.size.x += deltaSize.x;
				rect.size.y += deltaSize.y;
				sel.SetResizeRect(rect);
			}
		}
	}

	//----------------------------------------------------------------------------------------------

	/** build the UI elements for the level editor scene */
	private InitializeUI(): void{

		// create the initial UI objects and parent them appropriately
		this._uiRoot = new UIContainer(this.parentGame);
		this.addChild(this._uiRoot);
		
		// create right panel
		let rpanelWidth = 200;
		this._uiPanelRight = new UIElement();
		this._uiRoot.addChild(this._uiPanelRight);
		this._uiPanelRight.uiOffset = Vect.Create(this.parentGame.renderer.width - rpanelWidth, 0);
		this._uiPanelRight.uiSize = Vect.Create(0, 1);
		this._uiPanelRight.uiSizeOffset = Vect.Create(rpanelWidth, 0);
		this._uiPanelRight.UpdatePanelGraphic();
		this._uiPanelRight.UpdatePanelCollider();
		
		// create bottom panel
		let bpanelWidth = 250;
		let bpanelHeight = 40;
		this._uiPanelBottom = new UIElement();
		this._uiRoot.addChild(this._uiPanelBottom);
		this._uiPanelBottom.uiSizeOffset = Vect.Create(bpanelWidth, bpanelHeight);
		this._uiPanelBottom.uiOffset = Vect.Create(
			this.parentGame.renderer.width - rpanelWidth - bpanelWidth, 
			this.parentGame.renderer.height - bpanelHeight
		);
		this._uiPanelBottom.UpdatePanelGraphic();
		this._uiPanelBottom.UpdatePanelCollider();

		// entity selection button
		let entitySelection = new UIInteractiveElement();
		this._uiPanelBottom.addChild(entitySelection);
		entitySelection.uiAnchor = Vect.Create(1, 0);
		entitySelection.uiPivot = Vect.Create(1, 0);
		entitySelection.uiOffset = Vect.Create(-5, 5);
		entitySelection.uiSize = Vect.Create(0.5, 1);
		entitySelection.uiSizeOffset = Vect.Create(-7, -10);
		entitySelection.UpdatePanelCollider();
		entitySelection.UpdatePanelGraphic();

		let entSelText = UITextElement.CreateFromString(this._selectedCreationObject.name);
		entitySelection.addChild(entSelText.AlignedCenter());

		let ths = this;
		entitySelection.submitAction.AddListener(function(action){
			ths._uiContextMenu = ths.CreateContextMenu_EntitySelection(entSelText);
		});

		// edit mode selection button
		let editModeButton = new UIInteractiveElement();
		this._uiEditModeButton = editModeButton;
		this._uiPanelBottom.addChild(editModeButton);
		editModeButton.uiAnchor = Vect.Create();
		editModeButton.uiPivot = Vect.Create();
		editModeButton.uiOffset = Vect.Create(5, 5);
		editModeButton.uiSize = Vect.Create(0.5, 1);
		editModeButton.uiSizeOffset = Vect.Create(-7, -10);
		editModeButton.UpdatePanelCollider();
		editModeButton.UpdatePanelGraphic();

		this.UpdateEditModeText();

		editModeButton.submitAction.AddListener(function(action){
			ths._uiContextMenu = ths.CreateContextMenu_EditModeSelection();
		});

		// create the selected objects list
		this._uiSelectedObjectsRoot = new UIElement();
		this._uiSelectedObjectsRoot.position.set(50, 25);
		this._uiPanelRight.addChild(this._uiSelectedObjectsRoot);
		let selectionListText = new PIXI.Text("Selection:", {
			fontFamily: "Consolas",
			fontSize: 12,
			fill: 0xFFFFFF,
			stroke: 0x000000,
			strokeThickness: 1
		});
		selectionListText.position.set(-20, -14);
		this._uiSelectedObjectsRoot.addChild(selectionListText);
	}

	private UpdateEditModeText(){

		let textElem = this._uiEditModeButton.GetChildOfType<UITextElement>(UITextElement);
		textElem?.destroy();

		let editSelText = UITextElement.CreateFromString(EditMode.ToString(this._editMode));
		this._uiEditModeButton.addChild(editSelText.AlignedCenter());
	}

	private SetUIEnabled(enabled = false): void{
		
		// if enabled
		if(this._uiRoot.parentEntity == this){
			if(!enabled){
				this.removeChild(this._uiRoot);
			}
		}

		// if not enabled
		else{
			if(enabled){
				this.addChild(this._uiRoot);
			}
		}
	}

	private HandleUIElementAction(elements: Array<SceneEntity>, action: number): void{

		// iterate through each specified element
		for(let i = elements.length - 1; i >= 0; i--){

			let elem = elements[i];
			if(!(elem instanceof UIElement)) break;

			// if the element has a custom action
			if(IUISubmittable.ImplementedIn(elem)){
				elem.Submit(action);
				break;
			}

			// if the element is part of the selected objects list
			if(
				elem instanceof UIElemEntityLink &&
				elem.parentEntity == this._uiSelectedObjectsRoot
			){
				// get the index of the entity
				let index = this._selectedObjects.indexOf(elem.linkedEntity);

				// if the level editor is in entity property selection mode
				if(this.isSelectingEntity){
					this._selectEntityProp.SetValue(
						this._selectEntityPropTarget, elem.linkedEntity
					);
					this._selectEntityPropText.text.text = elem.linkedEntity.name;
					this._selectEntityProp = null;
					this._selectEntityPropTarget = null;
					return;
				}

				// if right click
				if(action == 2){
					this.CreateObjectPropertiesWindow(elem.linkedEntity);
					break;
				}

				// if not right click, deselect the entity that corresponds to the element in 
				// the list
				else if(index >= 0){
					this._selectedObjects.splice(index, 1);
					this.RefreshSelectedObjectsUI();
					break;
				}
			}
		}
	}

	private RefreshSelectedObjectsUI(): void{

		// remove all selection list ui entities
		let curChildren = this._uiSelectedObjectsRoot.children;
		for(let i = curChildren.length - 1; i >= 1; i--){
			curChildren[i].destroy();
		}

		// define text style and line spacing for list
		let lineSpacing = 14;

		// create a ui element for each selected object
		for(let i = 0; i < this._selectedObjects.length; i++){
			
			let obj = this._selectedObjects[i];
			let uiEntity = new UIElemEntityLink(obj);
			this._uiSelectedObjectsRoot.addChild(uiEntity);
			uiEntity.uiOffset = Vect.Create(0, lineSpacing * i);
			uiEntity.MatchSizeOffsetToText(10, 0);
			uiEntity.uiSizeOffset.y = lineSpacing;
			uiEntity.UpdatePanelCollider();
		}
	}

	private CreateObjectPropertiesWindow(obj: SceneEntity): UIElement{

		if(this._uiObjectPropertyWindow != null){
			this._uiObjectPropertyWindow.destroy();
			this._uiObjectPropertyWindow = null;
		}

		let uiWindow = new UIElement();
		uiWindow.uiAnchor = Vect.Create(1, 0);
		uiWindow.uiPivot = Vect.Create(1, 0);
		let bpanelRect = this._uiPanelBottom.GetUiRect();
		uiWindow.uiOffset = Vect.Create(
			bpanelRect.position.x + bpanelRect.size.x - 5,
			1
		);
		let title = UITextElement.CreateFromString(obj.name + " properties:").AlignedCenter();
		title.uiAnchor.y = 0;
		title.uiPivot.y = 0;
		title.uiOffset.y = 10;
		uiWindow.UpdatePanelGraphic();
		uiWindow.addChild(title);

		// TODO add object properties
		let selectedObject = obj;
		let propsContainer = new UIElement();
		propsContainer.uiOffset.y = 30;
		propsContainer.uiSize.x = 1;
		propsContainer.uiSizeOffset.y = 10;

		let yOff = 0;
		let props: Array<EditorProperty<SceneEntity, any>> = 
			Object.getPrototypeOf(selectedObject).constructor.GetEditorProperties();
		for(let i = 0; i < props.length; i++){
			
			// create a text element for the property name
			let propElem = props[i].GetUIElement(selectedObject);
			propElem.uiOffset.y = yOff;
			propsContainer.addChild(propElem);
			propElem.ExpandToFitChildren();
			yOff += propElem.GetUiRect().size.y;
		}

		let closeButton = new UIInteractiveElement();
		closeButton.uiAnchor = Vect.Create(1, 1);
		closeButton.uiPivot = Vect.Create(1, 1);
		closeButton.uiOffset = Vect.Create(-5, -5);
		closeButton.uiSize.x = 0.5;
		closeButton.uiSizeOffset = Vect.Create(-7, 25);
		closeButton.UpdatePanelGraphic();
		closeButton.UpdatePanelCollider();
		closeButton.addChild(UITextElement.CreateFromString("Close").AlignedCenter());
		
		let applyButton = new UIInteractiveElement();
		applyButton.uiAnchor = Vect.Create(0, 1);
		applyButton.uiPivot = Vect.Create(0, 1);
		applyButton.uiOffset = Vect.Create(5, -5);
		applyButton.uiSize.x = 0.5;
		applyButton.uiSizeOffset = Vect.Create(-7, 25);
		applyButton.UpdatePanelGraphic();
		applyButton.UpdatePanelCollider();
		applyButton.addChild(UITextElement.CreateFromString("apply").AlignedCenter());

		uiWindow.addChild(propsContainer, applyButton, closeButton);
		uiWindow.ExpandToFitChildren(20, 0);

		// expand to fit the properties block
		propsContainer.ExpandToFitChildren(0, 0);
		uiWindow.uiSizeOffset.y += propsContainer.GetUiRect().size.y;
		let rect = uiWindow.GetUiRect();
		let bottom = rect.position.y + rect.size.y;
		let hOff = this._uiPanelBottom.GetUiRect().position.y - bottom;
		if(hOff < 0)
			uiWindow.uiSizeOffset.y += hOff;

		uiWindow.UpdateUiPosition(true);
		uiWindow.UpdatePanelCollider();
		uiWindow.UpdatePanelGraphic();
		// props.UpdatePanelGraphic();
		applyButton.UpdatePanelGraphic();
		applyButton.UpdatePanelCollider();
		closeButton.UpdatePanelGraphic();
		closeButton.UpdatePanelCollider();

		let ths = this;
		let closeAction = function(action: number){
			ths._uiObjectPropertyWindow.destroy();
			ths._uiObjectPropertyWindow = null;
		};
		closeButton.submitAction.AddListener(closeAction);

		this._uiRoot.addChild(uiWindow);
		this._uiObjectPropertyWindow = uiWindow;
		return uiWindow;
	}

	//----------------------------------------------------------------------------------------------

	private CloseContextMenu(): void{
		if(this._uiContextMenu != null){
			this._uiContextMenu.destroy();
			this._uiContextMenu = null;
		}
	}

	private CreateContextMenu_EntitySelection(textModify: UITextElement): UIElement{

		this.CloseContextMenu();

		let panRect = this._uiPanelBottom.GetUiRect();
		let height = 10;
		let width = panRect.size.x - 10;

		let r = new UIElement();
		r.UpdatePanelGraphic();
		r.uiPivot = Vect.Create(0.5, 1);
		r.uiOffset = Vect.Create(
			panRect.position.x + panRect.size.x * 0.5, 
			panRect.position.y - 5
		);
		r.uiSizeOffset = Vect.Create(width, height);
		this._uiRoot.addChild(r);

		let ths = this;
		let func_selectEnt = function(entity: SceneEntity){
			ths.CloseContextMenu();
			ths._selectedCreationObject = entity;
			textModify.SetTextString(entity.name);
		};

		let spacing = 15;
		for(let i = 0; i < this._entityCreationPool.length; i++){
			let ent = this._entityCreationPool[i];
			let elem = new UIInteractiveElement();
			elem.uiOffset.y = i * spacing + 5;
			elem.uiAnchor.x = 0.5;
			elem.uiPivot.x = 0.5;
			elem.submitAction.AddListener(function(action){ func_selectEnt(ent); });
			r.addChild(elem);
			elem.addChild(UITextElement.CreateFromString(ent.name).AlignedCenter());
			elem.ExpandToFitChildren();
			elem.UpdatePanelCollider();
		}

		r.ExpandToFitChildren(0, 10);
		r.UpdateUiPosition(true);
		r.UpdatePanelCollider();
		r.UpdatePanelGraphic();

		this._uiContextMenu = r;
		return r;
	}

	private CreateContextMenu_EditModeSelection(): UIElement{

		this.CloseContextMenu();

		let panRect = this._uiPanelBottom.GetUiRect();
		let height = 10;
		let width = panRect.size.x - 10;
		
		let r = new UIElement();
		r.UpdatePanelGraphic();
		r.uiPivot = Vect.Create(0.5, 1);
		r.uiOffset = Vect.Create(
			panRect.position.x + panRect.size.x * 0.5, 
			panRect.position.y - 5
		);
		r.uiSizeOffset = Vect.Create(width, height);
		this._uiRoot.addChild(r);

		let ths = this;
		let func_selectMode = function(mode: EditMode){
			ths.CloseContextMenu();
			ths._editMode = mode;
			ths.UpdateEditModeText();
		};

		let spacing = 15;
		for(let i = 1; i <= 3; i++){
			let mode = i as EditMode;
			let elem = new UIInteractiveElement();
			elem.uiOffset.y = (i - 1) * spacing + 5;
			elem.uiAnchor.x = 0.5;
			elem.uiPivot.x = 0.5;
			elem.submitAction.AddListener(function(action){ func_selectMode(mode); });
			r.addChild(elem);
			elem.addChild(UITextElement.CreateFromString(EditMode.ToString(mode)).AlignedCenter());
			elem.ExpandToFitChildren();
			elem.UpdatePanelCollider();
		}

		r.ExpandToFitChildren(0, 10);
		r.UpdateUiPosition(true);
		r.UpdatePanelCollider();
		r.UpdatePanelGraphic();

		this._uiContextMenu = r;
		return r;
	}

	//----------------------------------------------------------------------------------------------

	/** puts the editor into entity selection mode */
	public StartEntitySelection<TTarget>(
		property: EditorProperty<TTarget, SceneEntity>, 
		target: any,
		text: UITextElement): 
	void {
		this._selectEntityProp = property;
		this._selectEntityPropTarget = target;
		this._selectEntityPropText = text;
	}

	public CancelEntitySelection(){

		if(!this.isSelectingEntity) return;
		this._selectEntityProp.SetValue(this._selectEntityPropTarget, null);
		this._selectEntityProp = null;
		this._selectEntityPropTarget = null;
		this._selectEntityPropText = null;
	}

	/**
	 * select all the objects in a given area in the level
	 * @param rect the selection rect to select all objects who overlap this rectangle
	 * @param modifier the selection modifier to apply (neutral is default)
	 */
	public SelectArea(rect: IRect, modifier: SelectionModifier): void{

		// create an axis aligned box collider over the area of the mouse drag
		let center = Rect.Center(rect);
		let col = AABBCollider.Create(Vect.MultiplyScalar(rect.size, 0.5));
		col.position.set(center.x, center.y);
		col.layer = CollisionLayer.none;

		// find all the colliders that intersect the mouse drag box
		let obRoot = this._objectRoot as Scene;
		let cols = obRoot.colliders.FindOverlapColliders(col);
		
		// reset the selected object array and add each selected collider to the array
		if(modifier == SelectionModifier.neutral){
			this._selectedObjects.splice(0, this._selectedObjects.length);
		}

		// iterate through each selected object and add or remove it
		for(let i = cols.length - 1; i >= 0; i--){

			let obj: SceneEntity = cols[i];
			if(obj instanceof Collider){
				if(!(obj.parentEntity instanceof Terrain)){
					obj = obj.parentEntity;
				}
			}

			// get the array of the object in the selection
			let selIndex = this._selectedObjects.indexOf(obj);

			// if modifier is set to remove the object and it's in the array, remove the object
			if(modifier == SelectionModifier.negative){
				if(selIndex >= 0){
					this._selectedObjects.splice(selIndex, 1);
				}
			}

			// if the object does not exist in the selection already and modifier is set to add it
			else if(selIndex < 0){
				this._selectedObjects.push(obj);
			}
		}

		this.RefreshSelectedObjectsUI();
	}

	private GetSelectedObjectsOnMouse(): Array<SceneEntity>{

		let r = new Array<SceneEntity>();
		let mpos = this.worldMousePos;

		for(let i = this._selectedObjects.length - 1; i >= 0; i--){
			let obj = this._selectedObjects[i];

			if(ICollidable.ImplementedIn(obj)){
				if(obj.collider.OverlapsPoint(mpos)){
					r.push(obj);
					continue;
				}
			}
		}

		return r;
	}

	private GetMouseDragArea(snapToGrid: boolean = false): IRect{

		// calculate the mouse drag start and end position
		let rectPos = Vect.Clone(this._dragStart);
		let dragEnd:IVect = this._camTransform.applyInverse(this.parentGame.mousePos);

		// snap to the grid if specified to do so
		if(snapToGrid){
			rectPos = Vect.MultiplyScalar(rectPos, 1 / this._snapGridSize);
			rectPos.x = Math.round(rectPos.x);
			rectPos.y = Math.round(rectPos.y);
			rectPos = Vect.MultiplyScalar(rectPos, this._snapGridSize);
			dragEnd = Vect.MultiplyScalar(dragEnd, 1 / this._snapGridSize);
			dragEnd.x = Math.round(dragEnd.x);
			dragEnd.y = Math.round(dragEnd.y);
			dragEnd = Vect.MultiplyScalar(dragEnd, this._snapGridSize);
		}

		// difference between drag end and start
		let dif = Vect.Create(
			dragEnd.x - rectPos.x, 
			dragEnd.y - rectPos.y
		);

		// ensure x size is not negative
		if(dif.x < 0){
			dif.x *= -1;
			rectPos.x -= dif.x;
		}

		// ensure y size is not negative
		if(dif.y < 0){
			dif.y *= -1;
			rectPos.y -= dif.y;
		}

		// get the area from the mouse drag start and end positions
		let rect = Rect.Create(rectPos, dif);
		return rect;
	}

	private DrawSelectedObjects(): void{

		let graphic = this._selectedObjectsGraphic;
		graphic.clear();
		graphic.lineStyle({
			color: 0x00FF00,
			alpha: 0.65,
			width: 1
		});

		for(let i = this._selectedObjects.length - 1; i >= 0; i--){
			
			// if the emphasized selection is current iteration object, skip it so that we can
			// draw it later with a different style
			if(this._selectedObjects[i] == this._emphasizedSelectionEntity)
				continue;

			this.DrawSelectedEntity(this._selectedObjects[i]);
		}

		graphic.lineStyle({
			color: 0xFFFF00,
			alpha: 0.9,
			width: 2
		});

		// draw empasized selection
		let emphasis = this._emphasizedSelectionEntity;
		if(emphasis != null){
			this.DrawSelectedEntity(emphasis);
		}
	}

	private DrawSelectedEntity(entity: SceneEntity): void{

		let graphic = this._selectedObjectsGraphic;
		let pos = entity.globalPosition;
		let rect: IRect = Rect.Create(Vect.Create(pos.x - 10, pos.y - 10), Vect.Create(20));

		if(ICollidable.ImplementedIn(entity)){
			let selCol = entity.collider;
			rect = selCol.GetColliderAABB();
			rect.position.x -= 5,
			rect.position.y -= 5
			rect.size.x += 10;
			rect.size.y += 10;
		}

		let cSize = 7;

		// draw top left corner
		graphic.moveTo(rect.position.x, rect.position.y + cSize);
		graphic.lineTo(rect.position.x, rect.position.y);
		graphic.lineTo(rect.position.x + cSize, rect.position.y);

		// top right
		graphic.moveTo(rect.position.x + rect.size.x - cSize, rect.position.y);
		graphic.lineTo(rect.position.x + rect.size.x, rect.position.y);
		graphic.lineTo(rect.position.x + rect.size.x, rect.position.y + cSize);

		// bottom right
		graphic.moveTo(rect.position.x + rect.size.x - cSize, rect.position.y + rect.size.y);
		graphic.lineTo(rect.position.x + rect.size.x, rect.position.y + rect.size.y);
		graphic.lineTo(rect.position.x + rect.size.x, rect.position.y + rect.size.y - cSize);
		
		// bottom left
		graphic.moveTo(rect.position.x, rect.position.y + rect.size.y - cSize);
		graphic.lineTo(rect.position.x, rect.position.y + rect.size.y);
		graphic.lineTo(rect.position.x + cSize, rect.position.y + rect.size.y);
	}

	private DeleteSelectedObjects(): void{

		// iterate throug each object
		for(let i = this._selectedObjects.length - 1; i >= 0; i--){

			// get a reference to the object that's selected
			let obj = this._selectedObjects[i];
			
			// remove the object
			console.log("Deleted " + obj.name);
			obj.RemoveFromScene();
		}

		// clear selected objects so we dont get null refs in the array
		this._selectedObjects.splice(0, this._selectedObjects.length);
		this.RefreshSelectedObjectsUI();
	}

	private AddSelectionRectTerrain(rect: IRect, terrain: Terrain = null): void{

		// don't add empty terrains
		if(rect.size.x == 0 || rect.size.y == 0) return;

		// create a collider from the selection rect to add to the terrain
		let col = AABBCollider.Create(Vect.Create());
		col.layer = CollisionLayer.terrain;
		
		// if no terrain object was specified then we'll need to find one in the scene
		if(terrain == null){
			
			// iterate thorugh each object in the scene until we find a terrain object
			// to add the collider to
			for(let i = this._objectRoot.children.length - 1; i >= 0; i--){
				if(this._objectRoot.children[i] instanceof Terrain){
					terrain = this._objectRoot.children[i] as Terrain;
					break;
				}
			}
			
			// no terrain in level object root
			if(terrain == null){
				throw "No Terrain object found in object root";
			}
			
		}
		terrain.addChild(col);
		
		// position the terrain collider to match the selection rect
		col.SetRect(rect);
		col.CreateWireframe(0xFFFFFF, 1);

		console.log("Terrain collider added to " + terrain.name);

		let serialized = terrain.GetSerializedObject();
		console.log(terrain);
		console.log(serialized);
		console.log(ISerializable.GetDeserializedObject(serialized));

		// so that the selection rect is removed without modifying current selection
		this._isSelecting = false;
	}

	//----------------------------------------------------------------------------------------------

	public SetZoomLevel(zoom: number){

		let vpX = this.parentGame.renderer.view.width;
		let vpY = this.parentGame.renderer.view.height;

		let curZoom = Matrix.GetScale(this._camTransform).x;
		let curPos = Matrix.GetTranslation(this._camTransform);
		
		let dZoom = zoom / curZoom;
		let p0 = this.worldMousePos;
		this._camTransform.scale(dZoom, dZoom);
		let p1 = this.worldMousePos;
		
		let dx = p1.x - p0.x;
		let dy = p1.y - p0.y;

		this._camTransform.translate(dx * zoom, dy * zoom);
	}

	public PanCamera(translation: IVect){

		let zoom = Matrix.GetScale(this._camTransform);
		this._camTransform.translate(translation.x * zoom.x, translation.y * zoom.y);
	}

	//----------------------------------------------------------------------------------------------

	/**
	 * serializes the level in base64 encoded json and returns the string
	 */
	public GetSerializedLevel(): string{

		let r: any = this._objectRoot.GetSerializedObject();
		// TODO: convert to base 64
		console.log(r);
		return JSON.stringify(r, null, '\t');
	}
}

/** 
 * an object meant to be used as a ui element in the level editor that has a link to an entity in 
 * the level 
 */
class UIElemEntityLink extends UITextElement{

	constructor(linkedEntity: SceneEntity){
		super(new PIXI.Text(linkedEntity.name, UITextElement.DEFAULT_STYLE));
		this.linkedEntity = linkedEntity;
	}

	public linkedEntity: SceneEntity = null;
}