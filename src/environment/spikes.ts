///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	Collider, CollisionLayer, AABBCollider, ICollidable, ICollision, Collision,
	IVect, Vect, Side,
	IEditorResizable, EditorProperty,
	SceneEntity,
	ITarget,
	IRect, Rect,
	Allegiance, Aggression,
	GroundHazard, SpriteSheet
} from '../internal';

export class Spikes extends GroundHazard {

	constructor(){ 
		super();
		this._name = "Spikes";
	}

	public static readonly SPRITESHEET: SpriteSheet = 
		new SpriteSheet("./dist/assets/graphics/Spikes.png");

	/// --------------------------------------------------------------------------------------------

	protected override Initialize(){

		this._collider = new AABBCollider();
		this._collider.layer = CollisionLayer.projectiles;
		this._collider.halfExtents = Vect.Create(this._thickness * 0.5);
		this._collider.CreateWireframe(0xFF0000, 1);

		let ths = this;
		this.collider.onCollision.AddListener(function(col){ths.OnCollision(col)});

		let spr = new PIXI.TilingSprite(null);
		spr.anchor.set(0.5, 1);
		this._sprite = spr;

		Spikes.SPRITESHEET.DoOnLoaded(function(){
			ths._sprite.texture = Spikes.SPRITESHEET.sprites[0];
		});
		
		this.addChild(this._collider);
		this.addChild(this._sprite);

		this.UpdateSprite();
	}

	private OnCollision(col: ICollision){

		let other = Collision.OtherCollider(col, this.collider);

		let entity = other.parentEntity;
		if(ITarget.ImplementedIn(entity)){
			if(Allegiance.HAZARD.AggresionToward(entity) == Aggression.hostile){
				entity.Hit(10, Vect.Create(), col.collisionPoint, this);
			}
		}
	}

	/// --------------------------------------------------------------------------------------------

	public static override GetEditorProperties(): EditorProperty<SceneEntity, any>[] {
		
		let r = super.GetEditorProperties();

		r.push(
			EditorProperty.CreateKVProp("upDirection", "Up Direction", [
				{key: "up", value: Side.up},
				{key: "right", value: Side.right},
				{key: "down", value: Side.down},
				{key: "left", value: Side.left}
			]),
			EditorProperty.CreateNumberProp("length", "Length"),
		);

		return r;
	}
}