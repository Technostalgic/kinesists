///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	GameScene,
	CollisionLayer, AABBCollider, ICollision, Collision,
	Vect, Side,
	PhysicsEntity,
	Player,
	GroundHazard, SpriteSheet,
	LineParticle, ParticleEmitter, ParticleSystem
} from '../internal';

export class JumpPad extends GroundHazard{
	
	constructor(){
		super();
		this.Initialize();
		this._name = "Jump Pad";
	}

	public static readonly SPRITESHEET: SpriteSheet = 
		new SpriteSheet("./dist/assets/graphics/JumpPad.png");

	private _particleSystem: ParticleSystem<LineParticle> = null;
	private _particleEmitter: ParticleEmitter = null;

	public force: number = 200;

	/// --------------------------------------------------------------------------------------------

	public override Update(deltaTime: number): void {
		super.Update(deltaTime);

		if(this._sprite.texture == null){
			this.InitGraphic();
		}

		if(this._particleEmitter == null){
			this.CreateParticleEffects();
		}
	}

	protected override Initialize(){

		this._collider = new AABBCollider();
		this._collider.layer = CollisionLayer.projectiles;
		this._collider.halfExtents = Vect.Create(this._thickness * 0.5);
		this._collider.CreateWireframe(0xFF0000, 1);

		let ths = this;
		this.collider.onCollision.AddListener(function(col){ths.OnCollision(col)});

		this.InitGraphic();
		this.addChild(this._collider);

		this.UpdateSprite();
		this.CreateParticleEffects();
	}

	private InitGraphic(){

		if(!JumpPad.SPRITESHEET.isLoaded) return;

		let plane = new PIXI.NineSlicePlane(JumpPad.SPRITESHEET.sprites[0]);
		plane.topHeight = 0;
		plane.bottomHeight = 0;
		plane.leftWidth = 3;
		plane.rightWidth = 3;

		this._sprite = plane;
		this._sprite.pivot.set(this._sprite.width * 0.5, this._sprite.height);
		
		this.addChild(this._sprite);
		this.UpdateSprite();
	}

	private CreateParticleEffects(){

		if(!(this.parentScene instanceof GameScene)) return;

		let pSystem = new ParticleSystem(LineParticle, 25);
		this.parentScene.addChild(pSystem);
		this._particleSystem = pSystem;
		let emitter = new ParticleEmitter(pSystem);
		this.addChild(emitter);
		this._particleEmitter = emitter;

		emitter.particleLife = 0.5;
		emitter.particleLifeVar = 0.1;
		emitter.speed = 0;
		emitter.speedVar = 0;
		emitter.emissionOverDist = 0;
		emitter.radius = 0;
		emitter.colorA = 0x44AAFF;
		emitter.colorB = 0x88FFFF;

		this.UpdateParticleEmitterShape();
	}

	public UpdateParticleEmitterShape(){
		if(this._particleEmitter == null) return;

		this._particleEmitter.halfExtents = Vect.Clone(this.collider.halfExtents);
		this._particleEmitter.startVelocity = Vect.MultiplyScalar(Side.GetVector(this.upDirection), 50);
		this._particleEmitter.emissionOverTime = 
			this._particleEmitter.halfExtents.x * this._particleEmitter.halfExtents.y * 0.25;
	}

	private OnCollision(col: ICollision){

		let other = Collision.OtherCollider(col, this.collider);

		let entity = other.parentEntity;
		if(entity instanceof PhysicsEntity){

			let vect = Side.GetVector(this.upDirection);
			vect.x *= this.force;
			vect.y *= this.force;

			if(!this.isHorizontal){
				if(Math.sign(vect.x) != Math.sign(entity.velocity.x))
					entity.velocity.x = vect.x;
			}
			else{
				if(Math.sign(vect.y) != Math.sign(entity.velocity.y))
					entity.velocity.y = vect.y;
			}
		}
	}

	/// --------------------------------------------------------------------------------------------

	protected override UpdateSprite(): void {
		if(this._sprite == null) return;
		super.UpdateSprite();
		this._sprite.pivot.set(this._sprite.width * 0.5, this._sprite.height);
	}
}