///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	Game, AABBCollider, ICollidable, ICollision, Collision,
	IVect, Vect, Side,
	IEditorResizable, EditorProperty,
	SceneEntity,
	IRect, Rect,
	ISizeableDisplayObject
} from '../internal';

export abstract class GroundHazard extends SceneEntity implements IEditorResizable, ICollidable{

	protected _upDirection: Side = Side.up;
	protected _length: number = 10;

	protected _thickness = 10;
	protected _sprite: ISizeableDisplayObject = null;
	protected _collider: AABBCollider = null;

	public get upDirection() { return this._upDirection; }
	public set upDirection(val) { 
		this._upDirection = val;
		this.UpdateColliderToLength();
		this.UpdateSprite();
	}

	public get length() { return this._length; }
	public set length(val) { 
		this._length = val;
		this.UpdateColliderToLength();
		this.UpdateSprite();
	}

	public get collider() { return this._collider; }
	public get isHorizontal() { return !Side.IsHorizontal(this._upDirection); }
	
	/// --------------------------------------------------------------------------------------------

	/** @inheritdoc */
	public static override GetEditorProperties(): EditorProperty<SceneEntity, any>[] {
		
		let r = super.GetEditorProperties();

		r.push(
			EditorProperty.CreateKVProp("upDirection", "Up Direction", [
				{key: "up", value: Side.up},
				{key: "right", value: Side.right},
				{key: "down", value: Side.down},
				{key: "left", value: Side.left}
			]),
			EditorProperty.CreateNumberProp("length", "Length"),
		);

		return r;
	}

	/// --------------------------------------------------------------------------------------------

	/** @inheritdoc */
	public ClosestPoint(point: IVect): IVect {
		return this.collider.ClosestPoint(point);
	}

	/** @inheritdoc */
	public GetResizeRect(): IRect {
		return this.collider.GetColliderAABB();
	}
	
	/** @inheritdoc */
	public SetResizeRect(rect: IRect): void {
		
		let r = Rect.Clone(rect);

		// set length to rect width if it's a horizontal spike strip
		if(this.isHorizontal){
			r.size.y = this._thickness;
			this._length = rect.size.x;
		}
		
		// set length to rect height if it's a vertical spike strip
		else{
			r.size.x = this._thickness;
			this._length = rect.size.y;
		}

		this.UpdateColliderToLength(Rect.Center(r));
		this.collider.RedrawWireframe();
		this.UpdateSprite();
	}
	
	/// --------------------------------------------------------------------------------------------

	protected UpdateSprite(){

		if(this._sprite == null) return;
		
		let rect = this.collider.GetColliderAABB();
		let length = this.isHorizontal ? rect.size.x : rect.size.y;

		this._sprite.width = length;
		this._sprite.height = this._sprite.texture.height;

		switch(this._upDirection){
			case Side.up: 
				this._sprite.rotation = 0;
				break;
			case Side.right: 
				this._sprite.rotation = Math.PI * 0.5;
				break;
			case Side.down:
				this._sprite.rotation = Math.PI;
				break;
			case Side.left: 
				this._sprite.rotation = Math.PI * -0.5;
				break;
		}
	}

	protected UpdateColliderToLength(center: IVect = null){
		
		let oRect = this.collider.GetColliderAABB();
		let r = Rect.Clone(oRect);
		
		// only resize along the x axis if it's a horizontal spike strip
		if(this.isHorizontal){
			r.position.y = oRect.position.y;
			r.size.x = this._length;
			r.size.y = this._thickness;
		}
		
		// only resize along the y axis if it's a vertical spike strip
		else{
			r.position.x = oRect.position.x;
			r.size.x = this._thickness;
			r.size.y = this._length;
		}

		center = center ?? this.globalPosition;
		this.collider.SetRect(r);
		this.globalPosition = center;
		this.collider.globalPosition = center;
	}
	
	/// --------------------------------------------------------------------------------------------

	/** @inheritdoc */
	protected override GetSerializedChildren(): any[]{
		let r = new Array<any>();

		for(let i = 0; i < this.entities.length; i++){

			// dont copy the collider
			if(this.entities[i] == this._collider) continue;
			let serEnt = this.entities[i].GetSerializedObject();
			if(serEnt != null)
				r.push(serEnt);
		}

		return r;
	}

	/** @inheritdoc */
	public override Deserialize(data: any): void {
		super.Deserialize(data);
		this.UpdateColliderToLength();
		this.UpdateSprite();
	}
}