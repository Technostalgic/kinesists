///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	InputControl, InputCondition, InputType,
	Actor, GameScene, SpriteSheet,
	AABBCollider, Collider, CollisionLayer,
	Collision, ICollision,
	IVect, Maths, Rect, Side, Vect,
	SceneEntity, ISolid,
	Handgun, Weapon, Allegiance, 
	IControllable, ObjectController, PlayerController, Stat, Stats, ISerializable, Shotgun, BeamLaser
} from '../internal';

/** 
 * the character entity which the player has control over
 * */
export class Player extends Actor{
	
	constructor(){
		super();

		this._name = "Player";
		this.mass = 65;

		this._stats = new Stats();
		this._stats.health.baseValue = 100;
		this._stats.speed.baseValue = 30;

		this._allegiance = Allegiance.PlayerDefault();
	}
	
	/// --------------------------------------------------------------------------------------------
	/// private field declarations

	protected _controller: ObjectController = new PlayerController();

	private _upDirection: Side = Side.up;
	private _isFlipped: boolean = false;
	private _onGround: boolean = false;
	
	private _orientationContainer: SceneEntity;
	private _graphic: PIXI.Graphics;
	private _sprite: PIXI.Sprite;
	private _spriteSheet: SpriteSheet;
	private _size: IVect;
	private _movement: IVect = Vect.Create();
	private _aimDirection: number = 0;
	private _aimMagnitude: number = 0;
	private _aimVect: IVect = Vect.Create();
	private _aimVector: IVect = Vect.Create();
	
	private _weapon: Weapon;
	private _weaponPosition: IVect = Vect.Create(5, -1.5);
	private _isStrafing: boolean = false;
	private _aimReticle: PIXI.Graphics = null;
	
	private _spriteAnim: number = 2;
	private _animFrame: number = 1;
	private _animWait: number = 0;
	private _animDelay: number = 0.05;
	
	/// --------------------------------------------------------------------------------------------
	/// public field and property accessors

	public get maxSpeed(): number{ return this.stats.speed.value * 7; }
	private get jumpPower(): number{ return this.stats.speed.value * 7; }

	public get groundAcceleration(): number { return this._stats.speed.value * 40; }
	public get airAcceleration(): number { return this._stats.speed.value * 10; }

	public get colliderAABB() { return this._collider as AABBCollider; }
	public get graphic() { return this._graphic; }

	/** the relative upward direction of the player, aka the direction that their head is pointing */
	public get upDirection() {
		return this._upDirection;
	}

	/** the relative forward direction of the player, aka the direction they are facing */
	public get forwardDirection(){
		if(this._isFlipped)
			return Side.RotateCCW(this._upDirection);
		else
			return Side.RotateCW(this._upDirection);
	}

	/** whether or not the player is standing on a horizontal surface, facing either left or right */
	public get isHorizontal(){ 
		return this._upDirection == Side.up || this._upDirection == Side.down;
	}

	/** the currently equipped active item of the player */
	public get weapon() { return this._weapon; }
	public set weapon(wep) {

		// destroy previous weapon and remove from child array
		if(this._weapon != null){
			this._weapon.RemoveFromScene();
		}

		// this._entities.push(wep);
		this._orientationContainer.addChild(wep);
		this._weapon = wep;
	};

	/** the direction vector that the player is aiming (not necessarily normalized) */
	public get aimVector() { return this._aimVector; }

	/** whether or not the player's sprite is flipped along the y-axis */
	public get isFlipped() { return this._isFlipped; }
	public set isFlipped(flipped) {
		this._isFlipped = flipped;
		this._orientationContainer.scale.x = 
			Math.abs(this._orientationContainer.scale.x) * (flipped ? -1 : 1);
	}

	/** whether or not the player is on a surface that can be walked on */
	public get onGround() { return this._onGround; }

	/// --------------------------------------------------------------------------------------------

	public static CreateDefault(){
		return new Player();
	}

	protected static SetSerializableFields(): void{

		// TODO
		super.SetSerializableFields();
		ISerializable.MakeSerializable(this, x => [
			x._upDirection,
			x._isFlipped,
			x._onGround,
			x._size,
			x._weapon,
			x._weaponPosition,
			x._orientationContainer
		]);
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------
	/// Initilialization

	protected override Initialize(): void{
		super.Initialize();

		if(this._orientationContainer == null){
			this._orientationContainer = new SceneEntity();
			this._orientationContainer.name = "Orient Container";
			this.addChild(this._orientationContainer);
		}

		this.CreateGraphic();

		if(this.weapon == null) this.weapon = new BeamLaser();
		else if(this.weapon.parentEntity != this._orientationContainer){
			this._orientationContainer.addChild(this.weapon);
		}
	}

	private CreateGraphic(): void{

		let size = {x: this._size.x, y: this._size.y};

		this._graphic = new PIXI.Graphics();
		this._graphic.beginFill(0xC0C0C0, 1);
		this._graphic.drawRoundedRect(size.x / -2, size.y / -2, size.x, size.y, 2);
		this._graphic.endFill();
		this._orientationContainer.addChild(this._graphic);
		this._graphic.visible = false;

		let sheet = new SpriteSheet("./dist/assets/graphics/Player.png");
		sheet.GenerateSprites(5, 5, Vect.Create(0.5));
		this._spriteSheet = sheet;

		this._sprite = new PIXI.Sprite(null);
		this._sprite.anchor.set(0.5, 0.5);
		this._sprite.pivot.y = 1;

		this._orientationContainer.addChild(this._sprite);
		
		this._aimReticle = new PIXI.Graphics();
		this._aimReticle.beginFill(0x00FF00, 1);
		this._aimReticle.drawRect(-0.5, -0.5, 1, 1);
		this._aimReticle.endFill();
		this.addChild(this._aimReticle);
	}

	protected CreateCollider(): void{

		this._size = Vect.Create(8, 14);

		let col = new AABBCollider();
		col.layer = CollisionLayer.actors;
		col.halfExtents.x = this._size.x * 0.5;
		col.halfExtents.y = this._size.y * 0.5;

		this._collider = col;
		this.addChild(this._collider);
	}

	/// --------------------------------------------------------------------------------------------
	/// Main logic step overrides

	/** @inheritdoc */
	public override Update(deltaTime: number): void{

		this.HandleGroundAndWallChecking();
		//this.HandleInput(deltaTime);
		super.Update(deltaTime);
		this.HandleMovementPhysics(deltaTime);
		this.HandleAiming(this._aimDirection, this._aimMagnitude);

		this.HandleCamera(deltaTime);
	}

	public PostControllerUpdate(deltaTime: number): void {
		this.HandleAnimation(deltaTime);
	}

	/// --------------------------------------------------------------------------------------------
	/// Internal world interaction callbacks

	protected OnCollision(collision: ICollision){
		
		let otherCol = Collision.OtherCollider(collision, this.collider);
		if(this.IsValidGround(otherCol)){

			if(!this.onGround){
				this.FindGround(collision);
			}

			this.ResolveSolidCollision(collision);
		}
	}

	private FindGround(collision: ICollision){
		
		let norm = collision.directionNormal;
		if(this._collider == collision.colliderB){
			norm = Vect.Invert(norm);
		}
		
		let nextUpDir = Side.FromDirection(norm);

		// if the player is rotating 180 degrees, flip to ensure forward stays the same
		if(Side.Opposite(nextUpDir) == this._upDirection)
			this.isFlipped = !this.isFlipped;

		this.SetUpDirection(nextUpDir);
		Collision.Recalculate(collision);
		this._onGround = true;
	}

	private DoWallClimb(collision: ICollision){

		// get the collisions normal, and reverse it if the collision is from the perspective of
		// the other collider
		let norm = Vect.Clone(collision.directionNormal);
		if(collision.colliderA != this.collider){
			norm.x *= -1; 
			norm.y *= -1;
		}

		// ensure that the wall climb is not happening on the ground the player is standing on
		let colSide = Side.FromDirection(norm);
		if(Side.IsHorizontal(colSide) == Side.IsHorizontal(this.upDirection))
			return;
		
		// only allow wall climb if player is moving in the opposite-ish direction as the 
		// collision normal
		// let movDot = Vect.Dot(this._movement, norm);
		// if(movDot >= 0)
		// 	return;

		// maintain speed if the player is moving in the proper diagonal velocity-normal direction
		// let velDot = Vect.Dot(Vect.Normalize(this._movement), Vect.Normalize(this._velocity));
		// if(velDot < 1){
		// 	let proj = Vect.Project(this._movement, Vect.Rotate(norm, Math.PI * 0.5));
		// 	let dir = Vect.Normalize(proj);
		// 	let vel = Vect.MultiplyScalar(dir, Vect.Magnitude(this._velocity));
		// 	this.velocity = vel;
		// }

		let side = Side.FromDirection(norm);
		this.SetUpDirection(side);
		Collision.Recalculate(collision);
		this.FindGround(collision);
	}

	private SetUpDirection(dir: Side){

		this._upDirection = dir;
		let drawAng = Side.GetAngle(Side.RotateCW(this._upDirection));
		this._orientationContainer.rotation = drawAng;
		
		this.UpdateCollider();
	}

	/// --------------------------------------------------------------------------------------------
	/// private state handling

	private HandleGroundAndWallChecking(){

		// shift the collider in the relative player down direction by 1 pixel
		let colShift = Side.GetVector(this._upDirection);
		colShift.x *= -1;
		colShift.y *= -1;
		this.collider.position.x += colShift.x;
		this.collider.position.y += colShift.y;
		
		this._onGround = false;
		
		// iterate through each collider in scene
		let cols = this._parentScene.colliders.FindOverlapColliders(this.collider);
		for(let i = cols.length - 1; i >= 0; i--){

			// don't collide with self
			if(cols[i] == this._collider) continue;

			// don't check invalid grounds
			if(!this.IsValidGround(cols[i])) continue;

			// there is a collision with the shifted collider, so we are on the ground
			this._onGround = true;
			break;
		}

		// undo the collider shift
		this.collider.position.x -= colShift.x;
		this.collider.position.y -= colShift.y;

		// handle wall checking while in air
		if(!this._onGround){
			let wallCol = this.DoWallCheck();
			if(wallCol != null) this.DoWallClimb(wallCol);
		}
	}

	private DoWallCheck(): ICollision{
		
		let tCol: Collider = null;

		// shift the collider in the relative player forward direction by 1 pixel
		let colShift = Side.GetVector(this.forwardDirection);
		this.collider.position.x += colShift.x;
		this.collider.position.y += colShift.y;
		
		// iterate through each collider overlapping the shifted collider
		let cols = this._parentScene.colliders.FindOverlapColliders(this.collider);
		for(let i = cols.length - 1; i >= 0; i--){

			// don't collide with self
			if(cols[i] == this._collider)
				continue;

			// there is a collision with the shifted collider, so we are against a wall
			tCol = cols[i];
			break;
		}

		// undo the collider shift
		this.collider.position.x -= colShift.x;
		this.collider.position.y -= colShift.y;

		// shift the collider in the other direction
		colShift.x *= -1;
		colShift.y *= -1;
		this.collider.position.x += colShift.x;
		this.collider.position.y += colShift.y;
		
		// iterate through each collider overlapping the shifted collider
		cols = this._parentScene.colliders.FindOverlapColliders(this.collider);
		for(let i = cols.length - 1; i >= 0; i--){

			// don't collide with self
			if(cols[i] == this._collider)
				continue;

			// there is a collision with the shifted collider, so we are against a wall
			tCol = cols[i];
			break;
		}

		// undo the collider shift
		this.collider.position.x -= colShift.x;
		this.collider.position.y -= colShift.y;

		// return the wall collision or null if there was none
		if(tCol == null) return null;

		// if the collider was part of an object that cannot be walked on, return nothing
		if(!this.IsValidGround(tCol)) return null

		let col = this.collider.GetCollision(tCol);
		return col;
	}

	private IsValidGround(collider: Collider): boolean{
		if(!ISolid.ImplementedValue(collider)) return false;
		if(ISolid instanceof Actor) return false;

		return true;
	}

	private HandleAnimation(deltaTime: number): void{

		// calculate animation speed from velocity
		let velMag = Vect.Magnitude(this._velocity);
		let animSpeed = velMag / this.maxSpeed;

		// animate the running animation cycle
		this._animWait -= deltaTime * animSpeed;
		if(this._animWait <= 0){

			this._animFrame++;
			if(this._animFrame > 3){
				this._animFrame = 1;
			}

			this._animWait = this._animDelay + this._animWait % this._animDelay;
		}

		// if standing on ground, use 'standing' animation frame
		let animFrame = this._animFrame;
		if(this._onGround){
			if(velMag < this.maxSpeed * 0.15)
				animFrame = 0;
		}

		// if in air, use 'jumping' frame
		else {
			animFrame = 4;
		}

		
		// apply the sprite frame
		if(!this._spriteSheet.isLoaded) return;
		let frame = animFrame + this._spriteAnim * 5;
		this._sprite.texture = this._spriteSheet.sprites[frame];
	}

	private HandleCamera(deltaTime: number): void{

		if(!(this.parentScene instanceof GameScene))
			return;

		let cam = this.parentScene.camera;
		if(cam == null) return;

		cam.CenterAt(this.globalPosition);
	}
	
	/** updates the player's collider to have the proper size and orientation */
	public UpdateCollider(): void{

		let col = this.colliderAABB;
		let hExts = {
			x: this._size.x * 0.5,
			y: this._size.y * 0.5
		}

		if(this.isHorizontal){
			col.halfExtents.x = hExts.x;
			col.halfExtents.y = hExts.y;
		}

		else{
			col.halfExtents.x = hExts.y;
			col.halfExtents.y = hExts.x;
		}
	}

	private HandleAimingAnimation(aim: IVect = this._aimVector): void{

		// if the weapon is not ready we want the player sprite to flip based on their velocity
		if(!this.weapon.readyPrimary) aim = this._velocity;

		switch(this._upDirection){

			case Side.up:
				if(aim.x < 0)
					this.isFlipped = true;
				else if (aim.x > 0)
					this.isFlipped = false;
				break;
			
			case Side.down:
				if(aim.x < 0)
					this.isFlipped = false;
				else if (aim.x > 0)
					this.isFlipped = true;
				break;

			case Side.left:
				if(aim.y < 0)
					this.isFlipped = false;
				else if (aim.y > 0)
					this.isFlipped = true;
				break;

			case Side.right:
				if(aim.y < 0)
					this.isFlipped = true;
				else if (aim.y > 0)
					this.isFlipped = false;
				break;
		}
		
		// if weapon is not ready we don't need to do the aim animation
		if(!this.weapon.readyPrimary) {
			this._spriteAnim = 2;
			return;
		}
		
		// if no aim, set to default aim animation
		if(aim.x == 0 && aim.y == 0){
			this._spriteAnim = 2;
			return;
		}

		let orientVect = Vect.Direction(Side.GetVector(Side.RotateCW(this._upDirection)));
		let orientAim = Vect.Direction(aim);
		let dif = Maths.SignedAngleDif(orientVect, orientAim);

		let aimOff = Vect.FromDirection(dif, Vect.Magnitude(aim));
		if(this.isFlipped)
			aimOff.x *= -1;
		
		let po4 = Math.PI * 0.25;
		let offDir = Vect.Direction(aimOff);
		offDir /= po4;
		offDir = Math.round(offDir);

		// if(offDir <= -2 && this._onGround)
		// 	offDir = -1;

		this._spriteAnim = 2 - offDir;
	}

	private HandleWeaponOrientation(){

		let weppos = Vect.Clone(this._weaponPosition);
		switch(this._spriteAnim){
			case 0:
				weppos.x -= 2.5;
				weppos.y -= 4.5;
				break;
			case 1:
				weppos.x -= 1;
				weppos.y -= 3.5;
				break;
			case 2:
				break;
			case 3:
				weppos.x -= 1;
				weppos.y += 2;
				break;
			case 4:
				weppos.x -= 4.5;
				weppos.y += 2.5;
				break;
		}
		this._weapon.position.set(weppos.x, weppos.y);

		let trot = Vect.Direction(this._aimVector);
		let brot = Side.GetAngle(this.forwardDirection);
		if(this._isFlipped){
			trot *= -1;
			if(!this.isHorizontal)
				trot += Math.PI;
		}
		this._weapon.rotation = Maths.SignedAngleDif(trot, brot);
	}

	/// --------------------------------------------------------------------------------------------
	/// input controller

	/** @inheritdoc */
	public GetActionType(action: IControllable.Action): IControllable.ActionMode {
		let act = IControllable.Action;

		switch(action){
			case act.AimRotate:
			case act.AimMagnitude:
			case act.XMove:
			case act.YMove:
				return IControllable.ActionMode.Analogue;
			
			case act.Jump:
			case act.InteractSend:
			case act.UseItemPrimary:
			case act.UseItemAlternate:
			case act.UseItemTertiary:
				return IControllable.ActionMode.Digital;
		}

		return IControllable.ActionMode.None;
	}

	/** @inheritdoc */
	public CanDoAction(action: IControllable.Action): boolean {
		let act = IControllable.Action;
		switch(action){
			
			case act.AimRotate: return true;
			case act.AimMagnitude: return true;
			case act.XMove: return true;
			case act.YMove: return true;
			case act.Jump: return this.onGround;
			case act.UseItemPrimary: return this.weapon != null;
			case act.UseItemAlternate: return this.weapon?.readyAlternate;
			case act.UseItemTertiary: return this.weapon?.readyTertiary;
		}

		return this.GetActionType(action) !== IControllable.ActionMode.None;
	}

	/** @inheritdoc */
	public DoAction(action: IControllable.Action, value: number): void {
		if(!this.CanDoAction(action)) return;
		let act = IControllable.Action;

		switch(action){
			case act.XMove: this._movement.x = value; break;
			case act.YMove: this._movement.y = value; break;
			case act.AimRotate: this._aimDirection = value; break;
			case act.AimMagnitude: this._aimMagnitude = value; break;
			case act.Jump: this.Jump(); break;
			case act.UseItemPrimary: this.UseWeapon(); break;
		}
	}

	/**
	 * forces the player to aim in the specified direction
	 * @param inputVector the direction the player should aim
	 * @returns 
	 */
	private HandleAiming(rotation: number, magnitude: number = 1): void{

		this._aimVector = Vect.FromDirection(rotation, magnitude);
		// if(inputVector.x == 0 && inputVector.y == 0){
		// 	this._aimVector = Side.GetVector(this.forwardDirection);
		// 	this.HandleAimingAnimation(this._aimVector);
		// 	this.HandleWeaponOrientation();
		// 	return;
		// }

		let downSide = Side.Opposite(this._upDirection);
		let downVec = Side.GetVector(downSide);
		let downDir = Vect.Direction(downVec);
		let inputDir = Vect.Direction(this._aimVector);
		let dirDif = Maths.SignedAngleDif(downDir, inputDir);

		let po4 = Math.PI * 0.25;
		let dirDifSeg = Math.round(dirDif / po4);

		let r = Vect.Clone(this._aimVector);
		// if(dirDifSeg == 0 && this.onGround){
		// 	if(this._isFlipped)
		// 		r = Vect.Rotate(r, po4);
		// 	else
		// 		r = Vect.Rotate(r, -po4);
		// }

		this._aimVector = r;
		this.HandleAimingAnimation(this._aimVector);
		this.HandleWeaponOrientation();

		if(this.weapon.readyPrimary){
			this._aimReticle.visible = true;
			SceneEntity.SetGlobalPosition(this._aimReticle, 
				Vect.Add(this._weapon.barrelPosition, Vect.MultiplyScalar(this._aimVector, 10))
			);
		}
		else this._aimReticle.visible = false;
	}

	// god mode
	// public Hit(damage: number, impulse: IVect, hitPoint: IVect, damager?: SceneEntity): void { }

	/**
	 * make the player move with the specified paramaters
	 * @param deltaTime how long the player movement should be simulated
	 * @param movement the direction of movement
	 */
	private HandleMovementPhysics(deltaTime: number, movement: IVect = this._movement): void{

		let mov = Vect.Clone(movement);
		let acc = this.airAcceleration;
		let dvel = Vect.Normalize(mov);

		// touching ground
		if(this._onGround){

			acc = this.groundAcceleration;

			// only allow movement along ground plane
			if(this.isHorizontal) dvel.y = 0;
			else dvel.x = 0;
		}

		// in mid-air
		else{
			
			// accelerate slower in air than on ground
			acc *= 0.65;

			// if facing left/right
			if(Side.IsHorizontal(this.forwardDirection)){
				dvel.y *= 0.275;
			}

			// facing up/down
			else{
				dvel.x *= 0.275;
			}
		}

		// apply acceleration over delta time
		dvel.x *= deltaTime * acc;
		dvel.y *= deltaTime * acc;

		// calculate target velocity
		let tVel = {
			x: this._velocity.x + dvel.x,
			y: this._velocity.y + dvel.y
		};
		
		// enforce max speed horizontally
		if(tVel.x > this.maxSpeed || tVel.x < -this.maxSpeed){
			tVel.x = this.maxSpeed * Math.sign(tVel.x);
		}

		// enforce max speed vertically
		if(tVel.y > this.maxSpeed || tVel.y < -this.maxSpeed){
			tVel.y = this.maxSpeed * Math.sign(tVel.y);
		}

		// enforce friction while on ground
		if(this._onGround){

			// apply friction
			let dirDot = Vect.Dot(mov, this._velocity);
			let velMag = Vect.Magnitude(this._velocity);
			if(velMag > 0 && dirDot <= 0){

				let fricMag = 600 * deltaTime;
				if(fricMag < velMag){
					let fric = 
						Vect.MultiplyScalar(
							Vect.Normalize(this._velocity), 
							-fricMag
						);

					tVel.x += fric.x;
					tVel.y += fric.y;
				}
				else{
					tVel.x = 0;
					tVel.y = 0;
				}
			}
		}

		// apply the movement
		this._velocity.x = tVel.x;
		this._velocity.y = tVel.y;

		// cache the movement from this handler call
		this._movement.x = movement.x;
		this._movement.y = movement.y;
	}

	/** make the player jump in their relative upward direction */
	private Jump(): void{

		let jumpVel = Side.GetVector(this._upDirection);
		jumpVel = Vect.MultiplyScalar(jumpVel, this.jumpPower);

		this._velocity.x += jumpVel.x;
		this._velocity.y += jumpVel.y;

		this._onGround = false;
	}

	/** make the player use the currently equiped weapon */
	private UseWeapon(): void{
		if(this._weapon == null)
			return;
		
		this.weapon.Use(this, Vect.Direction(this.aimVector));
	}

	/// --------------------------------------------------------------------------------------------
}