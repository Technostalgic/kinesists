///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	IAllied, Allegiance,
	EditorProperty,
	ITarget, 
	IVect, 
	SceneEntity, 
	Scene,
	PhysicsEntity, 
	Collider, 
	ICollision, 
	ICollidable, 
	IControllable, 
	ObjectController,
	IdleController,
	Stats,
	ISerializable,
	ISerializableType
} from '../internal';

export abstract class Actor extends PhysicsEntity 
implements ITarget, ICollidable, IAllied, IControllable {

	protected constructor(){
		super();
	}
	
	public static readonly Types: any[] = [];
	
	// ---------------------------------------------------------------------------------------------
	protected _stats: Stats = null;

	protected _graphics: SceneEntity = null;
	protected _allegiance: Allegiance = null;

	public get stats() { return this._stats; }

	public get graphics() { return this._graphics; }
	public get allegiance() { return this._allegiance; }

	public get team(){ return this._allegiance.team; }
	public set team(val){ this._allegiance.team = val; }
	
	protected _controller: ObjectController = new IdleController();
	
	private get _maxHealth() { return this._stats.health.maxValue; }
	private set _maxHealth(val) { this._stats.health.baseValue = val; }
	
	// ---------------------------------------------------------------------------------------------

	public static CreateDefault(): Actor{
		return null;
	}

	protected static SetSerializableFields(): void{
		super.SetSerializableFields();
		ISerializable.MakeSerializable(this as unknown as ISerializableType<Actor>, x => [
			x._graphics,
			x._stats,
			x._allegiance,
			x._controller
		]);
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------
	public Update(deltaTime: number): void {

		if(deltaTime > 0){
			this._controller.Update(deltaTime, this);
		}

		this.PostControllerUpdate(deltaTime);
		super.Update(deltaTime);
	}

	/**
	 * meant to be overridden, called immediately after _controller.Update and before 
	 * (Actor)super.Update
	 * @param deltaTime
	 */
	public PostControllerUpdate(deltaTime: number){ }

	// ---------------------------------------------------------------------------------------------

	public Hit(damage: number, impulse: IVect, hitPoint: IVect, damager: SceneEntity = null): void {

		this.stats.health.value -= damage;
		this.ApplyImpulse(impulse);

		// kill check
		if(this.stats.health.value <= 0) this.Kill(damager);
	}

	// ---------------------------------------------------------------------------------------------

	protected override Initialize(){
		super.Initialize();
		if(this.collider == null) this.CreateCollider();
		this.collider.onCollision.AddListener(this.OnCollision.bind(this));
	}

	public static override GetEditorProperties(): EditorProperty<SceneEntity, any>[] {
		
		let r = super.GetEditorProperties();
		r.push(
			EditorProperty.CreateNumberProp("_maxHealth", "Max Health"),
			EditorProperty.CreateEntityProp("_deleteThis", "randomEntity")
		);

		return r;
	}

	// ---------------------------------------------------------------------------------------------
	/** Called when the actor object is constructed */
	protected abstract CreateCollider(): void;

	/** Called when the actor's collider hits another collider */
	protected abstract OnCollision(collision: ICollision): void;

	// ---------------------------------------------------------------------------------------------
	/** @inheritdoc */
	public override destroy(){
		this.collider.destroy();
		super.destroy();
	}
	
	/**
	 * kills the enemy
	 * @param killer the object that killed the enemy
	 */
	public Kill(killer: SceneEntity = null): void{

		console.log(this.name + " killed by " + killer?.name);
		this.RemoveFromScene();
	}

	// ---------------------------------------------------------------------------------------------
	public abstract GetActionType(action: IControllable.Action): IControllable.ActionMode;
	public abstract CanDoAction(action: IControllable.Action): boolean;
	public abstract DoAction(action: IControllable.Action, value: number): void;
}