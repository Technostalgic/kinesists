///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { 
	Actor, Aggression,
	CircleCollider, CollisionLayer,
	PhysicsEntity,
	IVect, Vect, Ray, ITarget, AiController, CombatMode, IControllable, AiBehavior, Maths, ISerializable
} from '../../internal';

export namespace EnemyBehaviors{

	export class Idle extends AiBehavior{
		static { this.SetSerializableFields(); }
		public Update(controller: AiController, deltaTime: number, actor: IControllable & Actor): 
		void { }
	}

	export class Wander extends AiBehavior{
		static { this.SetSerializableFields(); }

		private _lastMoveDir: IVect = Vect.Create();
		private _moveDir: IVect = Vect.Create();
		private _moveTime: number = 0;

		public Update(controller: AiController, deltaTime: number, actor: IControllable & Actor): 
		void {

			// choose new direction if necessary
			if(this._moveTime <= 0) this.ChooseRandomDirection();
			else this._moveTime -= deltaTime; 

			// apply movement
			actor.DoAction(IControllable.Action.XMove, this._moveDir.x);
			actor.DoAction(IControllable.Action.YMove, this._moveDir.y);
		}
		
		private ChooseRandomDirection(){

			// create list of move directions that are not equal to the current move direction or last
			// move direction
			let moveDirs = [
				Vect.Create(1, 0),
				Vect.Create(-1, 0),
				Vect.Create(0, 1),
				Vect.Create(0, -1)
			];
			for(let i = moveDirs.length - 1; i >= 0; i--){
				if(Vect.Equal(moveDirs[i], this._moveDir) || Vect.Equal(moveDirs[i], this._lastMoveDir))
					moveDirs.splice(i, 1);
			}

			// choose random move direction
			this._lastMoveDir = this._moveDir;
			this._moveDir = moveDirs[Math.floor(Math.random() * moveDirs.length)];

			this._moveTime = 1.5;
		}
	}

	export class ChaseTarget extends AiBehavior{
		static { this.SetSerializableFields(); }

		private _moveDir: IVect = Vect.Create();
		private _moveTime: number = 0;

		public Update(controller: AiController, deltaTime: number, actor: IControllable & Actor): 
		void {

			// do nothing if no target
			if(controller.target == null) return;
	
			// move toward target
			if(this._moveTime <= 0) this.SetMoveDir(controller, actor);
			else this._moveTime -= deltaTime;
			
			// apply movement
			actor.DoAction(IControllable.Action.XMove, this._moveDir.x);
			actor.DoAction(IControllable.Action.YMove, this._moveDir.y);
		}

		private SetMoveDir(controller: AiController, actor: IControllable & Actor){
			
			// move toward target
			this._moveDir = Vect.Normalize(Vect.Subtract(
				controller.target.globalPosition, actor.globalPosition)
			);
			
			this._moveTime = 0.5;
		}
	}

	export class ShootTarget extends AiBehavior{
		static { this.SetSerializableFields(); }
		
		public Update(controller: AiController, deltaTime: number, actor: IControllable & Actor): 
		void {

			if(controller.target == null) return;

			// stop moving
			actor.DoAction(IControllable.Action.XMove, 0);
			actor.DoAction(IControllable.Action.YMove, 0);
			
			// rotate toward target
			let targetRot = Vect.Direction(Vect.Subtract(
				controller.target.globalPosition,
				actor.globalPosition
			));
			actor.DoAction(IControllable.Action.AimRotate, targetRot);
	
			// shoot target
			this.Attack(controller, actor);
		}
		
		private Attack(controller: AiController, actor: IControllable & Actor){
			let target = controller.target;
			if(target == null) return;

			let aimThresh = 0.5;
			let posDif = Vect.Subtract(target.globalPosition, actor.globalPosition);
			let aimDif = Maths.SignedAngleDif(Vect.Direction(posDif), actor.rotation);
			if(Math.abs(aimDif) <= aimThresh){
				actor.DoAction(IControllable.Action.AttackPrimary, 1);
			}
		}
	}
}