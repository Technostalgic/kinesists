///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import { 
	Actor, Aggression,
	CircleCollider, CollisionLayer,
	IVect, Vect, Ray, ITarget, AiController, CombatMode, IControllable, AiBehavior, EnemyBehaviors, ISerializable
} from '../../internal';

export class SeekerAi extends AiController{

	private _combatMode: CombatMode = CombatMode.none;
	private _moveDir: IVect = Vect.Create();
	private _behaviorTime: number = 0;

	private _behaviorCalm: AiBehavior = new EnemyBehaviors.Wander();
	private _behaviorAggro: AiBehavior = new EnemyBehaviors.ChaseTarget();

	public get combatMode(): CombatMode { return this._combatMode }
	
	/// --------------------------------------------------------------------------------------------

	public static CreateDefault(){ return new this(); }

	static {
		ISerializable.MakeSerializable(this, x => [
			x._combatMode,
			x._moveDir,
			x._behaviorTime,
			x._behaviorCalm,
			x._behaviorAggro
		]);
	}

	// ---------------------------------------------------------------------------------------------

	public override Update(deltaTime: number, actor: IControllable & Actor): void {

		// search for target on timer expire
		if(this._behaviorTime <= 0){
			this.FindTarget(actor);

			// if target found, chase them otherwise wander aimlessly
			if(this.target != null) {
				this._currentBehavior = this._behaviorAggro;
				this._combatMode = CombatMode.engaged;
			}
			else {
				this._currentBehavior = this._behaviorCalm;
				this._combatMode = CombatMode.passive;
			}
		}

		// otherwise deduct the behavior time
		else this._behaviorTime -= deltaTime;
		
		// do current behavior
		super.Update(deltaTime, actor);
	}

	private FindTarget(actor: Actor){
		
		// create a collider search query a specific radius around the actor
		let searchCollider = CircleCollider.Create(500);
		searchCollider.globalPosition = actor.globalPosition;

		// find all potential target colliders
		let partitions = actor.parentScene.colliders;
		let targets = partitions.FindOverlapCollidersOnLayers(
			searchCollider, CollisionLayer.actors
		);

		// iterate through each potential target
		let targetFound = false;
		for(let i = targets.length - 1; i >= 0; i--){
			let target = targets[i].parentEntity;
			if(!ITarget.ImplementedIn(target)) continue;

			// chech if actor is aggressive against potential target
			let aggression = actor.allegiance.AggresionToward(target);
			if(aggression == Aggression.hostile){

				// see if target is in line of sight
				let ray = Ray.FromPoints(actor.globalPosition, target.globalPosition);
				let cols = partitions.RaycastAgainstLayers(ray, CollisionLayer.terrain);
				if(cols.length <= 0){

					// if it's in line of sight, set the target field
					this._target = target;
					targetFound = true;
					break;
				}
			}
		}

		// remove target if it can no longer be seen
		if(!targetFound){
			this._target = null;
		}

		// dispose of the collider used for search query
		searchCollider.destroy();
	}
}