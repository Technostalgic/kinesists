///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	Actor, IControllable, AiController, SeekerAi, Aggression,
	CircleCollider, CollisionLayer,
	Collision, ICollision, Enemy,
	IVect, Vect, ITarget, SpriteSheet,
	SceneEntity, ISolid, Maths,
	Allegiance, CombatMode, Stats, ISerializable
} from '../../internal';

export class Seeker extends Enemy{

	public constructor(){
		super();
		this.CreateGraphics();
		this._name = "Seeker";
		this.mass = 55;

		this._stats = new Stats();
		this._stats.health.baseValue = 15;
		this._stats.speed.baseValue = 75;
	}

	// ---------------------------------------------------------------------------------------------

	private _spriteSheet: SpriteSheet = new SpriteSheet("./dist/assets/graphics/Seeker.png");
	private _sprite: PIXI.Sprite = null;
	private _attackRecovery: number = 0;
	private _movement: IVect = Vect.Create();

	protected override _controller: AiController = new SeekerAi();

	// ---------------------------------------------------------------------------------------------

	public static CreateDefault(){
		return new Seeker();
	}

	protected static SetSerializableFields(): void{
		super.SetSerializableFields();
		ISerializable.MakeSerializable(this, x => []);
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------
	protected CreateGraphics(){
		
		let sprite = new PIXI.Sprite(null);
		this._graphics = new SceneEntity();
		this._sprite = sprite;

		this._graphics.addChild(sprite);
		this.addChild(this._graphics);
	}

	protected override CreateCollider(): void{

		// this._collider = AABBCollider.Create(Vect.Create(10));
		this._collider = CircleCollider.Create(11);
		//this._collider.CreateWireframe(0xFF0000, 1);
		this._collider.layer = CollisionLayer.actors;
		this.addChild(this._collider);
	}

	public override OnCollision(col: ICollision): void{

		super.OnCollision(col);

		let other = Collision.OtherCollider(col, this._collider);

		if(other.layer != CollisionLayer.terrain){
		    if(this._attackRecovery <= 0){
		    	if(ITarget.ImplementedIn(other.parentEntity)){
		    		let aggro = this.allegiance.AggresionToward(other.parentEntity);
		    		if(aggro == Aggression.hostile){
		    			this.Attack(other.parentEntity);
		    		}
		    	}
		    }
        }
	}

	// ---------------------------------------------------------------------------------------------
	public Attack(target: ITarget): void{

		let impulseMag = 7500;
		let impulse = Vect.Normalize(this.velocity);
		impulse.x *= impulseMag;
		impulse.y *= impulseMag;
		
		let attackPoint = Vect.MidPoint(this.globalPosition, target.globalPosition);
		target.Hit(1, impulse, attackPoint, this)

		impulse.x *= -1;
		impulse.y *= -1;
		this.ApplyImpulse(impulse);

		this._attackRecovery = 0.25;
	}
	
	/** @inheritdoc */
	public override Kill(killer: SceneEntity){
		super.Kill(killer);
	}

	// ---------------------------------------------------------------------------------------------
	/** @inheritdoc */
	public override Update(delta: number): void{
		super.Update(delta);
		this.HandleMovement(delta);

		this._controller.Update(delta, this);
		this.HandleAnimation();

		this._attackRecovery -= delta;
	}

	// ---------------------------------------------------------------------------------------------
	private HandleAnimation(): void{
		if(!this._spriteSheet.isLoaded){ 
			return; 
		}
		else{
			if(this._spriteSheet.sprites.length <= 1)
				this._spriteSheet.GenerateSprites(2, 1, Vect.Create(0.5));
		}

		// calculate difference between the current rotation and the direction of the target
		let target = (this._controller as SeekerAi).target;
		let targetDir = 0; 
		if(target != null)
			targetDir = Vect.Direction(Vect.Subtract(
				target.globalPosition, 
				this.globalPosition
			));
		else targetDir = Vect.Direction(this.velocity);
		let dirDif = Maths.SignedAngleDif(targetDir, this._graphics.rotation);

		this._graphics.rotation += dirDif;
		this._graphics.rotation = Maths.Mod(this._graphics.rotation, Math.PI * 2);
		if(this._graphics.rotation > Math.PI) this._graphics.rotation -= Math.PI * 2;

		// flip it upright
		if(Math.abs(this._graphics.rotation) > Math.PI / 2){
			this._graphics.scale.y = Math.abs(this._graphics.scale.y) * -1;
		}
		else{
			this._graphics.scale.y = Math.abs(this._graphics.scale.y);
		}
		
		switch(this._controller.combatMode){
			case CombatMode.engaged: 
				this._sprite.texture = this._spriteSheet.sprites[1]; 
				break;
			case CombatMode.passive: 
				this._sprite.texture = this._spriteSheet.sprites[0]; 
				break;
		}
		let anchor = this._sprite.texture.defaultAnchor;
		this._sprite.anchor.set(anchor.x, anchor.y);

		if(this._attackRecovery > 0.1){
			this._sprite.texture = this._spriteSheet.sprites[0]
		}
	}

	// ---------------------------------------------------------------------------------------------
	
	public GetActionType(action: IControllable.Action): IControllable.ActionMode {
		let act = IControllable.Action;

		switch(action){
			case act.XMove: return IControllable.ActionMode.Analogue;
			case act.YMove: return IControllable.ActionMode.Analogue;
		}
		return IControllable.ActionMode.None;
	}

	public CanDoAction(action: IControllable.Action): boolean {
		return this.GetActionType(action) != IControllable.ActionMode.None;
	}

	public DoAction(action: IControllable.Action, value: number): void{
		let act = IControllable.Action;
		switch(action){
			case act.XMove: this._movement.x = value; break;
			case act.YMove: this._movement.y = value; break;
		}
	}
	
	private HandleMovement(deltaTime: number, movementVect: IVect = this._movement){
		
		let speed = this._stats.speed.value;
		let combatMode = (this._controller as AiController).combatMode;
		if(combatMode == CombatMode.engaged) 
			speed = 150;

		// apply movement
		this.AccelerateToVelocity(
			Vect.MultiplyScalar(movementVect, speed), 
			deltaTime * speed * 2
		);
	}
}