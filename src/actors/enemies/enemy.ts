///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	Actor, IControllable, AiController, SeekerAi, Aggression,
	CircleCollider, CollisionLayer,
	Collision, ICollision,
	IVect, Vect, ITarget, SpriteSheet,
	SceneEntity, ISolid, Maths,
	Allegiance,
	Stats
} from '../../internal';

export abstract class Enemy extends Actor{

	protected constructor(){
		super();

		this._allegiance = Allegiance.EnemyDefault();
	}

	protected _controller: AiController;

	// ---------------------------------------------------------------------------------------------
	/** called when a collison occurs with this object */
	protected override OnCollision(collision: ICollision): void {
		
		let other = Collision.OtherCollider(collision, this.collider);
		
		// terrain collision
		if(ISolid.ImplementedValue(other)){
			this.ResolveSolidCollision(collision);
			return;
		}
	}
}

export class TestDummy extends Enemy{

	public constructor(){
		super();
		this._name = "Test Dummy";
		this.mass = 10;
		
		this._stats = new Stats();
		this._stats.health.baseValue = 1000;
	}

	// ---------------------------------------------------------------------------------------------
	protected override CreateCollider(){

		this._collider = CircleCollider.Create(10);
		this._collider.CreateWireframe(0xFF0000, 1);
		this._collider.layer = CollisionLayer.actors;
		this.addChild(this._collider);
	}

	public GetActionType(action: IControllable.Action): IControllable.ActionMode { 
		return IControllable.ActionMode.None;
	}
	public DoAction(action: IControllable.Action, value: number): void { }
	public CanDoAction(action: IControllable.Action): boolean { return false; }
}