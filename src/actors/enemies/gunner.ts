///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	Actor, IControllable, AiController, GunnerAi, Enemy, Aggression,
	IAllied, CombatMode,
	CircleCollider, CollisionLayer,
	Collision, ICollision, Weapon,
	IVect, Vect, ITarget, SpriteSheet,
	SceneEntity, ISolid, Maths,
	Allegiance,
	Projectile,
	ISerializable,
	Stats
} from '../../internal';

export class Gunner extends Enemy{

	public constructor(){
		super();
		this.CreateGraphics();
		this._name = "Gunner";

		this.mass = 55;
		this._stats = new Stats();
		this._stats.health.baseValue = 25;
		this._stats.speed.baseValue = 75;
		this.addChild(this._weapon);
	}

	private _spriteSheet: SpriteSheet = new SpriteSheet("./dist/assets/graphics/Gunner.png");
	private _sprite: PIXI.Sprite = null;
	private _movement: IVect = Vect.Create();
	private _aimDirection: number = this.rotation;
	private _weapon: Weapon = new GunnerWeapon();

	protected _controller: AiController = new GunnerAi();

	// ---------------------------------------------------------------------------------------------

	public static CreateDefault(){
		return new Gunner();
	}

	protected static SetSerializableFields(): void{
		super.SetSerializableFields();
		ISerializable.MakeSerializable(this, x => []);
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------
	protected CreateGraphics(): void{

		this._graphics = new SceneEntity();
		this._sprite = new PIXI.Sprite(null);
		this._sprite.anchor.set(0.5, 0.5);

		this._graphics.addChild(this._sprite);
		this.addChild(this._graphics);
	}

	/** @inheritdoc */
	protected override CreateCollider(): void {

		this._collider = CircleCollider.Create(12);
		// this._collider.CreateWireframe(0xFF0000, 1);
		this._collider.layer = CollisionLayer.actors;
		this.addChild(this._collider);
	}

	// ---------------------------------------------------------------------------------------------
	
	/** @inheritdoc */
	public override Update(delta: number): void{
		super.Update(delta);

		this.HandleMovement(delta);
		this.HandleAiming(delta);

		this._controller.Update(delta, this);
		this._weapon.Update(delta);

		this.HandleAnimation();
	}

	// ---------------------------------------------------------------------------------------------
	
	private HandleAnimation(){
		if(!this._spriteSheet.isLoaded){ return; }
		else if(this._spriteSheet.sprites.length <= 1){
			this._spriteSheet.GenerateSprites(2, 1, Vect.Create(0.5));
			if(this._sprite.texture == null) { 
				this._sprite.texture = this._spriteSheet.sprites[0];
			}
		}

		let combatMode = this._controller.combatMode;

		if(combatMode == CombatMode.engaged || combatMode == CombatMode.suspicious){
			this._sprite.texture = this._spriteSheet.sprites[1];
		}
		else if(combatMode == CombatMode.passive){
			this._sprite.texture = this._spriteSheet.sprites[0];
		}
	}
	
	public Hit(damage: number, impulse: IVect, hitPoint: IVect, damager?: SceneEntity): void {

		let maxRotImpulse = Vect.Magnitude(impulse) * 0.5;
		let hitNorm = Vect.Normalize(Vect.Subtract(this.collider.globalPosition, hitPoint));
		hitNorm = Vect.Rotate(hitNorm, Math.PI * -0.5);
		let dot = Vect.Dot(hitNorm, Vect.Normalize(impulse));
		let rotImp = dot * maxRotImpulse;
		let directFactor = (1 - Math.abs(dot));

		super.Hit(damage * directFactor, impulse, hitPoint, damager);
		this.ApplyTorqueImpulse(rotImp);
	}
	
	// ---------------------------------------------------------------------------------------------

	public GetActionType(action: IControllable.Action): IControllable.ActionMode {
		let act = IControllable.Action;
		switch(action){
			case act.XMove: return IControllable.ActionMode.Analogue;
			case act.YMove: return IControllable.ActionMode.Analogue;
			case act.AimRotate: return IControllable.ActionMode.Analogue;
			case act.AttackPrimary: return IControllable.ActionMode.Digital;
		}
		return IControllable.ActionMode.None;
	}

	public CanDoAction(action: IControllable.Action): boolean {
		let act = IControllable.Action;

		switch(action){
			case act.AttackPrimary: return this._weapon?.readyPrimary;
		}

		return this.GetActionType(action) != IControllable.ActionMode.None;
	}

	public DoAction(action: IControllable.Action, value: number): void{
		let act = IControllable.Action;
		switch(action){
			case act.XMove: this._movement.x = value; break;
			case act.YMove: this._movement.y = value; break;
			case act.AimRotate: this._aimDirection = value; break;
			case act.AimRotate: this._aimDirection = value; break;
			case act.AttackPrimary: this._weapon.Use(this, this.rotation); break;
		}
	}

	private HandleMovement(deltaTime: number, movementVect: IVect = this._movement){
		
		let speed = this.stats.speed.value;

		// apply movement
		this.AccelerateToVelocity(
			Vect.MultiplyScalar(movementVect, speed), 
			deltaTime * speed * 2
		);
	}

	private HandleAiming(deltaTime: number, aimDir: number = this._aimDirection){
		
		// dampen rotational velocity
		this._rotationalVelocity *= Math.pow(0.9, deltaTime * 60);

		let rotSpeed = 4;

		// enforce rotation speed limit
		let rotDif = Maths.SignedAngleDif(aimDir, this.rotation);
		if(Math.abs(rotDif) > rotSpeed * deltaTime){
			rotDif = Math.sign(rotDif) * rotSpeed * deltaTime;
		}
		
		this.rotation += rotDif;
	}
}

/** weapon used by gunner enemy */
class GunnerWeapon extends Weapon{

	constructor(){
		super();
		this._payload = new Projectile();
		this._payload.damage = 5;
		this._payload.size = 2;
		this._payload.mass = 0.55;
	}

	protected _barrelPosition: IVect = Vect.Create(10, 0);
	private _fireWait: number = 0;
	private _fireDelay: number = 0.45;
	
	public spread = 0.15;
	public clipSize = 3;
	public reloadTime = 2.5;

	private _payload: Projectile = null;
	private _roundsInClip: number = this.clipSize;

	public get readyPrimary(): boolean { return this._fireWait <= 0; }

	// ---------------------------------------------------------------------------------------------

	public static CreateDefault(){
		return new this();
	}

	protected static SetSerializableFields(): void{
		super.SetSerializableFields();
		ISerializable.MakeSerializable(this, x => []);
	}

	static{
		this.SetSerializableFields(); 
	}

	// ---------------------------------------------------------------------------------------------
	public override Update(deltaTime: number){
		super.Update(deltaTime);

		// deduct the fire timer
		if(this._fireWait > 0)
			this._fireWait -= deltaTime;
		else if(this._fireDelay < 0)
			this._fireWait = 0;
	}

    public override Use(user: IAllied, aim: number): void {
		
		// fire if able
		if(this._fireWait <= 0){
			this.Fire(user, aim);
		}
	}

	public Fire(user: IAllied, aim: number): void{

		this._roundsInClip--;
		this._fireWait = this._fireDelay;
		if(this._roundsInClip <= 0){
			this._fireWait = this.reloadTime;
			this._roundsInClip = this.clipSize;
		}

		let tpos = this.barrelPosition;
		let dir = aim + (Math.random() - 0.5) * this.spread;
		let proj = this._payload.FastClone()
		proj.FireFrom(user, tpos, Vect.FromDirection(dir, 500));
		this.parentScene.addChild(proj);
	}
}