///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///
import * as PIXI from 'pixi.js';
import { 
	IControllable, Vect,
	ObjectController,
	InputControl,
	InputCondition,
	InputType,
	Player,
	ISerializable
} from '../internal';

export class PlayerController extends ObjectController{

	public static CreateDefault(){ return new this(); }
	static { ISerializable.MakeSerializable(this, x => []); }

	private _input_aimX: InputControl = InputControl.CreateAnalogue(InputType.mouseAnalogueX, 0);
	private _input_aimY: InputControl = InputControl.CreateAnalogue(InputType.mouseAnalogueY, 0);
	private _input_moveLeft: InputControl = InputControl.Create(InputType.keyboardButton, 65, InputCondition.whileHeld);
	private _input_moveRight: InputControl = InputControl.Create(InputType.keyboardButton, 68, InputCondition.whileHeld);
	private _input_moveUp: InputControl = InputControl.Create(InputType.keyboardButton, 87, InputCondition.whileHeld);
	private _input_moveDown: InputControl = InputControl.Create(InputType.keyboardButton, 83, InputCondition.whileHeld);
	private _input_jump: InputControl = InputControl.Create(InputType.keyboardButton, 32, InputCondition.onPress);
	private _input_useWeaponPrimary: InputControl = InputControl.Create(InputType.mouseButton, 0, InputCondition.whileHeld);
	private _input_useWeaponAlt: InputControl = InputControl.Create(InputType.mouseButton, 2, InputCondition.whileHeld);

	public get input_aimX() { return this._input_aimX; }
	public get input_aimY() { return this._input_aimY; }
	public get input_moveLeft() { return this._input_moveLeft; }
	public get input_moveRight() { return this._input_moveRight; }
	public get input_moveUp() { return this._input_moveUp; }
	public get input_moveDown() { return this._input_moveDown; }
	public get input_jump() { return this._input_jump; }
	public get input_useWeaponPrimary() { return this._input_useWeaponPrimary; }
	public get input_useWeaponAlt() { return this._input_useWeaponAlt; }

	public Update(deltaTime: number, controlled: IControllable): void {

		let player = controlled as Player;
		if(player.parentGame == null) return;
		
		// update control input states
		this._input_aimX.Update(player.parentGame);
		this._input_aimY.Update(player.parentGame);
		this._input_moveLeft.Update(player.parentGame);
		this._input_moveRight.Update(player.parentGame);
		this._input_moveUp.Update(player.parentGame);
		this._input_moveDown.Update(player.parentGame);
		this._input_jump.Update(player.parentGame);
		this._input_useWeaponPrimary.Update(player.parentGame);
		this._input_useWeaponAlt.Update(player.parentGame);

		// shortcut ref
		let action = IControllable.Action;
		
		// handle movement input
		let moveX = 0;
		let moveY = 0;
		if(this._input_moveRight.IsTriggered()) moveX += 1;
		if(this._input_moveLeft.IsTriggered()) moveX -= 1;
		if(this._input_moveDown.IsTriggered()) moveY += 1;
		if(this._input_moveUp.IsTriggered()) moveY -= 1;
		controlled.DoAction(action.XMove, moveX);
		controlled.DoAction(action.YMove, moveY);

		if(this._input_jump.IsTriggered()){
			controlled.DoAction(action.Jump, 1);
		}

		// handle aiming
		let aimVect = Vect.Create(this._input_aimX.GetValue(), this._input_aimY.GetValue());
		if(Vect.Magnitude(aimVect) > 1) {
			aimVect = Vect.Normalize(aimVect);
			this._input_aimX.SetValue(aimVect.x);
			this._input_aimY.SetValue(aimVect.y);
		}
		controlled.DoAction(action.AimRotate, Vect.Direction(aimVect));
		controlled.DoAction(action.AimMagnitude, Vect.Magnitude(aimVect));

		// weapon/item usage
		if(this._input_useWeaponPrimary.IsTriggered()){
			controlled.DoAction(action.UseItemPrimary, 1);
		}
		if(this._input_useWeaponAlt.IsTriggered()){
			controlled.DoAction(action.UseItemAlternate, 1);
		}

	}
}