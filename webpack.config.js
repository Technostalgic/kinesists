const path = require('path');

module.exports = {
	mode: 'production',
	entry: './src/index.ts',
	devtool: 'source-map',

	optimization: {
		minimize: false,
		concatenateModules: false,
		flagIncludedChunks: false,
		innerGraph: false,
		providedExports: false,
		realContentHash: false,
		removeAvailableModules: false,
		removeEmptyChunks: false,
		usedExports: false
	},

	module: {
		rules: [
			{
				test: /\.ts?$/,
				use: 'ts-loader',
				exclude: /node_modules/,
			},
		]
	},

	resolve: {
		extensions: ['.ts', '.js'],
	},
	
	output: {
		library:{
			name: "Kinesis",
			type: "window"
		},
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'dist'),
	},

	externals: [
		{'pixi.js' : "PIXI"}
	]
};